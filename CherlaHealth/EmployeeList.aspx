﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EmployeeList.aspx.cs" Inherits="CherlaHealth.EmployeeList" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            height: 26px;
        }

        .tpadleft {
            padding-left: 20px;
        }

        .tpadright {
            padding-right: 20px;
        }

        .form-control1 {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Employee List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
               <div class="col-md-12">
                <div class="col-md-6">
                    <asp:TextBox ID="TextBox1" AutoPostBack="True" runat="server" class="form-control"
                        OnTextChanged="TextBox1_TextChanged" placeholder="Search By Employee Name or Designation or Mobile No........"></asp:TextBox>

                </div>

                <div class="col-md-6">
                    <asp:Button ID="Button1" runat="server" Text="Add Employee" PostBackUrl="~/AddEmployee.aspx"
                        class="btn btn-warning" Style="margin-left: 25px"></asp:Button>
                </div>
            </div>
                        <div class="col-md-12" style="padding-top: 15px">

                <div class="table table-responsive" style="overflow: scroll; height: 420px">

                    <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed cf"
                        AutoGenerateColumns="false" DataKeyNames="EmployeeId" OnRowDeleting="GridView1_RowDeleting"
                        runat="server">
                        <Columns>
                            
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" class="fa fa-edit" ForeColor="Green" OnClick="lnkEdit_Click"
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="EmployeeId" Visible="false" />
                            <asp:TemplateField HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" HeaderText="Employee Photo">
                                <ItemTemplate>
                                    <asp:Image ID="Image1" class="img-circle" Width="80px" Height="80px" ImageUrl='<%# "profile.ashx?EmployeeID1=" + Eval("EmployeeID1")%>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Company" HeaderText="Company" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Desigantion1" HeaderText="Department" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EmployeName" HeaderText="Employee Name" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EmployeeID1" HeaderText="Employee ID" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="DOB" HeaderText="Date of Birth" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="DOJ" HeaderText="Date of Join" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Gender" HeaderText="Gender" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Desigantion" HeaderText="Designation" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EmployeeType" HeaderText="Type" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="CompanyEmail" HeaderText="Email" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="CellNo" HeaderText="Phone No." HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="BloodGroup" HeaderText="Blood Group" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Password" HeaderText="App Password" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Emptype" HeaderText="Medicare/Not" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Salary" HeaderText="Salary" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:CommandField CausesValidation="false" ShowDeleteButton="true" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Red"  />

                        </Columns>
                    </asp:GridView>
                      </div>
            </div>
                    <asp:Label ID="lblmsg" runat="server" />
                    <asp:Button ID="modelPopup" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup"
                        PopupControlID="updatePanel" CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="updatePanel" runat="server" BackColor="White" Height="500px" Width="850px"
                        Style="display: none" BorderStyle="Groove">

                                    <h3 style="text-align:center">Edit Employee</h3>
                                <hr />
                       <%-- <div class="col-md-3">
                            <div class="form-group">--%>

                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                                
                          <%--  </div>
                        </div>--%>
                             <div class="col-md-3">
                            <div class="form-group">
                                Company
                                <asp:DropDownList ID="dropcomapany" CssClass="form-control" runat="server"></asp:DropDownList>
                                   <%-- <asp:Label ID="lblstor_id0" runat="server" class="form-control"></asp:Label>--%>
                                </div>
                               </div>
                        <asp:Label ID="lblmsgcompany" runat="server"  Visible="false" Text="Required"></asp:Label>
                             <div class="col-md-3">
                            <div class="form-group">
                               Department
                                    <asp:DropDownList ID="DropDownList1" class="form-control" runat="server"></asp:DropDownList>
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                             Employee Name
                               
                                    <asp:TextBox ID="txtstate" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                                ID
                                    <asp:TextBox ID="txtstate0" runat="server" ReadOnly="true" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                            Date of Birth
                                    <asp:TextBox ID="txtstate1" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                              Date Of Joining
                                
                                    <asp:TextBox ID="txtstate2" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                                Gender
                                    <asp:TextBox ID="txtstate3" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                             Designation
                              
                                    <asp:TextBox ID="txtstate5" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                               Type
                               
                                    <asp:TextBox ID="txtstate13" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                               Email
                               
                                    <asp:TextBox ID="txtstate6" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                       Cell No
                                
                                    <asp:TextBox ID="txtstate7" runat="server" class="form-control" />
                                </div>
                               </div>
                             <div class="col-md-3">
                            <div class="form-group">
                        Address
                              
                                    <asp:TextBox ID="txtstate8" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
                               Blood Group
                               
                                    <asp:TextBox ID="txtstate9" runat="server" class="form-control" />
                                </div>
                                 </div>
                             <div class="col-md-3">
                            <div class="form-group">
Image 
                                    <p>
                                        <asp:FileUpload ID="FileUpload1" runat="server" Style="float: left"></asp:FileUpload>
                                        <asp:Button ID="Button2" runat="server" Text="Update" OnClick="btnModity1_Click" />
                                    </p>
                                </div>
                                </div>
                             <div class="col-md-3">
                            <div class="form-group">
                         Category




                                  <%--  <p>--%>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="HolidayList" CssClass="form-control" runat="server" Style="float: left" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="HolidayList" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <asp:Button ID="Button3" runat="server" Text="Update" OnClick="btnModity12_Click" />
                                 <%--   </p>--%>
                            
                                </div>
                                 </div>
                           <div class="col-md-3">
                            <div class="form-group">
                                Password 
                                    <asp:TextBox ID="txtpass" runat="server" class="form-control" />
                                </div>
                               </div>
                        <div class="clearfix"></div>
                         <div class="col-md-3">
                            <div class="form-group">
                                 Select Type
                            <asp:DropDownList ID="dropmed" CssClass="form-control" runat="server">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="2">Medicare</asp:ListItem>
                                <asp:ListItem Value="1">Non-Medicare</asp:ListItem>
                            </asp:DropDownList>
                                </div>
                             </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                Salary 
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control" />
                                </div>
                               </div>
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                               
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" OnClick="btnModity_Click"
                                        Text="Update Data" class="btn btn-success" />
                                </div>

                                 </div>
                       
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                               </div>
                               
                            </div>
                    </asp:Panel>

            </div>
        </div>
</asp:Content>
