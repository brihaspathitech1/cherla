﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class RankingCandidates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = Session["Candidate"].ToString();
            name();
        }
        //binding name of the ccandidate using id
        private void name()
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con = new SqlConnection(connstrg);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from [Candidates] where Id='" + Label1.Text + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Label2.Text = reader["Name"].ToString();
                Label5.Text = reader["JPosition"].ToString();
            }
            reader.Close();

            con.Close();
        }
        //updating their performance based on skills
        protected void Update(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con = new SqlConnection(connstrg);
            con.Open();
            string str = "update [Candidates] set Com_Skills='" + Com_skill.Text + "',Tech_Skills='" + Tech_skill.Text + "',[Screening]='" + Screen_Test.Text + "',[Reportedby]='" + TextBox1.Text + "',[TechReportedby]='" + TextBox2.Text + "',[ScreenReportedby]='" + TextBox3.Text + "' where Id='" + Label1.Text + "' ";
            SqlCommand comm1 = new SqlCommand(str, con);

            comm1.ExecuteNonQuery();
            con.Close();
            //calling function
            select();
            ModalPopupExtender1.Show();
            //Response.Write("<script>alert('Performance Updated')</script>");
            //Response.Redirect("ViewCandidates.aspx");

        }

        private void select()
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con = new SqlConnection(connstrg);
            con.Open();
            SqlCommand cmd = new SqlCommand("select sum (convert(float,[Com_Skills])+convert(float,[Tech_Skills])+convert(float,Screening)) as total from [Candidates]  where Id='" + Label1.Text + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Label3.Text = reader["total"].ToString();
            }
            reader.Close();
            con.Close();


            double a = 3;
            double b = Convert.ToDouble(Label3.Text);
            double c = b / a;


            if (c >= 4.5)
            {
                String connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection con1 = new SqlConnection(connstrg1);
                con1.Open();
                string str1 = "update Candidates set [Avg]='Excellent' where Id='" + Label1.Text + "' ";
                SqlCommand comm1 = new SqlCommand(str1, con1);

                comm1.ExecuteNonQuery();
                con.Close();
            }
            else if (c <= 4.5 && c >= 3.5)
            {
                String connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection con1 = new SqlConnection(connstrg1);
                con1.Open();
                string str1 = "update Candidates set [Avg]='Good' where Id='" + Label1.Text + "' ";
                SqlCommand comm1 = new SqlCommand(str1, con1);

                comm1.ExecuteNonQuery();
                con.Close();
            }
            else if (c <= 3.5 && c >= 2.8)
            {
                String connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection con1 = new SqlConnection(connstrg1);
                con1.Open();
                string str1 = "update Candidates set [Avg]='Average' where Id='" + Label1.Text + "' ";
                SqlCommand comm1 = new SqlCommand(str1, con1);

                comm1.ExecuteNonQuery();
                con.Close();
            }
            else if (c <= 2.8)
            {
                String connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection con1 = new SqlConnection(connstrg1);
                con1.Open();
                string str1 = "update Candidates set [Avg]='Poor' where Id='" + Label1.Text + "' ";
                SqlCommand comm1 = new SqlCommand(str1, con1);

                comm1.ExecuteNonQuery();
                con.Close();
            }

        }
    }
}