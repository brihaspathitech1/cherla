﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddHoliday.aspx.cs" Inherits="CherlaHealth.AddHoliday" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Add Holiday</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           
            <div class="col-md-6">
                <div class="form-group">
                    Holiday Name<span style="color:red">*</span> <asp:TextBox ID="txtholiday" class="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtholiday" ForeColor="Red"></asp:RequiredFieldValidator>

                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                Holiday Date <span style="color:red">*</span> <asp:TextBox ID="txtdate" class="form-control" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlId="txtdate" />
   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtdate" ForeColor="Red"></asp:RequiredFieldValidator>

                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                Holiday Type <span style="color:red">*</span><asp:DropDownList ID="droptype" CssClass="form-control" runat="server">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="1">National Holiday</asp:ListItem>
                    <asp:ListItem Value="2">Common Festival Holiday</asp:ListItem>
                    <asp:ListItem Value="3">Discretionary</asp:ListItem>
                             </asp:DropDownList>
   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="droptype" ForeColor="Red"></asp:RequiredFieldValidator>

                </div>
                </div>
            <div class="col-md-6">
                <div class="form-group">
                    Description <asp:TextBox ID="txtdescription" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                      

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success btn-block" runat="server" Text="Submit" OnClick="Submit_Click" />
                    </div>
                </div>
            </div>
        </div>
    
</asp:Content>
