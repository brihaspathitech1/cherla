﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Joining.aspx.cs" Inherits="CherlaHealth.Joining" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Joining Letter</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             
             <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="update" runat="server"><ContentTemplate>
                  Name of the Employee<span style="color:red">*</span> <asp:DropDownList ID="dropcandidate" OnSelectedIndexChanged="dropcandidate_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="dropcandidate" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                 </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
            <%--<div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                          Date OF Joining<span style="color:red">*</span> <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>
                         <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="TextBox1" runat="server" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="TextBox1" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>--%>
             
                
             
                 <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                         Joining Report Date<span style="color:red">*</span> <asp:TextBox ID="joindate" CssClass="form-control" runat="server"></asp:TextBox>
                         <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="joindate" runat="server" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="joindate" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="Updatepanel25" runat="server"><ContentTemplate>
                        Time of Joining<span style="color:red">*</span><asp:TextBox ID="jointime" CssClass="form-control" runat="server" placeholder="eg-10:30am"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator25" runat="server" ErrorMessage="Required" ControlToValidate="jointime" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                       </ContentTemplate></asp:UpdatePanel>
                </div>
            </div>
             <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                         Designation <span style="color:red">*</span> <asp:TextBox ID="txtdesignation" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtdesignation" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                         Address <span style="color:red">*</span> <asp:TextBox ID="TextBox2" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="TextBox2" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
                         Appointment Letter Date <span style="color:red">*</span> <asp:TextBox ID="TextBox3" CssClass="form-control" runat="server"></asp:TextBox>
                             <ajaxToolkit:CalendarExtender ID="CalendarExtender3" TargetControlID="TextBox3" runat="server" />
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ControlToValidate="TextBox3" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
                 

            
            
           
            <div class="clearfix"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                </div>
            </div>
            </div>
        </div>

</asp:Content>

