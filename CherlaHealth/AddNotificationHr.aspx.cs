﻿using inventory.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class AddNotificationHr : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtGlobalNotification.Text = Convert.ToString(Application["Notification"]);

                FillGrid();
            }
        }
        protected void btnPublish_Click(object sender, EventArgs e)
        {
            if (txtGlobalNotification.Text == "")
            { Response.Write("<script>alert('Notification Text Required..!');</script>"); return; }
            Application["Notification"] = txtGlobalNotification.Text;
            DataQueries.InsertCommon("insert into tblNotification values('" + txtGlobalNotification.Text.Trim() + "','" + DateTime.Now.ToString() + "')");
          
            Response.Write("<script>alert('Notification Published Successfully..!');</script>");
            FillGrid();
            txtGlobalNotification.Text = null;
        }
        protected void FillGrid()
        {
            gvNotification.DataSource = DataQueries.SelectCommon("Select * from tblNotification");
            gvNotification.DataBind();
        }
    }
}