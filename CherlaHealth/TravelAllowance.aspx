﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="TravelAllowance.aspx.cs" Inherits="CherlaHealth.TravelAllowance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function onCalendarShown() {

        var cal = $find("calendar1");
        //Setting the default mode to month
        cal._switchMode("months", true);

        //Iterate every month Item and attach click event to it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }
    }

    function onCalendarHidden() {
        var cal = $find("calendar1");
        //Iterate every month Item and remove click event from it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }

    }
    function call(eventElement) {
        var target = eventElement.target;
        switch (target.mode) {
            case "month":
                var cal = $find("calendar1");
                cal._visibleDate = target.date;
                cal.set_selectedDate(target.date);
                cal._switchMonth(target.date);
                cal._blur.post(true);
                cal.raiseDateSelectionChanged();
                break;
        }
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Add Travel Allowance</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
            <div class="col-md-3">
                <div class="form-group">
                  
                    Month<span style="color:red">*</span> <asp:TextBox ID="txtmonth" CssClass="form-control" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtmonth" OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                                    Enabled="True" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtmonth" ForeColor="Red"></asp:RequiredFieldValidator>
                       
                </div>                
            </div>
             </ContentTemplate>
                        </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
            <div class="col-md-3">
                <div class="form-group">
                   
                    Employee <span style="color:red">*</span><asp:DropDownList ID="dropemp" CssClass="form-control" runat="server"></asp:DropDownList>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropemp" InitialValue="0" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                      
                </div>
                </div>
              </ContentTemplate></asp:UpdatePanel>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                    Select Vehicle<span style="color:red">*</span> <asp:DropDownList ID="dropcondition" CssClass="form-control" runat="server" OnSelectedIndexChanged="Condition_Change" AutoPostBack="true">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="0">For Car</asp:ListItem>
                        <asp:ListItem Value="1">For Bike</asp:ListItem>
                        <asp:ListItem Value="2">For Fixed</asp:ListItem>
                              </asp:DropDownList>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="dropcondition" ForeColor="Red"></asp:RequiredFieldValidator>
                     </ContentTemplate></asp:UpdatePanel>
                    </div>
                </div>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
            <div class="col-md-3" runat="server" id="div1" visible="true">
                <div class="form-group">
                    
                    Kilometers<asp:TextBox ID="txtkms" Text="0" CssClass="form-control" runat="server" OnTextChanged="Distance_Change" AutoPostBack="true"></asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtkms" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="Red" ErrorMessage="Enter only digits and ." ControlToValidate="txtkms" ValidationExpression="^[0-9.]{0,10}$"></asp:RegularExpressionValidator>
                       
                     </div>
                </div>
                 </ContentTemplate></asp:UpdatePanel>
             <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
            <div class="col-md-3">
                <div class="form-group">
                 
                    Amount<asp:TextBox ID="txtamt" Text="0" CssClass="form-control" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="txtamt" ForeColor="Red"></asp:RequiredFieldValidator>
    
                </div>
                </div>
                </ContentTemplate></asp:UpdatePanel>
               <asp:UpdatePanel ID="UpdatePanel6" runat="server"><ContentTemplate>
            <div class="col-md-3">
                <div class="form-group">
                
                    Extra Charges<asp:TextBox ID="txtcharges" Text="0" CssClass="form-control" runat="server"></asp:TextBox>
                     
                    </div>
                </div>
                 </ContentTemplate></asp:UpdatePanel>
            <div class="col-md-3" style="padding-top:18px">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
