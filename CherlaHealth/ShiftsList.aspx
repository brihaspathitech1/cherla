﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ShiftsList.aspx.cs" Inherits="CherlaHealth.ShiftsList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>

.tpadleft{padding-left:20px}
.tpadright{padding-right:20px;    padding-bottom: 10px;
    padding-top: 10px;}
.form-control1 {
    display: block;
    width: 100%;
    height: 20px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Shifts List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div class="col-lg-12">
                <div class="col-lg-6">

                    <asp:TextBox
                        ID="TextBox1"
                        AutoPostBack="True" runat="server" class="form-control" OnTextChanged="TextBox1_TextChanged " placeholder="Search By Shift Name or Shift Code........"></asp:TextBox>
                </div>
                <div class="col-lg-6">
                    <asp:Button ID="Button1" runat="server" Text="Add Shift" PostBackUrl="~/AddShift.aspx" class="btn btn-warning" Style="margin-left: 25px"></asp:Button>

                </div>

            </div>



            <div class="col-md-12" style="padding-top: 15px">

                <div class="table table-responsive" style="overflow: scroll; height: 420px">
                    <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" AutoGenerateColumns="false" DataKeyNames="ShiftId"
                        runat="server" OnRowDeleting="GridView1_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="ShiftId" Visible="false" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="ShiftName" HeaderText="Shift Name" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="ShiftCode" HeaderText="Shift Code" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="BeginTime" HeaderText="Begin Time" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EndTime" HeaderText="End Time" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />

                            <asp:CommandField CausesValidation="false" ShowDeleteButton="true" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Red" />
                            <asp:TemplateField HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" class="glyphicon glyphicon-pencil" runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                    </div>


            </div>

                    <asp:Label ID="lblmsg" runat="server" />
                    <asp:Button ID="modelPopup" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup" PopupControlID="updatePanel"
                        CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>


                    <asp:Panel ID="updatePanel" runat="server" BackColor="White" Height="276px" Width="357px">
                        <table width="100%" cellspacing="4" style="background: #fff; margin-top: -70px">
                            <tr style="background-color: #12b5da">
                                <td colspan="2" align="center">
                                    <h3 style="color: #fff">Edit Shift</h3>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="width: 45%"></td>
                                <td>
                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Shift Name
                                </td>
                                <td class="style1 tpadright">
                                    <asp:TextBox ID="txtstor_address" runat="server" class="form-control" />

                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Shift Code:
                                </td>
                                <td class="style1 tpadright">
                                    <asp:TextBox ID="txtcity" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Begin Time
                                </td>
                                <td class="style1 tpadright">
                                    <asp:TextBox ID="txtstate" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">End Time
                                </td>
                                <td class="style1 tpadright">
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update"
                                        OnClick="btnModity_Click" Text="Update Data" class="btn btn-success" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            </div>
        </div>
</asp:Content>
