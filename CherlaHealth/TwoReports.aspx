﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="TwoReports.aspx.cs" Inherits="CherlaHealth.TwoReports" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">ToDate Report</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                        <div class="row mt">
                <div class=" col-lg-12">
                 
                    <div class=" col-sm-12">
                       

                        <div class="row">
                            <div class="col-sm-4">
                                Company
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                                    <ContentTemplate>

                                        <asp:DropDownList ID="DropDownList2" class="form-control" runat="server"
                                            AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="DropDownList1" />
                                    </Triggers>

                                </asp:UpdatePanel>
                            </div>
                            <div class="col-sm-4">
                                Department
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="Department" runat="server" class="form-control"
                            AutoPostBack="True" OnSelectedIndexChanged="Department_SelectedIndexChanged">
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Department" />
                    </Triggers>
                </asp:UpdatePanel>


                            </div>
                            <div class="col-sm-4">
                                Employee
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="DropDownList3" runat="server" class="form-control"
                            AutoPostBack="True">
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DropDownList3" />
                    </Triggers>
                </asp:UpdatePanel>


                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-4" style="padding-top: 16px"  >
                                Type
    <asp:DropDownList ID="DropDownList1" class="form-control" runat="server"
       >
        <asp:ListItem></asp:ListItem>
        <%-- <asp:ListItem>Basic</asp:ListItem>
     <asp:ListItem>Detailed Attendance</asp:ListItem>
      <asp:ListItem>Summary Attendance report</asp:ListItem>--%>
        <asp:ListItem>Detailed Summary Attendance </asp:ListItem>
        <%-- <asp:ListItem>In-Out Duration</asp:ListItem>
         <asp:ListItem>Present</asp:ListItem>
          <asp:ListItem>Absent</asp:ListItem>
           <asp:ListItem>EarlyGoes</asp:ListItem>
            <asp:ListItem>LateComes</asp:ListItem>--%>
    </asp:DropDownList>

                                <%--  Total Days :<asp:Label ID="Label1" runat="server"  ></asp:Label>
     Presents: <asp:Label ID="Label2" runat="server"  ></asp:Label>
   Absents:  <asp:Label ID="Label3" runat="server"></asp:Label>
    Percentage Persents:  <asp:Label ID="Label4" runat="server" ></asp:Label>
    HalfDays:  <asp:Label ID="Label5" runat="server" ></asp:Label>--%>
                            </div>

                            <%--<asp:Button ID="Button1" class="btn btn-theme" runat="server" Text="Button" onclick="Button1_Click" />--%>


                            <div class="col-sm-4">
                                <br />
                                <div class="col-sm-6">
                                    Start Date
            
             
                    <asp:TextBox ID="TAN" class="form-control" runat="server"></asp:TextBox>

                                    <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="TAN" runat="server"></cc1:CalendarExtender>
                                </div>
                                <div class="col-sm-6">
                                    End Date
            
             
                    <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-sm-4 btn1 centered" style="padding-top: 16px">
                                <br />
                                <asp:Button ID="Button1" runat="server" class="btn btn-success btn-block" Text="Generate Report" OnClick="Button1_Click" />

                            </div>
                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="TextBox1" runat="server"></cc1:CalendarExtender>
                            <div class="col-lg-12">
                                <div style="display: none;" runat="server" id="divmsg">
                                    <div class="row mt">
                                        <div class="col-lg-12">
                                            <div class="overflowy  padtop" style="overflow: scroll;">
                                                <section id="no-more-tables">
                                                    <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" HeaderStyle-ForeColor="black" HeaderStyle-BackColor="#21badd" runat="server">
                                                    </asp:GridView>
                                                </section>


                                            </div>

                                            <div class="col-lg-12">
                                                Total Days :<asp:Label ID="Label1" Style="background-color: #FFFF00; font-weight: 700" runat="server"></asp:Label>
                                                &nbsp;,
     Presents:
                                                <asp:Label ID="Label2" Style="background-color: #FFFF00; font-weight: 700" runat="server"></asp:Label>
                                                &nbsp;,
    Presents(No Out Punch):
                                                <asp:Label ID="Label9" Style="background-color: #FFFF00; font-weight: 700" runat="server"></asp:Label>
                                                &nbsp;,
   Absents: 
                                                <asp:Label ID="Label3" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>
                                                &nbsp;,
    Percentage Persents: 
                                                <asp:Label ID="Label4" runat="server"
                                                    Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;%,
    HalfDays: 
                                                <asp:Label ID="Label5" runat="server"
                                                    Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
          Week of Present: 
                                                <asp:Label ID="Label6" runat="server"
                                                    Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
         Leaves: 
                                                <asp:Label ID="Label7" runat="server"
                                                    Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
            Holidays: 
                                                <asp:Label ID="Label8" runat="server"
                                                    Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;
                                            </div>

                                            <div class="col-md-12">
                                                <br />
                                                <div class="col-md-3">
                                                    <p>A-Absent</p>
                                                    <p>P-Present</p>
                                                    <p>H-Holiday</p>
                                                    <p>L-Leave</p>
                                                </div>
                                                <div class="col-md-3">

                                                    <p>HOP-Holiday Present</p>
                                                    <p>HO-Half Day</p>
                                                    <p>WO-Week Off</p>
                                                </div>
                                                <div class="col-md-3">

                                                    <p>WOP-Week Off Present</p>
                                                    <p>PNOP-Present But No Out Punch </p>
                                                    <p>OT-Over Time</p>



                                                </div>


                                            </div>

                                            <div class="col-lg-12">
                                                <asp:Button ID="Button6" runat="server" class="btn btn-success"
                                                    Text="Export Ms-Word" OnClick="Button6_Click" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:Button ID="Button7" runat="server" class="btn btn-success"
                       Text="Export Excel" OnClick="Button7_Click" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button8" runat="server" class="btn btn-success"
                        Text="Export Pdf" OnClick="Button8_Click" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     
                                       <asp:Button ID="Button9" runat="server"
                                           class="btn btn-success" Text="Export CSV" OnClick="Button9_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
</asp:Content>
