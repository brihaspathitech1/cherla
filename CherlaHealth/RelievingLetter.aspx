﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RelievingLetter.aspx.cs" Inherits="CherlaHealth.RelievingLetter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }




        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }

            #Button1 {
                display: none;
            }

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint {
                display: none;
            }

            a {
                display: none;
            }
        }




        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:Label ID="Email" runat="server" Text="" Visible="false"></asp:Label>
         <div class="row mt " id="content">
            <div class="col-lg-offset-2 col-md-8 panel" >
                <div class="row text-center" style="padding-top:25px">
                <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                    <asp:Button runat="server" Text="Send Mail" type="button"  id="Button2" onclick="btn_relievemail" class="btn btn-danger"></asp:Button>
                   <%-- <a href="Letters.aspx" id="hikemail" type="button"  onclick="btn_hikemail"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Send Mail</strong></a>--%>
                <a href="RelievingLetterNew.aspx" id="btnPrint" type="button"  onclick="goBack()"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>
                <br />
                <br />
                <br />
                      <br />
                <br />
                <br />
                   </div>
                <div class="row">
                
                <div class="col-md-12">
                
                
               <h4  style="   color:#000; font-weight:bold">Date: <label><strong><asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></strong></label></h4>
                <br />

                    <br />

                   <h4  style="   color:#000; font-weight:bold"> <strong><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></strong></h4>
                    <h4 style="color:#000;">Hyderabad.</h4>

                </div>
                <div class="col-md-12">
              
                    <h3 class="text-center" style="text-decoration:underline;  color:#000;">Sub: Relieving Letter</h3>


                <%--<p style="font-size:15px"><strong>Designation:<asp:Label ID="Designation" runat="server" Text="Label"></asp:Label></strong></p>--%>
               
                
             
             <p style="font-size:15px;color:#000;">
             Date TO:<strong><asp:Label ID="DateTo" runat="server" Text="Label"></asp:Label></strong>
                 </p>
           <p style="font-size:15px;color:#000;"> Dear <strong><asp:Label ID="Name1" runat="server" Text="Label"></asp:Label></strong></p>
                 
               <p style="font-size:15px;color:#000;">   
                We wish to inform you that your relieving is accept and you are being relieved from the
service of the company with effect from close of the office hours on <strong><asp:Label ID="Datefrom" runat="server" Text="Label"></asp:Label></strong>.</p>
            
                    
                      <p style="font-size:15px;color:#000;">  Your contribution to the organisation and its success will always be appreciated. we at
<b>Brihaspathi Technologies Pvt Ltd.,</b> wish you all the best in your future endeavours.</b>
             </p>
              
                    <br />
  <p style="font-size:15px;   ; color:#000;  "><strong>Yours Sincerely,
</strong></p>

                      <p style="font-size:15px;   ; color:#000;  "> For <strong>Brihaspathi Technologies Pvt Ltd.,
</strong></p>
   <br />
                    <br />
                    <br />

  <p style="font-size:15px;    color:#000;  "><strong>Hyma P,</strong></p>
                    <p style="font-size:15px;   color:#000;  "><strong>HR Executive</strong></p>
             


</div>
    </div>
    </div>
    </form>
</body>
</html>

