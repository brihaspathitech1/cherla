﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ExperianceLetter.aspx.cs" Inherits="CherlaHealth.ExperianceLetter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Experiance Letter</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           
             <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel10" runat="server"><ContentTemplate>
                     Select Employee <span style="color:red">*</span><asp:DropDownList ID="dropemp" CssClass="form-control" OnSelectedIndexChanged="Employee_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ForeColor="Red" Display="Dynamic" InitialValue="0" ControlToValidate="dropemp"></asp:RequiredFieldValidator>
                         </ContentTemplate></asp:UpdatePanel>
                 </div>
             </div>
             <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                     Join Date <asp:TextBox ID="txtdate" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                         </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                     Leaving Date<span style="color:red">*</span> <asp:TextBox ID="txtleave" CssClass="form-control" runat="server"></asp:TextBox>
                     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtleave" />
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtleave" ForeColor="Red"></asp:RequiredFieldValidator>
                         </ContentTemplate></asp:UpdatePanel>
                     </div>
                </div>
            <%--<div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                     Reasons<span style="color:red">*</span> <asp:TextBox ID="txtreason" CssClass="form-control" runat="server"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtreason" ForeColor="Red"></asp:RequiredFieldValidator>
                         </ContentTemplate></asp:UpdatePanel>
                     </div>
                </div>--%>

            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                     Conduct <span style="color:red">*</span> <asp:TextBox ID="txtconduct" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtconduct" ForeColor="Red"></asp:RequiredFieldValidator>
                         </ContentTemplate></asp:UpdatePanel>
                     </div>
                </div>
            <div class="col-md-3" style="padding-top:18px">
                 <div class="form-group">
                     <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                     </div>
                </div>
            
            </div>
        </div>
</asp:Content>
