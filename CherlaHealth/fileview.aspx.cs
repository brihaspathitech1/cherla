﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class fileview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string FileName;

            FileName = Session["FileName"].ToString();

            string filepath = Server.MapPath(FileName);
            // string FilePath = Server.MapPath(FileName);
            //string FilePath = Server.MapPath(FileName);
            WebClient User = new WebClient();
            Byte[] FileBuffer = User.DownloadData(filepath);



            if (FileBuffer != null)
            {

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", FileBuffer.Length.ToString());
                Response.BinaryWrite(FileBuffer);


            }
        }
    }
}