﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class AddCompany : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit(object sender,EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;


            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Companies WHERE CompanyName = @Name", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name", this.Company.Text.Trim());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.lblStatus.Text = "Duplicate Entry";
                        }
                        else
                        {

                            SqlConnection conn1 = new SqlConnection(connstrg);
                            String insert = "insert into Companies(CompanyCode,CompanyName,RegNo,RegDate,Tanno,PanNo,TinNo,Name,Desiganation,Email,Address1,Phone,PhoneSecond,MobileNo,FaxNo,WebsiteURL,latitude,longitude)values('" + Code.Text + "','" + Company.Text + "','" + Regno.Text + "','" + RegDate.Text + "','" + TAN.Text + "','" + Pan.Text + "','" + TIN.Text + "','" + Name.Text + "','" + Desiganation.Text + "','" + Address.Text + "','" + Email.Text + "','" + Phone.Text + "','" + Phone2.Text + "','" + Mobile.Text + "','" + FAX.Text + "','" + Website.Text + "','"+txtlat.Text+"','"+txtlong.Text+"')";
                            SqlCommand comm1 = new SqlCommand(insert, conn1);
                            conn1.Open();
                            comm1.ExecuteNonQuery();
                            conn1.Close();
                        }
                    }
                }
            }

           
            Response.Write("<script>alert('Data inserted successfully')</script>");
            Response.Redirect("~/CompanyList.aspx");
        }
    }
}