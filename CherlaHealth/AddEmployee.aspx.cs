﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace CherlaHealth
{
    public partial class AddEmployee : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                //Candidates();

            }
            if (!IsPostBack)
            {
                BindStudy123();
                BindStudy4();
            }
            if (!IsPostBack)
            {
                BindStudy1();
                BindStudy2();
                //BindShift();
            }
        }
        //private void Candidates()
        //{
        //    string strcon = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        //    SqlConnection connection = new SqlConnection(strcon);
        //    connection.Open();
        //    SqlCommand cmd = new SqlCommand("select * from [Candidates] where  [Status]='Selected' order by id desc", connection);
        //    SqlDataReader dr = cmd.ExecuteReader();
        //    Employee.DataSource = dr;
        //    Employee.DataTextField = "Name";
        //    Employee.DataValueField = "Id";
        //    Employee.DataBind();
        //    Employee.Enabled = true;
        //    connection.Close();
        //    Employee.Items.Insert(0, new ListItem("", "0"));
        //}
        private void BindStudy4()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT Id,Name1 FROM Tax";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        droptax.DataSource = dt;
                        droptax.DataValueField = "Id";
                        droptax.DataTextField = "Name1";
                        droptax.DataBind();
                        sqlConn.Close();

                        droptax.Items.Insert(0, new ListItem("", "0"));





                    }
                }
            }
            catch { }
        }

        private void BindStudy1()
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT CategoryId,CategoryName FROM Categories order by CategoryId desc";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        HolidayList.DataSource = dt;
                        HolidayList.DataValueField = "CategoryId";
                        HolidayList.DataTextField = "CategoryName";
                        HolidayList.DataBind();
                        sqlConn.Close();


                        HolidayList.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }

        }

        private void BindStudy123()
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT CompanyId,CompanyName FROM Companies ";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        Company.DataSource = dt;
                        Company.DataValueField = "CompanyId";
                        Company.DataTextField = "CompanyName";
                        Company.DataBind();
                        sqlConn.Close();


                        Company.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }

        }
        //protected void BindShift()
        //{
        //    try
        //    {
        //        using (SqlConnection sqlConn = new SqlConnection(connstrg))
        //        {
        //            using (SqlCommand sqlCmd = new SqlCommand())
        //            {
        //                sqlCmd.CommandText = "SELECT ShiftId,ShiftName from Shifts ";
        //                sqlCmd.Connection = sqlConn;
        //                sqlConn.Open();
        //                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
        //                DataTable dt = new DataTable();
        //                da.Fill(dt);
        //                dropshift.DataSource = dt;
        //                dropshift.DataValueField = "ShiftId";
        //                dropshift.DataTextField = "ShiftName";
        //                dropshift.DataBind();
        //                sqlConn.Close();


        //                dropshift.Items.Insert(0, new ListItem("", "0"));




        //            }
        //        }
        //    }
        //    catch { }
        //}
        private void BindStudy2()
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT Id,Bank FROM BankAccount";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        dropbank.DataSource = dt;
                        dropbank.DataValueField = "Id";
                        dropbank.DataTextField = "Bank";
                        dropbank.DataBind();
                        sqlConn.Close();

                        dropbank.Items.Insert(0, new ListItem("", "0"));





                    }
                }
            }
            catch { }
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            ModalPopupExtender1.Show();
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from BankAccount where Bank='" + txtbank.Text + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                this.lblStatus1.Text = "Duplicate Entry";
            }
            else
            {
                SqlCommand cmd12 = new SqlCommand("Insert into BankAccount(Bank) values('" + txtbank.Text + "')", conn);
                conn.Open();
                cmd12.ExecuteNonQuery();
                conn.Close();
                BindStudy2();
            }
        }
        protected void TextBox1_TextChanged(object sender, System.EventArgs e)
        {

            //Candidates();
        }

        protected void Company_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT DeptId,Department FROM Department";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        Department.DataSource = dt;
                        Department.DataValueField = "DeptId";
                        Department.DataTextField = "Department";
                        Department.DataBind();
                        sqlConn.Close();


                        Department.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            string serverpath = string.Empty;
            string name1 = Request.Form["Name"];
            Address.Text = name1;

            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Employees WHERE EmployeeId=@Name1", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name1", this.EmployeeId.Text.Trim());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.lblStatus.Text = "Duplicate Entry";

                        }

                        else
                        {
                            if (Imgprev.HasFile)
                            {
                                string filename = Path.GetFileName(Imgprev.PostedFile.FileName);
                                string path = Server.MapPath("~/images/") + filename;
                                Imgprev.PostedFile.SaveAs(path);
                                serverpath = @"http://Cherla.bterp.in/images/" + filename;
                            }
                            SqlConnection conn1 = new SqlConnection(connstrg);
                            string insert;
                            if (Department.SelectedItem.Text == "Physician")
                            {
                                insert = "insert into Employees(EmployeeId,Salutation,EmployeName,EmpImage,EmployeeID1,EmployeeCodeInDevice,DOB,DOJ,Gender,CompanyId,DeptId,Desigantion,EmployeeType,CompanyEmail,CellNo,Address,BloodGroup,CategoryId,Company,Desigantion1,status,BankName,Bankaccountno,PfNo,ESINo,PanNo,Casualleaves,PFStatus,mobilebill,Taxid,salaryperhour,IFSC,password,EmpType,EmpTypeid,image)values('" + EmployeeId.Text + "','" + Salutation.SelectedItem.Text + "','" + Employee1.Text + "',@IMG,'" + EmployeeId.Text + "','" + EmployeeId.Text + "','" + DOB.Text + "','" + DOJ.Text + "','" + Gender.SelectedValue + "','" + Company.SelectedValue + "','" + Department.SelectedValue + "','" + Desigantion.Text + "','" + dropemptype.SelectedItem.Text + "','" + CompanyEmail.Text + "','" + CellNumber.Text + "','" + Address.Text + "','" + BloodGroup.SelectedItem.Text + "','" + HolidayList.SelectedValue + "','" + Company.SelectedItem.Text + "','" + Department.SelectedItem.Text + "','1','" + dropbank.SelectedItem.Text + "','" + txtaccount.Text + "','" + txtpf.Text + "','" + txtesi.Text + "','" + txtpan.Text + "','" + txtleaves.Text + "','" + radiopf.SelectedItem.Text + "','" + txtmobbill.Text + "','" + droptax.SelectedValue + "','" + txthrsal.Text + "','" + txtifsc.Text + "','" + txtpwd.Text + "','" + dropmed.SelectedItem.Text + "','" + dropmed.SelectedValue + "','" + serverpath + "')";
                            }
                            else
                            {
                                insert = "insert into Employees(EmployeeId,Salutation,EmployeName,EmpImage,EmployeeID1,EmployeeCodeInDevice,DOB,DOJ,Gender,CompanyId,DeptId,Desigantion,EmployeeType,CompanyEmail,CellNo,Address,BloodGroup,CategoryId,Company,Desigantion1,status,BankName,Bankaccountno,PfNo,ESINo,PanNo,Casualleaves,PFStatus,mobilebill,Taxid,Salary,IFSC,password,EmpType,EmpTypeid,image)values('" + EmployeeId.Text + "','" + Salutation.SelectedItem.Text + "','" + Employee1.Text + "',@IMG,'" + EmployeeId.Text + "','" + EmployeeId.Text + "','" + DOB.Text + "','" + DOJ.Text + "','" + Gender.SelectedValue + "','" + Company.SelectedValue + "','" + Department.SelectedValue + "','" + Desigantion.Text + "','" + dropemptype.SelectedItem.Text + "','" + CompanyEmail.Text + "','" + CellNumber.Text + "','" + Address.Text + "','" + BloodGroup.SelectedItem.Text + "','" + HolidayList.SelectedValue + "','" + Company.SelectedItem.Text + "','" + Department.SelectedItem.Text + "','1','" + dropbank.SelectedItem.Text + "','" + txtaccount.Text + "','" + txtpf.Text + "','" + txtesi.Text + "','" + txtpan.Text + "','" + txtleaves.Text + "','" + radiopf.SelectedItem.Text + "','" + txtmobbill.Text + "','" + droptax.SelectedValue + "','" + txtsal.Text + "','" + txtifsc.Text + "','" + txtpwd.Text + "','" + dropmed.SelectedItem.Text + "','" + dropmed.SelectedValue + "','" + serverpath + "')";
                            }
                            string insert2 = "insert into EmployeeDetails(Id,TaxId,Name,EmpNo,DOB,DOJ,Departrment,Designation,Bank,BankAc,PFNo,ESINo,PanNo,EmpId,Casual,PF1,Empstatus,IFSC) values ('" + EmployeeId.Text + "','" + droptax.SelectedValue + "','" + Employee1.Text + "','" + EmployeeId.Text + "','" + DOB.Text + "','" + DOJ.Text + "','" + Department.SelectedItem.Text + "','" + Desigantion.Text + "','" + dropbank.SelectedItem.Text + "','" + txtaccount.Text + "','" + txtpf.Text + "','" + txtesi.Text + "','" + txtpan.Text + "','" + EmployeeId.Text + "','" + txtleaves.Text + "','" + radiopf.SelectedItem.Text + "','1','" + txtifsc.Text + "')";
                            SqlCommand comm1 = new SqlCommand(insert, conn1);
                            SqlCommand cmd1 = new SqlCommand(insert2, conn1);
                            int img1 = Imgprev.PostedFile.ContentLength;
                            byte[] msdata1 = new byte[img1];
                            Imgprev.PostedFile.InputStream.Read(msdata1, 0, img1);
                            comm1.Parameters.AddWithValue("@IMG", msdata1);
                            conn1.Open();
                            comm1.ExecuteNonQuery();
                            conn1.Close();
                        }
                    }
                }



            }
            Salutation.SelectedItem.Text = null;
            Employee1.Text = null;
            EmployeeId.Text = null;
            //DeviceId.Text = null;
            DOB.Text = null;
            DOJ.Text = null;
            Desigantion.Text = null;
            dropemptype.Text = null;
            CompanyEmail.Text = null;
            CellNumber.Text = null;
            Address.Text = null;
            BloodGroup.SelectedItem.Text = null;
            HolidayList.SelectedItem.Text = null;
            txtaccount.Text = null;
            txtesi.Text = null;
            txtleaves.Text = null;
            txtmobbill.Text = null;
            txtpan.Text = null;
            radiopf.SelectedItem.Text = null;
            txtpf.Text = null;


            Response.Write("<script>alert('Data inserted successfully')</script>");
            Response.Redirect("EmployeeList.aspx");
        }
        protected void Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Department.SelectedItem.Text == "Physician")
            {
                divhour.Visible = true;
                divanum.Visible = false;
            }
            else
            {
                divhour.Visible = false;
                divanum.Visible = true;
            }
        }
    }
}

