﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="CherlaHealth.test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Appointment Letter</title>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }




        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                 
            }

            #Button1 {
                display: none;
            }

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint {
                display: none;
            }

            a {
                display: none;
            }
        }




        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
    <style>
        .page {
            width:70%;background:#d3d3d357;margin:100px 0px -100px 180px;text-align:justify;padding:5px 50px 0px 70px;
        }
        tr,td{border-collapse:collapse;
              border:1px solid black;
         
        }
        .row {
            column-span:3;
        }
        th, td {
    padding: 5px;
 
}

    </style>

</head>
<body style="color:black">
    <form id="form1" runat="server">
         
             <div class="page">
                 <div style="text-align:center;padding-top:25px ;">
             <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                <a href="appointment.aspx" id="btnPrint" type="button"  onclick="goBack()"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>

        <%--    <asp:Button ID="btnpdf" CssClass="btn btn-success" runat="server" Text="PDF" OnClick="pdf_Click" />--%>
                      </div>

    <h3 style="text-align:center"><u>Letter of Appointment</u></h3>
        <br />
        <p>Date:<asp:Label ID="lbldate" runat="server" Text=""></asp:Label></p>
        <p>Name:<asp:Label ID="lblname" runat="server" Text=""></asp:Label></p>
        
        <p>Address:<asp:Label ID="lbladdress" runat="server" Text=""></asp:Label></p>
        <p>Dear <asp:Label ID="lblname2" runat="server" Text=""></asp:Label>,</p>
        <br />
        <p><b></b>Sub:</b> Letter of Appointment</p>
        <br />
        <p>We are pleased to offer you, the position of <asp:Label ID="lbldesig" runat="server" Text=""></asp:Label> with Cherla Health Pvt. Ltd. You will be based at <asp:Label ID="Label1" runat="server" Text=""></asp:Label>.</p>
         
        <p>You will be on probation for a period of six months.</p>
         
        <p>You will be paid gross emoluments as detailed in Annexure – A.</p>
         
        <p>Your employment with us will be governed by the Terms & Conditions as detailed in Annexure – B. You are required to agree to terms and conditions as described in Annexure – B.</p>
         
        <p>You are required to sign the Confidentiality Agreement as described in  Annexure – C.</p>
         
        <p>Your offer has been made based on information furnished by you. However, if there is a discrepancy in the copies of documents or certificates given by you as a proof of above we retain the right to review our offer of employment.</p>
         
        <p>Employment as per this offer is subject to your being medically fit.</p>
         
        <p>Please sign and return duplicate copy of this letter in token of your acceptance.</p>
         
        <p>We congratulate you on your appointment and wish you a long and successful career with us. We are confident that your contribution will take us further in our journey towards becoming world leaders. We assure you of our support for your professional development and growth.</p>
        <br />
        <p>Yours truly,</p>
        <br />
        <p>Dr. Gautam</p>
        <p>Co-Founder, Cherla Health</p>
        <p>Encl.: As above</p>
        </div>

        <div class="page">
       
               <h3><u>Annexure – A</u></h3>
         <table style="width:90%;padding:20px;">
             <tr  >
                 <td colspan="3" >Employee Name <asp:Label ID="lblname3" runat="server" Text=""></asp:Label> </td>
             </t>
       
       


 
        <tr >
            <td colspan="3">Annexure to Appointment letter of  dated : <asp:Label ID="lbldate3" runat="server" Text=""></asp:Label></p></td>
            </tr>
        <tr>
            <td>Salary Components</td>
            <td>Per Month</td>
            <td>Per Annum</td>
       	</tr>

        <tr  >
            <td>Monthly Components <asp:Label ID="lblmonth" runat="server" Text=""></asp:Label> <asp:Label ID="lblyear" runat="server" Text=""></asp:Label></td>
            <td></td>
            <td></td>
        </tr>
        <tr >
            <td> Basic <asp:Label ID="lblmbasic" runat="server" Text=""></asp:Label> <asp:Label ID="lblybasic" runat="server" Text=""></asp:Label>           </td>
            <td></td>
            <td></td>

        </tr> 
             
        <tr>
            <td>HRA <asp:Label ID="lblmhra" runat="server" Text=""></asp:Label> <asp:Label ID="lblyhra" runat="server" Text=""></asp:Label></p></td>
            <td></td>
            <td></td>
        </tr>     
        
         <tr>
             <td>
                  Conveyance <asp:Label ID="lblmconveyance" runat="server" Text=""></asp:Label> <asp:Label ID="lblyconvey" runat="server" Text=""></asp:Label>
                 <td></td>
                 <td></td>
             </td>
         </tr>
                 <tr>
             <td>
                  Children Education Allowance  <asp:Label ID="lblmcha" runat="server" Text=""></asp:Label> <asp:Label ID="lblycha" runat="server" Text=""></asp:Label></p>
                 <td></td>
                 <td></td>
             </td>
         </tr>
         <tr>
             <td>
                 Special Allowance <asp:Label ID="lblmspecial" runat="server" Text=""></asp:Label> <asp:Label ID="lblyspecial" runat="server" Text=""></asp:Label>
                 <td></td>
                 <td></td>
             </td>
         </tr>
          
         <tr>
             <td>
                 Sub-Total <asp:Label ID="lblmtotal" runat="server" Text=""></asp:Label> <asp:Label ID="lblytotal" runat="server" Text=""></asp:Label>
                 <td></td>
                 <td></td>
             </td>
         </tr>
         <tr >
             <td>
                 Statutory <asp:Label ID="lblmsta" runat="server" Text=""></asp:Label> <asp:Label ID="lblysta" runat="server" Text=""></asp:Label>
                 <td></td>
                 <td></td>
             </td>
         </tr>
         <tr>
             <td>
                  ESI <asp:Label ID="lblmesi" runat="server" Text=""></asp:Label> <asp:Label ID="lblyesi" runat="server" Text=""></asp:Label>
                 <td></td>
                 <td></td>
             </td>
         </tr>  
        <tr>
            <td>
                PF <asp:Label ID="lblmpf" runat="server" Text=""></asp:Label> <asp:Label ID="lblypf" runat="server" Text=""></asp:Label>
                <td></td>
                 <td></td>
            </td>
        </tr>
         <tr>
             <td>
                 Sub-Total <asp:Label ID="lblmtotal1" runat="server" Text=""></asp:Label> <asp:Label ID="lblytotal1" runat="server" Text=""></asp:Label> 
                 <td></td>
                 <td></td>
             </td>
         </tr>     
         <tr>
             <td>
                 Total CTC <asp:Label ID="lblmctc" runat="server" Text=""></asp:Label> <asp:Label ID="lblyctc" runat="server" Text=""></asp:Label>
                 <td></td>
                 <td></td>
             </td>
         </tr>
         
     </table>
            </div>
       
        <div class="page">
              
            
        <h3 ><u>Annexure – B</u></h3>
             
        <br />
        <p>1.	Commencement of Employment</p>
        <p>Your employment will be effective, as of <asp:Label ID="lblappoint" runat="server" Text=""></asp:Label></p>
        <br />
        <p>2.	Job Title</p>
        <p>Your job title will be <asp:Label ID="lbldesignation" runat="server" Text=""></asp:Label>, and you will report to (Mr/Ms) <asp:Label ID="lblsuperior" runat="server" Text=""></asp:Label>, (Supervisor Designation).</p>
        <br />
        <p>3.	Place of posting</p>
        <p>You will be posted at<asp:Label ID="lblcity" runat="server" Text=""></asp:Label>  <asp:Label ID="lblstate" runat="server" Text=""></asp:Label>. You may however be required to work at any place of business which the company has, or may later acquire.</p>
        <br />
        <p>4.	Personal Particulars:</p>
        <p>You will keep us informed of any change in your residential address, your family status or any other relevant particulars. You would also let us know the name and address of your legal heir/nominee. </p>
        <br />
        <p>5.	Notices</p>
        <p>Notices may be given by you to the company at its registered office address. Notices may be given by the company to you at the address intimated by you in the official records.</p>
        <br />
        <p>6.	Nature of Work:</p>
        <p>You will work at high standard of initiative, creativeness, efficiency and economy in the organization. The nature of work and responsibilities will be assigned and explained to you by your senior from time to time.</p>
        <br />
        <p>7.	Working Hours:</p>
        <p>The regular working hours of the company are from <asp:Label ID="lblstart" runat="server" Text=""></asp:Label> through <asp:Label ID="lblend" runat="server" Text=""></asp:Label>. You will be required to work for such hours as necessary for the proper discharge of your duties to the Company. You shall be available for such number of hours and days in a week as we may decide from time to time.</p>
        <br />
        <p>8.	Assignment, Transfer and Deputation:</p>
        <p>Though you have been engaged to a specific position, the company reserves the right to send you on deputation/transfer/assignment to any of the company’s branch offices in India or abroad, whether existing at the time of your appointment or to be set up in the future.</p>
        <br />
        <p>9.	Training:</p>
        <p>You will hold yourself in readiness for any training at any place whenever required. Such training would be imparted to you at the company’s expense. Kindly note that refusal to participate in a training programme without any extraneous circumstances would lead to automatic termination of your employment.</p>
        <br />
        <p>10.	Intellectual Property Right:</p>
        <p>If during the period of your employment with us you achieve any invention, process improvement, operational improvement, or other process/method likely to result in more efficient operation of any of the activities of the company, the company shall be entitled to use, utilize and exploit such improvement and you shall assign all rights thereof to the company for the purpose of seeking any patent rights or for any other purpose. The company shall have the sole ownership rights of all the intellectual property rights that you may create during the tenure of association with the company including but not limited to the creative concept that you may develop during your association with the company.</p>
        <br />
        <p>11.	Secrecy/Confidentiality:</p>
        <p>i.	You will not during the course of your employment with the company or at any time there after divulge or disclose to any person whomsoever, make any use whatsoever for your own purpose or for any other purpose other than that of the company, of any information or knowledge obtained by you during your employment as to the business or affairs of the company including development, process reports and reporting system and you will during the course of your employment hereunder also use your best endeavour to prevent any other person from doing so. 
ii.	During your employment with the Company you will devote your whole time, attention and skill to the best of your ability for its business. You shall not, directly or indirectly, engage or associate yourself with, be connected with, concerned, employed or engaged in any other business or activities or any other post or work part time or pursue any course of study whatsoever, without the prior permission of the Company.
</p>
        <br />
        <p>12.	Restrain:</p>
        <p>i.	Access to Information:
Information is available on need to know basis for specific groups and the network file server of the company is segregated to allow individual sectors information access for projects and units. Access to this is authorized through access privileges approved by unit mentors or project mentors.
</p>
        <p>ii.	Restriction on Personal Use:
Use of company resources for personal use is strictly restricted. This includes usage of computer resources, information, internet service, and working time of the company for any personal use.
</p>
        <br />
        <p>13.	Leave: </p>
        <p>You will be entitled to leave as per law in force and as laid down in the Standing Orders of the company. Three days advance intimation is required to be given for availing leave. The company follows strict time schedule and late comings are discouraged, unless otherwise notified by you in advance. Late marks will be accorded to you for every late entry with one day of absence counted for every three late marks.</p>
        <br />
        <p>14.	Security:</p>
        <p>Security is an important aspect of our communication and office infrastructure. Communication security is maintained by controlling physical access to computer system, disabling all working stations, floppy disk drives and companywide awareness about the need for protection of intellectual property and sensitive customer information.</p>
        <br />
        <p>15.	Termination of Service:</p>
        <p>i.	Your appointment can be terminated by the Company, without any reason, by giving you not less than (Notice period) month’s prior notice in writing or salary in lieu thereof. For the purpose of this clause, salary shall mean basic salary.</p>
<p>ii.	You may terminate your employment with the Company, without any cause, by giving no less than (Employee Notice) month’s prior notice or salary for unsaved period, left after adjustment of pending leaves, as on date.</p>
<p>iii.	The company reserves the right to terminate your employment summarily without any notice period or termination payment, if it has reasonable ground to believe you are guilty of misconduct or negligence or have committed any fundamental breach of contract or caused any loss to the company. You will be governed by the laid down code of conduct of the company and if there is any breach of the same or nonconformance of contractual obligation or with the terms and conditions laid down in this agreement, your service can be terminated without any notice; notwithstanding any other terms and conditions stipulated herein the company reserves the right to invoke other legal remedies as it deems fit to protect its legitimate interest.</p>
<p>iv.	Unauthorized absence or absence without permission from duty for a continuous period of 7 days would make you loose your lien on employment. In such case your employment shall automatically come to an end without any notice of termination or notice pay. </p>
<p>v.	Spreading rumors, soliciting employees of the company for outside employment or coercing coworkers to leave the company is strictly prohibited and frowned upon. Such actions will result in immediate termination of employment without notice and forfeiture of your holding amount.</p>
<p>vi.	On the termination of your employment for whatever reason, you will return to the company all property, documents and paper, both original and copies thereof, including any samples, literature, contracts, records, lists, drawings, blueprints, letters, notes, data and the like; and Confidential Information, in your possession or under your control relating to your employment or to client’s business affairs.

</p>
        <br />
        <p>16.	Applicability of Company Policy</p>
        <p>The Company shall be entitled to make policy declarations from time to time pertaining to matters like leave entitlement, maternity leave, employees’ benefits, working hours, transfer policies, etc., and may alter the same from time to time at its sole discretion. All such policy decisions of the Company shall be binding on you and shall override this Agreement to that extent.</p>
        <br />
        <p>17.	Standing Orders:</p>
        <p>You will abide by the Standing Orders, rules & regulations and service conditions that may be in force or application to the organization or are framed from time to time by the company.</p>
        <br />
        <p>18.	Governing law/ Jurisdiction</p>
        <p>Your employment with the Company is subject to Indian laws. All disputes shall be subject to the jurisdiction of High Court <asp:Label ID="lblstate3" runat="server" Text=""></asp:Label> only.</p>
        <br />
        <p>19.	Borrowing/accepting gifts</p>
        <p>You will not borrow or accept any money, gift, reward or compensation for your personal gains from or otherwise place yourself under pecuniary obligation to any person/client with whom you may be having official dealings.</p>
        <br />
        <p>20.	Appointment in Good Faith:</p>
        <p>It must be specifically understood that this offer is made based on your proficiency on technical/professional skills you have declared to possess as per your application for employment and your ability to handle any assignment/job independently. In case at a latter date any of your statements/furnished are found to be false or misleading or your performance is not up to the mark or falls short of the minimum standard set by the company, the company shall have the right to terminate your services forthwith without giving any notice notwithstanding any other terms and conditions stipulated therein.

The above terms and conditions are based on the company’s policy, procedures and other rules currently applicable in India and are subject to amendments and adjustments from time to time. In all matter including those not specifically covered here such as traveling, retirement, etc. you will be governed by the rules of the company as shall be in force from time to time.

Please indicate your understanding and acceptance of the above terms and conditions by signing in the space provided below and returning the duplicate copy.
</p>
        <br />
        <br />
        <p>I accept.</p>
        <p>(Signature, name & date)</p>
            </div>
    </div>
    </form>
</body>
</html>
