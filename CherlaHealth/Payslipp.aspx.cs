﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class Payslipp : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            Dist();
        }

        private void Dist()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;

            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select Payslip,Name,Basic,pf,esi,Total from  Payslip ORDER BY Id desc", conn44);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        private void employeee()
        {
            if (RadioButtonList1.SelectedValue == "1")
            {
                string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;

                SqlConnection conn44 = new SqlConnection(connstrg);
                conn44.Open();
                SqlCommand cmd = new SqlCommand("select Payslip,Name,Basic,pf,esi,Total from  Payslip where PFStatus='NPF' ORDER BY Id desc", conn44);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn44.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        
       else if(RadioButtonList1.SelectedValue == "2")
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;

            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select Payslip,Name,Basic,pf,esi,Total from  Payslip where PFStatus='PF' ORDER BY Id desc", conn44);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
            else if(RadioButtonList1.SelectedValue=="3")
            {
                string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;

                SqlConnection conn44 = new SqlConnection(connstrg);
                conn44.Open();
                SqlCommand cmd = new SqlCommand("select Payslip,Name,Basic,pf,esi,Total from  Payslip ORDER BY Id desc", conn44);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn44.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
}

        protected void Button1_Click(object sender, EventArgs e)
        {
            employeee();
        }
        
    }
       
}