﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewCompensatory.aspx.cs" Inherits="CherlaHealth.ViewCompensatory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">View Compensatory Off's</h2>
            <hr />
        </div>
        <!-- /.box-header -->
       
        <div class="col-md-3">
            <div class="form-group">
                <asp:UpdatePanel ID="Update" runat="server"><ContentTemplate>
                Search By Employee<asp:DropDownList ID="dropemp" CssClass="form-control" OnSelectedIndexChanged="Employee_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
            </ContentTemplate></asp:UpdatePanel>
                    </div>
        </div>
         <div class="col-md-3" style="padding-top:18px;float:right">
            <div class="form-group">
                <asp:Button ID="btnsubmit" CssClass="btn btn-success" PostBackUrl="~/Addcompensatory.aspx" runat="server" Text="Add Compensatory Off" />
                </div>
             
             </div>
        <div class="box-body">
            <div class="col-md-12">
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                <asp:GridView ID="gvCompensatory" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None" runat="server">
                    <Columns>
                        <asp:BoundField DataField="EmployeeId" HeaderText="Employee Id" />
                        <asp:BoundField DataField="EmployeeName" HeaderText="Employee Name" />
                        <asp:BoundField DataField="Worked_date" HeaderText="Worked Date" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:BoundField DataField="CL" HeaderText="CL" />
                        <asp:BoundField DataField="description" HeaderText="Description" />
                        
                    </Columns>
                </asp:GridView>
                     </ContentTemplate></asp:UpdatePanel>
            </div>
            </div>
        </div>

</asp:Content>
