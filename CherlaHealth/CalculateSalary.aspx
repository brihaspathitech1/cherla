﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CalculateSalary.aspx.cs" Inherits="CherlaHealth.CalculateSalary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCalendarShown() {

            var cal = $find("calendar1");
            //Setting the default mode to month
            cal._switchMode("months", true);

            //Iterate every month Item and attach click event to it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }
       

        function onCalendarHidden() {
            var cal = $find("calendar1");
            //Iterate every month Item and remove click event from it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }

        }
        
        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "month":
                    var cal = $find("calendar1");
                    cal._visibleDate = target.date;
                    cal.set_selectedDate(target.date);
                    cal._switchMonth(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged();
                    break;
            }
        }
            
    </script>
    <style>
        .inline-rb label {
            font-weight: bold !important;
            color: #00daff;
            font-size: 20px;
        }
        .auto-style1 {
            position: relative;
            min-height: 1px;
            float: left;
            width: 25%;
            left: 0px;
            top: 0px;
            padding-left: 15px;
            padding-right: 15px;
        }
    </style>

     <style>
       #overlay
       {
           position:fixed;
           z-index:99;
           top:0px;
           left:0px;
           background-color:#ffffff;
           width:100%;
           height:100%;
           filter:Alpha(opacity=80);
           opacity:0.80;
           -moz-opacity:0.80;
           
       }
       #theprogress{
           background-color:#D3BB9C;
           height:110px;
           width:24px;
           text-align:center;
           filter:Alpha(Opacity=100);
           opacity:1;
           -moz-opacity:1;

       }
       #modalprogress{
           position:absolute;
           top:50%;
           left:50%;
           margin:-11px 0 0 -55px;
           color:white;

       }
       body>#modalprogress{
           position:fixed;
       }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Calculate Salary</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <asp:UpdatePanel ID="dd" runat="server">
                    <ContentTemplate>
            <div class="row">
                <asp:Label ID="PFcompany" runat="server" Text="Label" Visible="false"></asp:Label>
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:RadioButtonList ID="RadioPf" RepeatDirection="Horizontal" Width="100px" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            <asp:ListItem Value="1">NON PF</asp:ListItem>
                            <asp:ListItem Value="2">PF</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="RadioPf"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        Employee Name<asp:DropDownList ID="dropEmployee" CssClass="form-control" runat="server">
                            <asp:ListItem></asp:ListItem>
                            <%--  <asp:ListItem>Malathi A</asp:ListItem>
                      <asp:ListItem>Shanmi G</asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropEmployee" InitialValue="0" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        Month<asp:TextBox ID="txtmonth" CssClass="form-control" runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="yyyy-MM-dd" TargetControlID="txtmonth"
                            OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                            Enabled="True"></cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtmonth" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>


                 <%--<div class="col-md-3">
                    <div class="form-group">
                        Start date<asp:TextBox ID="txtstartdate" CssClass="form-control" runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" TargetControlID="txtstartdate"
                            OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" Enabled="True"></cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtstartdate" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div> 
                
                
                <div class="col-md-3">
                    <div class="form-group">
                        End date<asp:TextBox ID="txtenddate" CssClass="form-control" runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="yyyy-MM-dd" TargetControlID="txtenddate"
                            OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown"  Enabled="True"></cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="txtenddate" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
      --%>          <div class="col-md-3" style="padding-top: 18px">
                    <div class="form-group">
                        <asp:Button ID="btnclick" CssClass="btn btn-success" OnClick="Button1_Click" runat="server" Text="Click" />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Absent<asp:TextBox ID="txtabsent" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Holiday<asp:TextBox ID="txtholiday" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Present<asp:TextBox ID="txtpresent" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Present(NOP)<asp:TextBox ID="txtnop" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Week Off<asp:TextBox ID="txtoff" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    OD<asp:TextBox ID="txtOD" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Advance Amount<asp:TextBox ID="txtadvance" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtadvance" FilterType="Numbers" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Mobile Bill<asp:TextBox ID="txtmobile" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtmobile" FilterType="Numbers" />
                </div>
            </div>
             <div class="col-md-3"  >
                <div class="form-group">
                    Travel Allowance
                    <asp:TextBox ID="txttravellin" runat="server" Text="0" CssClass="form-control"></asp:TextBox>
                    <%-- <asp:Label ID="txttravelling" runat="server" Text="0" CssClass="form-control"></asp:Label>--%>
                    </div>
                 </div>
             <div class="col-md-3"  >
                <div class="form-group">
                    Income Tax 
                    <asp:TextBox ID="txtincome" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                    </div>
                 </div>
            <div class="col-md-3"  >
                <div class="form-group">
                    Professional Tax 
                    <asp:TextBox ID="proftax" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                    </div>
                 </div>
            <div class="col-md-3"  >
                <div class="form-group">
                    Lose of Pay Days
                    <asp:TextBox ID="casual" CssClass="form-control" runat="server" Text="0"></asp:TextBox>
                    </div>
                 </div>
            <div class="col-md-3" style="padding-top: 18px">
                <div class="form-group">
                    <asp:Button ID="btncalculate" CssClass="btn btn-success" runat="server" OnClick="Button1_Click1" Text="Calculate Salary" />
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="col-md-12">

                <div>
                    <div class="col-md-3">
                        Basic
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblbasic" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        HRA
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="Label1" runat="server" Text="0"></asp:Label>
                        <asp:Label ID="lblhra" runat="server" Text="0" Visible="false"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        EA 
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblea" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">CA</div>
                    <div class="col-md-3">
                        <asp:Label ID="lblca" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <%--<div>--%>
                    <%--<div class="col-md-3">
                        Children Education
                    </div>--%>
                    <%--<div class="col-md-3">--%>
                        <asp:Label ID="lblDA" runat="server" Text="0" Visible="false"  ></asp:Label>
                       <%-- <asp:Label ID="CE" runat="server" Text="0"></asp:Label>--%>
                    <%--</div>
                </div>--%>
                <div>
                    <div class="col-md-3">
                        Special Allowances
                    </div>
                    <div class="auto-style1">
                        <asp:Label ID="lblactual" runat="server" Text="0" Visible="false" ></asp:Label>
                        <asp:Label ID="specialalowance" runat="server" Text="0" ></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">Gross Salary</div>
                    <div class="col-md-3">
                        <asp:Label ID="lblgross" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        PF
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblpf" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        ESI
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblesi" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        IT
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblit" runat="server" Text="0"></asp:Label>
                    </div>
                </div>



                <div>
                    <div class="col-md-3">
                        Mobile Bill
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblmobile" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        Other Deductions
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblother" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        Advance Salary
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lbladvance" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        Lose Pay
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblpay" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                 <div>
                    <div class="col-md-3">
                        Professional Tax
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblprofession" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        Deduction
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lbldeduction" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        Net Salary
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblNet" runat="server" Text="0"></asp:Label>
                    </div>
                </div>
                <div>
                    <div class="col-md-3">
                        In Words
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblWords" runat="server" Text="0"></asp:Label>
                    </div>
                </div>

            </div>
            <br />
            <br />
            <div class="col-md-3" style="margin-left: 875px;">
                <div class="form-group">
                    <asp:Label ID="lblStatus" runat="server" Text="" ForeColor="red"></asp:Label>
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" OnClick="Button3_Click" runat="server" Text="Submit" />
                </div>
            </div>
            <asp:Label ID="ADMIN" runat="server" Text="1.36" Visible="false"></asp:Label>

            <asp:Label ID="Label2" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label3" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label6" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label7" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label9" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label11" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label10" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label12" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label13" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label14" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label15" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label16" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label17" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="Label18" runat="server" Text="" Visible="false"></asp:Label>
             
            <%--Working Days--%>
            <asp:TextBox ID="TextBox33" runat="server" Text="0" Visible="false"></asp:TextBox>
            <%-- Year--%>
            <asp:TextBox ID="TextBox34" runat="server" Text="0" Visible="false"></asp:TextBox>
            <%-- Month--%>
            <asp:TextBox ID="TextBox35" runat="server" Text="0" Visible="false"></asp:TextBox>
            <%-- Month--%>
            <asp:TextBox ID="TextBox36" runat="server" Text="0" Visible="false"></asp:TextBox>
            <%-- Casual--%>
            <asp:TextBox ID="TextBox37" runat="server" Text="0" Visible="false"></asp:TextBox>


            <asp:TextBox ID="TextBox39" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox1" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox2" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox3" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox4" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox5" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox7" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox8" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox522" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox9" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox10" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox11" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox12" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox22" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox23" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox24" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox32" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox25" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox26" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox27" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox28" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox29" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox30" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox31" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox40" runat="server" Text="0" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox42" runat="server" Visible="false"></asp:TextBox>
           

        </div>
  </ContentTemplate></asp:UpdatePanel>  </div>
        <asp:UpdateProgress AssociatedUpdatePanelID="dd" id="hd" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <div id="modalprogress">
                <div id="theprogress">
                    <img alt="indicator" src="giphy.gif" />
                </div>
            </div></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
