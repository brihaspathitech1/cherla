﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace CherlaHealth
{
    public partial class EmployeeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindStudy();
            }
        }
        private void BindStudy()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select Company,Desigantion1,EmployeName,EmployeeID1,convert(varchar(10),DOB,110) as DOB,convert(varchar(10),DOJ,110) as DOJ,Gender,Salary,Desigantion,EmployeeType,CompanyEmail,CellNo,Address,BloodGroup,EmployeeId,Password,Emptype from  Employees where status='1'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        protected void btnModity1_Click(object sender, EventArgs e)
        {
            string serverpath = string.Empty;
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            string DeptId = lblstor_id.Text;
            conn44.Open();
            if(FileUpload1.HasFile)
            {
                string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                FileUpload1.PostedFile.SaveAs(path);
                serverpath = @"http://Cherla.bterp.in/images/" + filename;
            }
            SqlCommand cmd = new SqlCommand("update Employees set EmpImage=@IMG,image='"+serverpath+"' where EmployeeId=" + lblstor_id.Text, conn44);
            int img1 = FileUpload1.PostedFile.ContentLength;
            byte[] msdata1 = new byte[img1];
            FileUpload1.PostedFile.InputStream.Read(msdata1, 0, img1);
            cmd.Parameters.AddWithValue("@IMG", msdata1);

            cmd.ExecuteNonQuery();

            conn44.Close();
            lblmsg.Text = "Data Updated...";
            BindStudy();
        }
        protected void btnModity12_Click(object sender, EventArgs e)
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            string DeptId = lblstor_id.Text;
            conn44.Open();

            SqlCommand cmd = new SqlCommand("update Employees set CategoryId='" + HolidayList.Text + "' where EmployeeId=" + lblstor_id.Text, conn44);
            cmd.ExecuteNonQuery();
            conn44.Close();
            lblmsg.Text = "Data Updated...";
            BindStudy();
        }


        private void BindStudy11()
        {
            try
            {
                string INSSS = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;

                using (SqlConnection sqlConn = new SqlConnection(INSSS))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT CategoryId,CategoryName FROM Categories";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        HolidayList.DataSource = dt;
                        HolidayList.DataValueField = "CategoryId";
                        HolidayList.DataTextField = "CategoryName";
                        HolidayList.DataBind();
                        sqlConn.Close();


                        HolidayList.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }

        }
        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);

            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Employees where EmployeName like '%'+@date1+'%'  or Desigantion like '%'+@date1+'%' or CellNo like '%'+@date1+'%'  ", conn44);
            cmd.Parameters.Add("@date1", SqlDbType.VarChar).Value = TextBox1.Text.Trim();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }


        private void BindStudy1()
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "select * from  Department";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList1.DataSource = dt;
                        DropDownList1.DataValueField = "DeptId";
                        DropDownList1.DataTextField = "Department";
                        DropDownList1.DataBind();
                        sqlConn.Close();

                        DropDownList1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "0"));

                    }
                }
            }
            catch { }

        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            BindStudy1();
            BindStudy11();
            BindComapany();
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();

            //dropcomapany.SelectedItem.Text = gRow.Cells[3].Text.Replace("&nbsp;", "");
            DropDownList1.SelectedItem.Text = gRow.Cells[4].Text.Replace("&nbsp;", "");
            txtstate.Text = gRow.Cells[5].Text.Replace("&nbsp;", "");
            txtstate0.Text = gRow.Cells[6].Text.Replace("&nbsp;", "");
            txtstate1.Text = gRow.Cells[7].Text.Replace("&nbsp;", "");

            txtstate2.Text = gRow.Cells[8].Text.Replace("&nbsp;", "");
            txtstate3.Text = gRow.Cells[9].Text.Replace("&nbsp;", "");
            txtstate5.Text = gRow.Cells[10].Text.Replace("&nbsp;", "");
            txtstate13.Text = gRow.Cells[11].Text.Replace("&nbsp;", "");
            txtstate6.Text = gRow.Cells[12].Text.Replace("&nbsp;", "");
            txtstate7.Text = gRow.Cells[13].Text.Replace("&nbsp;", "");
            txtstate8.Text = gRow.Cells[14].Text.Replace("&nbsp;", "");
            txtstate9.Text = gRow.Cells[15].Text.Replace("&nbsp;", "");
            txtpass.Text= gRow.Cells[16].Text.Replace("&nbsp;", "");
            
            dropmed.SelectedItem.Text= gRow.Cells[17].Text.Replace("&nbsp;", "");
            TextBox2.Text = gRow.Cells[18].Text.Replace("&nbsp;", "");
            // txtstate7.Text = gRow.Cells[15].Text;
            this.ModalPopupExtender2.Show();
        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn33 = new SqlConnection(connstrg);
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lbldeleteid = (Label)row.FindControl("EmployeeId");
            conn33.Open();
            SqlCommand cmd3 = new SqlCommand("update  Employees set status='0' where EmployeeId='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'", conn33);
            cmd3.ExecuteNonQuery();
            conn33.Close();
            BindStudy();

        }
        protected void btnModity_Click(object sender, EventArgs e)
        {
            if(dropmed.SelectedItem.Text== "Medicare")
            {
                dropmed.SelectedValue = "2";
            }
            else
            {
                dropmed.SelectedValue = "1";
            }
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            string DeptId = lblstor_id.Text;
            conn44.Open();
            SqlCommand cmd = new SqlCommand("update Employees set Company='" + dropcomapany.SelectedItem.Text + "',CompanyId='" + dropcomapany.SelectedValue+"',Desigantion1='" + DropDownList1.SelectedItem.Text + "',EmployeName='" + txtstate.Text + "',EmployeeID1='" + txtstate0.Text + "',DOB='" + txtstate1.Text + "',DOJ='" + txtstate2.Text + "', Gender='" + txtstate3.Text + "',Desigantion='" + txtstate5.Text + "',EmployeeType='" + txtstate13.Text + "',CompanyEmail='" + txtstate6.Text + "',CellNo='" + txtstate7.Text + "',Address='" + txtstate8.Text + "',BloodGroup='" + txtstate9.Text + "',Password='"+txtpass.Text+"',Emptype='"+dropmed.SelectedItem.Text+"',emptypeid='"+dropmed.SelectedValue+"',Salary='"+TextBox2.Text+"' where EmployeeId=" + lblstor_id.Text, conn44);
            cmd.ExecuteNonQuery();
            conn44.Close();
            lblmsg.Text = "Data Updated...";
            BindStudy();
        }

        protected void BindComapany()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            SqlCommand cmd = new SqlCommand("Select CompanyId,CompanyName from Companies", conn44);
            conn44.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropcomapany.DataSource = dr;
            dropcomapany.DataValueField = "CompanyId";
            dropcomapany.DataTextField = "CompanyName";
            dropcomapany.DataBind();
            dropcomapany.Items.Insert(0, new ListItem("", "0"));
        }

        
    }
}