﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddDepartment.aspx.cs" Inherits="CherlaHealth.AddDepartment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
         <style type="text/css">
        .style1 {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Add Department</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
              <div class="col-md-12">
            <div class="col-sm-6">
                Company
                <span class="style1">*</span>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="DropDownList1" class="form-control" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddl1" runat="server" ErrorMessage="*Select a company"
                            ControlToValidate="DropDownList1" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DropDownList1" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
            <div class="col-sm-6">
                Department
                <span class="style1">*</span>
                <asp:TextBox ID="Code" class="form-control" runat="server"></asp:TextBox>
                <asp:Label ID="lblStatus" runat="server" Style="color: #FF0000"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCode" runat="server" ErrorMessage="* Fill Department"
                    ControlToValidate="Code" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                Short Name
                <span class="style1">*</span>
                <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextbox1" runat="server" ErrorMessage="* Select a ShortName"
                    ControlToValidate="TextBox1" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
            <div class="col-sm-6">
                Description

                <span class="style1"></span>
                <asp:TextBox ID="TextBox2" class="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-12 btn1 fr">
            <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Submit" OnClick="Button1_Click" />
        </div>
            </div>
        </div>
</asp:Content>
