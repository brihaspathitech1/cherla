﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class appointment : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindCandidates();
                BindEmployee();
                Bindaddress();
                Bindshift();
            }
        }

        protected void BindCandidates()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Name,Id from Candidates where status='Selected' order by id desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropcandidate.DataSource = dr;
            dropcandidate.DataTextField = "Name";
            dropcandidate.DataValueField = "Id";
            dropcandidate.DataBind();
            dropcandidate.Items.Insert(0, new ListItem("", "0"));
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemployee.DataSource = dr;
            dropemployee.DataTextField = "EmployeName";
            dropemployee.DataValueField = "EmployeeId";
            dropemployee.DataBind();
            dropemployee.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Bindaddress()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select CompanyId,CompanyName from Companies order by CompanyId desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropaddress.DataSource = dr;
            dropaddress.DataTextField = "CompanyName";
            dropaddress.DataValueField = "CompanyId";
            dropaddress.DataBind();
            dropaddress.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Bindshift()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select ShiftId,ShiftName from Shifts order by ShiftId desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropshift.DataSource = dr;
            dropshift.DataTextField = "ShiftName";
            dropshift.DataValueField = "ShiftId";
            dropshift.DataBind();
            dropshift.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Candidate_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd1 = new SqlCommand("Select Top 1 joining_date,salary,designation from offerletter where candid='" + dropcandidate.Text + "' order by id desc", conn);
            conn.Open();
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if(dr1.Read())
            {
                txtdesignation.Text = dr1["designation"].ToString();
                txtsal.Text = dr1["salary"].ToString();
                DateTime date = DateTime.Parse(dr1["joining_date"].ToString());
                txtjoin.Text = date.ToString("yyyy-MM-dd");
            }
            else
            {
                txtdesignation.Text = null;
                txtsal.Text = null;
                txtjoin.Text = null;
            }
            conn.Close();
            SqlCommand cmd = new SqlCommand("Select Address from Candidates where id='" + dropcandidate.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                txtaddress.Text = dr["Address"].ToString();
            }
            conn.Close();
        }
        protected void Submit_Click(object sender,EventArgs e)
        {
            double salary = Convert.ToDouble(txtsal.Text);
            double month = salary / 12;
            double mbasic = month * 0.35;
            double mhra = mbasic / 2;
            double conveyance = 800;
            double CHA = 200;
            double mpf = mbasic * 0.12;
            double msubtotal1 = month - mpf;

            double mspecial = msubtotal1 - (mbasic + mhra + conveyance + CHA);
            double ESI = 0;
            double Statutory = 0;
            double msubtotal2 = ESI + mpf;
            double mtotCTC = msubtotal1 + msubtotal2;

            double ybasic = mbasic * 12;
            double yhra = mhra * 12;
            double yconvey = conveyance * 12;
            double yCHA = CHA * 12;
            double ypf = mpf * 12;
            double ysubtotal1 = msubtotal1 * 12;
            double yspecial = mspecial * 12;

            double yesi = ESI * 12;
            double ystatutory = Statutory * 12;
            double ysubtotal2 = msubtotal2 * 12;
            double ytotCTC = mtotCTC * 12;

            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("proc_appointment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@date", System.DateTime.Now.ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@candid", dropcandidate.SelectedValue);
            cmd.Parameters.AddWithValue("@name", dropcandidate.SelectedItem.Text);
            cmd.Parameters.AddWithValue("@address", txtaddress.Text);
            cmd.Parameters.AddWithValue("@desig", txtdesignation.Text);
            cmd.Parameters.AddWithValue("@apdate", txtjoin.Text);
            cmd.Parameters.AddWithValue("@caddress", dropaddress.SelectedItem.Text);
            cmd.Parameters.AddWithValue("@city", txtcity.Text);
            cmd.Parameters.AddWithValue("@state", txtstate.Text);
            cmd.Parameters.AddWithValue("@sal", txtsal.Text);
            cmd.Parameters.AddWithValue("@monthsal", month.ToString());
            cmd.Parameters.AddWithValue("@mbasic", mbasic.ToString());
            cmd.Parameters.AddWithValue("@mhra", mhra.ToString());
            cmd.Parameters.AddWithValue("@mconvey", conveyance.ToString());
            cmd.Parameters.AddWithValue("@mcha", CHA.ToString());
            cmd.Parameters.AddWithValue("@mspec", mspecial.ToString());
            cmd.Parameters.AddWithValue("@msub1", msubtotal1.ToString());
            cmd.Parameters.AddWithValue("@mstat", Statutory.ToString());
            cmd.Parameters.AddWithValue("@mesi", ESI.ToString());
            cmd.Parameters.AddWithValue("@mpf", mpf.ToString());
            cmd.Parameters.AddWithValue("@msub2", msubtotal2.ToString());
            cmd.Parameters.AddWithValue("@mtot", mtotCTC.ToString());
            cmd.Parameters.AddWithValue("@yearsal", salary.ToString());
            cmd.Parameters.AddWithValue("@ybasic", ybasic.ToString());
            cmd.Parameters.AddWithValue("@yhra",yhra.ToString());
            cmd.Parameters.AddWithValue("@yconvey",yconvey.ToString());
            cmd.Parameters.AddWithValue("@ycha",yCHA.ToString());
            cmd.Parameters.AddWithValue("@yspec",yspecial.ToString());
            cmd.Parameters.AddWithValue("@ysub1",ysubtotal1.ToString());
            cmd.Parameters.AddWithValue("@ystat",ystatutory.ToString());
            cmd.Parameters.AddWithValue("@yesi",yesi.ToString());
            cmd.Parameters.AddWithValue("@ypf",ypf.ToString());
            cmd.Parameters.AddWithValue("@ysub2",ysubtotal2.ToString());
            cmd.Parameters.AddWithValue("@ytot",ytotCTC.ToString());
            cmd.Parameters.AddWithValue("@superior", dropemployee.SelectedItem.Text);
            cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
            conn.Open();
            cmd.ExecuteNonQuery();
            string id = cmd.Parameters["@id"].Value.ToString();
            conn.Close();
            Session["id"] = id;
            Session["shift"] = dropshift.SelectedValue;
            Session["salute"] = salut.SelectedItem.Text;
            Response.Redirect("~/appointmentPrint.aspx");
        }
    }
}