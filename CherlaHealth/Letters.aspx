﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true"  CodeBehind="Letters.aspx.cs" Inherits="CherlaHealth.Letters" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<!--home-content-top starts from here-->
     <div class="box">
        <div class="box-header">
            <div class="row mt">
                <div class=" col-lg-12 panel" style="padding: 20px">
                  
                    <div class=" col-lg-12">
 
    
    <!--our-quality-shadow-->
    <div class="clearfix"></div>
    <h5 class="heading1">Appointment Letter</h5>
    <div class="tabbable-panel margin-tops4 ">
      <div class="tabbable-line">
        <ul class="nav nav-tabs tabtop  tabsetting">
        </ul>
        <div class="tab-content margin-tops">
          <div class="tab-pane active fade in" id="tab_default_1">
         
            <div class="col-md-12">
                 <div class="row">
                <div class="col-md-3">
                    <label>
                        Selected Candidates<span class="style1">*</span></label>

                       
                    <asp:DropDownList ID="DropDownList1" class="form-control" runat="server" OnSelectedIndexChanged="dropselect" AutoPostBack="true">
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="DropDownList1" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                </div>
                      <div class="col-md-3">
                    <label>
                        Interview Date<span class="style1">*</span></label>
                    <asp:TextBox ID="Interviewdate" class="form-control" runat="server"></asp:TextBox>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="Interviewdate"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    <%--<cc1:CalendarExtender ID="CalendarExtender3" TargetControlID="Interviewdate" runat="server" Format="dd-MM-yyyy">
                    </cc1:CalendarExtender>--%>
                </div>
                <div class="col-md-3">
                    <label>
                        Joining Date<span class="style1">*</span></label>
                    <asp:TextBox ID="Join_date" class="form-control" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="Join_date" runat="server" Format="dd-MM-yyyy">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Join_date"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-3">
                    <label>
                        Salary Per Anum<span class="style1">*</span></label>

                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                    <asp:TextBox ID="Sal_Anum" class="form-control" runat="server" OnTextChanged="Sal_Anum_TextChanged"
                        AutoPostBack="true" placeholder="Enter Digits Only..."></asp:TextBox>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="Sal_Anum"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Sal_Anum" FilterType="Numbers"/>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                   
                </div>
                      </div>
                </div>
               
              <div class="form-group">
                 
                  <div class="col-md-12">
                        <br />
              <br />
                       <div class="row">
                <div class="col-md-3">
                    <label>
                        Salary  Per Month<span class="style1">*</span></label>
                         <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                    <asp:TextBox ID="Sal_Month" class="form-control" runat="server" placeholder="Enter Digits Only..."></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="Sal_Month"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                              <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="Sal_Month" FilterType="Numbers"/>
                    </ContentTemplate>
                        </asp:UpdatePanel>
                </div>
                    
              
              
                     
                
                <div class="col-md-3">
                    <label>
                        Designation<span class="style1">*</span></label>

                   <asp:TextBox ID="Designation" class="form-control" runat="server" placeholder="Enter Valid Designation Only..."></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="Designation"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    
                  
                </div>
                <div class="col-md-2" style="padding-top:23px">
                    <asp:Button ID="Button1" runat="server" class="btn btn-success" Text="Submit" OnClick="ClikIt" />
                </div>
            </div>
                    </div>  
                  </div>    
            
            </div>
            
         
          <div class="tab-pane fade" id="tab_default_2">
           
            <div class="col-md-12">
             
                 <div class="row">
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <label>
                                Employee</label>
                            <asp:DropDownList ID="DropDownList2" class="form-control" OnSelectedIndexChanged="OrgType_SelectedIndexChanged"
                                AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%-- <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label4" runat="server" Text=""></asp:Label>--%>
                </div>
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label>
                                Previous Salary/Anum</label>
                            <asp:TextBox ID="Prev_Salary" class="form-control" runat="server"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <label>
                                Hike Salary/Anum</label>
                            <asp:TextBox ID="Hike_Salary" class="form-control" runat="server" OnTextChanged="Hike_Salary_TextChanged"
                                AutoPostBack="true"></asp:TextBox>
                                 <asp:Label ID="salry_words" runat="server" Text="" style="color:Green"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-2">
                    <label>
                        Hike Date</label>
                    <asp:TextBox ID="Hike_date" class="form-control" runat="server" ></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="Hike_date" runat="server" Format="dd-MM-yyyy" >
                    </cc1:CalendarExtender>
                </div>
                <div class="col-md-2" style="padding-top:23px">
                    <asp:Button ID="Button2" runat="server" class="btn btn-success" Text="Submit" OnClick="ClikItNow" />
                </div>
            </div>
             
             
              </div>
          </div>
          

          

            <div class="tab-pane fade" id="tab_default_3">
           
            <div class="col-md-12">
             
                 <div class="row">
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <label>
                                Employee</label>
                            <asp:DropDownList ID="DropDownList4" class="form-control" autopostback="true"
                                runat="server" OnSelectedIndexChanged="DropDownList4_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%-- <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label4" runat="server" Text=""></asp:Label>--%>
                </div>
                <div class="col-md-3">
                     <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>  <label> 
                               Designation</label>
                       <asp:TextBox ID="empDesignation" runat="server" class="form-control"></asp:TextBox>
                            </ContentTemplate>
                    </asp:UpdatePanel>
                
                </div>
                <div class="col-md-3">
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                             <label>
                                DateFrom</label>
                            <cc1:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="TextBox4"  Format="dd-MM-yyyy"></cc1:CalendarExtender>
                            <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                   
                </div>
                <div class="col-md-2">
                   <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                          
                             <label>
                               Date To</label>
                            <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="TextBox1"  Format="dd-MM-yyyy"></cc1:CalendarExtender>
                            <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                  <%--  <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>--%>
                </div>
                <div class="col-md-2" style="padding-top:23px">
                    <asp:Button ID="Button4" runat="server" class="btn btn-success" Text="Submit" OnClick="submit" />
                </div>
            </div>
             
             
              </div>
          </div>

            
            <div class="tab-pane fade" id="tab_default_4">
           
            <div class="col-md-12">
             
                 <div class="row">
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <label>
                                Employee</label>
                            <asp:DropDownList ID="DropDownList5" class="form-control" 
                                runat="server">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%-- <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label4" runat="server" Text=""></asp:Label>--%>
                </div>
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                             <label>
                                DateFrom</label>
                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="TextBox5"  Format="dd-MM-yyyy"></cc1:CalendarExtender>
                            <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-3">
                    <%--<asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                          
                             <label>
                               Date To</label>
                            <cc1:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="TextBox6"  Format="dd-MM-yyyy"></cc1:CalendarExtender>
                            <asp:TextBox ID="TextBox6" runat="server" class="form-control"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
              
                <div class="col-md-2" style="padding-top:23px">
                    <asp:Button ID="Button3" runat="server" class="btn btn-success" Text="Submit" onclick="submit1" />
                </div>
            </div>
             
             
              </div>
          </div>


        </div>
      </div>
    </div>
  </div>
  </div>

  </div>
 </div>
         </div>
    
</asp:Content>
