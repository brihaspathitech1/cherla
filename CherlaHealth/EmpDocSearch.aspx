﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EmpDocSearch.aspx.cs" Inherits="CherlaHealth.EmpDocSearch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">View Documents</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                   
                    Search By Employee <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server" OnSelectedIndexChanged="dropemp_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>              
                         </div>
            </div>
            <div class="col-md-12">
              
                <asp:GridView ID="gvDocuments" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None" runat="server">
                    <Columns>
                        <asp:BoundField DataField="EmployeeId" HeaderText="Employee Id" />
                        <asp:BoundField DataField="EmployeName" HeaderText="Employee Name" />
                        <asp:BoundField DataField="Desigantion1" HeaderText="Department" />
                        <asp:TemplateField HeaderText="Resume">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkresume" OnClick="Resume_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Degree1">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkdeg1" OnClick="Degree1_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Degree2">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkdeg2" OnClick="Degree2_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Degree3">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkdeg3" OnClick="Degree3_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Degree4">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkdeg4" OnClick="Degree4_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Degree5">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkdeg5" OnClick="Degree5_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="MCI_Expire" HeaderText="MCI Expire Date" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:TemplateField HeaderText="MCI">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkMCI" OnClick="MCI_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SCI_Expire" HeaderText="PCI Expire Date" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:TemplateField HeaderText="PCI">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkSCI" OnClick="SCI_click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Others">
                            <ItemTemplate>
                                <asp:LinkButton ID="linkoth1" OnClick="Other1_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">Others1</asp:LinkButton><br />
                                <asp:LinkButton ID="linkoth2" OnClick="Other2_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">Others2</asp:LinkButton><br />
                                <asp:LinkButton ID="linkoth3" OnClick="Other3_Click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">Others3</asp:LinkButton><br />
                                <asp:LinkButton ID="linkoth4" OnClick="Other4_click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">Others4</asp:LinkButton><br />
                                <asp:LinkButton ID="linkoth5" OnClick="Other5_click" ForeColor="#0099cc" CommandArgument='<%# Eval("EmployeeId") %>' runat="server">Others5</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnupdate" CssClass="btn btn-success btn-xs" runat="server" Text="Update" CommandArgument='<%# Eval("EmployeeId") %>' OnClick="Update_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                     
            </div>

            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" TargetControlID="HiddenField1" PopupControlID="Panel1" CancelControlID="btncancel" runat="server"></ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" BackColor="White" Height="300px" Width="600px"
                        Style="display: none;margin-left:35px;border-style: groove;">
                <h3 style="text-align:center">Update Documents</h3>
                <hr />
                <div class="row" style="margin-left: 3px;">
                <div class="col-md-6">
                    <div class="form-group">
                        MCI Validation Date <asp:TextBox ID="txtmci" CssClass="form-control" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtmci" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        Document <asp:FileUpload ID="filemci" runat="server" />
                    </div>
                </div>
                    </div>
                 <div class="row" style="margin-left: 3px;">
                 <div class="col-md-6">
                    <div class="form-group">
                        PCI Validation Date <asp:TextBox ID="txtsci" CssClass="form-control" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtsci" />
                        </div>
                     </div>
                 <div class="col-md-6">
                    <div class="form-group">
                          Document <asp:FileUpload ID="filesci" runat="server" />
                        </div>
                     </div>
                     </div>
                <div class="row" style="margin-left: 5px;">
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:Button ID="btnupdate12" CssClass="btn btn-success" runat="server" Text="Update" OnClick="Update" />
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class="form-group">
                        <asp:Button ID="btncancel" CssClass="btn btn-danger" runat="server" Text="Cancel" />
                        </div>
                     </div>
                    </div>
            </asp:Panel>
            <asp:Label ID="lblid" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblmci" Visible="false" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblsci" Visible="false" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblmcidate" Visible="false" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblscidate" Visible="false" runat="server" Text=""></asp:Label>
            </div>
        </div>
</asp:Content>
