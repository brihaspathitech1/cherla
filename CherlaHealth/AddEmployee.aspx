﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="CherlaHealth.AddEmployee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#imgprvw').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <style type="text/css">
        .style1 {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Add Employee</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-sm-3">
                    Salutation<span class="style1">*</span>
                    <asp:DropDownList ID="Salutation" class="form-control" runat="server">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem>Mr.</asp:ListItem>
                        <asp:ListItem>Ms.</asp:ListItem>
                        <asp:ListItem>Dr.</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator31" ControlToValidate="Salutation"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <%--<div class="col-sm-3">
                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                    <ContentTemplate>
                        Date of Joining
                        <asp:TextBox ID="TextBox1" class="form-control" AutoPostBack="true" runat="server"
                            OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                        <cc1:calendarextender id="CalendarExtender3" targetcontrolid="TextBox1" runat="server"
                            format="dd-MM-yyyy">
                </cc1:calendarextender>
                    </ContentTemplate>
                </asp:UpdatePanel>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator33" ControlToValidate="TextBox1"
                            runat="server" ErrorMessage="Required Doj" Style="color: #FF0000"></asp:RequiredFieldValidator>
            </div>--%>
                <div class="col-sm-3">
                    <asp:UpdatePanel ID="UpdatePanel51" runat="server">
                        <ContentTemplate>
                            Employee Name <span class="style1">*</span>
                        <%-- <asp:DropDownList ID="Employee" class="form-control" runat="server">
                            </asp:DropDownList>--%>
                           <asp:TextBox ID="Employee1" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:Label ID="lblStatus" runat="server" Style="color: #FF0000"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator34" ControlToValidate="Employee1" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <%--<div class="col-sm-3">
                Employee Name <span class="style1">*</span>
                <asp:TextBox ID="Employee" class="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Employee"
                    runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                <asp:Label ID="lblStatus" runat="server" Style="color: #FF0000"></asp:Label>
            </div>--%>
                <div class="col-sm-3">
                    <div class="col-sm-4 padbot" style="margin-right: 1px">
                        <img id="imgprvw" height="100px" width="100px" /><br />
                    </div>
                    <div class="col-sm-8 padtop">
                        <asp:FileUpload ID="Imgprev" runat="server" onchange="showimagepreview(this)" Width="214px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" ControlToValidate="Imgprev"
                            runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    Employee ID&nbsp; <span class="style1">*</span><asp:TextBox ID="EmployeeId" class="form-control"
                        runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="EmployeeId"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
               <%-- <div class="col-sm-4">
                    Device ID<span class="style1">*</span>
                    <asp:TextBox ID="DeviceId" class="form-control" runat="server" Placeholder="Enter only digits...!"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="DeviceId"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="DeviceId" FilterType="Numbers" />
                </div>--%>
                <div class="col-sm-4">
                    DOB<span class="style1">*</span>
                    <asp:TextBox ID="DOB" class="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator25" ControlToValidate="DOB"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="DOB" runat="server"></cc1:CalendarExtender>
                </div>
                 <div class="col-sm-4">
                    DOJ<span class="style1">*</span>
                    <asp:TextBox ID="DOJ" class="form-control" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="DOJ" runat="server"></cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="DOJ"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
               
                <div class="col-sm-4">
                    Gender<span class="style1">*</span>
                    <asp:RadioButtonList ID="Gender" runat="server" RepeatDirection="Horizontal" Width="250px">
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator26" ControlToValidate="Gender"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <div class="col-sm-4">
                    Company <span class="style1">*</span>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="Company" runat="server" class="form-control" AutoPostBack="True"
                                OnSelectedIndexChanged="Company_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Company" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="Company" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                 <div class="col-sm-4">
                    Department<span class="style1">*</span>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="Department" runat="server" class="form-control" AutoPostBack="True" OnTextChanged="Department_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="Department"
                                runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Department" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="row">
               
                <div class="col-sm-4">
                    Designation<span class="style1">*</span>
                    <asp:TextBox ID="Desigantion" class="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ControlToValidate="Desigantion"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <div class="col-sm-4">
                    Employee Type<span class="style1">*</span>
                <%--<asp:TextBox ID="EmployeeType" class="form-control" runat="server"></asp:TextBox>--%>
                    <asp:DropDownList ID="dropemptype" class="form-control" runat="server">
                        <asp:ListItem>Contract</asp:ListItem>
                        <asp:ListItem>Less then 1year</asp:ListItem>
                        <asp:ListItem>More then 1 year</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator28" ControlToValidate="dropemptype"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                 <div class="col-sm-4">
                    PF/NPF<span class="style1">*</span>
                    <asp:RadioButtonList ID="radiopf" runat="server" RepeatDirection="Horizontal" Width="250PX">
                        <asp:ListItem>PF</asp:ListItem>
                        <asp:ListItem>NPF</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="radiopf"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row" style="padding-bottom: 8px">
               
                <div class="col-sm-3">
                    Bank 
                      <asp:DropDownList ID="dropbank" runat="server" class="form-control"></asp:DropDownList>
                    <asp:Label ID="lblStatus1" runat="server" Style="color: #FF0000"></asp:Label>
                </div>
                <div class="col-md-1" style="padding-top: 18px">
                    <asp:Button ID="btnadd" runat="server" CssClass="btn btn-success" Text="+" OnClick="btnadd_Click" CausesValidation="false" />

                </div>
                <div class="col-sm-4">
                    Account Number
                      <asp:TextBox ID="txtaccount" runat="server" class="form-control"></asp:TextBox>
                </div>
                 <div class="col-sm-4">
                    IFSC Code:
                      <asp:TextBox ID="txtifsc" runat="server" class="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="padding-bottom: 8px">
               
                <div class="col-sm-4">
                    PF No
                      <asp:TextBox ID="txtpf" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-sm-4">
                    ESI No
                    <asp:TextBox ID="txtesi" runat="server" class="form-control"></asp:TextBox>
                </div>
                 <div class="col-sm-4">
                    PAN No
                    <asp:TextBox ID="txtpan" runat="server" class="form-control"></asp:TextBox>
                </div>

            </div>
            <div class="row" style="padding-bottom: 8px">
               
                <%--<div class="col-sm-4">
                    Assign Shift
                     <asp:DropDownList ID="dropshift" runat="server" class="form-control">
                        
                     </asp:DropDownList>
                    </div>
                  
                 <div class="col-sm-4">
                     Start Date
                        <asp:TextBox ID="txtstart" runat="server" class="form-control"></asp:TextBox>
                      <cc1:calendarextender id="CalendarExtender3" targetcontrolid="txtstart" runat="server">
                </cc1:calendarextender>
                     </div>--%>




                <%--<div class="col-sm-4">
                     End Date
                        <asp:TextBox ID="txtend" runat="server" class="form-control"></asp:TextBox>
                      <cc1:calendarextender id="CalendarExtender4" targetcontrolid="txtend" runat="server">
                </cc1:calendarextender>
                     </div> --%>
                <div class="col-sm-4">
                    Casual Leaves
                    <asp:TextBox ID="txtleaves" runat="server" class="form-control" placeholder="Enter digits only..."></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                        TargetControlID="txtleaves" />
                </div>
                <div class="col-sm-4">
                    Mobile Bill
                    <asp:TextBox ID="txtmobbill" runat="server" class="form-control" placeholder="Enter digits only..."></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                        TargetControlID="txtmobbill" />
                </div>

                 <div class="col-sm-4">
                    Company Email<span class="style1">*</span>
                <asp:TextBox ID="CompanyEmail" class="form-control" runat="server"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator29" ControlToValidate="CompanyEmail"
                            runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="CompanyEmail"
                        ErrorMessage="* Enter correct email address"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ForeColor="Red"></asp:RegularExpressionValidator>


                </div>


            </div>
            <div class="row" style="padding-bottom: 8px">
               
                <div class="col-sm-4">
                    Phone Number<span class="style1">*</span>
                    <asp:TextBox ID="CellNumber" class="form-control" runat="server" placeholder="Enter digits only..."></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator30" ControlToValidate="CellNumber"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                        TargetControlID="CellNumber" />

                </div>
                <div class="col-sm-4">
                    Address
                <asp:TextBox ID="Address" class="form-control" runat="server" TextMode="MultiLine"
                    Visible="false"></asp:TextBox>
                    <textarea id="autocomplete" class="form-control zin" rows="1" cols="10" type="text" placeholder="Address"
                        name="Name" onfocus="geolocate()"></textarea>

                </div>

                 <div class="col-sm-4">
                    Select<span class="style1">*</span>
                <asp:DropDownList ID="droptax" runat="server" class="form-control">
                </asp:DropDownList>
                     
                </div>
            </div>
            <div class="row" >
               
                <div class="col-sm-4">
                    Blood Group
                <asp:DropDownList class="form-control" ID="BloodGroup" runat="server">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem>O+ve</asp:ListItem>
                    <asp:ListItem>O-ve</asp:ListItem>
                    <asp:ListItem>A+ve</asp:ListItem>
                    <asp:ListItem>A-ve</asp:ListItem>
                    <asp:ListItem>B+ve</asp:ListItem>
                    <asp:ListItem>B-ve</asp:ListItem>
                    <asp:ListItem>AB+ve</asp:ListItem>
                    <asp:ListItem>AB-ve</asp:ListItem>
                </asp:DropDownList>
                </div>


                <div class="col-sm-4">
                    Category
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="HolidayList" runat="server" class="form-control" AutoPostBack="True">
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="HolidayList" />
                    </Triggers>
                </asp:UpdatePanel>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator32" ControlToValidate="HolidayList"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <asp:UpdatePanel ID="upd1" runat="server"><ContentTemplate>
               <div id="divanum" runat="server" visible="false">
                 <div class="col-sm-4">
                    
                        <asp:Label ID="Labeld" runat="server">  Salary per Annum </asp:Label><span class="style1">*</span> 
                            <asp:TextBox ID="txtsal" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtsal"
                                runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                                TargetControlID="txtsal" />
                        </div>

                   </div>
               </ContentTemplate></asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
               <div id="divhour" runat="server" visible="false">
                    <div class="col-sm-4">
                        <asp:Label ID="Label1" runat="server" >  Salary Per Hour </asp:Label><span class="style1">*</span> 
                            <asp:TextBox ID="txthrsal" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txthrsal"
                                runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers" TargetControlID="txthrsal" />
                        </div>
                   </div>
                    </ContentTemplate></asp:UpdatePanel>
                <div class="row" style="padding-bottom: 8px">
                    <div class="col-sm-12">
                       
                         <div class="col-sm-4">
                           Set App Password<span class="style1">*</span>
                            <asp:TextBox ID="txtpwd" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtpwd"
                                runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            </div>
                        <div class="col-sm-4">
                            Select Type<span class="style1">*</span>
                            <asp:DropDownList ID="dropmed" CssClass="form-control" runat="server">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="2">Medicare</asp:ListItem>
                                <asp:ListItem Value="1">Non-Medicare</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="dropmed"
                                runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            </div>
                        <div class="col-sm-4 btn1" style="padding-top: 16px">
                            <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Submit" OnClick="Button2_Click" CausesValidation="true" />
                        </div>              
                    </div>
                </div>

                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" PopupControlID="Panel1" CancelControlID="btncancel"></cc1:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px" Width="500px" Height="150px">
                    <h4 class="text-center">Add Bank</h4>
                    <hr />
                    <div class="col-md-6">
                        Bank Name:<asp:TextBox ID="txtbank" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </div>
                    <div class="col-md-3" style="padding-top: 18px">
                        <asp:Button ID="btninsert" runat="server" Text="Add" CssClass="btn btn-success btn-block" OnClick="Add_Click" CausesValidation="false" />
                    </div>
                    <div class="col-md-3" style="padding-top: 18px">
                        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="btn btn-danger btn-block" CausesValidation="false" />
                    </div>
                </asp:Panel>
            </div>
        </div>
    <script>
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
               { types: ['geocode'] });

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK6IjVZM_CESaUtFIM_JcdaGWJmOqjuC0&libraries=places&callback=initAutocomplete"
        async defer></script>
        </div>
</asp:Content>
