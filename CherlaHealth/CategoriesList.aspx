﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CategoriesList.aspx.cs" Inherits="CherlaHealth.CategoriesList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Category List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
              <div class="col-md-12">

                <div class="col-md-6">

                    <asp:TextBox
                        ID="TextBox1"
                        AutoPostBack="True" runat="server" class="form-control" OnTextChanged="TextBox1_TextChanged" placeholder="Search by Category Name........"></asp:TextBox>
                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="TextBox1"
                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1"
                        CompletionInterval="1" ServiceMethod="GetCompany" UseContextKey="True">
                    </cc1:AutoCompleteExtender>
                </div>
                <div class="col-md-6">
                    <asp:Button ID="Button1" runat="server" Text="Add Category" PostBackUrl="~/AddCategory.aspx" class="btn btn-warning" Style="margin-left: 25px"></asp:Button>
                </div>
            </div>
                  <div class="col-md-12" style="padding-top: 15px">

                <div class="table table-responsive" style="overflow: scroll; height: 420px">
                    <asp:GridView ID="GridView1" runat="server" class="table table-bordered table-striped table-condensed" AutoGenerateColumns="false" DataKeyNames="CategoryId"
                        OnRowDeleting="GridView1_RowDeleting">

                        <Columns>
                            <asp:BoundField DataField="CategoryName" HeaderText="Category" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="CategorySName" HeaderText="Short Name" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="OTFormula" HeaderText="OT Formula" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="MinOT" HeaderText="Min OT" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="GraceTimeForLateComing" HeaderText="Late Login Grace Time" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="GracetimeForEarlyGoing" HeaderText="Early Logout Grace Time" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="IsWeeklyOff1" HeaderText="Weekoff-1" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="WeeklyOff1Day" HeaderText="Day" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="IsWeeklyOff2" HeaderText="Weekoff-2" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="WeeklyOff2Day" HeaderText="Day" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="WeeklyOff2days" HeaderText="Week" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="IsCHalfForLessD" HeaderText="Half Day Salary Cut" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="CHalfForLessDMins" HeaderText="Time in Mins" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="IsCAbsentDisLessThan" HeaderText="Absent Cal" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="CAbsentDisLessThanMins" HeaderText="Time in Mins" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="IsMarkHalfDay_LateBy" HeaderText="Half Day If Late By" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="MarkHalfDay_LateByMins" HeaderText="Time in Mins" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="IsMarkHalfDay_EarlyGoing" HeaderText="Half Day If Early Going By " HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="MarkHalfDay_EarlyGoing" HeaderText="Time in Mins" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:CommandField CausesValidation="false" ShowDeleteButton="true" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Red" />
                            <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" class="glyphicon glyphicon-pencil" OnClick="lnkEdit_Click" runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:Label ID="lblmsg" runat="server" />
                </div>
            </div>
 <asp:Button ID="modelPopup" runat="server" Style="display: none" />

            <cc1:ModalPopupExtender
                ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup" PopupControlID="updatePanel"
                CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
            </cc1:ModalPopupExtender>
<asp:Panel ID="updatePanel" runat="server" BackColor="White" Height="600px" Width="800px" Style="display: none;">
     <div class="table table-responsive" style="height: 500px; overflow-y: scroll">
                    <div class="col-md-12">

                        <table class="table table-responsive table-striped">
                            <tr style="background-color: #DC7A01">
                                <td colspan="2" align="center">
                                    <h3 style="color: #fff">Edit Category</h3>
                                </td>
                            </tr>

                            <tr>
                                <td align="right" style="width: 45%"></td>
                                <td>
                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>

                                </td>
                            </tr>

                            <tr>
                                <td align="left " class="style3 tpadleft">Category
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstor_address" runat="server" class="form-control" />

                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Shortname:
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtcity" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">OT Formula
                                </td>
                                <td class="tpadright">
                                    <asp:DropDownList class="style3 tpadleft" ID="txtstate" runat="server" CssClass="form-control">
                                        <asp:ListItem>(Out Punch)-(Shift End Time)</asp:ListItem>
                                        <asp:ListItem>(Total Duration)+(Shift Hours)</asp:ListItem>
                                        <asp:ListItem>(Early Going)+(Late Coming)</asp:ListItem>
                                        <asp:ListItem>OT Not Applicable</asp:ListItem>
                                    </asp:DropDownList>

                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Min OT
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstate33" runat="server" class="form-control" />

                                </td>
                            </tr>
                            <tr>
                                <td align="left " class="style3 tpadleft">Grace Time for Late Login
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstor_address1" runat="server" class="form-control" />

                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Grace Time for Early Logout
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtcity1" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Weekly off 1
                                </td>
                                <td class="tpadright">
                                    <asp:DropDownList ID="txtstate1" class="form-control" runat="server">
                                        <asp:ListItem>True</asp:ListItem>
                                        <asp:ListItem>False</asp:ListItem>
                                    </asp:DropDownList>

                                </td>
                            </tr>
                            <tr>
                                <td align="left " class="style3 tpadleft">Weekly off 1 day
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstor_address2" runat="server" class="form-control" />

                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Weekly off 2
                                </td>
                                <td class="tpadright">
                                    <asp:DropDownList ID="txtcity2" class="form-control" runat="server">
                                        <asp:ListItem>True</asp:ListItem>
                                        <asp:ListItem>False</asp:ListItem>
                                    </asp:DropDownList>

                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Weekly off 2 Day
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstate2" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left " class="style3 tpadleft"></td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstor_address01" runat="server" class="form-control" />

                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Calculate half day based on Work duration</td>
                                <td class="tpadright">
                                    <asp:DropDownList ID="txtcity02" class="form-control" runat="server">
                                        <asp:ListItem>True</asp:ListItem>
                                        <asp:ListItem>False</asp:ListItem>
                                    </asp:DropDownList>

                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Calculate Half Day if Work Duration is less than
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstate03" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Calculate Absent based on Work duration
                                </td>
                                <td class="tpadright">
                                    <asp:DropDownList ID="txtstor_address11" class="form-control" runat="server">
                                        <asp:ListItem>True</asp:ListItem>
                                        <asp:ListItem>False</asp:ListItem>
                                    </asp:DropDownList>


                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Calculate Absent if Work Duration is less than
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstor_address111" runat="server" class="form-control" />

                                </td>
                            </tr>
                            <tr>
                                <td align="left " class="style3 tpadleft">Consider Half day based on Late Login

                                </td>
                                <td class="tpadright">
                                    <asp:DropDownList ID="txtcity11" class="form-control" runat="server">
                                        <asp:ListItem>True</asp:ListItem>
                                        <asp:ListItem>False</asp:ListItem>
                                    </asp:DropDownList>



                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Mark Half Day If Late By
                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtstate11" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left " class="style3 tpadleft">IS Mark Half Day If Early Going By
                                </td>
                                <td class="tpadright">
                                    <asp:DropDownList ID="txtstor_address21" class="form-control" runat="server">
                                        <asp:ListItem>True</asp:ListItem>
                                        <asp:ListItem>False</asp:ListItem>
                                    </asp:DropDownList>


                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Consider Half day based on Early Logout

                                </td>
                                <td class="tpadright">
                                    <asp:TextBox ID="txtcity21" runat="server" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tpadleft" style="padding: 10px 0px 10px 15px"></td>
                                <td style="padding: 10px 0px 10px 0px"></td>
                            </tr>
                        </table>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update Data" OnClick="btnModity_Click" class="btn btn-success" />

                    </div>
                    <div class="col-md-6">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                    </div>
</div>
                
    </asp:Panel>
            </div>
        </div>
</asp:Content>
