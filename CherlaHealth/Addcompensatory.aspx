﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Addcompensatory.aspx.cs" Inherits="CherlaHealth.Addcompensatory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Add Compensatory Off</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">                   
                    Employee Name <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
            </div>
             <div class="col-md-3">
                <div class="form-group"> 
                    Worked Date <asp:TextBox ID="txtdate" CssClass="form-control" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtdate" runat="server" />
                    </div>
                 </div>
            <div class="col-md-3">
                <div class="form-group"> 
                    Description<asp:TextBox ID="txtdescription" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
            <div class="col-md-3" style="padding-top:18px">
                <div class="form-group"> 
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>

</asp:Content>
