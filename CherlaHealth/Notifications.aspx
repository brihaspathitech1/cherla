﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Notifications.aspx.cs" Inherits="CherlaHealth.Notifications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .marqueebg {
            height: 240px;
            box-shadow: 2px 3px 8px 2px;
            border-radius: 6px;
            background: #fbfbfb;
        }

        .hr1 {
            border-top: 3px solid #ff6b00;
            margin-bottom: 8px;
            margin-top: -5px;
            width: 173px;
        }

        .hr2 {
            border-top: 3px solid #289007;
            margin-bottom: 8px;
            margin-top: -5px;
            width: 173px;
        }


        .app-icon {
            padding: 35px;
            display: inline-block;
            margin: auto;
            text-align: center;
            border-radius: 16px;
            cursor: pointer;
            box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.15);
        }

        .app {
            padding-top: 85px;
        }

        .app-icon .fa {
            font-size: 25px;
            color: #fff;
        }

        .fa-calculator:before {
            content: "\f1ec";
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px; color: green; font-weight: bold;">Notifications</h2>
            <hr />

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                         <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-12">
                        <%-- <div class="col-lg-4 ds" style="padding-top: 0px !important">--%>
                        <!--COMPLETED ACTIONS DONUTS CHART-->
                        <h3 style="line-height: 44px; height: 40px; background: #09c8f3">MCI Expire Date Notifications</h3>
                        <marquee direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();"
                            scrollamount="3">
                      <!-- First Action -->
                      <div class="">
                      <asp:DataList ID="DataList1" runat="server" Width="50%" class="table" Font-Bold="true" ForeColor="#01712e" >
                                        <ItemTemplate>
                                        <label><span class="badge bg-theme"><i class="fa fa-clock-o" ></i></span></label>
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("EmployeName") %>'></asp:Label>'s MCI certifaicate expired on
                                        <asp:Label ID="Label34" runat="server" Text='<%# Bind("MCI_Date") %>'></asp:Label> <%--on
                                        <asp:Label ID="Label1" runat="server"  Text='<%# Bind("date") %>'></asp:Label>--%>
                                        
                                            </ItemTemplate>
                                           </asp:DataList>
                      
                      </marquee>
                                            </div>
                                        </ContentTemplate></asp:UpdatePanel>
                    
 </div>
                    <%--</div>--%>
                    <%--  </div>--%>
                    <%--</div>--%>
                    <div class="col-md-6">
                         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-12">
                        <%--<div class="col-lg-4 ds" style="padding-top: 0px !important">--%>
                        <!--COMPLETED ACTIONS DONUTS CHART-->
                        <h3 style="line-height: 44px; height: 40px; background: #09c8f3">SCI Expire Date Notifications</h3>
                        <marquee direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();"
                            scrollamount="3">
                      <!-- First Action -->
                      <div class="">
                      <asp:DataList ID="DataList2" runat="server" Width="50%" class="table" Font-Bold="true" ForeColor="#01712e" >
                                        <ItemTemplate>
                                        <label><span class="badge bg-theme"><i class="fa fa-clock-o" ></i></span></label>
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("EmployeName") %>'></asp:Label>'s SCI certifaicate expired on
                                        <asp:Label ID="Label34" runat="server" Text='<%# Bind("SCI_Date") %>'></asp:Label> <%--on
                                        <asp:Label ID="Label1" runat="server"  Text='<%# Bind("date") %>'></asp:Label>--%>
                                        
                                            </ItemTemplate>
                                           </asp:DataList>
                      
                      </marquee>
                                            </div>  
                    </ContentTemplate>
                             </asp:UpdatePanel>
 <%--</div>--%>
            </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
