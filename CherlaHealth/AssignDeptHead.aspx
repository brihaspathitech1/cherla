﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AssignDeptHead.aspx.cs" Inherits="CherlaHealth.AssignDeptHead" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Assign Department Head</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                        Select Department <asp:DropDownList ID="dropdept" CssClass="form-control" OnSelectedIndexChanged="Department_Change" AutoPostBack="true" runat="server"></asp:DropDownList> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="dropdept" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                         </ContentTemplate></asp:UpdatePanel>
                            </div>
                </div> 
             <div class="col-md-3">
                    <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                        Select Employee <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server"></asp:DropDownList>
           <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropemp" ForeColor="Red"></asp:RequiredFieldValidator>
</ContentTemplate></asp:UpdatePanel>
                         </div>
                 </div>
            <div class="col-md-3" style="padding-top:18px">
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnSubmit" />
                        </div>
                </div>

            <div class="col-md-12">
                <asp:GridView ID="GridView1" AutoGenerateColumns="false" GridLines="None" CssClass="table table-responsive" runat="server">
                    <Columns>
                        <asp:BoundField DataField="Department" HeaderText="Department" />
                        <asp:BoundField DataField="EmployeName" HeaderText="Head of the department" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnedit" CssClass="btn btn-success btn-xs" runat="server" CommandArgument='<%# Eval("Id") %>' OnClick="Edit_Click" Text="Edit" CausesValidation="false" />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btndelete" CssClass="btn btn-danger btn-xs" runat="server" Text="Delete" CommandArgument='<%# Eval("Id") %>' OnClick="Delete" CausesValidation="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            
            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" PopupControlID="Panel1" CancelControlID="btnclose"></ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" BackColor="White" Height="250px" Width="800px" Style="display: none;">
                <h3 style="text-align:center">Update Details</h3>
                <hr />
                <div class="col-md-6">
                    <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                        Select Department <asp:DropDownList ID="ddldept" CssClass="form-control" OnSelectedIndexChanged="Department" AutoPostBack="true" runat="server"></asp:DropDownList>
                    </ContentTemplate></asp:UpdatePanel>
                             </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                        Select Employee <asp:DropDownList ID="ddlemp" CssClass="form-control" runat="server"></asp:DropDownList>
                            </ContentTemplate></asp:UpdatePanel>
                    </div>
                </div>
                 <div class="col-md-3" style="padding-top:18px">
                    <div class="form-group">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="Update" CausesValidation="false" />
                        </div>
                     </div>
                 <div class="col-md-3" style="padding-top:18px">
                    <div class="form-group">
                          <asp:Button ID="btnclose" CssClass="btn btn-danger" runat="server" Text="Cancel" CausesValidation="false" />
                        </div>
                     </div>
            </asp:Panel>
            <asp:Label ID="lblid" Visible="false" runat="server" Text="Label"></asp:Label>
            </div>

         </div>
</asp:Content>
