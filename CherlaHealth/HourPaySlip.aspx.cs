﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class HourPaySlip : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindValues();
            }
        }

        protected void BindValues()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from hourpayslip where id='" + Session["payid"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                month.Text = dr["month"].ToString();
                month1.Text= dr["month"].ToString();
                year.Text = dr["year"].ToString();
                year1.Text = dr["year"].ToString();
                lblempid.Text = dr["EmpNo"].ToString();
                lblemployee.Text = dr["Name"].ToString();
                lbldays.Text = (double.Parse(dr["present"].ToString()) + double.Parse(dr["Nop"].ToString())).ToString();
                lblpay.Text= (double.Parse(dr["present"].ToString()) + double.Parse(dr["Nop"].ToString())).ToString();
                lblacno.Text = dr["Accountno"].ToString();
                lbldesignation.Text = dr["designation"].ToString();
                DateTime date=DateTime.Parse(dr["DOJ"].ToString());
                lbldoj.Text = date.ToString("yyyy-MM-dd");
                lblpfno.Text = dr["PFno"].ToString();
                lblsal.Text = dr["hrsalary"].ToString();
                lblhrs.Text = dr["hours"].ToString();
                lblincentives.Text = dr["incentives"].ToString();
                lbltax.Text = dr["Taxded"].ToString();
                lbldedtot.Text = dr["Taxded"].ToString();
                lbltotal.Text = dr["total"].ToString();
                lblnet.Text = dr["gross"].ToString();
                lblwords.Text = dr["words"].ToString();
            }
        }
    }
}