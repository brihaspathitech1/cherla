﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class LeavesList : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("select e.EmployeName,l.AppliedDate,l.CL,l.SL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId,l.leaveType,l.Status from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnManagerDesk'",conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                gvLeaveslist.DataSource = ds;
                gvLeaveslist.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvLeaveslist.DataSource = ds;
                gvLeaveslist.DataBind();
                int columncount = gvLeaveslist.Rows[0].Cells.Count;
                gvLeaveslist.Rows[0].Cells.Clear();
                gvLeaveslist.Rows[0].Cells.Add(new TableCell());
                gvLeaveslist.Rows[0].Cells[0].ColumnSpan = columncount;
                gvLeaveslist.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}