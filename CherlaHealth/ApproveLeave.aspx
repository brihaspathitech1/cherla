﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ApproveLeave.aspx.cs" Inherits="CherlaHealth.ApproveLeave" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


 <style>
        .head4 {
            color: #0089ff !important;
            font-weight: bold !important;
        }

        .nav-tabs {
            border-bottom: 2px solid #DDD !important;
        }

            .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                border-width: 0 !important;
            }

            .nav-tabs > li > a {
                border: none !important;
                color: #666 !important;
                padding: 11px !important;
            }

                .nav-tabs > li.active > a, .nav-tabs > li > a:hover {
                    border: none !important;
                    color: #4285F4 !important;
                    background: transparent !important;
                    padding: 11px !important;
                }

                .nav-tabs > li > a::after {
                    content: "";
                    background: #4285F4 !important;
                    height: 2px !important;
                    position: absolute !important;
                    width: 100% !important;
                    left: 0px !important;
                    bottom: -1px !important;
                    transition: all 250ms ease 0s !important;
                    transform: scale(0) !important;
                }

            .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after {
                transform: scale(1) !important;
            }

        .tab-nav > li > a::after {
            background: #21527d none repeat scroll 0% 0% !important;
            color: #fff !important;
        }

        .tab-pane {
            padding: 15px 0 !important;
        }

        .tab-content {
            padding: 20px !important;
        }

        .card {
            background: #FFF none repeat scroll 0% 0% !important;
            box-shadow: none !important;
            margin-bottom: 30px !important;
        }

        .zin{z-index:9999 !important}
    </style>

    <script type="text/javascript">
       function onCalendarShown() {

        var cal = $find("calendar1");
        //Setting the default mode to month
        cal._switchMode("months", true);

        //Iterate every month Item and attach click event to it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }
    }

    function onCalendarHidden() {
        var cal = $find("calendar1");
        //Iterate every month Item and remove click event from it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }

    }
    function call(eventElement) {
        var target = eventElement.target;
        switch (target.mode) {
            case "month":
                var cal = $find("calendar1");
                cal._visibleDate = target.date;
                cal.set_selectedDate(target.date);
                cal._switchMonth(target.date);
                cal._blur.post(true);
                cal.raiseDateSelectionChanged();
                break;
        }
    }
  
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="myscript" runat="server"></asp:ScriptManager>
 <section id="main-content">
       <section class="wrapper site-min-height">
          	<!---<h3><i class="fa fa-angle-right"></i> Blank Page</h3>--->
           <div class="row" runat="server" visible="false">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Monthly</a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Yearly</a></li>

                                    
                                </ul>

                                
                            </div>


                        </div>
            
          	<div class="row mt">
          		<div class="col-lg-12 panel">
 <asp:Panel ID="pnlApprove" runat="server" Visible="true">
        <div class="form-panel col-sm-12">
            <h4>
                List of Pending Leaves:-
                <asp:Label ID="lblListDetail" runat="server" Text="" ForeColor="#DC7A01"></asp:Label>
            </h4>
            <div style="height: 500px; overflow-y: scroll; overflow-x: scroll">
                <asp:GridView ID="gvlistofLeaves" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="LeaveID">
                    <Columns>
                        <asp:BoundField DataField="EmployeedID" HeaderText="Employee Id" />
                        <asp:BoundField DataField="EmployeName" HeaderText="Employee" />
                        <asp:BoundField DataField="LeaveId" HeaderText="LeaveId" Visible="False" />
                        <asp:BoundField DataField="AppliedDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Applied Date" />
                        <asp:BoundField DataField="CL" HeaderText="Leave Type" />
                        <asp:BoundField DataField="SL" HeaderText="SL" />
                        <asp:BoundField DataField="Pl" HeaderText="PL/EL" />
                        <asp:BoundField DataField="COF" HeaderText="Compensatory Off" />
                        <asp:BoundField DataField="LOP" HeaderText="LWP" />
                        <asp:BoundField DataField="LeaveType" HeaderText="Leave Type" />
                        <asp:BoundField DataField="FromDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="From Date" />
                        <asp:BoundField DataField="ToDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="To Date" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="BalanceLeaves" HeaderText="Balance Leaves" />
                        <asp:BoundField DataField="ReasontoApply" HeaderText="Reason to Apply" />
                       <%-- <asp:BoundField DataField="Empreplace" HeaderText="Replace Employee" />--%>
                       <%-- <asp:BoundField DataField="Status" HeaderText="Status" />--%>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <div class="btn-group" style="float: right">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                        Actions <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu " role="menu">
                                        <li>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" CommandArgument='<%# Eval("LeaveId") %>'
                                                OnClick="Approve_Click">Approve</asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandArgument='<%# Eval("LeaveId") %>'
                                                OnClick="Reject_Click">Reject</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                                <%--Text="Approve" ForeColor="White" BackColor="#DC7A01" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
                      <div class="tab-content">

                            <!-----Customer Panel Starts----->
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>

                                    <div class="col-md-3" style="z-index:999" runat="server" visible="false">
                                 Month:<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" OnTextChanged="month_changed" AutoPostBack="true" style="z-index:9999"></asp:TextBox>
             <cc1:CalendarExtender ID="CalendarExtender1" runat="server"  TargetControlID="TextBox1" OnClientHidden="onCalendarHidden"  OnClientShown="onCalendarShown" BehaviorID="calendar1" 
    Enabled="True"  />
                                        </div>
                                        <div class="clearfix"></div>
                                        </ContentTemplate>
                                </asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
    <asp:Panel ID="pnlReadOnly" runat="server" Visible="false">
      
           
              

            <div style="height: 700px;" class="col-md-12">
                 <h4>
                List of Leaves:-
                <asp:Label ID="lblReadOnlyListOfLeaves" runat="server" Text="" ForeColor="#DC7A01"></asp:Label>
            </h4>
                <asp:GridView ID="gvListofROLeaves" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="True">
                </asp:GridView>
            </div>
      
    </asp:Panel>
                                        </ContentTemplate>
    </asp:UpdatePanel>
                                        
                                </div>
                           <div role="tabpanel" class="tab-pane" id="profile">
                              <%--<asp:TextBox ID="TextBox2" runat="server" placeholder="Type year only.." OnTextChanged="year_changed" AutoPostBack="true"></asp:TextBox>--%>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                         <div class="col-md-3">
                               Select Year:<asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="year1_changed" CssClass="form-control" AutoPostBack="true">
                           <asp:ListItem></asp:ListItem>
                           <asp:ListItem>2016</asp:ListItem>
                           <asp:ListItem>2017</asp:ListItem>
                           <asp:ListItem>2018</asp:ListItem>
                           <asp:ListItem>2019</asp:ListItem>
                           <asp:ListItem>2020</asp:ListItem>
                           <asp:ListItem>2021</asp:ListItem>
                           <asp:ListItem>2022</asp:ListItem>
                           <asp:ListItem>2023</asp:ListItem>
                           <asp:ListItem>2024</asp:ListItem>
                           <asp:ListItem>2025</asp:ListItem>
                           <asp:ListItem>2026</asp:ListItem>
                           <asp:ListItem>2027</asp:ListItem>
                           <asp:ListItem>2028</asp:ListItem>
                           <asp:ListItem>2029</asp:ListItem>
                           <asp:ListItem>2030</asp:ListItem>
             
                       </asp:DropDownList>
                                             </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                               <asp:Panel ID="Panel1" runat="server" Visible="false">
        <div class="form-panel col-sm-12">
            <h4>
                List of Leaves:-
                <asp:Label ID="Label1" runat="server" Text="" ForeColor="#DC7A01"></asp:Label>
            </h4>
                  
            <div style="height: 700px;">
                <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="True">
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
                                        </ContentTemplate>
                                   </asp:UpdatePanel>
                               </div>
                          </div>
    </div>
    </div>
    </section>
    </section>
    
</asp:Content>
