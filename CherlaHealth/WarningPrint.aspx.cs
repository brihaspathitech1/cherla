﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Net;
using System.Net.Mail;
using System.Text;


namespace CherlaHealth
{
    public partial class WarningPrint : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lbldate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
                lblreason.Text = Session["reason"].ToString();
                lblreason1.Text = Session["reason"].ToString();
                lblreason2.Text = Session["reason"].ToString();
                lblreason3.Text = Session["reason"].ToString();
                lblreason4.Text = Session["reason"].ToString();
                lblsug.Text = Session["sugg"].ToString();
                lbldate2.Text = Session["date"].ToString();
                lblemail.Text = Session["CompanyEmail"].ToString();
                salutation.Text = Session["salute"].ToString();
                Label1.Text = Session["salute"].ToString();
                BindValues();
            }
        }

        protected void BindValues()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from Employees where EmployeeId='" + Session["employid"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                lblname.Text = dr["EmployeName"].ToString();
                lblname2.Text= dr["EmployeName"].ToString();
                lbldesig.Text = dr["Desigantion"].ToString();
                //lblemail.Text = dr["CompanyEmail"].ToString();

            }
            conn.Close();
            SqlCommand cmd1 = new SqlCommand("Select * from Employees where EmployeeId='20180515'", conn);
            conn.Open();
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if(dr1.Read())
            {
                lblhr.Text= dr1["EmployeName"].ToString();
                lblhrdesig.Text= dr1["Desigantion"].ToString();
                lblmail.Text = dr1["CompanyEmail"].ToString();
                lblphone.Text = dr1["CellNo"].ToString();
            }
            conn.Close();
        }
        protected void pdf_Click(object sender,EventArgs e)
        {
            Image1.Visible = false;
           // head1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Warning Letter.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            //string imagePath = Server.MapPath("sign board.jpg") + "";

            //iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            //image.Alignment = Element.ALIGN_TOP;

            //image.SetAbsolutePosition(40, 700);

            //image.ScaleToFit(500f, 400f);
            //image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            //pdfDoc.Add(image);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        protected void btnemail_Click(object sender, EventArgs e)
        {
            Smail();
        }
        protected void Smail()
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();
                    {
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");


                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Date: " + System.DateTime.Now.ToString("yyyy-MM-dd") + "</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>To</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + lblname.Text + "</p>");

                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>"+ lbldesig.Text+"</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'> </p>");
                        sb.Append("<p><br></p>");


                       
                        sb.Append("<p><br></p>");


                        sb.Append("<p><br></p>");


                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Cherla Health Private Ltd </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Sub. – Warning Letter-Reminder-1</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Dear "+lblname2.Text+"</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>It has been brought to our notice that you have "+ lblreason.Text+ " , resulting in gross negligence of duties, in your capacity and designation. As I had communicated to you in the earlier correspondence on "+lbldate2.Text+", "+lblreason1.Text + " is mandatory. However,"+lblreason2.Text + " , inspite of more than enough time provided to you to pursue the same.</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>"+lblreason3.Text+ "is a misconduct for which you are making yourself liable for necessary action.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>You are hereby warned to refrain from such activities; failure to do so shall invoke appropriate action</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>You are further advised to submit a written explanation on  "+lblreason4.Text + " as soon as you receive this letter . You are being notified to respond to this letter by proper channel with full and proper explanation for this discrepancy. This is a formal notification advising you "+lblsug.Text + "</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Kindly treat this as very urgent.</ p >");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>For Cherla Health Care Pvt.Ltd</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>"+ lblhr .Text+"</p>");

                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + lblhrdesig.Text + "</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + lblmail.Text + "</p>");

                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>HR Department</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Cherla Health (P) Ltd</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");

                        sb.Append("<p><br></p>");

                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");


                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            string path = Server.MapPath("Files");
                            string filename = path + "/" + "Warning Letter" + ".Pdf";

                            PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();

                            string imagePath = Server.MapPath("sign.jpeg") + "";
                            for (int i = 0; i < 1; i++)
                            {

                                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                                image.Alignment = Element.HEADER;

                                image.SetAbsolutePosition(30, 650);

                                image.ScaleToFit(550f, 550f);
                                image.Alignment = iTextSharp.text.Image.HEADER | iTextSharp.text.Image.HEADER;


                                pdfDoc.Add(image);
                                htmlparser.Parse(sr);

                                PdfContentByte content = writer.DirectContent;
                                Rectangle rectangle = new Rectangle(pdfDoc.PageSize);
                                rectangle.Left += pdfDoc.LeftMargin;
                                rectangle.Right -= pdfDoc.RightMargin;
                                rectangle.Top -= pdfDoc.TopMargin;
                                rectangle.Bottom += pdfDoc.BottomMargin;
                                content.SetColorStroke(BaseColor.BLACK);
                                //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                                content.Stroke();
                            }
                            pdfDoc.Close();




                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();

                            MailMessage mm = new MailMessage();
                            mm.From = new MailAddress("hr.cherlahealth@gmail.com", "Warning Letter");
                            mm.To.Add(lblemail.Text);
                            //.CC.Add("md@brihaspathi.com");
                            //mm.CC.Add("hr@brihaspathi.com");
                            // mm.CC.Add("hra@brihaspathi.com");

                            mm.IsBodyHtml = true;
                            mm.Subject = "Warning Letter";
                            mm.Body = "Warning Letter<br /><br />Regards<br/><br/>HR<br/><br /><b>Thank you.</b>";
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Warning Letter.pdf"));
                            mm.Priority = MailPriority.High;
                            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                            //smtp.UseDefaultCredentials = true;
                            smtp.Credentials = new System.Net.NetworkCredential("hr.cherlahealth@gmail.com", "HR@cherla");
                            smtp.EnableSsl = true;
                            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.Send(mm);
                        }
                    }
                }
            }
        }
    }
}