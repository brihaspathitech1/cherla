﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pharmacyprint.aspx.cs" Inherits="CherlaHealth.Pharmacyprint" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Pharmacist Bond</title>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
                
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }




        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            
            #Button1 {
                display: none;
             
            }
          

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint {
                display: none;
            }
            input#btnpdf {
                display: none;
            }
            input#btnemail {
                display: none;
            }
           
            a {
                display: none;
            }
            
  
        }




        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
    <style>
    .page21 {
            width:100%;background:#d3d3d357;text-align:justify;padding:150px 50px 0px 70px;
        }
        tr, td {
            /*border:1px solid black;
            border-collapse:collapse;
            padding:15px;*/
      
       }
        .tab-text { margin-left:40px;font-weight:bold;
        }
        ol {
            margin-left:50px;
        }
   </style> 
    <style>
        .a{
            text-align:center;
        }
    </style>
    <style>
        .d{
            text-align:right;
        }
    </style>
</head>
<body style="color:black;">
     <form id="form1" runat="server">
    <asp:Panel ID="Panel1" runat="server">
    <div class="page21">     
   
        <div style="text-align:center;padding-top:25px ;">

             <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                <a href="Pharmacy.aspx" id="btnPrint" type="button"  onclick="goBack()"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>

            <asp:Button ID="btnpdf"  CssClass="btn btn-success" runat="server" Text="PDF" OnClick="btnpdf_Click" />
            
            <%--<asp:Button ID="btngmail" CssClass="btn btn-success" runat="server" Text="Gmail" OnClick="btngmail_Click" />--%>
        </div>
     <br /><br />
              <table width="100%">
                <tr>
                    <td>
                       <%-- <img id="img1" alt="/" src="sign board.jpg" width="1250px" height="120px" />--%>
                     <%--    <img id="img1" runat="server" src="~/sign board.png" width="1250px" height="120px" />--%>
                      <%--  <asp:Image ID="Image1" ImageUrl="~/sign board.jpg" Width="1250px" Height="120px" runat="server" />
                   --%> </td>
                </tr>
            </table>
        <br /><br />
        
       <div class="a"> <label><u>A G R E E M E N T</u></label></div>
        <br />
        <p>The Articles of Agreement made and executed this on&nbsp;&nbsp; <asp:Label ID="lbldate" runat="server" Text=""></asp:Label>,</p>
        <br />
       <div class="a"><label>B E T W E E N</label></div>
        <p><label>Cherla Health (P) Ltd</label> a company registered under The Companies Act, and having its Corporate Office at 101, Optimus Prime, Gachibowli, Hyderabad - 500080, Telangana, India. hereinafter called or referred to as the <label>`Company'</label> which expression shall mean and include Cherla Health (P). Ltd, and all its subsidiaries as constituted today or as it may stand constituted from time to time, during the currency of this agreement and/or the period of its operation </p>
        <div class="d"><p><label>……… of the First Part</label> </p></div>
        
        <div class="a"><label>AND</label></div>
        <p>Mr/Ms.&nbsp; <asp:Label ID="lblname" runat="server"></asp:Label>, S/o or D/o Mr/Ms.&nbsp;
            <asp:Label ID="lblname2" runat="server"></asp:Label>
            Address: #D.No&nbsp; <asp:Label ID="lbladdress" runat="server"></asp:Label> &nbsp;Aged&nbsp;&nbsp; <asp:label ID="lblage" runat="server"></asp:label> &nbsp;
            <asp:Label ID="lblgender" runat="server"></asp:Label>
            , Occupation
            <asp:Label ID="lbloccup" runat="server"></asp:Label>
            &nbsp; hereinafter called and referred to as Mr/Ms. <asp:Label ID="lblname3" runat="server"></asp:Label> Name of Employee or as `The Employee' as the context may require or permit                                                                                                            </p>
        <div class="d"><p><label>……… of the Second Part</label></p></div>
        <p>AND WHERE AS Mr/Ms.&nbsp;
            <asp:Label ID="lblname4" runat="server"></asp:Label>
            &nbsp; has been working as Trainee-&nbsp;
            <asp:Label ID="lbloccup2" runat="server"></asp:Label>
            &nbsp; in the Company since&nbsp; <asp:Label ID="lbldate4" runat="server"></asp:Label></p>
       
        <p>AND WHEREAS the Employer felt it necessary to enter into a service bond of <asp:Label ID="yr" runat="server"></asp:Label> &nbsp; Years (approx or till the successful completion of the Probation period whichever is later) with effect from the&nbsp; <asp:Label ID="date" runat="server"></asp:Label>&nbsp; with the Employee in view of the cost incurred by the Employer in respect of process of selection, training, induction etc. </p>
       
        <p>AND WHEREAS the Employee has also agreed and undertaken to enter into a service-bond for a period of&nbsp; <asp:Label ID="lblyr" runat="server"></asp:Label> &nbsp; Years (Approx) with the Company from&nbsp; <asp:Label ID="date2" runat="server"></asp:Label> &nbsp; on the terms and conditions as stated and expressed hereunder: -</p>
        
        <p>NOW THEREFORE THESE PRESENT WITNESSES AND IT IS HEREBY MUTUALLY AGREED TO BY AND BETWEEN THE PARTIES HERETO AS UNDER: -</p>
        <p>1. That the Employee agrees to serve with and be employed in this Company, or concern or associates to which the Employee&#39;s services may be transferred by this Company for a period of&nbsp; Years (Approx) from&nbsp; <asp:Label ID="lbdate5" runat="server"></asp:Label> &nbsp; to <asp:Label ID="lbda" runat="server"></asp:Label> &nbsp; on the terms and subject to the conditions hereinafter contained.</p>
        
        <p>2. The Company shall pay following annual salary initially :&nbsp; <asp:label ID="Salary" runat="server"></asp:label> &nbsp; (CTC Annual)</p>
        <p>Give salary brake up
         <p>The above amount includes the retention cost of employee and considering retention cost the above salary is fixed jointly by both the parties.  If the employee completes training / probation period successfully as decided by the company, then he will be absorbed in a suitable grade as per Organisation’s rule.</p>
         <p>3.	If any time, during the Training / Probation Period, the employee does not show the expected performance or remains absent without prior permission or misbehaves, his service can be terminated by giving One Day's Notice or by making payment of one day in lieu of notice</p>
        <p>4.	That the employee shall be entitled to all the benefits and/or amenities as may be available to the Employees of class employed in the Company from time to time during the period of his services with this Company or with the Company or firm or concern or associates to which his services may be transferred from time to time.</p>
        <p>5.	That during the period of his employment, the Employee shall be bound to observe and abide by all terms and conditions and stipulations hereinafter contained as also by such other rules and regulations as may be framed by the Company from time to time to be observed by or to be applicable to the Employees of his class.</p>
        <p>6.	That during the period of his employment the Employee shall be punctual in attendance and diligent in his work and the Employee shall devote all his working time and the best of his abilities, exclusively for the benefit of the Company. The Employee further agrees to obey all orders of and carry out all duties entrusted to him from time by his Senior Officers.</p>
        <p>7.	That it shall be competent to the Company to transfer the services of the Employee to any other Company or Firm or concern to which the Management of this Company deem it necessary or fit to transfer and in the event of such other Company or Firm or Concern and shall be entitled to the continuity of the services under these presents and shall be entitled to all the amenities or benefits which may be in force in such Company, Company or concern to which his services may be transferred. The Company hereby stipulates that in the event of the transfer of the Employee to any other Company or Firm or concern as aforesaid the emoluments to which he is entitled by virtue of these presents shall not in any way or manner be reduced or allowed to suffer. All clauses herein shall apply `Mutatis Mutandis' and shall continue to remain binding on the Employee even in the event of his transfer to other Company or Firm or Concern, as if his present agreement was entered into such Company or Firm or Concern.</p>
        <p>8.	That the Employee hereby agrees and undertakes to safeguard and hold in trust all notes, notebooks, memorandums, papers, drawings, sketches, diagrams, formulas, designs, books, letters, lists, CDs, DVDs, Floppies including those of or pertaining to raw materials, finished products, goods-in-process, names of suppliers, names of purchasers, or dealers as also price lists of the Company's product or of things in which Company may be dealing in, or may be contemplating to deal in, and all other papers and documents of whatsoever nature and kind that may have come in possession of the Employee while in the Employment of the Company and not to handover the same to any unauthorized person or suffer or permit the same to be handed over to any unauthorized person and the Employee hereby undertakes to handover all such papers or things above said to Senior Officers whenever called upon to do so and the Employee further agrees not to make copies of or to take extracts from any papers, documents etc., belonging to Company for any purpose other than those of the employment in the Company and further agrees not to carry any such paper, document or copies of extracts outside the premises of the Company and the Employee further agrees to deliver up all such papers, documents etc., in his possession to his Senior Officers whenever called upon to do so and it shall not be competent for the Employee in such an eventuality to withhold any papers or documents of whatsoever nature on the ground that they relate to his employment directly or indirectly.</p>
        <p>9.	That in the event of the Employee making any discovery or invention relating to the Manufacture, developments or processing of any product that may have been undertaken to be manufactured by the Company or to be experimented on, the same be the property of the Company, and in the event of the Company deciding to apply for any patent or registered trade mark in connection therewith, the Employee shall join the Company in all applications to the authorities concerned for obtaining and getting vested such patent rights and/or trademark rights in the same to and in favor of the Company exclusively.</p>
        <p>10. That the Employees hereby expressly agrees and undertakes that he shall not at any time during the period of his employment and for a period of&nbsp; <asp:Label ID="yrs" runat="server"></asp:Label> &nbsp; Years after the termination of the Employment, for any reason whatsoever undertake or carry on either long or in partnership or in collaboration, nor to be employed directly or indirectly in any capacity whatsoever in the Union of India in any manufacture which is of the same kind and nature as to business and manufacturing carries on by this Company or any of its associate companies or firms or concerns to which his services may be transferred, or in the manufacture of any products, articles or things for which this Company or such Other Company or Firm or Concern to which his services may be transferred or in the manufacture of any products, articles or things for which this Company or such other Company or Firm or Concern may have experimented in and/ or have prepared designs for the 
            manufacture of such products or have made preparations for the Manufacture of such products irrespective of whether such products actually been manufactured or not and during the said period shall not lend his assistance for such manufacture etc., in any form or manner whatsoever whether as Consultant or Advisor or Actual manufacture or as employee or in any other capacity or manner whatsoever.</p>
        <p>11.	That the Employee has agreed and hereby agrees and undertakes that the Employee shall not at     any time either during the course of his employment with this Company, with any of its associate Companies or concerns to which his services may be transferred or AT ANY TIME THEREAFTER divulge, displace or make known or suffer to be made known directly or indirectly any of the secrets pertaining to the designs, formulas, processes, specifications or manufacturing details or material requirements, material used or any of instruments, equipment, products or articles or things which this Company or any of its associate companies of Firms or concern may have undertaken to experiment in or to manufacture fabricate, design or process and whether the same is actually manufactured, designed, processed, fabricated or experimented in or not as also not divulge, disclose or communicate any information regarding the formulas, specifications, technical or patent information or `Know How' or the use of any processes  materials, instruments, tools or other requirements of the manufacture of the various products and other things manufactured, fabricated or processed by the Company or any of the other Companies or concerns to which the services of the Employee may be transferred : nor any of the secrets or knowledge or information regarding the plant facilities, machinery, equipment, Organization, production lines or production flow as also the method and procedure of production or of the dealings of the firm, or of the others above referred to and the employee hereby agrees and undertakes to keep all such information strictly confidential  which may have been communicated to him in his capacity as `Employee' or  during the course of his duties or the period of his contract and irrespective whether such information or knowledge came to be acquired by him as secret or not and not to divulge and/or suffer the same to be divulged, made known to anyone excepting to a duly authorized officer of this Company.</p>
        <p>12.	That if the Employee shall be guilty of any misconduct or commit any breach of the provision of this agreement or absent him self without any reasonable excuse or become incapable of attending to his duties, it shall be lawful for the Company immediately to terminate the Services hereby created.</p>
        <p>13.	That in the event of the Employee leaving the services of the Company before the Expiry of the period of the Contract, as also in the event of the Employee making it obligatory or necessary or expedient on the part of the Company to terminate his services for any reason of whatsoever nature and for things mentioned in paragraph (11) and / or (12) above, the employee shall be liable to pay and shall pay to the Company or to any of the Companies or concerns to which his services may be transferred as sum equivalent to Twelve Calendar Months (Basic) present salary by way of agreed liquidated damages and the Employee agrees to pay the same without any proof of actual damages suffered by the Company or by the other Companies or concerns concerned to which his services may have been transferred, being required to be produced and such payments shall always be independent of and not in lieu of any other compensation or damages that the Employees may have rendered herself liable to pay to Company or any of the Companies or concerns as above said, by reason of his having committed breach of any of the conditions herein before contained by reason of his having caused any damages or injury to the Comping or concern to which his services might have been transferred, by his conduct or omissions while in employment, the Employee, further agrees that, the Company shall have lien on all amounts due to the Employees from the Company in respect of the damages aforesaid and the Company shall be entitled to exercise the same and to set the said amounts off against such damages without reference to the Employee.</p>
        <p>14.	That in the event of the Company desiring to terminate the services of the Employee at any time during the currency of this agreement without assigning any reason for such termination, the Company shall be entitled to do so by paying the Employee, a sum equivalent to One Month's (Basic) Present Salary than being drawn by him in addition to whatever may be due and payable to him; however in the event of his services being terminated by the Firm at any time when the unexpired period of the contract is less than six months, the employee shall be entitled to the payment of whatever may be due and payable to him only.  In case of misconduct of misbehavior of an employee, the services of the employee can be terminated without notice or notice pay as stated hereinabove.</p>
        <p>15.	That in the event of his leaving the services of the Company at any time during the Currency of this Agreement he will have to give One month's notice to the Company or One Month's (Basic) Present salary in lieu of  notice, to the Company. This is in addition to damages provided under clause No. 13 above.</p>
        <p>16.	In the event of dispute regarding the terms and conditions and interpretations of the above clauses, the same shall be referred to the Arbitrator under Arbitration & Conciliation Act 1966</p>
        <p>IN WITNESS where of the Company has set the signature of its Designated Person hereto in the presence of and the Employee has got his signature in his own hand hereunto on the <asp:Label ID="lbld" runat="server"></asp:Label></p>
       <br />
        <br />
        <br /><br />

        <table>
            <tr>
                <td>Signature of Designated person</td>
                
                   
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Signature of Employee</td>
                
            </tr>
        </table>
        <%--Signature of Designated person<div class="a">Signature of Employee </div>--%>
        <br />
        <table>
            <tr>
                <td>WITNESSES</td>
                <td>WITNESSES</td>
            </tr>
            <tr>
                <td> 1…………………………</td>
                <td> 1…………………………</td>
            </tr>
            <tr>
                <td>2………………………… </td>
                <td>2………………………… </td>

            </tr>
        </table>
                                  
       
        
       

        


        </div>

        
    </asp:Panel>  
         </form>
</body>
</html>
