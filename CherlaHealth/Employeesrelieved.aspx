﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Employeesrelieved.aspx.cs" Inherits="CherlaHealth.Employeesrelieved" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            height: 26px;
        }

        .tpadleft {
            padding-left: 20px;
        }

        .tpadright {
            padding-right: 20px;
        }

        .form-control1 {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Employee List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
               <div class="col-md-12">
                
            </div>
            <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
                        <div class="col-md-12" style="padding-top: 15px">

                <div class="table table-responsive" style="overflow: scroll; height: 420px">

                    <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed cf"
                        AutoGenerateColumns="false" DataKeyNames="EmployeeId" 
                        runat="server">
                        <Columns>
                            
                            
                            <asp:BoundField DataField="EmployeeId" Visible="false" />
                            <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btn-xs" Text="update" CommandArgument='<%# Eval("EmployeeId") %>' OnClick="btnEdit_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" HeaderText="Employee Photo">
                                <ItemTemplate>
                                    <asp:Image ID="Image1" class="img-circle" Width="80px" Height="80px" ImageUrl='<%# "profile.ashx?EmployeeID1=" + Eval("EmployeeID1")%>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Company" HeaderText="Company" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Desigantion1" HeaderText="Department" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EmployeName" HeaderText="Employee Name" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EmployeeID1" HeaderText="Employee ID" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="DOB" HeaderText="Date of Birth" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="DOJ" HeaderText="Date of Join" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Gender" HeaderText="Gender" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Desigantion" HeaderText="Designation" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            
                            <asp:BoundField DataField="EmployeeType" HeaderText="Type" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="CompanyEmail" HeaderText="Email" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="CellNo" HeaderText="Phone No." HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="BloodGroup" HeaderText="Blood Group" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Password" HeaderText="App Password" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Emptype" HeaderText="Medicare/Not" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Salary" HeaderText="Salary" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            

                        </Columns>
                    </asp:GridView>
                      </div>
            </div>
            </div></div></asp:Content>