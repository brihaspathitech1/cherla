﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace CherlaHealth
{
    public partial class ViewCandidates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InterView_Candidates();
            }
        }

      
        private void InterView_Candidates1()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand(" select * from [Candidates] where Status='" + DropDownList2.SelectedValue + "' and Name!='' order by Id desc", conn44);


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }
        }
        //dispaly total details in gridview
        private void InterView_Candidates()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand(" select * from [Candidates] where Name!='' order by Id desc", conn44);


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }
        }
        //selcting particular employee full details
        private void select()
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con = new SqlConnection(connstrg);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from [Candidates]  where Id='" + lblstor_id.Text + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                FName.Text = reader["Name"].ToString();
                JPosition.Text = reader["JPosition"].ToString();
                CCompany.Text = reader["Ccompany"].ToString();
                Exp_CTC.Text = reader["SalaryExp"].ToString();
                Interview_date.Text = reader["InterviewShedule"].ToString();
                Referby.Text = reader["Reference"].ToString();
                Communication.Text = reader["Com_Skills"].ToString();
                Technical.Text = reader["Tech_Skills"].ToString();
                Screening.Text = reader["Screening"].ToString();
                Reason.Text = reader["Reason"].ToString();
                Email.Text = reader["Email"].ToString();
                lblbranch.Text = reader["Branch"].ToString();
                status.Text = reader["status"].ToString();
                lblexperiance.Text = reader["Experiance"].ToString();
                lblremarks.Text = reader["Remarks"].ToString();
            }
            reader.Close();
            con.Close();
        }
        //details button
        protected void Details(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            //
            select();
            this.ModalPopupExtender2.Show();

        }
        //redirecting to ranking candidates
        protected void SSS(object sender, EventArgs e)
        {
            int Id = int.Parse((sender as LinkButton).CommandArgument);


            Session["Candidate"] = Id;
            Response.Redirect("~/RankingCandidates.aspx");

        }
        //pdf resume view
        protected void View(object sender, EventArgs e)
        {

            int id = int.Parse((sender as LinkButton).CommandArgument);
            string embed = "<object data=\"{0}{1}\" type=\"application/pdf\" width=\"500px\" height=\"500px\">";
            embed += "If you are unable to view file, you can download from <a href = \"{0}{1}&download=1\">here</a>";
            embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
            embed += "</object>";
            ltEmbed.Text = string.Format(embed, ResolveUrl("~/Pdf.ashx?Id="), id);

        }
        //updating status
        protected void Click1(object sender, EventArgs e)
        {
            // string id = GridView11.SelectedRow.Cells[0].Text;
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            Label2.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            //Label2.Text = gRow.Cells[8].Text.Replace("&nbsp;", "");




            ModalPopupExtender1.Show();


        }
        protected void buttn2(object sender, EventArgs e)
        {


            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String Update = "update [Candidates] set Status='" + DropDownList1.SelectedItem.Text + "',Remarks='"+txtremarks.Text+"' where Id='" + Label2.Text + "' ";
            SqlCommand comm = new SqlCommand(Update, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();



            Response.Redirect("ViewCandidates.aspx");





        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            InterView_Candidates1();
        }
    }
}