﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class Pharmacyprint : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            lbldate.Text = Session["fromdate1"].ToString();
            date.Text=Session["fromdate1"].ToString();
            date2.Text=Session["fromdate1"].ToString();
            lbld.Text=Session["fromdate1"].ToString();
            lblname2.Text = Session["father1"].ToString();
            lblname.Text = Session["emp1"].ToString();
            lblname3.Text = Session["emp1"].ToString();
            lblname4.Text = Session["emp1"].ToString();
            lbladdress.Text = Session["address1"].ToString();
            lblage.Text = Session["age1"].ToString();
            lbloccup.Text = Session["occupation1"].ToString();
            lbloccup2.Text = Session["occupation1"].ToString();
            lblgender.Text = Session["gender1"].ToString();
            lbda.Text = Session["todate1"].ToString();
            lblyr.Text = Session["yrs1"].ToString();
            yr.Text = Session["yrs1"].ToString();
            yrs.Text = Session["yrs1"].ToString();
            Salary.Text = Session["salary1"].ToString();
            lbldate4.Text = Session["fromdate1"].ToString();
            lbdate5.Text= Session["fromdate1"].ToString();

        }

        protected void btnpdf_Click(object sender, EventArgs e)
        {

          //  Image1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Pharmacist bond.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);

            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //string imagePath = Server.MapPath("sign board.jpg") + "";

            //for (int i = 0; i < 1; i++)
            //{

            //    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            //    image.Alignment = Element.ALIGN_TOP;

            //    image.SetAbsolutePosition(40, 700);

            //    image.ScaleToFit(500f, 400f);
            //    image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            //    pdfDoc.Add(image);
                htmlparser.Parse(sr);

           // }
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();




        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }


    }
}