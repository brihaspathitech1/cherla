﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddHolidays.aspx.cs" Inherits="CherlaHealth.AddHolidays" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 20px">Add Holiday</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div class="col-md-12">
                            <div class="col-sm-6">
                                Name
                    <span class="style1">*</span>
                                <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                    ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>

                            </div>
                            <div class="col-sm-6">
                                Holiday Date 
                    <span class="style1">*</span><asp:TextBox ID="TextBox2" class="form-control" runat="server"></asp:TextBox>
                                <asp:Label
                                    ID="lblStatus" runat="server" Style="color: #FF0000"></asp:Label>
                                <asp:RequiredFieldValidator ID="Required" runat="server" ControlToValidate="TextBox2"
                                    ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                                <cc1:CalendarExtender ID="CalendarExtender3" Format="yyyy-MM-dd" TargetControlID="TextBox2" runat="server" />
                            </div>
                            <div class="col-sm-6">
                                Holiday Type&nbsp;
                   <asp:DropDownList ID="DropDownList1" class="form-control" runat="server">
                       <asp:ListItem></asp:ListItem>
                       <asp:ListItem>National Holiday</asp:ListItem>
                       <asp:ListItem>Common Festival Holiday</asp:ListItem>
                       <asp:ListItem>Discretionary</asp:ListItem>
                   </asp:DropDownList>

                            </div>
                            <div class="col-sm-6">
                                Description
                    <asp:TextBox ID="TextBox3" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>

                            </div>
                            <div class="col-md-12 btn1">
                                <br />
                                <br />
                                <br />



                            </div>


                            <div class="col-sm-12 text-center">
                                <asp:Button ID="Button3" class="btn btn-success"
                                    runat="server" Text="Insert" OnClick="Button3_Click" />
                            </div>
                        </div>
            </div>
        </div>

</asp:Content>
