﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ExperianceletterNew.aspx.cs" Inherits="CherlaHealth.ExperianceletterNew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Company List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
              <div class="row mt">
                <div class=" col-lg-12 panel" style="padding: 20px">
                  
                    <div class=" col-lg-12">
 
    
    <!--our-quality-shadow-->
    <div class="clearfix"></div>
    <h5 class="heading1">Experience Letter</h5>
    <div class="tabbable-panel margin-tops4 ">
      <div class="tabbable-line">
        <ul class="nav nav-tabs tabtop  tabsetting">
      
        </ul>
        <div class="tab-content margin-tops">
      
          

          
             
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <label>
                                Employee</label>
                            <asp:DropDownList ID="DropDownList4" class="form-control" autopostback="true"
                                runat="server" OnSelectedIndexChanged="DropDownList4_SelectedIndexChanged">
                            </asp:DropDownList>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="DropDownList4" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%-- <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label4" runat="server" Text=""></asp:Label>--%>
                </div>
                <div class="col-md-3">
                     <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>  <label> 
                               Designation</label>
                       <asp:TextBox ID="empDesignation" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="empDesignation" 
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            </ContentTemplate>
                    </asp:UpdatePanel>
                
                </div>
                <div class="col-md-3">
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                             <label>
                                Date From</label>
                            <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                              <cc1:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="TextBox4"  Format="dd-MM-yyyy"></cc1:CalendarExtender>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="TextBox4" runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-3">
                   <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                             <label>
                               To</label>
                            <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="TextBox1"  Format="dd-MM-yyyy"></cc1:CalendarExtender>
                            <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="TextBox1" 
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                  <%--  <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>--%>
                </div>
                <div class="col-md-2" style="padding-top:23px">
                    <asp:Button ID="Button4" runat="server" class="btn btn-success" Text="Submit" OnClick="submit" />
                </div>
            </div>
             
              </div>
    </div>
                        </div>
                    </div>
                  </div>
            </div>
        </div>
    
        </asp:Content>