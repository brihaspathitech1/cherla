﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddRules.aspx.cs" Inherits="CherlaHealth.AddRules" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Add Rule</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                        <div class="row mt">
               <%-- <div class=" col-lg-12 panel" style="padding: 20px">--%>
                 
                    <div class=" col-lg-12">
                        <div class=" col-lg-12">

                            <asp:Label ID="Session11" runat="server" Visible="false" Text="Label"></asp:Label>


                            <div class=" col-xs-6 form-group ">
                                <label>Name</label><br />
                                <asp:TextBox ID="TextBox3" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="TextBox3"
                                    ErrorMessage="* Name " ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                            <div class=" col-xs-6 form-group ">
                                <label>House Rent Allowance (%)</label><br />

                                <asp:TextBox ID="TextBox4" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox3"
                                    ErrorMessage="* House Rent Allowance " ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>

                        </div>








                        <div class=" col-lg-12">




                            <div class=" col-xs-6 form-group ">
                                <label>Education Allowance(%)</label><br />
                                <asp:TextBox ID="TextBox5" class="form-control" runat="server"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox5"
                                    ErrorMessage="* Education Allowance" ForeColor="Red"></asp:RequiredFieldValidator>

                            </div>
                            <div class=" col-xs-6 form-group ">
                                <label>Dearly Allowance(%)</label><br />

                                <asp:TextBox ID="TextBox6" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox6"
                                    ErrorMessage="* Dearly Allowance" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class=" col-lg-12">




                            <div class=" col-xs-6 form-group ">
                                <label>Conveyance Allowance(%) </label>
                                <br />
                                <asp:TextBox ID="TextBox7" class="form-control" runat="server"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox7"
                                    ErrorMessage="* Conveyance Allowance" ForeColor="Red"></asp:RequiredFieldValidator>

                            </div>
                            <div class=" col-xs-6 form-group ">
                                <label>Income Tax (%)</label><br />

                                <asp:TextBox ID="TextBox8" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox8"
                                    ErrorMessage="* Income Tax" ForeColor="Red"></asp:RequiredFieldValidator>

                            </div>
                        </div>

                        <div class=" col-lg-12">
                            <div class=" col-xs-6 form-group ">
                                <label>Life Insurance Corporation (%)</label><br />

                                <asp:TextBox ID="TextBox9" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBox9"
                                    ErrorMessage="* Life Insurance Corporation" ForeColor="Red"></asp:RequiredFieldValidator>

                            </div>
                            <div class=" col-xs-6 form-group ">
                                <label>Provident Fund(%)</label><br />

                                <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBox1"
                                    ErrorMessage="* Provident Fund" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class=" col-lg-12">
                            <div class=" col-xs-6 form-group ">
                                <label>Employees State Insurance Corporation  (%)</label><br />

                                <asp:TextBox ID="TextBox2" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="TextBox2"
                                    ErrorMessage="* Employees State Insurance Corporation" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                            <div class=" col-xs-6 form-group ">
                                <label>Medical Allowance  (%)</label><br />

                                <asp:TextBox ID="TextBox10" class="form-control" runat="server"
                                    OnTextChanged="TextBox10_TextChanged"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBox10"
                                    ErrorMessage="* Medical Allowance" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>



                        <div class=" col-lg-12">

                            <div class=" col-lg-12">


                                <asp:Button ID="Button9" runat="server" CausesValidation="true"
                                    class="btn btn-success" Text="Submit" OnClick="Button9_Click" />
                            </div>
                        </div>
                    </div>



                    <div class=" col-lg-12" style="padding-top: 10px">

                        <%-- <div  class="table-responsive"  style="overflow:scroll; height:300px">--%>



                        <div class=" col-lg-12">

                            <asp:GridView ID="GridView1" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" class="table table-bordered table-striped" runat="server" AutoGenerateColumns="false" DataKeyNames="Id">
                                <Columns>



                                    <asp:BoundField DataField="Name1" HeaderText="Name" />


                                    <asp:BoundField DataField="HRA" HeaderText="HRA" />
                                    <asp:BoundField DataField="EA" HeaderText="EA" />
                                    <asp:BoundField DataField="CA" HeaderText="CA" />
                                    <asp:BoundField DataField="DA" HeaderText="DA" />
                                    <asp:BoundField DataField="IT" HeaderText="IT" />

                                    <asp:BoundField DataField="LIC" HeaderText="LIC" />
                                    <asp:BoundField DataField="PF" HeaderText="PF" />

                                    <asp:BoundField DataField="ESI" HeaderText="ESI" />
                                    <asp:BoundField DataField="MA" HeaderText="MA" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnedit" CssClass="btn btn-success btn-xs" OnClick="Edit_Click" CommandArgument='<%# Eval("Id") %>' runat="server" Text="Edit" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <%--<asp:CommandField CausesValidation="false" ShowEditButton="true" ControlStyle-ForeColor="Green" ControlStyle-Font-Bold="true" />--%>
                                    <asp:CommandField CausesValidation="false" ShowDeleteButton="true" ControlStyle-ForeColor="Red" ControlStyle-Font-Bold="true" />
                                </Columns>
                            </asp:GridView>

                        </div>
                        <%--</div>--%>
                    </div>


                </div>
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" PopupControlID="Panel1" CancelControlID="btnclose"></cc1:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="Red" Height="500px" Width="800px" Style="display: none;">
                  <h3 style="text-align:center">TAX Details</h3>
                <hr />
                <div class="col-md-4">
                    <div class="form-group">
                       Name <asp:TextBox ID="txtname" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        House Rent Allowance (%)<asp:TextBox ID="txthouse" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Education Allowance(%)<asp:TextBox ID="txtea" CssClass="form-control" runat="server"></asp:TextBox>

                        </div>
                    </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Dearly Allowance(%)<asp:TextBox ID="txtda" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Conveyance Allowance(%)  <asp:TextBox ID="txtca" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Income Tax (%)<asp:TextBox ID="txtit" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        Life Insurance Corporation (%)<asp:TextBox ID="txtLIC" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                     </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        Provident Fund(%)<asp:TextBox ID="txtpf" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                     </div>
                <div class="col-md-4">
                    <div class="form-group">
                       ESI Corporation (%) <asp:TextBox ID="txtESI" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                  <div class="col-md-4">
                    <div class="form-group">
                        Medical Allowance (%)<asp:TextBox ID="txtma" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                      </div>
                <div class="col-md-3" style="padding-top:18px">
                    <div class="form-group">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="Update" CausesValidation="false" />
                        </div>
                    </div>
                <div class="col-md-3" style="padding-top:18px">
                    <div class="form-group">
                        <asp:Button ID="btnclose"  CssClass="btn btn-danger" runat="server" Text="Close" CausesValidation="false" />
                        </div>
                    </div>
            </asp:Panel>
            <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
            </div>
            </div>
      <%--  </div>--%>
</asp:Content>
