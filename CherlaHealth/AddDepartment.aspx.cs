﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class AddDepartment : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindStudy();
            }
        }
        private void BindStudy()
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT CompanyID,CompanyName FROM Companies";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList1.DataSource = dt;
                        DropDownList1.DataValueField = "CompanyID";
                        DropDownList1.DataTextField = "CompanyName";
                        DropDownList1.DataBind();
                        sqlConn.Close();


                        DropDownList1.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;


            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Department WHERE Department = @Name", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name", this.Code.Text.Trim());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.lblStatus.Text = "Duplicate Entry";
                        }

                        else
                        {
                            SqlConnection con3 = new SqlConnection(connstrg);
                            String insert = "insert into Department(CompanyId,Department,SN,Decripation)values('" + DropDownList1.SelectedValue + "','" + Code.Text + "','" + TextBox1.Text + "','" + TextBox2.Text + "')";
                            SqlCommand comm1 = new SqlCommand(insert, con3);
                            con3.Open();
                            comm1.ExecuteNonQuery();
                            con3.Close();


                            Code.Text = null;
                            TextBox1.Text = null;
                            TextBox2.Text = null;
                            Response.Write("<script>alert('Data inserted successfully')</script>");
                            Response.Redirect("~/DepartmentList.aspx");
                        }
                        con.Close();
                    }
                }
            }
        }
    }
}