﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace CherlaHealth
{
    public partial class Notifications : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                MCI_Expire();
                SCI_Expire();
            }
        }

        protected void MCI_Expire()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select e.EmployeName,CONVERT(varchar,d.MCI_Expire,113) as MCI_Date,d.MCI from Empdocuments d inner join Employees e on d.EmployeeId=e.EmployeeId where MCI_Expire<=DATEADD(MM,2,GETDATE())", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                DataList1.DataSource = ds;
                DataList1.DataBind();
            }
        }

        protected void SCI_Expire()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand(" Select e.EmployeName,CONVERT(varchar,d.SCI_Expire,113) as SCI_Date,d.SCI from Empdocuments d inner join Employees e on d.EmployeeId=e.EmployeeId where SCI_Expire<=DATEADD(MM,2,GETDATE())", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataList2.DataSource = ds;
                DataList2.DataBind();
            }
        }
    }
}