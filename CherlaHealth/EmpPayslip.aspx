﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EmpPayslip.aspx.cs" Inherits="CherlaHealth.EmpPayslip" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript">
        function onCalendarShown() {

            var cal = $find("calendar1");
            //Setting the default mode to month
            cal._switchMode("months", true);

            //Iterate every month Item and attach click event to it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }

        function onCalendarHidden() {
            var cal = $find("calendar1");
            //Iterate every month Item and remove click event from it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }

        }
        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "month":
                    var cal = $find("calendar1");
                    cal._visibleDate = target.date;
                    cal.set_selectedDate(target.date);
                    cal._switchMonth(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged();
                    break;
            }
        }

    </script>
    <style >

        .inline-rb label {
    font-weight: bold !important;
    color: #00daff;
    font-size: 20px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size:24px">Payslip</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                        <div class="row mt">

              <%--  <div class=" col-lg-12 panel" style="padding: 20px">--%>
                    <%--<h3 style="color: Red; font-weight: bold;">
                        <label>Payslips</label></h3>--%>
                  <%--  <hr />--%>

                    <div class=" col-lg-12">
                        <div class=" col-lg-12" style="padding-top: 17px">

                            <div class=" col-lg-12">
                                <div class="col-md-3" style="padding-top: 20px;">
                                  
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server"
                                                OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" CssClass="inline-rb" RepeatDirection="Horizontal" AutoPostBack="true">
                                                <asp:ListItem Value="1">NPF &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp;</asp:ListItem>
                                                <asp:ListItem Value="2">PF</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Required" ControlToValidate="RadioButtonList1" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </div>
                                <div class="col-md-3 ">
                                    Month
        <asp:TextBox ID="Date" runat="server" class="form-control" Width="150px"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="Date"
                                        OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                                        Enabled="True" />
                                </div>

                                <div class="col-md-3">
                                    Employee
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:DropDownList ID="Employee" runat="server" class="form-control"></asp:DropDownList>
         </ContentTemplate>
     </asp:UpdatePanel>
                                </div>
                                <div class="col-md-3" style="padding-top: 17px">
                                    <asp:Button ID="Button1" runat="server" class="btn btn-danger" Text="Submit" OnClick="Button1_Click" />
                                    <asp:Label ID="label1" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="label2" runat="server" Text="" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class=" col-lg-12" style="padding-top: 17px">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" AutoGenerateColumns="false" DataKeyNames="Id"
                                    runat="server">
                                    <Columns>

                                        <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />

                                        <asp:BoundField DataField="Basic" HeaderText="Basic" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="PF" HeaderText="PF" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="ESI" HeaderText="ESI" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Total" HeaderText="Total" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />

                                        <asp:BoundField DataField="GrossSalary" HeaderText="Gross" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />


                                        <asp:TemplateField HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White">
                                            <HeaderTemplate>
                                                Payslip
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <h5 class=" text-center">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SSS" class="glyphicon glyphicon-print"
                                                        CommandArgument='<%# Eval("Id") %>' CausesValidation="false"></asp:LinkButton></h5>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      <%--  <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White">
                                            <ItemTemplate>
                                                <asp:Button ID="Edit" runat="server" Text="Edit" OnClick="Edit_click" CommandArgument='<%# Eval("Payslip") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                    </Columns>

                                </asp:GridView>

                            </div>
                        </div>
                        <%--<div class=" col-lg-12" style="padding-top:15px">
     <div class="col-md-3">
        <asp:Button ID="Button2" class="btn btn-success" runat="server" Text="Export"  />
    </div>
    </div>--%>
                    </div>
             <%--   </div>--%>
            </div>
            </div>
         </div>
</asp:Content>
