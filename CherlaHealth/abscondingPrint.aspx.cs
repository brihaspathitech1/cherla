﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using System.Net;
using System.Net.Mail;
using System.Text;

namespace CherlaHealth
{
    public partial class abscondingPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                lblname.Text = Session["employname"].ToString();
                lblname2.Text = Session["employname"].ToString();
                lbldesign.Text = Session["desig"].ToString();
                lblemail.Text = Session["email"].ToString();
                gen.Text = Session["salute"].ToString();
                gen1.Text = Session["salute"].ToString();
                lbldate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        protected void pdf_Click(object sender, EventArgs e)
        {
            //Image1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Absconding Letter.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            //string imagePath = Server.MapPath("sign board.jpg") + "";

            //iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            //image.Alignment = Element.ALIGN_TOP;

            //image.SetAbsolutePosition(40, 700);

            //image.ScaleToFit(500f, 400f);
            //image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            //pdfDoc.Add(image);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        protected void btnemail_Click(object sender, EventArgs e)
        {
            Smail();
        }
        protected void Smail()
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();
                    {
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");


                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Date: " + System.DateTime.Now.ToString("yyyy-MM-dd") + "</p>");
                       
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + lblname.Text + "</p>");
                        
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'> " + lbldesign.Text +" </p>");
                        sb.Append("<p><br></p>");
                       
                        sb.Append("<p><br></p>");
                        
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'> Sub. – Absconding from duties. </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Dear "+lblname2.Text+" </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>It has been observed that you have proceeded on leave without prior permission of the concerned authorities, resulting in willful insubordination and gross negligence of duties, in your capacity as Designation.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Absenting yourself from duties without prior intimation is a misconduct for which you are making yourself liable for necessary action.</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>You are hereby warned to refrain from such activities; failure to do so shall invoke appropriate action.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>You are further advised to submit a written explanation on your unauthorized leave as soon as you receive this letter or as soon as you resume duties.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Kindly treat this as very urgent.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>For Cherla Health Care (P) Ltd</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Name:</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Authorized Signatory,</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");

                        sb.Append("<p><br></p>");
                        
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                       

                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            string path = Server.MapPath("Files");
                            string filename = path + "/" + "Absconding letter" + ".Pdf";

                            PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();

                            string imagePath = Server.MapPath("sign.jpeg") + "";
                            for (int i = 0; i < 1; i++)
                            {

                                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                                image.Alignment = Element.HEADER;

                                image.SetAbsolutePosition(30, 650);

                                image.ScaleToFit(550f, 550f);
                                image.Alignment = iTextSharp.text.Image.HEADER | iTextSharp.text.Image.HEADER;


                                pdfDoc.Add(image);
                                htmlparser.Parse(sr);

                                PdfContentByte content = writer.DirectContent;
                                Rectangle rectangle = new Rectangle(pdfDoc.PageSize);
                                rectangle.Left += pdfDoc.LeftMargin;
                                rectangle.Right -= pdfDoc.RightMargin;
                                rectangle.Top -= pdfDoc.TopMargin;
                                rectangle.Bottom += pdfDoc.BottomMargin;
                                content.SetColorStroke(BaseColor.BLACK);
                                //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                                content.Stroke();
                            }
                            pdfDoc.Close();




                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();

                            MailMessage mm = new MailMessage();
                           
                            mm.From = new MailAddress("hr.cherlahealth@gmail.com", "Absconding letter");

                            mm.To.Add(lblemail.Text);
                            //.CC.Add("md@brihaspathi.com");
                            //mm.CC.Add("hr@brihaspathi.com");
                            // mm.CC.Add("hra@brihaspathi.com");

                            mm.IsBodyHtml = true;
                            mm.Subject = "Absconding letter";
                           mm.Body = "Absconding letter <br /><br />Regards<br/><br/>HR<br/><br /><b>Thank you.</b>";
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Absconding letter.pdf"));
                            mm.Priority = MailPriority.High;
                            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                            //smtp.UseDefaultCredentials = true;
                            smtp.Credentials = new System.Net.NetworkCredential("hr.cherlahealth@gmail.com", "HR@cherla");
                            smtp.EnableSsl = true;
                            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.Send(mm);
                        }
                    }
                }
            }
        }
    }
    }
