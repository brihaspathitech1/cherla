﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OfferLetterPrint.aspx.cs" Inherits="CherlaHealth.OfferLetterPrint" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Offer Letter</title>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
                
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }




        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            
            #Button1 {
                display: none;
             
            }
          

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint {
                display: none;
            }
            input#btnpdf {
                display: none;
            }
            input#btnemail {
                display: none;
            }
           
            a {
                display: none;
            }
            
  
        }




        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
    <style>
    .page21 {
            width:100%;background:#d3d3d357;text-align:justify;padding:150px 50px 0px 70px;
        }
        tr, td {
            /*border:1px solid black;
            border-collapse:collapse;
            padding:15px;*/
      
       }
        .tab-text { margin-left:40px;font-weight:bold;
        }
        ol {
            margin-left:50px;
        }
   </style> 
</head>
<body style="color:black;">
     <form id="form1" runat="server">
    <asp:Panel ID="Panel1" runat="server">
    <div class="page21">     
   
        <div style="text-align:center;padding-top:25px ;">

             <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                <a href="OfferLetter.aspx" id="btnPrint" type="button"  onclick="goBack()"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>

            <asp:Button ID="btnpdf"  CssClass="btn btn-success" runat="server" Text="PDF" OnClick="pdf_Click" />
            <asp:Button ID="btnemail"  CssClass="btn btn-success" runat="server" Text="Gmail" OnClick="btnemail_Click" Visible="false"/>
            <%--<asp:Button ID="btngmail" CssClass="btn btn-success" runat="server" Text="Gmail" OnClick="btngmail_Click" />--%>
        </div>
     <br /><br />
              <table width="100%">
                <tr>
                    <td>
                       <%-- <img id="img1" alt="/" src="sign board.jpg" width="1250px" height="120px" />--%>
                     <%--    <img id="img1" runat="server" src="~/sign board.png" width="1250px" height="120px" />--%>
                       <%-- <asp:Image ID="Image1" ImageUrl="~/sign board.jpg" Width="1250px" Height="120px" runat="server" />
                    --%></td>
                </tr>
            </table>
        <br /><br />
        <p>Date: <asp:Label ID="lblpdate" runat="server" Text=""></asp:Label>
         </p>
        
        <br />
        <p>To,</p>
        <p><asp:Label ID="salutation" runat="server">&nbsp;&nbsp;</asp:Label><asp:Label ID="lblname" runat="server" Text=""></asp:Label></p>
        <p><asp:Label ID="lbladdress" runat="server" Text=""></asp:Label></p>
        <p><asp:Label ID="lblemail" runat="server" Text=""></asp:Label></p>
        <p>Sub: OFFER LETTER</p>
        <br />
        <p>Dear <asp:Label ID="Label2" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblname2" runat="server" Text=""></asp:Label>,</p>
        <br />
        <br />
        
        <br />
        <p>We are pleased to offer you the position of <asp:Label ID="lbldesig" runat="server" Text=""></asp:Label> at Cherla Health Pvt. Ltd subject to satisfactory fulfillment of our company’s pre-employment criteria. Your appointment shall be based at <asp:Label ID="Label1" runat="server" Text=""></asp:Label>.</p>
        <br />
        <p>The position we are offering you entails a monthly Gross salary of Rs. <asp:Label ID="lblsal" runat="server" Text=""></asp:Label>/- with an annual cost to company of Rupees <asp:Label ID="lblrupees" runat="server" Text=""></asp:Label>/-. You will be on probation for a period of <asp:Label ID="lblperiod" runat="server" Text=""></asp:Label> months. </p>
        <p>Note:<asp:Label ID="lblnote" runat="server" Text=""></asp:Label> </p>
        <br />
        <p>We would like you to start working at Cherla Health from <asp:Label ID="lbldate" runat="server" Text=""></asp:Label> &nbsp;&nbsp;<asp:Label ID="lbltime" runat="server" Text=""></asp:Label>. The role, in which you join us, is a very important one which entails dealing with important and sensitive information, records and such other matters of the company. You will, therefore, be required to sign a “Code of Conduct and Secrecy Agreement” of our company. </p>
        <br />
        <p>Please find enclosed a list of documents to be provided on the day of joining.</p>
        <br />
        <p>Please reply to this letter immediately, to indicate your acceptance of this offer and date of your joining.</p>
        <br />
        <p>We are confident you will be able to make a significant contribution to the success of Cherla Health Pvt Ltd and look forward to working with you.</p>
        <p>
            &nbsp;</p>
        <br />
        <p>Sincerely,</p>
        <br />
        <p>P Purushothama Sarma Manager HR</p>
        <p></p>
        <p>Cherla Health (P) Ltd</p>

        </div>

        <div class="page21">
        <br /><br />
        <p><b><u>Documents to be submitted at the time of accepting this offer</u></b></p>
        <p style="margin-left:20px">•	Letter of acceptance</p>
        <br />
        <br />
        
         <p><b>Documents to be submitted on the day of joining.</b> </p>
        <table  width="80%";>
            <tr>
                <td style="border-collapse:collapse;padding:15px">
                 <p class="tab-text">Identity & Address  proof: (Xerox)</p>   
                 <ol>
                    
            
            <li>Passport size photos ( 4 nos)</li>
            <li>PAN Card </li>
            <li>Driver’s license</li>
            <li>Aadhar Card</li>

                  </ol> 
                </td>
            </tr>
            <tr>
                <td style="border-collapse:collapse;padding:15px">
                     <p class="tab-text">Educational Qualification Certificates</p>
                     <ol>
                              <li>	10th class 2,+2</li>
                              
                                <li>Degree certificate</li>
     </ol>
                </td>
            </tr>

            <tr>
                <td style="border-collapse:collapse;padding:15px"> 
                    <p class="tab-text">Previous employment related</p>
                                <ol>
                                    
                                        <li>	Salary certificate of previous company</li>
                                       <li>Resignation copy</li>
                                       <li>Relieving letter</li> 
                                       <li>Experience certificate</li>
             <li>Last Three (03) month’s salary slip from current employer </li>
  </ol>

                </td></tr>
            
        

   
             </table>
 
    </div>
    
    
    </asp:Panel>  
         </form>
</body>
</html>
