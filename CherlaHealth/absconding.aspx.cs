﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace CherlaHealth
{
    public partial class absconding : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Employee_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Desigantion,CompanyEmail from Employees where EmployeeId='" + dropemp.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                txtdesign.Text = dr["Desigantion"].ToString();
                TextBox1.Text = dr["CompanyEmail"].ToString();
                


            }
            conn.Close();
        }
        protected void btnsubmit_Click(object sender,EventArgs e)
        {
            Session["desig"] = txtdesign.Text;
            Session["employname"] = dropemp.SelectedItem.Text;
            Session["email"] = TextBox1.Text;
            Session["salute"] = salut.SelectedItem.Text;
            Response.Redirect("~/abscondingPrint.aspx");
        }
    }
}