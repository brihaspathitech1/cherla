﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddShift.aspx.cs" Inherits="CherlaHealth.AddShift" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            color: #0066FF;
        }
        .style3
        {
            color: #9966FF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 26px">Add Shift</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="form-panel col-md-12">
                <div class="form-panel col-md-12">
                    <h2>New Shift</h2>
                    <hr />
                    <div class="col-md-6">
                        Shift Name <span class="style1">*</span><asp:TextBox ID="shiftname" class="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                            ControlToValidate="shiftname" runat="server" ErrorMessage="Required"
                            Style="color: #FF0000"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblStatus" runat="server" Style="color: #FF0000"></asp:Label>
                        <br />
                        Shift Code
                        <asp:TextBox ID="shiftcode" class="form-control" runat="server"></asp:TextBox><br />

                        Start Time <span class="style1">*</span><asp:TextBox ID="start" class="form-control" runat="server"></asp:TextBox>
                        <span class="style3">Format</span><span class="style2">:HH:mm(24 hours)&nbsp;
                        </span>&nbsp;<asp:RegularExpressionValidator ID="regexStartTime" ControlToValidate="start"
                            ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
                            ErrorMessage="Input valid date" runat="server" Style="color: #FF0000" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="start" runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        <br />
                        End Time <span class="style1">*</span><asp:TextBox ID="end" class="form-control" runat="server"></asp:TextBox>
                        &nbsp; <span class="style3">Format</span><span class="style2">:HH:mm(24 hours)&nbsp;&nbsp;&nbsp;&nbsp;
                        </span>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="end"
                            ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
                            ErrorMessage="Input valid date" runat="server" Style="color: #FF0000" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="end" runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        <br />
                        <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Submit"
                            OnClick="Button1_Click" /><br />
                        <br />
                    </div>


                </div>



            </div>
            </div>
        </div>
</asp:Content>
