﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EmpShiftDetails.aspx.cs" Inherits="CherlaHealth.EmpShiftDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .tableBackground {
            background-color: silver;
            opacity: 0.7;
        }

        .tpadleft {
            padding-left: 20px;
        }

        .tpadright {
            padding-right: 20px;
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .form-control1 {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Employee Shift Details</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                <div class="col-lg-12">
                <div class="col-lg-6">
                    <asp:TextBox
                        ID="TextBox1"
                        AutoPostBack="True" runat="server" class="form-control" OnTextChanged="TextBox1_TextChanged" placeholder="Search by EmployeeName or ShiftName........"></asp:TextBox>
                </div>
                <div class="col-lg-6">
                    <asp:Button ID="Button1" runat="server" Text="Add Employee Shift" PostBackUrl="~/AssignShift.aspx" class="btn btn-warning" Style="margin-left: 25px"></asp:Button>
                </div>
            </div>

          
            <div class="col-md-12" style="padding-top: 15px">

                <div class="table table-responsive" style="overflow: scroll; height: 420px">

                    <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" AutoGenerateColumns="false" DataKeyNames="EmployeeShiftId"
                        runat="server" OnRowDeleting="GridView1_RowDeleting">
                        <Columns>

                            <asp:BoundField DataField="EmployeeName" HeaderText="Employee" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="ShiftName" HeaderText="Shift" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="ShiftStartDate" HeaderText="Start Date" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="ShiftEndDate" HeaderText="End Date" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />

                            <asp:CommandField CausesValidation="false" ShowDeleteButton="true" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Red" />

                        </Columns>

                    </asp:GridView>


                    <asp:Label ID="lblmsg" runat="server" />

                </div>
            </div>
            </div>
        </div>
</asp:Content>
