﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace CherlaHealth
{
    public partial class hourlypayroll : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Pf_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where PFStatus='" + RadioButtonList1.SelectedItem.Text + "' and status='1' and DeptId='7'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
        }

        protected void Click(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from Employees where EmployeeId='" + dropemp.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                lbldept.Text = dr["Desigantion1"].ToString();
                lbldesignation.Text = dr["Desigantion"].ToString();
                DateTime date = DateTime.Parse(dr["DOJ"].ToString());
                lbljoin.Text = date.ToString("yyyy-MM-dd");
                lblbank.Text = dr["Bankname"].ToString();
                lblacno.Text = dr["Bankaccountno"].ToString();
                lblpfno.Text = dr["PFno"].ToString();
                lblpf.Text = dr["PFStatus"].ToString();
            }
            conn.Close();
                
            String connstrg2 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn88 = new SqlConnection(connstrg2);
            SqlCommand com88 = new SqlCommand();
            com88 = new SqlCommand("Proc_payrol", conn88);
            com88.CommandType = CommandType.StoredProcedure;
            com88.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "A";
            com88.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropemp.SelectedValue;
            com88.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da = new SqlDataAdapter(com88);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable dt = ds.Tables[0];
            conn88.Close();
            if (dt.Rows.Count > 0)
            {
                txtabsent.Text = dt.Rows[0]["Cnt"].ToString();
            }
            else
            {
                txtabsent.Text = "0";
            }


            String connstrg21 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg21);
            SqlCommand com1 = new SqlCommand();
            com1 = new SqlCommand("Proc_payrol", conn1);
            com1.CommandType = CommandType.StoredProcedure;
            com1.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "H";
            com1.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropemp.SelectedValue;
            com1.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da1 = new SqlDataAdapter(com1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1);
            DataTable dt1 = ds1.Tables[0];
            conn1.Close();
            if (dt1.Rows.Count > 0)
            {
                txtholiday.Text = dt1.Rows[0]["Cnt"].ToString();

            }

            else
            {
                txtholiday.Text = "0";
            }

            String connstrg211 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn11 = new SqlConnection(connstrg211);
            SqlCommand com11 = new SqlCommand();
            com11 = new SqlCommand("Proc_payrol", conn11);
            com11.CommandType = CommandType.StoredProcedure;
            com11.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "P";
            com11.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropemp.SelectedValue;
            com11.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da11 = new SqlDataAdapter(com11);
            DataSet ds11 = new DataSet();
            da11.Fill(ds11);
            DataTable dt11 = ds11.Tables[0];
            conn11.Close();
            if (dt11.Rows.Count > 0)
            {
                txtpresent.Text = dt11.Rows[0]["Cnt"].ToString();

            }
            else
            {
                txtpresent.Text = "0";
            }
            String connstrg2111 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn111 = new SqlConnection(connstrg2111);
            SqlCommand com111 = new SqlCommand();
            com111 = new SqlCommand("Proc_payrol", conn111);
            com111.CommandType = CommandType.StoredProcedure;
            com111.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "P(NOP)";
            com111.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropemp.SelectedValue;
            com111.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da111 = new SqlDataAdapter(com111);
            DataSet ds111 = new DataSet();
            da111.Fill(ds111);
            DataTable dt111 = ds111.Tables[0];
            conn111.Close();
            if (dt111.Rows.Count > 0)
            {

                txtnop.Text = dt111.Rows[0]["Cnt"].ToString();
            }
            else
            {
                txtnop.Text = "0";
            }






            String connstrg21111 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn1111 = new SqlConnection(connstrg21111);
            SqlCommand com1111 = new SqlCommand();
            com1111 = new SqlCommand("Proc_payrol", conn111);
            com1111.CommandType = CommandType.StoredProcedure;
            com1111.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "WO";
            com1111.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropemp.SelectedValue;
            com1111.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da1111 = new SqlDataAdapter(com1111);
            DataSet ds1111 = new DataSet();
            da1111.Fill(ds1111);
            DataTable dt1111 = ds1111.Tables[0];
            conn1111.Close();
            if (dt1111.Rows.Count > 0)
            {

                txtwoff.Text = dt1111.Rows[0]["Cnt"].ToString();

            }
            else
            {
                txtwoff.Text = "0";
            }

            string conn12 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con = new SqlConnection(conn12);
            string q = "select salaryperhour from [Cherla].[dbo].[Employees] where EmployeeId='" + dropemp.SelectedValue + "'";
            SqlCommand cmd12 = new SqlCommand(q,con);
            SqlDataAdapter da12 = new SqlDataAdapter(cmd12);
            DataSet ds12 = new DataSet();
            da12.Fill(ds12);
            DataTable dt12 = ds12.Tables[0];
            con.Close();
            if(dt12.Rows.Count>0)
            {
                txtsal.Text = dt12.Rows[0]["salaryperhour"].ToString();
            }
            else
            {
                txtsal.Text = "0";
            }


            string contxthr = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conhr = new SqlConnection(contxthr);
            string qr = "SELECT employeeid,Sum(DateDiff(minute, 0, cast(duration as datetime))) As [TotalMinutes],Convert(varchar(5), Sum(DateDiff(minute, 0, cast(duration as datetime))+1) / 60)+ ':'+ Convert(char(2), Sum(DateDiff(minute, 0, cast(duration as datetime))+1) % 60) As [Description] FROM [Cherla].[AttendanceLogs2] where EmployeeID='" + dropemp.SelectedValue + "' group by employeeid";
            SqlCommand cmd12hr = new SqlCommand(qr, conhr);
            SqlDataAdapter da12hr = new SqlDataAdapter(cmd12hr);
            DataSet ds12hr = new DataSet();
            da12hr.Fill(ds12hr);
            DataTable dt12hr = ds12hr.Tables[0];
            conhr.Close();
            if (dt12hr.Rows.Count > 0)
            {
                txthrs.Text = dt12hr.Rows[0]["Description"].ToString();
            }
            else
            {
                txthrs.Text = "0";
            }
        }

        protected void salary_click(object sender,EventArgs e)
        {
            if(txthrs.Text!="0"&&txtsal.Text!="0")
            {
                double hrs = Convert.ToDouble(txthrs.Text);
                double sal = Convert.ToDouble(txtsal.Text);
                double value = hrs * sal;
                
                if(txtincentives.Text=="")
                {
                    txtincentives.Text = "0";
                }
                if(txttax.Text=="")
                {
                    txttax.Text = "0";
                }
                
                double net = value + Convert.ToDouble(txtincentives.Text);
                lbltot.Text = net.ToString();
                double total = net - Convert.ToDouble(txttax.Text);
                lblnet.Text = total.ToString();
                lblsalary.Text = sal.ToString();
                lblincentives.Text = txtincentives.Text;
                lbltax.Text = txttax.Text;
                lblhrs.Text = hrs.ToString();

                Int64 dfdf = Convert.ToInt64(total);
                Int64 NumVal = Convert.ToInt64(dfdf);
                lblwords.Text = Rupees(NumVal);
            }
        }

        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lack";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }
            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                result = result + ' ' + RupeesToWords(res) + " Hundred";
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = result + ' ' + " Rupees only";
            return result;
        }
        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }
            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        protected void Submit_Click(object sender,EventArgs e)
        {
            int value = 0;
            string val = string.Empty;
            string[] arr = txtmonth.Text.Split('-');
            string month = arr[1].ToString();
            string year = arr[0].ToString();
            int month1 = Convert.ToInt32(month);
            String month11 = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month1);
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Top 1 Payslip from hourpayslip order by id desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                val = dr["Payslip"].ToString();
            }
            conn.Close();
            if(val=="")
            {
                val = "0";
            }
            else
            {
                value = Convert.ToInt32(val.ToString()) + 1;
                val = value.ToString();
            }

            SqlCommand cmd1 = new SqlCommand("Select * from hourpayslip where EmpNo='" + dropemp.SelectedValue + "' and date='" + txtmonth.Text + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if(dt.Rows.Count>0)
            {
                this.lblStatus.Text = "Already Payslip Generated";
            }
            else
            {
                SqlCommand cmd2 = new SqlCommand("Insert into hourpayslip(payslip,Name,EmpNo,DOJ,department,designation,Bank,Accountno,pfstatus,PFno,Absent,present,Nop,Weekoff,holiday,date,month,year,hrsalary,hours,incentives,Taxded,gross,words,total)values('" + val + "','"+ dropemp .SelectedItem.Text+ "','"+ dropemp.SelectedValue+ "','"+ lbljoin.Text + "','"+ lbldept.Text + "','"+lbldesignation.Text+"','"+lblbank.Text+"','"+lblacno.Text+"','"+lblpf.Text+"','"+lblpfno.Text+"','"+txtabsent.Text+"','"+txtpresent.Text+"','"+txtnop.Text+"','"+txtwoff.Text+"','"+txtholiday.Text+"','"+txtmonth.Text+"','"+ month11 + "','"+year+"','"+lblsalary.Text+"','"+lblhrs.Text+"','"+lblincentives.Text+"','"+lbltax.Text+"','"+lblnet.Text+"','"+lblwords.Text+"','"+lbltot.Text+"') ",conn);
                conn.Open();
                cmd2.ExecuteNonQuery();
                conn.Close();
                Response.Redirect("~/hourlypayroll.aspx");
            }
        }
    }
}