﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="RelievingLetterNew.aspx.cs" Inherits="CherlaHealth.RelievingLetterNew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>

@import url(https://fonts.googleapis.com/css?family=Lato:400,900,700,300);
.heading4{font-size:18px;font-weight:400;font-family:'Lato', sans-serif;color:#111111;margin:0px 0px 5px 0px;}
.heading1{font-size:30px;line-height:20px;font-family:'Lato', sans-serif;text-transform:uppercase;color:#1b2834;font-weight:900;}
.content-quality{float:left;width:193px;}
.content-quality p{margin-left:10px;font-family:'Open Sans', sans-serif;font-size:14px;font-weight:600;line-height:17px;}
.content-quality p span{display:block;}
.tabtop li a{font-family:'Lato', sans-serif;font-weight:700;color:#1b2834;border-radius:0px;margin-right:22.008px;border:1px solid #ebebeb !important;}
.tabtop .active a:before{content:"♦";position:absolute;top:15px;left:82px;color:#e31837;font-size:30px;}
.tabtop li a:hover{color:#e31837 !important;text-decoration:none;}
.tabtop .active a:hover{color:#fff !important;}
.tabtop .active a{background-color:#e31837 !important;color:#FFF !important;}
.margin-tops{margin-top:30px;}
.tabtop li a:last-child{padding:10px 22px;}
.thbada{padding:10px 28px !important;}
section p{font-family:'Lato', sans-serif;}
.margin-tops4{margin-top:20px;}
.tabsetting{border-top:5px solid #ebebeb;padding-top:6px;}
.services{background-color:#d4d4d4;min-height:710px;padding:65px 0 27px 0;}
.services a:hover{color:#000;}
.services h1{margin-top:0px !important;}
.heading-container p{font-family:'Lato', sans-serif;text-align:center;font-size:16px !important;text-transform:uppercase;}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper site-min-height">
            <div class="row mt">
                <div class=" col-lg-12 panel" style="padding: 20px">
                  
                    <div class=" col-lg-12">
 
    
    <!--our-quality-shadow-->
    <div class="clearfix"></div>
    <h5 class="heading1">Relieving Letter</h5>
    <div class="tabbable-panel margin-tops4 ">
      <div class="tabbable-line">
        <ul class="nav nav-tabs tabtop  tabsetting">
         
        </ul>
        <div class="tab-content margin-tops">
       
           
            <div class="col-md-12">
             
                 <div class="row">
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <label>
                                Employee</label>
                            <asp:DropDownList ID="DropDownList5" class="form-control" 
                                runat="server">
                            </asp:DropDownList>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="DropDownList5" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                             <label>
                                Date From</label>
                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="TextBox5"  Format="dd-MM-yyyy"></cc1:CalendarExtender>
                            <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="TextBox5" 
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            \
              
                <div class="col-md-2" style="padding-top:23px">
                    <asp:Button ID="Button3" runat="server" class="btn btn-success" Text="Submit" onclick="submit1" />
                </div>
            </div>
             
             
              </div>
          </div>


        <%--</div>--%>
      </div>
    </div>
  </div>
  </div>

  </div>
  </section>
  </section>
</asp:Content>

