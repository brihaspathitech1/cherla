﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient; 

namespace CherlaHealth
{
    public partial class AssignDeptHead : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindDepartment();
                Bindgrid();
            }
        }

        protected void BindDepartment()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select DeptId,Department from Department", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropdept.DataSource = dr;
            dropdept.DataTextField = "Department";
            dropdept.DataValueField = "DeptId";
            dropdept.DataBind();
            dropdept.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Department_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees ", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void btnSubmit(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Insert into DeptMaster(DeptId,ManagerId) values ('" + dropdept.SelectedValue + "','" + dropemp.SelectedValue + "') ", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("AssignDeptHead.aspx");
        }

        protected void Bindgrid()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("select m.Id,d.Department,e.EmployeName from DeptMaster m inner join Department d on d.DeptId=m.DeptId inner join Employees e on e.EmployeeId=m.ManagerId", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void BindDepartment1()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select DeptId,Department from Department", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            ddldept.DataSource = dr;
            ddldept.DataTextField = "Department";
            ddldept.DataValueField = "DeptId";
            ddldept.DataBind();
            ddldept.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Edit_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            BindDepartment1();
            ModalPopupExtender1.Show();
        }

        protected void Department(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where DeptId='" + ddldept.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            ddlemp.DataSource = dr;
            ddlemp.DataTextField = "EmployeName";
            ddlemp.DataValueField = "EmployeeId";
            ddlemp.DataBind();
            ddlemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Update(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Update DeptMaster set DeptId='" + ddldept.SelectedValue + "',ManagerId='" + ddlemp.SelectedValue + "' where id='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Bindgrid();
        }

        protected void Delete(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("delete from DeptMaster where id='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Bindgrid();
        }
    }
}