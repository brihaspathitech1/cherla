﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace CherlaHealth
{
    public partial class docagrmnt : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCandidates();
                BindCandidates1();
            }
        }
        

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            Session["txt1"] = TextBox1.Text;
            Session["txt2"] = TextBox2.Text;
            Session["txt3"] = TextBox3.Text;
            Session["txt4"] = TextBox4.Text;
            Session["txt5"]= TextBox5.Text;
            Session["txt6"] = TextBox6.Text;
            Session["txt7"] = TextBox7.Text;
            Session["txt8"] = dropcandid.SelectedItem.Text;
            Session["txt9"] = dropemp.SelectedItem.Text;
            Session["place"] = dropaddress.SelectedItem.Text;




            Response.Redirect("~/agrmntprint.aspx");
        }

        

        protected void BindCandidates()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeName,EmployeeId from Employees where Desigantion1='Physician' order by EmployeeId desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropcandid.DataSource = dr;
            dropcandid.DataTextField = "EmployeName";
            dropcandid.DataValueField = "EmployeeId";
            dropcandid.DataBind();
            dropcandid.Items.Insert(0, new ListItem("", "0"));
        }

        protected void BindCandidates1()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeName,EmployeeId from Employees where Desigantion1='Physician' order by EmployeeId desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
        }

    }
}