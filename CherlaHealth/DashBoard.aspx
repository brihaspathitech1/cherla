﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="CherlaHealth.DashBoard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <style>
         .app-icon {
         padding: 35px;
         display: inline-block;
         margin: auto;
         text-align: center;
         border-radius: 16px;
         cursor: pointer;
         box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.15);
         }
         .app{ padding-top:85px}
         .app-icon .fa{font-size:25px; color:#fff; }
         .fa-calculator:before {
         content: "\f1ec";
         }
         #spotify {
         background: none !important;
         margin-top: -15px;
         background-attachment: relative;
         background-position: center center;
         min-height: 220px;
         width: 100%;
         -webkit-background-size: 100%;
         -moz-background-size: 100%;
         -o-background-size: 100%;
         background-size: 100%;
         -webkit-background-size: cover;
         -moz-background-size: cover;
         -o-background-size: cover;
         background-size: cover;
         }
         .nomar{ margin:0 !important}
      </style>



      
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="box">
        
        <!-- /.box-header -->
        <div class="box-body">
              <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
  <asp:Label ID="Label6" runat="server" Visible="false"></asp:Label>
    <section class="wraptop">
          <section class="">
        
        
        <div class="container-fluid">
                 
                  <div class="container-fluid">
                     <div class="col-lg-6" style="border-right:1px solid #cecece">
					 <p style="color:orange"><strong>Late Login</strong></p>
					 <marquee class="animated slideInUp" onmouseover="this.stop();" onmouseout="this.start();" >
                           <asp:DataList id="DataList1"   runat="server" RepeatColumns="500">
  
           <ItemTemplate>
                           <div class="col-md-12">
                              <div class="col-lg-6 col-md-6   mb text-center" style="margin-bottom:55px">
                                 <div class="content-panel pn" style="height: 240px !important;width:200px !important;">
                                    <div id="spotify ">
                                      <asp:Image ID="Image1"  width="198" Height="190"  ImageUrl='<%# "~/wb1.ashx?ImID=" + Eval("EmployeeID")%>' runat="server" /> 
                                       <h5 style="    color: #0058ff;font-weight:bold"><strong> <asp:Label id="lblBrandName"   runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EmployeeName") %>'></asp:Label></strong></h5>
                                       <div class="sp-title" style="padding-top:1px">
                                          <h5 style="color:#ff8300;font-weight:bold;margin-top: 5px;margin-bottom: 3px;"> <asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Desigantion11") %>'></asp:Label> </h5>
                                       </div>
                                    </div>
                                    <h4 class="followers nomar" style="
                                       background: rgba(255, 197, 108, 0.68);
                                       height: 25px;
                                       padding: 5px;
                                       border-radius: 3px;
                                       box-shadow: 2px 2px 2px #a2acff;
                                       "><span style="float:right; color:green !important"><i class="fa fa-clock-o"></i> <asp:Label id="Label2" class="colgreen" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InTime") %>'></asp:Label></span>  <span style="float:left; color:red !important"><i class="fa fa-clock-o"></i> <asp:Label id="Label3" class="colred" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LateComes") %>'></asp:Label></span></h4>
                                 </div>
                              </div>
                              
                           </div>
                              </ItemTemplate>
                         
       </asp:DataList>
                        </marquee>
                     </div>
                     <div class="col-lg-6" >
					 <p style="color:red"><strong>Absentees</strong></p>

                       <marquee class="animated slideInUp" onmouseover="this.stop();" onmouseout="this.start();" >
                           <asp:DataList id="DataList2"   runat="server" RepeatColumns="500">
  
           <ItemTemplate>
                           <div class="col-md-12">
                              <div class="col-lg-6 col-md-6   mb text-center" style="margin-bottom:55px">
                                 <div class="content-panel pn" style="height: 240px !important;width:200px !important;">
                                    <div id="spotify ">
                                       <asp:Image ID="Image2"   width="198" Height="190"  ImageUrl='<%# "~/wb2.ashx?ImID=" + Eval("EmployeeID")%>' runat="server" /> 
                                       <h5 style="    color: #0058ff;font-weight:bold"><strong><asp:Label id="Label4"   runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EmployeeName") %>'></asp:Label></strong></h5>
                                       <div class="sp-title" style="padding-top:1px">
                                          <h5 style="color:#ff8300;font-weight:bold;margin-top: 5px;margin-bottom: 3px;"> <asp:Label id="Label7" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Desigantion11") %>'></asp:Label></h5>
                                       </div>
                                    </div>
                                   <h4 class="followers nomar" style="
                                       background: rgba(255, 197, 108, 0.68);
                                       height: 25px;
                                       padding: 5px;
                                       border-radius: 6px;
                                       box-shadow: 2px 2px 2px #a2acff;
                                       "><span style="text-align:center; color:red !important">  <asp:Label id="Label8"   runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "IsAbsent") %>'></asp:Label></span></h4>
                                  </div>
                              </div>
                              
                           </div>
                         </ItemTemplate>
                         
       </asp:DataList>
   
                           
                        </marquee>
                     </div>
                     </div>

                     <div class="container-fluid">

                     <div class="col-lg-6" style="border-right:1px solid #cecece;border-top:1px solid #cecece">
					 <p style="color:blue"><strong>Early Login</strong></p>
                        <marquee class="animated slideInUp" onmouseover="this.stop();" onmouseout="this.start();" >
                             <asp:DataList id="DataList4"   runat="server" RepeatColumns="500">
  
           <ItemTemplate>
                           <div class="col-md-12">
                              <div class="col-lg-6 col-md-6   mb text-center" style="margin-bottom:55px">
                                 <div class="content-panel pn" style="height: 240px !important;width:200px !important;">
                                    <div id="spotify ">
                                     <asp:Image ID="Image3"    width="198" Height="190"  ImageUrl='<%# "~/wb4.ashx?ImID=" + Eval("EmployeeID")%>' runat="server" /> 
                                       <h5 style="    color: #0058ff;font-weight:bold"><strong> <asp:Label id="Label9"   runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EmployeeName") %>'></asp:Label></strong></h5>
                                       <div class="sp-title" style="padding-top:1px">
                                          <h5 style="color:#ff8300;font-weight:bold;margin-top: 5px;margin-bottom: 3px;"> <asp:Label id="Label10" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Desigantion11") %>'></asp:Label> </h5>
                                       </div>
                                    </div>
                                    <h4 class="followers nomar" style="
                                       background: rgba(255, 197, 108, 0.68);
                                       height: 25px;
                                       padding: 5px;
                                       border-radius: 3px;
                                       box-shadow: 2px 2px 2px #a2acff;
                                       "><span style="float:right; color:green !important"><i class="fa fa-clock-o"></i> <asp:Label id="Label11" class="colgreen" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InTime") %>'></asp:Label></span>  <span style="float:left; color:red !important"><i class="fa fa-clock-o"></i>  <asp:Label id="Label12" class="colgreen" style="color:#0093FF !important;" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EarlyComes") %>'></asp:Label></span></h4>
                                 </div>
                              </div>
                                </div>
                                 </ItemTemplate>
                         
       </asp:DataList>
   

                        </marquee>
                     </div>
                     <div class="col-lg-6" style="border-top:1px solid #cecece">
					 <p style="color:orange"><strong>Early Logout</strong></p>
                        <marquee class="animated slideInUp" onmouseover="this.stop();" onmouseout="this.start();" >
                           <asp:DataList id="DataList3"    runat="server" RepeatColumns="500">
  
           <ItemTemplate>
                           <div class="col-md-12">
                              <div class="col-lg-6 col-md-6   mb text-center" style="margin-bottom:55px">
                                 <div class="content-panel pn" style="height: 240px !important;width:200px !important;">
                                    <div id="spotify ">
                                      <asp:Image ID="Image4"   width="198" Height="190"  ImageUrl='<%# "~/wb3.ashx?ImID=" + Eval("EmployeeID")%>' runat="server" />
                                       <h5 style="    color: #0058ff;font-weight:bold"><strong> <asp:Label id="Label13"   runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EmployeeName") %>'></asp:Label></strong></h5>
                                       <div class="sp-title" style="padding-top:1px">
                                          <h5 style="color:#ff8300;font-weight:bold;margin-top: 5px;margin-bottom: 3px;"> <asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Desigantion11") %>'></asp:Label></h5>
                                       </div>
                                    </div>
                                    <h4 class="followers nomar" style="
                                       background: rgba(255, 197, 108, 0.68);
                                       height: 25px;
                                       padding: 5px;
                                       border-radius: 3px;
                                       box-shadow: 2px 2px 2px #a2acff;
                                       "><span style="float:right; color:green !important"><i class="fa fa-clock-o"></i> <asp:Label id="Label15" class="colgreen" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OutTime") %>'></asp:Label></span>  <span style="float:left; color:red !important"><i class="fa fa-clock-o"></i> <asp:Label id="Label16" class="colred" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EarlyGoes") %>'></asp:Label></span></h4>
                                 </div>
                              </div>
                              
                           </div>

                            </ItemTemplate>
                         
       </asp:DataList>
   
                        </marquee>
                     </div>
                  </div>
               </div>
   
           
</section>
                 </section>
            </div>
         </div>
</asp:Content>
