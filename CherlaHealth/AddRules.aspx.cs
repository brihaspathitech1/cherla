﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace CherlaHealth
{
    public partial class AddRules : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                BindStudy2();
            }
        }
        private void BindStudy2()
        {

            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Tax ORDER BY Id DESC", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        protected void TextBox10_TextChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            SqlConnection conn33 = new SqlConnection(connstrg);
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lbldeleteid = (Label)row.FindControl("Id");
            conn33.Open();
            SqlCommand cmd3 = new SqlCommand("delete FROM Tax where Id='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'", conn33);
            cmd3.ExecuteNonQuery();
            conn33.Close();
            BindStudy2();

        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindStudy2();
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            SqlConnection conn34 = new SqlConnection(connstrg);
            int Id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString());
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lblID = (Label)row.FindControl("conn34");


            TextBox textName1 = (TextBox)row.Cells[0].Controls[0];
            TextBox textName2 = (TextBox)row.Cells[1].Controls[0];
            TextBox textName3 = (TextBox)row.Cells[2].Controls[0];
            TextBox textName4 = (TextBox)row.Cells[3].Controls[0];
            TextBox textName5 = (TextBox)row.Cells[4].Controls[0];
            TextBox textName6 = (TextBox)row.Cells[5].Controls[0];
            TextBox textName7 = (TextBox)row.Cells[6].Controls[0];
            TextBox textName8 = (TextBox)row.Cells[7].Controls[0];
            TextBox textName9 = (TextBox)row.Cells[8].Controls[0];
            TextBox textName10 = (TextBox)row.Cells[9].Controls[0];


            GridView1.EditIndex = -1;
            conn34.Open();
            //SqlCommand cmd = new SqlCommand("SELECT * FROM detail", conn);
            SqlCommand cmd = new SqlCommand("update Tax set Name1='" + textName1.Text + "',HRA='" + textName2.Text + "',EA='" + textName3.Text + "',DA='" + textName4.Text + "',CA='" + textName5.Text + "',IT='" + textName6.Text + "',LIC='" + textName7.Text + "',PF='" + textName8.Text + "',ESI='" + textName9.Text + "',MA='" + textName10.Text + "'  where Id='" + Id + "'", conn34);
            cmd.ExecuteNonQuery();
            conn34.Close();
            BindStudy2();
            //GridView1.DataBind();
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            BindStudy2();
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindStudy2();
        }
        protected void Button9_Click(object sender, EventArgs e)
        {

            SqlConnection conn = new SqlConnection(connstrg);

            String insert = "insert into Tax(Name1,HRA,EA,DA,CA,IT,LIC,PF,ESI,MA)values('" + TextBox3.Text + "','" + TextBox4.Text + "','" + TextBox5.Text + "','" + TextBox6.Text + "','" + TextBox7.Text + "','" + TextBox8.Text + "','" + TextBox9.Text + "','" + TextBox1.Text + "','" + TextBox2.Text + "','" + TextBox10.Text + "')";
            SqlCommand comm = new SqlCommand(insert, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("AddRules.aspx");

        }

        protected void Edit_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from Tax where Id='" + lblid.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                txtname.Text = dr["Name1"].ToString();
                txthouse.Text = dr["HRA"].ToString();
                txtea.Text= dr["EA"].ToString();
                txtda.Text= dr["DA"].ToString();
                txtca.Text= dr["CA"].ToString();
                txtit.Text= dr["IT"].ToString(); 
                txtLIC.Text= dr["LIC"].ToString();
                txtpf.Text= dr["PF"].ToString(); 
                txtESI.Text= dr["ESI"].ToString();
                txtma.Text= dr["MA"].ToString();
            }
            conn.Close();
            ModalPopupExtender1.Show();
        }

        protected void Update(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("update Tax set Name1='" + txtname.Text + "',HRA='" + txthouse.Text + "',EA='" + txtea.Text + "',DA='" + txtda.Text + "',CA='" + txtca.Text + "',IT='" + txtit.Text + "',LIC='" + txtLIC.Text + "',PF='" + txtpf.Text + "',ESI='" + txtESI.Text + "',MA='" + txtma.Text + "'  where Id='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindStudy2();
        }



    }
}