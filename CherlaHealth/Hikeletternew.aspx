﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Hikeletternew.aspx.cs" Inherits="CherlaHealth.Hikeletternew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <section id="main-content">
        <section class="wrapper site-min-height">
            <div class="row mt">
                <div class=" col-lg-12 panel" style="padding: 20px">
                    <div class=" col-lg-12">
 
    
    <!--our-quality-shadow-->
    <div class="clearfix"></div>
    <h4 class="heading1">Hike Letter</h4>
    <div class="tabbable-panel margin-tops4 ">
      <div class="tabbable-line">
       
        <div class="tab-content margin-tops">
       
            <div class="col-md-12">
             
                 <div class="row">
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <label>
                                Employee</label>
                            <asp:DropDownList ID="DropDownList2" class="form-control" OnSelectedIndexChanged="OrgType_SelectedIndexChanged"
                                AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="DropDownList2" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                  
                </div>
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label>
                                Previous Salary/Annum</label>
                            <asp:TextBox ID="Prev_Salary" class="form-control" runat="server"></asp:TextBox>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Prev_Salary"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="Prev_Salary" FilterType="Numbers"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <label>
                                Hike Salary/Annum</label>
                            <asp:TextBox ID="Hike_Salary" class="form-control" runat="server" OnTextChanged="Hike_Salary_TextChanged"
                                AutoPostBack="true"></asp:TextBox>
                                 <asp:Label ID="salry_words" runat="server" Text="" style="color:Green"></asp:Label>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="Hike_Salary"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Hike_Salary" FilterType="Numbers"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                      <div class="col-md-3">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <label>
                                Increment Salary</label>
                            <asp:TextBox ID="txtinc" class="form-control" runat="server"></asp:TextBox>
                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtinc" FilterType="Numbers"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                     <div class="clearfix"></div>
                <div class="col-md-3">
                    <label>
                        Hike Date</label>
                    <asp:TextBox ID="Hike_date" class="form-control" runat="server" ></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="Hike_date" runat="server" Format="dd-MM-yyyy" >
                    </cc1:CalendarExtender>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="Hike_date"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2" style="padding-top:23px">
                    <asp:Button ID="Button2" runat="server" class="btn btn-success" Text="Submit" OnClick="ClikItNow" />
                </div>
            </div>
             
             
              </div>
          </div>
         


      <%--  </div>--%>
      </div>
    </div>
  </div>
  </div>

  </div>
  </section>
  </section>
</asp:Content>
