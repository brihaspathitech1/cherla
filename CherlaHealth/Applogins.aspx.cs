﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.DataAccessLayer;

namespace CherlaHealth
{
    public partial class Applogins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            DataSet ds=DataQueries.SelectCommon("Select EmployeeId,EmployeName from Employees where status='1'");
            dropemployee.DataSource = ds;
            dropemployee.DataTextField = "EmployeName";
            dropemployee.DataValueField = "EmployeeId";
            dropemployee.DataBind();
            dropemployee.Items.Insert(0, new ListItem("", "0"));
        }

        protected void BindGrid()
        {
            DataSet ds= DataQueries.SelectCommon("Select e.EmployeName,e.CellNo,r.admin,r.designation,r.name,r.password from register r inner join Employees e on e.EmployeeId=r.Name and e.CompanyEmail=r.Admin order by r.Name desc");
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
        }

        protected void Employee_Change(object sender, EventArgs e)
        {
            DataSet ds=DataQueries.SelectCommon("Select CompanyEmail,Desigantion from Employees where EmployeeId='" + dropemployee.SelectedValue + "'");
           
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblmail.Text = ds.Tables[0].Rows[0]["CompanyEmail"].ToString();
                lbldesignation.Text = ds.Tables[0].Rows[0]["Desigantion"].ToString();
            }
        }
        protected void Submit(object sender, EventArgs e)
        {
            if (dropemployee.SelectedItem.Text != "" && droprole.SelectedItem.Text != "" && txtpwd.Text != "")
            {
                DataSet ds= DataQueries.SelectCommon("Select * from register where Name='" + dropemployee.SelectedValue + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("<script>alert('Duplicate Entry')</script>");
                }
                else
                {
                    DataQueries.InsertCommon("Insert Into register(Name,Admin,Email,Designation,Password,Repassword) values('" + dropemployee.SelectedValue + "','" + lblmail.Text + "','" + droprole.SelectedItem.Text + "','" + lbldesignation.Text + "','" + txtpwd.Text + "','EmployeeHomePage')");
                  
                    Response.Redirect("AppLogins.aspx");

                }
            }

        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
           DataQueries.DeleteCommon("Delete from register where Name='" + id.ToString() + "'");
            BindGrid();
        }
    }
}