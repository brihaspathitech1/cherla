﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="docupload.aspx.cs" Inherits="CherlaHealth.docupload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
    h5{font-weight: bold !important;
    color: #0990da !important;}
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Upload Documents</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <h5>  Employee Details</h5>
            <hr />
            <div class="col-md-3">
                <div class="form-group">
                    
                    Employee Name<asp:DropDownList ID="dropemp" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Resume <asp:FileUpload ID="fileresume" runat="server" />
                    </div>
                </div>
            <div class="clearfix"> </div>

            <h5>  Degree Certificates</h5>
            <hr />

             <div class="col-md-2">
                <div class="form-group">
                  
                    <asp:FileUpload ID="filedeg1" runat="server" /> 
                     </div>
                 </div>
              <div class="col-md-2">
                <div class="form-group">
                  
                    <asp:FileUpload ID="filedeg2" runat="server" />
                      </div>
                 </div>
              <div class="col-md-2">
                <div class="form-group">
                    <asp:FileUpload ID="filedeg3" runat="server" />
                      </div>
                 </div>
              <div class="col-md-2">
                <div class="form-group">
                    <asp:FileUpload ID="filedeg4" runat="server" />
                      </div>
                 </div>
              <div class="col-md-2">
                <div class="form-group">
                    <asp:FileUpload ID="filedeg5" runat="server" />
                      </div>
                 </div>
     <div class="clearfix"></div>
                   <h5>  License Documents</h5>
            <hr />


            <div class="col-md-6">
                <h5>MCI</h5>
                <div class="row">
                    <div class="col-md-12">
                              MCI Expire Date <asp:TextBox ID="txtmci" CssClass="form-control" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtmci" />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtmci" runat="server" ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                  

                    </div>

                </div>

                 <div class="row">
                    <div class="col-md-12">
                         Document  <asp:FileUpload ID="filelicense1" runat="server" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="filelicense1" runat="server" ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                   
                        </div>
                     </div>

            </div>

              <div class="col-md-6">
               
                    <h5>PCI</h5>
                <div class="row">
                    <div class="col-md-12">
                         PCI Expire Date <asp:TextBox ID="txtsci" CssClass="form-control" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtsci" />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtsci" runat="server" ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                  
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-md-12">
                         Document  <asp:FileUpload ID="filelicence2" runat="server" /><br />
              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="filelicence2" runat="server" ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
               
                        </div>
                        </div>

            </div>









            <div class="clearfix"></div>
             <h5>  Others Documents</h5>
            <hr />
            <div class="col-md-2">
                <div class="form-group">
                   
                     <asp:FileUpload ID="fileother1" runat="server" />
                    </div>
                </div>
                     <div class="col-md-2">
                <div class="form-group">
                   

                     <asp:FileUpload ID="fileother2" runat="server" /> 
                     </div>
                </div>
                     <div class="col-md-2">
                <div class="form-group">
                     <asp:FileUpload ID="fileother3" runat="server" />
                     </div>
                </div>
                     <div class="col-md-2">
                <div class="form-group">
                     <asp:FileUpload ID="fileother4" runat="server" />

                     </div>
                </div>
                     <div class="col-md-2">
                <div class="form-group">
                     <asp:FileUpload ID="fileother5" runat="server" />
                     </div>
                </div>
                   
                    
               <div class="col-md-3">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                    </div>
                   </div>
            </div>
        </div>
</asp:Content>
