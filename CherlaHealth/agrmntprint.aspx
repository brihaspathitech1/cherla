﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agrmntprint.aspx.cs" Inherits="CherlaHealth.agrmntprint" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>DOCTOR AGREEMENT</title>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
                
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }




        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            
            #Button1 {
                display: none;
             
            }
          

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint {
                display: none;
            }
            input#btnpdf {
                display: none;
            }
            
            a {
                display: none;
            }
            
  
        }




        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
    <style>
    .page21 {
            width:100%;background:#d3d3d357;text-align:justify;padding:150px 50px 0px 70px;
        }
        tr, td {
            /*border:1px solid black;
            border-collapse:collapse;
            padding:15px;*/
      
       }
        .tab-text { margin-left:40px;font-weight:bold;
        }
        ol {
            margin-left:50px;
        }
   </style> 
    <style>
        .f{
            text-align:right;
        }
    </style>
    <style>
        .g
        {
            text-align:center;
        }
    </style>
</head>
<body style="color:black;">
     <form id="form1" runat="server">
    <asp:Panel ID="Panel1" runat="server">
    <div class="page21">    
   
        <div style="text-align:center;padding-top:25px ;">

             <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                <a href="docagrmnt.aspx" id="btnPrint" type="button"  onclick="goBack()"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>

            <asp:Button ID="btnpdf"  CssClass="btn btn-success" runat="server" Text="PDF" OnClick="pdf_Click"  visible="false"/>
            
            <%--<asp:Button ID="btngmail" CssClass="btn btn-success" runat="server" Text="Gmail" OnClick="btngmail_Click" />--%>
        </div>
     <br /><br />
              <table width="100%">
                <tr>
                    <td>
                       <%-- <img id="img1" alt="/" src="sign board.jpg" width="1250px" height="120px" />--%>
                     <%--    <img id="img1" runat="server" src="~/sign board.png" width="1250px" height="120px" />--%>
                       <%-- <asp:Image ID="Image1" ImageUrl="~/sign board.jpg" Width="1250px" Height="120px" runat="server" />
                       --%> <br />
                       <div style="text-align:center;" >Cherla health Pvt Ltd, 101, Optimus Prime, Plot 124, Lumbini avenue, Old Mumbai Highway, Gachibowli, Hyderabad 500032<br />

Phone: +91 9182561616 | Email: frontoffice@cherlahealth.com | www.cherlahealth.com</div>

                    </td>
                </tr>
            </table>
        <br /><br />

       <p>Date:&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbl1" runat="server"></asp:Label></p>

        <br />
        <br />

        <p>To&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbl8" runat="server"></asp:Label></p>

         <br />
        <br />
        <p>Dear&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbl9" runat="server"></asp:Label></p>
        <br />
        <br />
        <p>Cherla Health (P) Ltd., (“We”) is engaged in providing Health care services including tele-consultation and counselling to our customers/users. We provide Medical consultation along with online medical consultation using our proprietary virtual platform (Telemedicine Platform) to our customers.</p>

        <br />
        <p>On your representation that you are a qualified medical practitioner having registered membership of the Medical Council of India and capable of providing medical advice / diagnosis in India, we are pleased to appoint you as one of our consultants on terms and conditions mentioned in this Agreement (“Agreement”).</p>
        <br />
        <p>1.&nbsp;&nbsp;	We appoint you as a consultant to provide medical consultation services to our patients at our designated clinics and through our Telemedicine Platform. You shall be available for any number of hours and days in a week as we may decide from time to time.</p>
        <p>2.&nbsp;&nbsp;See Annexure 1 for date of Appointment.</p>
        <p>3.	This agreement creates an independent consultant relationship and not an employment relationship between us. </p>
        <p>4.	Your appointment shall be for a period of 12 months unless terminated by any of the parties with a prior written notice of 30 days for the reasons set forth in Clause 21 herein. This appointment shall automatically be extended for additional Eleven months renewal terms unless either party gives written notice to terminate this Agreement at least thirty (30) days prior to the end of the preceding term.</p>
        <p>5.	You are required to be "on duty" or "on call" at night or on weekends and holidays as solely determined by us and mutually agreed upon, unless exempted by us in writing.</p>
        <p>6.	You shall have no objection to record all consultations provided by you to patients through audio and video methods as part of Electronic Health Records.</p>
        <p>7.	Assignment, Transfer and Deputation: Though you have been engaged to a specific position, Cherla Health Pvt Ltd reserves the right to send you on deputation/transfer/assignment to any of Cherla Health Pvt Ltd’s branch clinics in Hyderabad, whether existing at the time of your appointment or to be set up in the future.</p>
        <p>8.	You must undergo training provided by us, abide by our policies as modified by us from time to time, and to acquaint yourself with the technology, mandatory protocols, and procedures which you need to follow while you are providing the Services to the patients. You shall use your best efforts and skill in providing the Services to the patient.</p>
           
        <p>9.	You shall spend such additional time as necessary to provide medical services which arise from Physician's practice with our medical centres including but not limited to (a) making hospital rounds, when necessary, (b) preparing, updating and uploading of paper and electronic medical records, (c) participating in appropriate educational and training programmes mandated by us, (d) participating in our medical committees or our educational  meetings, training, and compliance etc., (e) providing virtual health care services and patient-specific education and (f) providing all services that result therefrom.</p>
        <p>10.	You are required</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.	to periodically attend such educational programs as is deemed necessary to maintain state registration with THE MEDICAL COUNCIL and improve Physician's professional skills;</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.	to maintain a designated cell phone with internet with you at all times and also maintain a reliable Wi Fi connection at your home to carry out online consultation and deliver health care services to patients, if and when needed.  </p>
        <p>11.	You shall take good care of the equipment and software provided by us and shall use them only for the purposes of delivering our services and return the same to us on termination of your appointment.</p>
        
        <p><b>12.	You shall not directly solicit with the patients for physical examination and treatment beyond our Polyclinic/ medical centres without intimation to us. You shall not refer the patients to any medical facility, specialist clinics, individual, lab, diagnostic centres, hospital, Nursing Home, or any other allied or medically related individuals or facilities in lieu of any payments, commissions or other emoluments either in the form of cash or in kind of any form, unless approved by us in writing. </b></p>
             <p>13.	We pay you a monthly payment for the Services rendered by you to the patient as mentioned in Annexure 1, or as modified from time to time, in the immediately preceding calendar month, subject to the deductions, benefits, and taxes.</p>

             <p>14.	If a variable pay component is involved, this will be at the sole discretion of Cherla Health Pvt Ltd, and you cannot dispute our judgement in determining this. The quality measures used to determine variable pay will be at the sole discretion of the management and may change. Cherla Health Pvt Ltd has no obligation to disclose these criteria to you in advance or later. </p>
             <p>15.	Apart from the medical services, you are required to perform administrative services as may be required at our medical centres as part of the delivery of health care services to patients. No separate or additional fee or remuneration is paid to you for rendering such administrative services, unless specifically mentioned in Appendix 1.</p>
                 <p>16.	PATIENT CHARGES. Fees for professional services performed by you shall be determined by us. All payments received from patients shall be collected by us and always remain our property.  You shall cooperate with us in preparing all necessary paperwork to establish charges to patients for physician services rendered by you and shall assist us in billing for such services directly to patients or to third-party payers as appropriate. Physician hereby assigns to Cherla health Pvt. Ltd. Physician's right to bill for all services rendered under this Agreement.</p>
            
        <p>17.SECRECY/CONFIDENTIALITY: You will not during your association with Cherla Health Pvt Ltd or at any time thereafter, divulge or disclose to any person
whomsoever, make any use whatsoever for your own purpose or for any other
purpose other than that of Cherla Health Pvt Ltd, of any information or knowledge
obtained by you during your Association with Cherla health private Ltd. This
includes information related to all business and or affairs of Cherla Health Pvt Ltd
including development, processes and systems including algorithms, protocols,
guidelines, reports and reporting systems whatsoever. Also, you will during your
Association hereunder also use your best endeavour to prevent any other person
from doing so. The terms of this agreement, our business information, know-how,
medical records of the patients are confidential information and shall not be
disclosed to anyone. On termination, you shall return to us of all confidential
information.</p>
        <p>18.	INTELLECTUAL PROPERTY RIGHT: Copyright and other intellectual property rights in all works created by you during the course of providing Services will belong to us. If during the period of your Association with us you achieve any invention, process improvement, operational improvement, or other process/method likely to result in more efficient operation of any of the activities of Cherla Health Pvt Ltd, Cherla Health Pvt Ltd shall be entitled to use, utilize and exploit such improvement and you shall assign all rights thereof to Cherla Health Pvt Ltd for the purpose of seeking any patent rights or for any other purpose. Cherla Health Pvt Ltd shall have the sole ownership rights of all the intellectual property rights that you may create during the tenure of association with Cherla Health Pvt Ltd including but not limited to the creative concept that you may develop during your association with Cherla Health Pvt Ltd.</p>
        <p ><b>19.	You shall always have appropriate professional indemnity insurance coverage towards medical negligence during the agreement and keep us informed regularly.</b></p>
        <p>20.	You will indemnify us against any loss or damage which may arise due to (a) breach of the terms herein, and (b) medical negligence and c) any and all loss, damage, cost, and expense that is in any way related to the physician’s performance or failure to perform the services, responsibilities, and duties expected from the physician.</p>
        <p>21.	If you sustain any loss or damage by actions of patients during or subsequent to the consultation, we will not be liable.</p>
        <p><b>22. AGREEMENT NOT TO COMPETE.</b>In consideration of the compensation, marketing and benefits provided for herein, you specifically agree on the following reasonable restriction on your right to practice medicine in competition with the Cherla health Pvt. LTD</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbspa. During the term of this Agreement and twelve (12) months after the termination of this Agreement, you shall not, without our prior written consent which we may withhold in our sole discretion, in any manner, directly or indirectly, in any capacity whatsoever, on your own behalf or as an agent or representative or an employee of any person or entity, engage in, participate in, provide services, assistance or advice to, own, manage, operate, control, be connected with or become interested in, financially or otherwise, any business providing professional medical services, either directly or indirectly through agents, employees, independent consultants or contractual arrangements, in competition with the business of Cherla health Pvt. LTD.</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. The territorial scope of such prohibition shall be five (05) air kilometres from the locations from where Physician actually practiced during his/her relationship with Cherla Health Pvt. LTD.</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Physician agrees by signing this contract that Physician has been provided valuable consideration in exchange for Physician&#39;s agreement not to compete, and that the time and territorial scope of the non-compete is reasonable in all respects.</p>
                
        <p>23.	TERMINATION.  This appointment shall be terminated upon the happening of any of the following events:</p>
        <p>a.	Revocation, termination or suspension of Physician's license to practice medicine by the medical council of India;</p>
        <p>b.	Upon dissolution of Cherla Health (P) Ltd; </p>
        <p>c.	Upon the disability of Physician; </p>
        <p>d.	The filing of voluntary or involuntary bankruptcy by Physician or Cherla Health (P) Ltd;</p>
        <p>e.	If Physician fails to meet the qualifications necessary of a physician pursuant to the terms of any agreement that we may have with any other health provider that we may serve;</p>
        <p><b>f.	If you fail to perform Physician's duties as specified and required in this Agreement to a level and degree that is satisfactory to us;</b></p>
        <p><b>g.	Whenever we make the determination in good faith and upon investigation, review, and consultation with the appropriate medical staff that you are not providing adequate patient care or that the safety of patients is jeopardized by continuing you in the service;</b></p>
        <p>h.	The violation by Physician of any of the provisions of this Agreement;</p>
        <p>i.	If you are guilty of, or plead guilty to a crime involving moral turpitude;</p>
        <p>j.	Medically confirmed chronic alcohol or drug abuse by you;</p>
        <p>k.	If you are negligent in performing your duties or violate any of the terms of this agreement or bye-laws or any rules, regulations, policies or protocols of Cherla Health (P) Ltd.</p>
        <p>l.	If your conduct or demeanour results in, or you refuse to or are unable to work with patients, clients, other medical & administrative staff members, members of other health disciplines or any other group companies in a cooperative, professional manner that is essential for maintaining an environment appropriate to quality and efficient patient care.</p>
               
        <p>m.	It must be specifically understood that this offer is made based on your proficiency on technical/professional skills you have declared to possess as per your application for Association and your ability to handle any assignment/job independently. In case at a later date, any of your statements/particulars furnished are found to be false or misleading or your performance is not up to the mark or falls short of the minimum standard set by Cherla Health (P) Ltd, Cherla Health (P) Ltd shall have the right to terminate your services forthwith without giving any notice notwithstanding any other terms and conditions stipulated therein.</p>
        <p>24. In the event that you decide to leave Cherla Health (P) Ltd, the following shall apply as is the case: -
</p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will be required to give at least one month notice of resignation to Cherla Health (P) Ltd or pay to Cherla Health (P) Ltd one month’s salary in lieu of the notice. You will forfeit any amount in holding with Cherla Health (P) Ltd if the one-month notice is not given and kept.
        </p>
        <p>25.	Cherla Health (P) Ltd will provide you extensive marketing during your association with us and expects you to return the investment made on this marketing in the form of personal value addition to Cherla Health (P) Ltd. Any materials provided to you are property of Cherla Health (P) Ltd and confidential information. Carrying the materials outside Cherla Health (P) Ltd or disclosing them to anyone other than employees, associates or consultants of Cherla Health (P) Ltd is a breach of trust and will result in immediate forfeiture of any holding amount and termination of association without notice.</p>
        <p>26.	Spreading rumours, soliciting employees, associates or consultants of Cherla Health (P) Ltd for outside Association or coercing co-workers to leave Cherla Health Pvt Ltd is strictly prohibited and frowned upon. Such actions will result in immediate termination of Association without notice and forfeiture of any holding amount.</p>

<p>The above terms and conditions are based on Cherla Health Pvt Ltd’s policy, procedures and other rules currently applicable in India and are subject to amendments and adjustments from time to time. In all matters, including those not specifically covered here such as traveling, retirement, etc. you will be governed by the rules of Cherla Health (P) Ltd as shall be in force from time to time.</p>

<p>In the case of a dispute between us, the courts at Hyderabad shall have exclusive jurisdiction. </p>

<p>Please sign the duplicate copy of this agreement and return the same in confirmation of your understanding and acceptance of the appointment and all other terms and conditions as stated therein <br /><br />Sincerely,<br /> <br />For <b>Cherla Health Private Limited</b><br /></p>
        <p><b>Gautam Cherla</b><br /><b>Founder and Director</b><br /><b>Cherla Health (P) LTD</b></p>
                     
        <br />
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u> Acceptance of the Consultant</u></b></p>
        <p>
            &nbsp;&nbsp; I, hereby, agree to abide by the terms and conditions of this Agreement and confirm the representations,
        </p>
        
       
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; warranties, and undertakings made herein.</p>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <p>Date: &nbsp;&nbsp;<asp:Label ID="lbld1" runat="server"></asp:Label></p>
        <br />
        <br />
        <p>Place: &nbsp;&nbsp;<asp:Label ID="Lblp1" runat="server"></asp:Label></p><div class="f">(Signature of the Physician)</div>
        
       <div class="g"><p>ANNEXURE 1 </p></div>
       
        <table   class="g" >
            <tr>
                <td style="border:1px solid black;border-collapse:collapse;padding:5px">
                 <p class="tab-text">S.No</p></td>
                <td style="border:1px solid black;border-collapse:collapse;padding:15px" class="auto-style1">
                 <p class="tab-text">Particulars</p></td>
                    <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                 <p class="tab-text">Details</p> </td>   
                
            </tr>
            <tr>
                <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text">1</p></td>
                    <td style="border:1px solid black;border-collapse:collapse;padding:15px" class="auto-style1">
                     <p class="tab-text">Appointment will commence from</p></td>
                        <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text"><asp:Label ID="lbl2" runat="server"></asp:Label></p></td>
                               </tr>

            <tr>
                <td style="border:1px solid black;border-collapse:collapse;padding:15px"> 
                    <p class="tab-text">2</p></td>
                    <td style="border:1px solid black;border-collapse:collapse;padding:15px" class="auto-style1"> 
                    <p class="tab-text">Timings: </p>
                        <p class="tab-text">
                            Current Work Location:</p>
                </td>
                 <td style="border:1px solid black;border-collapse:collapse;padding:15px"> 
                    <p class="tab-text"><asp:Label ID="lbl3" runat="server"></asp:Label></p>
                     <p class="tab-text"><asp:Label ID="lbl4" runat="server"></asp:Label></p>

                 </td>
                               

                </tr>
            <tr>
                 <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text">3</p></td>
                    <td style="border:1px solid black;border-collapse:collapse;padding:15px" class="auto-style1">
                     <p class="tab-text">MCI Number:</p></td>
                        <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text"><asp:Label ID="lbl5" runat="server"></asp:Label></p></td>

            </tr>
            <tr>
                 <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text">4</p></td>
                    <td style="border:1px solid black;border-collapse:collapse;padding:15px" class="auto-style1">
                     <p class="tab-text">Specialization:</p></td>
                        <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text"><asp:Label ID="lbl6" runat="server"></asp:Label></p></td>
            </tr>
            <tr>
                 <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text">5</p></td>
                    <td style="border:1px solid black;border-collapse:collapse;padding:15px" class="auto-style1">
                   
                        <p class="tab-text">Qualifications:</p>
                        <p class="tab-text">Other Certifications if any:</p>
                    </td>
                        <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text"><asp:Label ID="lbl7" runat="server"></asp:Label></p></td>

            </tr>
            <tr>
                 <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text">6</p></td>
                    <td style="border:1px solid black;border-collapse:collapse;padding:15px" class="auto-style1">
                     <p class="tab-text">Monthly Professional Fee for</p>
                        <p class="tab-text">
                            Resident Physicians:</p>
                 </td>
                        <td style="border:1px solid black;border-collapse:collapse;padding:15px">
                     <p class="tab-text">Cherla Health Resident Physician Pay Package</p>
                            <p class="tab-text">Fixed Pay for evening hours&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p> 
                            <p class="tab-text">
                                Base Pay &gt; 5 Years After MBBS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs. 300&nbsp;&nbsp; per hour&nbsp;&nbsp; 6pm- 9pm</p>
                            <p class="tab-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt; 5 years after MBBS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs.250&nbsp;&nbsp;&nbsp;&nbsp; per hour&nbsp;&nbsp; 6pm-9pm&nbsp;&nbsp; </p>&nbsp; 
                            
                            <p class="tab-text">Fixed Pay for Morning hours&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p> 
                            <p class="tab-text">
                                Base Pay &gt; 5 Years After MBBS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs. 250&nbsp;&nbsp; per hour&nbsp;&nbsp; 9am- 1pm</p>
                            <p class="tab-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt; 5 years after MBBS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs.200&nbsp;&nbsp;&nbsp;&nbsp; per hour&nbsp;&nbsp; 9am-1pm&nbsp;&nbsp; </p>&nbsp; 
                            <p class="tab-text">Fixed Pay for Sunday Morning hours&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p> 
                            <p class="tab-text">
                                Base Pay &gt; 5 Years After MBBS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs. 300&nbsp;&nbsp; per hour&nbsp;&nbsp; 9am- 11pm</p>
                            <p class="tab-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt; 5 years after MBBS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs.250&nbsp;&nbsp;&nbsp;&nbsp; per hour&nbsp;&nbsp; 9am-11pm&nbsp;&nbsp; </p>&nbsp;
                            <p class="tab-text">Physician Incentive Package - Rs. 2500 at
Management Discretion.</p> 
                            <p class="tab-text">Criteria foreleigibility to Incentive Based on</p>
                            
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.Timeliness &amp; punctuality
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.Patient feed back, use of EMR and
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; protocols, prior intimation of leave and
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; communication
                              <br />
                              <p class="tab-text">Productivity Incentives</p>
                            
                            
                            <p class="tab-text">
                                To be determined if &gt; 250 paid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Based on Number</p>
                            <p class="tab-text">
                                patients seen per month&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Of&nbsp; Paid Patiants</p>
                            <p class="tab-text">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; seen&nbsp;</p>
                            
                            
                        </td>

            </tr>
           
            
        

   
             </table>
        </div>
        
        
        <p>We pay you a monthly payment for the Services rendered by you to patients as mentioned in Annexure 1, or as modified from time to time, in the immediately preceding calendar month, subject to the deductions, benefits, and taxes. Net Pay will be determined after deduction for TDS, Professional tax and any other government mandated Taxes/ deductions. </p>
        <p>You will be paid based on the agreement above.</p>
        <p>a.	No separate fee is paid for consultation/s</p>
        <p>b.	No separate fee is paid for administrative duties</p>
        <p>c.	No separate fee is paid for clarification of the use of medication</p>
        <p>d.	No separate fee is paid on verification of diagnostic reports followed by consultation and for prescription of drugs</p>
        <br />
        <br />
        <p>Please sign the duplicate copy of this Appendix-1 and return the same in confirmation of your understanding and acceptance of the appointment and all other terms and conditions as stated therein.</p>
        <br />
        <br />

        <p>Sincerely, </p>
        <br />
        <br />
        <p>For Cherla Health (P) Limited</p>
        <p>Gautam Cherla </p>
        <p>Founder and Director</p>
        <p>Cherla Health (P) LTD</p>

        <div class="g"><p><u>Acceptance of the Consultant</u></p></div>
        <p>I, hereby, agree to abide by the terms and conditions of this Agreement and confirm the representations, warranties, and undertakings made herein.</p>
        <p>Date:&nbsp;&nbsp;<asp:Label ID="lbld2" runat="server"></asp:Label></p>
        <p>Place:&nbsp;&nbsp;&nbsp;<asp:Label ID="lblplace" runat="server"></asp:Label> </p> <div class="f">(Signature of the Physician)</div>
    
    
         </asp:Panel>  
         </form>
</body>
</html>