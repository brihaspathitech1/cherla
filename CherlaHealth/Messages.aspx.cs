﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace CherlaHealth
{
    public partial class Messages : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindData();
            }
        }

        protected void BindEmpName()
        {
            if (Session["name"].ToString() != "admin")
            {
                SqlConnection conn = new SqlConnection(connstrg);
                SqlCommand cmd = new SqlCommand("Select EmployeName from Employees where EmployeeId='" + Session["name"].ToString() + "'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    lblname.Text = dr["EmployeName"].ToString();
                }
                conn.Close();
            }
        }
        protected void BindData()
        {
            BindEmpName();
            SqlConnection conn = new SqlConnection(connstrg);
            if (Session["name"].ToString() == "admin")
            {
                //int columncount = GridView1.Rows[0].Cells.Count;
                //GridView1.Rows[0].Cells.Clear();
                //GridView1.Rows[0].Cells.Add(new TableCell());
                //GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                //GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
            else
            {
                SqlCommand cmd = new SqlCommand("Select l.LeaveId,convert(varchar,l.FromDate,113) as FromDate,convert(varchar,l.ToDate,113) as ToDate,e.EmployeName,s.BeginTime,s.EndTime from LeavesStatus l inner join Employees e on l.EmployeedID=e.EmployeeId inner join Shifts s on e.shift=s.ShiftId where l.empreplace='" + lblname.Text+"' and l.Status='Approved' order by l.LeaveId desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
    }
}