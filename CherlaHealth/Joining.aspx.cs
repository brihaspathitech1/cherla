﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace CherlaHealth
{
    public partial class Joining : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCandidates();
                // Bindaddress();
            }
        }
        protected void BindCandidates()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Name,Id from Candidates where status='Selected' order by id desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropcandidate.DataSource = dr;
            dropcandidate.DataTextField = "Name";
            dropcandidate.DataValueField = "Id";
            dropcandidate.DataBind();
            dropcandidate.Items.Insert(0, new ListItem("", "0"));
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            Session["custname"] = dropcandidate.SelectedItem.Text;
            

            Session["des"] = txtdesignation.Text;
            Session["jdate"] = joindate.Text;
            Session["tme"] = jointime.Text;
            //Session["txt1"] = TextBox1.Text;
            Session["txt2"] = TextBox2.Text;
            Session["txt3"] = TextBox3.Text;
        
            Response.Redirect("~/Joinprint.aspx");
        }

        protected void dropcandidate_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select JPosition,Address from Candidates where id='" + dropcandidate.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                txtdesignation.Text= dr["JPosition"].ToString();
                TextBox2.Text = dr["Address"].ToString();
            }
        }
    }
}