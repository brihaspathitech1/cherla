﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace CherlaHealth
{
    public partial class ExperianceLetterPrint : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if(!IsPostBack)
                {
                   // lbldate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
                    string date1 = Session["leavedate"].ToString();
                    //DateTime date = DateTime.Parse(date1);

                   lblleave.Text = date1;
                  
                    lblconduct.Text = Session["conduct"].ToString();
                    //lblreasons.Text = Session["reason"].ToString();
                }
                if(!IsPostBack)
                {

                    Binddetails();
                }
            }
        }

        protected void Binddetails()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select e.Desigantion,e.DOJ,e.EmployeName,e.DOJ,e.EmployeeType,e.Salary,c.Email from Employees e inner join Companies c on e.CompanyId=c.CompanyId  where EmployeeId='" + Session["employid"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                lblname.Text = dr["EmployeName"].ToString();
                DateTime date = DateTime.Parse(dr["DOJ"].ToString());
                 lbljoin.Text = date.ToString("dd-MM-yyyy");
                
                lbldesig.Text = dr["Desigantion"].ToString();
               // lbltype.Text = dr["EmployeeType"].ToString();
                //lbllocation.Text = dr["Email"].ToString();
                lblsal.Text = dr["Salary"].ToString();
            }
            conn.Close();
        }

        protected void pdf_Click(object sender, EventArgs e)
        {
            
            //Image1.Width = 100;
            //Image1.Height = 100;
            //Image1.Visible = false;
            //head1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Experiance Letter.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
           
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            string imagePath = Server.MapPath("sign board.jpg") + "";

            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            image.Alignment = Element.ALIGN_TOP;

            image.SetAbsolutePosition(40, 700);

            image.ScaleToFit(500f, 400f);
            image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            pdfDoc.Add(image);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

        }


        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
    }
}