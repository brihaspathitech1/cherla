﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Applogins.aspx.cs" Inherits="CherlaHealth.Applogins" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Create App Logins</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row mt">
             <%--   <div class=" col-lg-12 panel" style="padding: 5px; margin-top: -30px">--%>


                    <div class=" col-lg-12">
                        <%--<div class=" col-lg-12">
                            <h3><span style="float: right"></span></h3>
                            &nbsp;
                        </div>--%>
                        <asp:UpdatePanel ID="Update" runat="server">
                            <ContentTemplate>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        Employee Name<span class="style1">*</span><asp:DropDownList ID="dropemployee" CssClass="form-control" OnSelectedIndexChanged="Employee_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="dropemployee" InitialValue="0" Display="Dynamic" ForeColor="Red" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Role<span class="style1">*</span><asp:DropDownList ID="droprole" CssClass="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Individual</asp:ListItem>
                                            <asp:ListItem>Manager</asp:ListItem>
                                            <asp:ListItem>Administrator</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="droprole" ForeColor="Red" runat="server" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Password<span class="style1">*</span><asp:TextBox ID="txtpwd" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtpwd" ForeColor="Red" runat="server" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <div class="col-md-3" style="padding-top: 20px">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit" />
                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <div class="col-md-12">
                                    <h4>View Logins:</h4>
                                    <asp:GridView ID="GridView1" AutoGenerateColumns="false" class="table table-bordered table-striped" GridLines="None" runat="server">
                                        <Columns>
                                            <asp:BoundField DataField="EmployeName" HeaderText="Employe Name" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                            <asp:BoundField DataField="CellNo" HeaderText="Mobile No" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                            <asp:BoundField DataField="admin" HeaderText="Email" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                            <asp:BoundField DataField="designation" HeaderText="Designation" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                            <asp:BoundField DataField="name" HeaderText="User Name" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                            <asp:BoundField DataField="password" HeaderText="Password" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                            <asp:TemplateField HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd">
                                                <ItemTemplate>
                                                    <asp:Button ID="btndelete" CssClass="btn btn-danger btn-xs" OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Name") %>' runat="server" Text="Delete" OnClick="Delete_Click" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:Label ID="lbldesignation" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Label ID="lblmail" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                </div>
            </div>
       </div>
      <%--  </div>--%>

</asp:Content>

