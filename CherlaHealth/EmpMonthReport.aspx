﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee.Master" AutoEventWireup="true" CodeBehind="EmpMonthReport.aspx.cs" Inherits="CherlaHealth.EmpMonthReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function onCalendarShown() {

        var cal = $find("calendar1");
        //Setting the default mode to month
        cal._switchMode("months", true);

        //Iterate every month Item and attach click event to it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }
    }

    function onCalendarHidden() {
        var cal = $find("calendar1");
        //Iterate every month Item and remove click event from it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }

    }
    function call(eventElement) {
        var target = eventElement.target;
        switch (target.mode) {
            case "month":
                var cal = $find("calendar1");
                cal._visibleDate = target.date;
                cal.set_selectedDate(target.date);
                cal._switchMonth(target.date);
                cal._blur.post(true);
                cal.raiseDateSelectionChanged();
                break;
        }
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Monthly Attendance Report</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    Select Month <asp:TextBox ID="txtmonth" CssClass="form-control" runat="server" OnTextChanged="Month_Change" AutoPostBack="true"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtmonth" OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                                    Enabled="True" />
                </div>
            </div>

               <div style="display: none;" runat="server" id="divmsg">
                            <div class="row mt">
                                <div class="col-lg-12">
                                    <div class="overflowy" style="overflow: scroll; height: 300px">

                                        <section id="no-more-tables">

                                            <asp:GridView ID="GridView1" class="table table-bordered table-striped table-responsive" runat="server" HeaderStyle-BackColor="#02ccec" HeaderStyle-ForeColor="Black">
                                            </asp:GridView>


                                        </section>

                                    </div>
                                </div>


                                <div class="col-lg-12">
                                    Total Days :<asp:Label ID="Label1" runat="server"
                                        Style="background-color: #FFFF00; font-weight: 700"></asp:Label>
                                    &nbsp;,
     Presents:
                                    <asp:Label ID="Label2" runat="server"
                                        Style="background-color: #FFFF00; font-weight: 700"></asp:Label>
                                    &nbsp;,
   Presents(No Out Punch):
                                    <asp:Label ID="Label9" Style="background-color: #FFFF00; font-weight: 700" runat="server"></asp:Label>
                                    &nbsp;,
   Absents: 
                                    <asp:Label ID="Label3" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>
                                    &nbsp;,
     Persents Percentage: 
                                    <asp:Label ID="Label4" runat="server"
                                        Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;%,
    HalfDays: 
                                    <asp:Label ID="Label5" runat="server"
                                        Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
          Week of Present: 
                                    <asp:Label ID="Label6" runat="server"
                                        Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
         Leaves: 
                                    <asp:Label ID="Label7" runat="server"
                                        Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
            Holidays: 
                                    <asp:Label ID="Label8" runat="server"
                                        Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;
                                </div>
                                <br />
                                <div class="col-md-12">
                                    <br />
                                    <div class="col-md-3">
                                        <p>A:-Absent</p>
                                        <p>P:-Present</p>
                                        <p>H:-Holiday</p>
                                        <p>L:-Leave</p>
                                    </div>
                                    <div class="col-md-3">

                                        <p>HOP:-Holiday Present</p>
                                        <p>HO:-Half Day</p>
                                        <p>WO:-Week Off</p>
                                    </div>
                                    <div class="col-md-3">

                                        <p>WOP:-Week Off Present</p>
                                        <p>P(NOP):-Present But No Out Punch </p>
                                        <p>OT:-Over Time</p>



                                    </div>


                                </div>


                                <div class="col-md-12">
                                    <asp:Button ID="Button6" runat="server" class="btn btn-success"
                                        Text="Export Ms-Word" OnClick="Button6_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:Button ID="Button7" runat="server" class="btn btn-success"
                       Text="Export Excel" OnClick="Button7_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button8" runat="server" class="btn btn-success"
                        Text="Export Pdf" OnClick="Button8_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     
                                       <asp:Button ID="Button9" runat="server"
                                           class="btn btn-success" Text="Export CSV" OnClick="Button9_Click" />
                                </div>
                            </div>

                        </div>

            <asp:Label ID="lblcompanyid" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblcompanyname" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lbldeptid" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblemployeeid" Visible="false" runat="server" Text="Label"></asp:Label>
            
            </div>
        </div>
</asp:Content>
