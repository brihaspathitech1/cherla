﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class Probation : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Employee_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Desigantion,DOJ,CompanyEmail from Employees where EmployeeId='" + dropemp.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                txtdesig.Text = dr["Desigantion"].ToString();
                DateTime date = DateTime.Parse(dr["DOJ"].ToString());
                txtjoin.Text = date.ToString("yyyy-MM-dd");
                TextBox1.Text = dr["CompanyEmail"].ToString();
            }
            conn.Close();
        }

        protected void Period_Change(object sender,EventArgs e)
        {
            
            var now = DateTime.Parse(txtjoin.Text);
            var StartOfMonth = new DateTime(now.Year, now.Month, 1, 0, 0, 0);
            var StartOfNextMonth = StartOfMonth.AddMonths(1).AddDays(-1);
            if (txtprob.Text == "1")
            {
                txtexpire.Text = now.AddMonths(1).ToString("yyyy-MM-dd");
            }
            if (txtprob.Text == "2") { txtexpire.Text = now.AddMonths(2).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "3") { txtexpire.Text = now.AddMonths(3).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "4") { txtexpire.Text = now.AddMonths(4).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "5") { txtexpire.Text = now.AddMonths(5).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "6") { txtexpire.Text = now.AddMonths(6).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "7") { txtexpire.Text = now.AddMonths(7).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "8") { txtexpire.Text = now.AddMonths(8).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "9") { txtexpire.Text = now.AddMonths(9).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "10") { txtexpire.Text = now.AddMonths(10).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "11") { txtexpire.Text = now.AddMonths(11).ToString("yyyy-MM-dd"); }
            if (txtprob.Text == "12") { txtexpire.Text = now.AddMonths(12).ToString("yyyy-MM-dd"); }
            //txtexpire.Text = date.ToString("yyyy-MM-dd");
        }


        protected void Submit_Click(object sender,EventArgs e)
        {
            Session["employname"] = dropemp.SelectedItem.Text;
            Session["designation"] = txtdesig.Text;
            Session["period"] = txtprob.Text;
            Session["expire"] = txtexpire.Text;
            Session["CompanyEmail"] = TextBox1.Text;
            Session["salute"] = salut.SelectedItem.Text;
            Response.Redirect("~/ProbationPrint.aspx");
        }
    }
}