﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewLeaveTypes.aspx.cs" Inherits="CherlaHealth.ViewLeaveTypes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">View Leave Types</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    Search By Type <asp:DropDownList ID="droptype" CssClass="form-control" OnSelectedIndexChanged="Type_Change" AutoPostBack="true" runat="server">
                       <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="2">Medicare</asp:ListItem>
                        <asp:ListItem Value="1">Non-Medicare</asp:ListItem>
                                   </asp:DropDownList>
                         </ContentTemplate></asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-3" style="float:right">
                <div class="form-group">
                    <asp:Button ID="btnleave" OnClick="Leave_Click" CssClass="btn btn-success btn-sm" runat="server" Text="Add Leave types" />
                </div>
            </div>
            <div class="col-md-12">
                <asp:UpdatePanel ID="Update" runat="server"><ContentTemplate>
            <asp:GridView ID="gvLeaves" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None" runat="server">
                <Columns>
                    <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd-MM-yyyy}" />
                    <asp:BoundField DataField="type" HeaderText="Type" />
                    <asp:BoundField DataField="LeaveTypeName" HeaderText="Leave Name" />
                    <asp:BoundField DataField="LeaveTypeCode" HeaderText="Short Name" />
                    <asp:BoundField DataField="Days" HeaderText="No Of Days" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:Button ID="btndelete" CssClass="btn btn-danger btn-xs" runat="server" Text="Delete" CommandArgument='<%# Eval("LeaveTypeId") %>' OnClick="delete_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                    </ContentTemplate></asp:UpdatePanel>
                </div>           
        </div>
        </div>
</asp:Content>
