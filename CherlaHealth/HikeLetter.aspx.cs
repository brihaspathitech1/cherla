﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.DataAccessLayer;

namespace CherlaHealth
{
    public partial class HikeLetter : System.Web.UI.Page
    { 
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = Session["Hike"].ToString();
            HikeLetter1();
        }
        private void HikeLetter1()
        {
            DataSet ds= DataQueries.SelectCommon("select [Employees].[CompanyEmail],Name,Empno,[Designation],Salary_Anum,Salary_Words,Salary_HikeDate from [EmployeeDetails] inner join [Employees] on [EmployeeDetails].id=[Employees].[EmployeeId] where Id='" + Label1.Text + "' ");
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                Name1.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                EmpId.Text = ds.Tables[0].Rows[0]["EmpNo"].ToString();
                Designation.Text = ds.Tables[0].Rows[0]["Designation"].ToString();
                Salry.Text = ds.Tables[0].Rows[0]["Salary_Anum"].ToString();
                AmountWords.Text = ds.Tables[0].Rows[0]["Salary_Words"].ToString();
                Date.Text = ds.Tables[0].Rows[0]["Salary_HikeDate"].ToString();
                Email.Text = ds.Tables[0].Rows[0]["CompanyEmail"].ToString();
            }
        }
        protected void btn_hikemail(object sender, EventArgs e)
        {
            if (Email.Text == "" || Email.Text == null)
            {
                Response.Write("<script>alert('No valid email');</script>");
            }
            else
            {
                SendPDFEmail();
            }
        }
        private void SendPDFEmail()
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<div style='text-align:center;font-size:20px'><b>HikeLetter</b></div>");
                    sb.Append("<br />");

                    sb.Append("<table width='100%'  border='0' cellpadding='2'>");
                    sb.Append("<tr><td align='center'><b></b></td></tr>");
                    //sb.Append("<tr><td align='center' style='background-color: #18B5F0;font-size='100px'' colspan = '20'  cellpadding='3'><b>Hike Letter</b></td></tr>");

                    sb.Append("<tr><td><b>Ref: CC to Managing Director, Executive Director, Accounts Manager</b></td></tr>");
                    sb.Append("<tr><td><b>Private & Confidential</b></td></tr>");
                    sb.Append("<tr><td><b>Name:</b>");
                    sb.Append(Name1.Text);
                    sb.Append("</td><td><b>Employee ID:</b>");
                    sb.Append(EmpId.Text);
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '1'></td></tr>");
                    sb.Append("<tr><td><b>Designation:</b>");
                    sb.Append(Designation.Text);
                    sb.Append("<tr><td><b>Sub: Salary Revision.</b>");

                    sb.Append("</td></tr>");
                    sb.Append("</table>");
                    sb.Append("<br />");
                    sb.Append("<br />");

                    sb.Append("<table width='100%' cellspacing='2' style='font-family:arial; font-size:13px'>");
                    sb.Append("<tr><td> We are pleased to inform you that based on your performance and contribution to the company, our Management has revised your compensation to Rs. " + Salry.Text + "/- (" + AmountWords.Text + ") Per Annum with effect from " + Date.Text + "<br /> We appreciate your initiative and expect you to take many more such responsibilities in future assignments to ensure company’s growth.<br />Your salary details are strictly private and confidential. The details in this letter must not be disclosed or discussed to others. Please acknowledge your acceptance of the revised terms by signing a duplicate copy of this letter.</td></tr>");

                    sb.Append("</table>");
                    sb.Append("<br />");
                    sb.Append("<br />");

                    sb.Append("<table width='100%'  border='0' cellpadding='7'>");
                    sb.Append("<tr><td align='center'><b></b></td></tr>");
                    sb.Append("<tr><td><b>Regards</b></td></tr>");
                    sb.Append("<tr><td><b>With Best Wishes</b></td></tr>");
                    sb.Append("<tr><td><b>HR Department</b></td></tr>");
                    sb.Append("</table>");
                    sb.Append("<br />");
                    StringReader sr = new StringReader(sb.ToString());

                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                       
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        htmlparser.Parse(sr);
                        string imagePath = Server.MapPath("top.jpg") + "";

                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                        image.Alignment = Element.ALIGN_TOP;

                        image.SetAbsolutePosition(30, 750);

                        image.ScaleToFit(550f, 550f);
                        image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;
                        
                        pdfDoc.Add(image);
                        PdfContentByte content = writer.DirectContent;
                        Rectangle rectangle = new Rectangle(pdfDoc.PageSize);
                        rectangle.Left += pdfDoc.LeftMargin;
                        rectangle.Right -= pdfDoc.RightMargin;
                        rectangle.Top -= pdfDoc.TopMargin;
                        rectangle.Bottom += pdfDoc.BottomMargin;
                        content.SetColorStroke(BaseColor.BLACK);
                        content.Stroke();
                        pdfDoc.Close();
                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();
                        MailMessage mm = new MailMessage(Email.Text, Email.Text);
                        mm.Subject = "Hike Letter";
                        mm.Body = "Hike Letter PDF Attachment";
                        mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "HikeLetter.pdf"));
                        mm.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential();
                        NetworkCred.UserName = "hre@brihaspathi.com";
                        NetworkCred.Password = "hymavathi";
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
            }
        }
    }
}
