﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace CherlaHealth
{
    public partial class Leaveapproved : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindStudy();
        }
        private void BindStudy()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("  select l.*,e.Employename from LeavesStatus l inner join Employees e on l.EmployeedID=e.EmployeeId order by LeaveId desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvlistofLeaves.DataSource = ds;
                gvlistofLeaves.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvlistofLeaves.DataSource = ds;
                gvlistofLeaves.DataBind();
                int columncount = gvlistofLeaves.Rows[0].Cells.Count;
                gvlistofLeaves.Rows[0].Cells.Clear();
                gvlistofLeaves.Rows[0].Cells.Add(new TableCell());
                gvlistofLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                gvlistofLeaves.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}