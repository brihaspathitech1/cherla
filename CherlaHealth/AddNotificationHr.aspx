﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddNotificationHr.aspx.cs" Inherits="CherlaHealth.AddNotificationHr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 20px">Add Notification</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
    <div class="row mt">
        <div class=" col-lg-12">

            <div class="form-panel col-sm-12">
                <%--<h2>Add Notification :</h2>
                <hr />--%>
                <div class="col-md-12 padbot">
                    <div class="col-sm-10">
                        Update Notification :
                <asp:TextBox ID="txtGlobalNotification" runat="server" class="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtGlobalNotification"
                            ErrorMessage="Text Required"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-sm-2">
                        <br />
                        <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Publish" OnClick="btnPublish_Click" />
                    </div>
                </div>

            </div>
            <div class="form-panel col-sm-12">
                <div style="height: 400px; overflow-y: scroll">
                    <h2>Notification List:-</h2>

                    <asp:GridView ID="gvNotification" class="table table-bordered table-striped table-condensed"
                        runat="server" AutoGenerateColumns="true">
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
            </div>
        </div>
</asp:Content>



