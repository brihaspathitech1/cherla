﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CompanyList.aspx.cs" Inherits="CherlaHealth.CompanyList" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .style1 {
            height: 26px;
        }

        .style2 {
            width: 45%;
        }

        .style3 {
            height: 26px;
            width: 45%;
        }

        .tpadleft {
            padding-left: 20px;
        }

        .tpadright {
            padding-right: 20px;
        }

        .form-control1 {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
        
    </style>

         <script type="text/javascript">
       window.oncontextmenu = function () {
           return false;
       }
       $(document).keydown(function (event) {
           if (event.keyCode == 123) {
               return false;
           }
           else if ((event.ctrlKey && event.shiftKey && event.keyCode == 73) || (event.ctrlKey && event.shiftKey && event.keyCode == 74)) {
               return false;
           }
       });
   </script>
<script>
document.onkeydown = function(e) {
       if (e.ctrlKey && 
           (e.keyCode === 67 || 
            e.keyCode === 86 || 
            e.keyCode === 85 || 
            e.keyCode === 117)) {
           //alert('not allowed');
           return false;
       } else {
           return true;
       }
};
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Company List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">

                <div class="col-md-6">

                    <asp:UpdatePanel ID="Update" runat="server">
                        <ContentTemplate>



                            <asp:TextBox ID="TextBox1" AutoPostBack="True" runat="server" class="form-control" OnTextChanged="TextBox1_TextChanged" placeholder="Search by Company Name........"></asp:TextBox>
                            <cc1:autocompleteextender id="AutoCompleteExtender2" runat="server" targetcontrolid="TextBox1"
                                minimumprefixlength="1" enablecaching="true" completionsetcount="1"
                                completioninterval="1" servicemethod="GetCompany" usecontextkey="True">
                            </cc1:autocompleteextender>


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-6">
                    <asp:Button ID="Button1" runat="server" Text="Add Company" PostBackUrl="~/AddCompany.aspx" class="btn btn-warning" Style="margin-left: 25px"></asp:Button>
                </div>

            </div>


            <div class="col-md-12" style="padding-top: 15px">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="table table-responsive" style="overflow: scroll; height: 420px">
                            <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed cf"
                                AutoGenerateColumns="false" DataKeyNames="CompanyID" OnRowDeleting="GridView1_RowDeleting" runat="server">
                                <Columns>
                                    <asp:CommandField CausesValidation="false" ShowDeleteButton="true" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Red" />
                                    <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" class="center glyphicon glyphicon-pencil" ForeColor="Green" OnClick="lnkEdit_Click" runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CompanyId" Visible="false" HeaderText="sss" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />


                                    <asp:BoundField DataField="CompanyName" HeaderText="Company" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />

                                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />

                                    <asp:BoundField DataField="Address1" HeaderText="Address" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="Phone" HeaderText="Phone No" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="WebsiteURL" HeaderText="Website URL" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="MobileNo" HeaderText="Mobile No" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="CompanyCode" HeaderText="Code" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />

                                    <asp:BoundField DataField="FaxNo" HeaderText="Fax No" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="RegNo" HeaderText="Reg No." HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="RegDate" HeaderText="Reg Date" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="Tanno" HeaderText="TAN No" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="PanNo" HeaderText="PAN No" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="TinNo" HeaderText="TIN No" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="PhoneSecond" HeaderText="Phone2 No" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="Desiganation" HeaderText="Designation" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="latitude" HeaderText="Latitude" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                                    <asp:BoundField DataField="longitude" HeaderText="Longitude" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Label ID="lblmsg" runat="server" />
            </div>
            <asp:Button ID="modelPopup" runat="server" Style="display: none" />
            <cc1:modalpopupextender id="ModalPopupExtender2" runat="server" targetcontrolid="modelPopup" popupcontrolid="updatePanel"
                cancelcontrolid="btnCancel" backgroundcssclass="tableBackground">
                </cc1:modalpopupextender>


            <asp:Panel ID="updatePanel" runat="server" BackColor="White" Height="230px" Width="300px" Style="display: none;">

                <table width="150%" cellspacing="4" style="background: #fff; margin-top: -70px">
                    <tr style="background-color: #12b5da">
                        <td colspan="2" align="center">
                            <h3 style="color: #fff">Edit Company</h3>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2"></td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style3 tpadleft">Company Code
                        </td>
                        <td class="style1 tpadright">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstor_address" runat="server" class="form-control1" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style3 tpadleft">Company Name</td>
                        <td class="style1 tpadright">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtcity" runat="server" class="form-control1" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">Name
                        </td>
                        <td class="tpadright">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="style3 tpadleft">Address</td>
                        <td class="style1  tpadright">
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtadrs" runat="server" class="form-control1" ValidateRequest="false" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">Email</td>
                        <td class=" tpadright">
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtemail" runat="server" class="form-control1" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">Phone No</td>
                        <td class=" tpadright">
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate2" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">Website Url </td>
                        <td class=" tpadright">
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate3" runat="server" class="form-control1" />
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style3 tpadleft">Mobile No</td>
                        <td class="style1 tpadright">
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate5" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style3 tpadleft">Fax No</td>
                        <td class="style1 tpadright">
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate13" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">Reg No</td>
                        <td class="tpadright">
                            <asp:TextBox ID="txtstate6" runat="server" class="form-control1 " />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">Reg Date</td>
                        <td class="tpadright">
                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate7" runat="server" class="form-control1" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style3 tpadleft">TAN No</td>
                        <td class="style1 tpadright">
                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate8" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">PAN No</td>
                        <td class="tpadright">
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate9" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style3 tpadleft">TIN No</td>
                        <td class="style1 tpadright">
                            <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate10" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft">Phone No2</td>
                        <td class="tpadright">
                            <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate11" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="style2 tpadleft">Designation</td>
                        <td class="tpadright">
                            <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtstate12" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                         <td align="left" class="style2 tpadleft">Latitude</td>
                         <td class="tpadright">
                            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtlat" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        </tr>
                    <tr>
                          <td align="left" class="style2 tpadleft">Longitude</td>
                         <td class="tpadright">
                            <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtlong" runat="server" class="form-control1 " />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style2 tpadleft" style="padding: 10px 0px 10px 15px">

                            <asp:Button ID="btnUpdate" runat="server" CommandName="Update"
                                OnClick="btnModity_Click" Text="Update Data" type="submit" class="btn btn-success" />

                        </td>
                        <td style="padding: 10px 0px 10px 0px">

                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />

                        </td>
                    </tr>
                </table>

            </asp:Panel>
        </div>
    </div>
</asp:Content>
