﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeePaySlip.aspx.cs" Inherits="CherlaHealth.EmployeePaySlip" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script type="text/javascript">
        function onCalendarShown() {

            var cal = $find("calendar1");
            //Setting the default mode to month
            cal._switchMode("months", true);

            //Iterate every month Item and attach click event to it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }

        function onCalendarHidden() {
            var cal = $find("calendar1");
            //Iterate every month Item and remove click event from it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }

        }
        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "month":
                    var cal = $find("calendar1");
                    cal._visibleDate = target.date;
                    cal.set_selectedDate(target.date);
                    cal._switchMonth(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged();
                    break;
            }
        }

    </script>
    <style >

        .inline-rb label {
    font-weight: bold !important;
    color: #00daff;
    font-size: 20px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Payslip</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    Select Month<asp:TextBox ID="txtmonth" CssClass="form-control" runat="server" OnTextChanged="Month_Change" AutoPostBack="true"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtmonth" OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                                        Enabled="True" />
                </div>
            </div>
               <div class=" col-lg-12" style="padding-top: 17px">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" AutoGenerateColumns="false" DataKeyNames="Id"
                                    runat="server">
                                    <Columns>

                                        <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />

                                        <asp:BoundField DataField="Basic" HeaderText="Basic" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="PF" HeaderText="PF" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="ESI" HeaderText="ESI" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Total" HeaderText="Total" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />

                                        <asp:BoundField DataField="GrossSalary" HeaderText="Gross" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />


                                        <asp:TemplateField HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White">
                                            <HeaderTemplate>
                                                Payslip
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <h5 class=" text-center">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SSS" class="glyphicon glyphicon-print"
                                                        CommandArgument='<%# Eval("Id") %>'></asp:LinkButton></h5>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      <%--  <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White">
                                            <ItemTemplate>
                                                <asp:Button ID="Edit" runat="server" Text="Edit" OnClick="Edit_click" CommandArgument='<%# Eval("Payslip") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                    </Columns>

                                </asp:GridView>
                                <asp:Label ID="lblpf" Visible="false" runat="server" Text=""></asp:Label>
                                 <asp:Label ID="label1" Visible="false" runat="server" Text=""></asp:Label>
                                 <asp:Label ID="label2" Visible="false" runat="server" Text=""></asp:Label>
                                 <asp:Label ID="lblname" Visible="false" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
            </div>
        </div>
</asp:Content>
