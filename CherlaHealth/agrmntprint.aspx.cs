﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using inventory.DataAccessLayer;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Net;
using System.Net.Mail;
using System.Text;


namespace CherlaHealth
{
    public partial class agrmntprint : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbl1.Text = Session["txt1"].ToString();
                lbl8.Text = Session["txt8"].ToString();
                lbl9.Text = Session["txt9"].ToString();
                lbl2.Text = Session["txt2"].ToString();
                lbl3.Text = Session["txt3"].ToString();

                lbl4.Text = Session["txt4"].ToString();
                lbl5.Text = Session["txt5"].ToString();
                lbl6.Text = Session["txt6"].ToString();
                lbl7.Text = Session["txt7"].ToString();
                lblplace.Text = Session["place"].ToString();
                lbld1.Text = Session["txt1"].ToString();
                Lblp1.Text= Session["place"].ToString();
                lbld2.Text = Session["txt1"].ToString();
            }
        }

       

        protected void pdf_Click(object sender, EventArgs e)
        {
            //Image1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Doctor Agreement.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            string imagePath = Server.MapPath("sign board.jpg") + "";

            //for (int i = 0; i < 1; i++)
            //{
                
            //    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            //    image.Alignment = Element.ALIGN_TOP;

            //    image.SetAbsolutePosition(40, 700);

            //    image.ScaleToFit(500f, 400f);
            //    image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            //    pdfDoc.Add(image);
                htmlparser.Parse(sr);
               
           // }
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();




        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

       
    }
}




