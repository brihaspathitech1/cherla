﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="abscondingPrint.aspx.cs" EnableEventValidation="false" Inherits="CherlaHealth.abscondingPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Absconding Letter</title>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
                text-align:justify;
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }

        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
                text-align:justify;
            }

            #Button1 {
                display: none;
            }

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint,#btnpdf {
                display: none;
            }
            input#btnemail {
                 display: none;
            }

            a {
                display: none;
            }
        }




        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
    <style>
    .page21 {
            width:100%;background:#d3d3d357;text-align:justify;padding:150px 50px 0px 70px; 
        }
        tr, td {
            /*border:1px solid black;
            border-collapse:collapse;
            padding:10px;*/
      
       }
        .tab-text { margin-left:40px;font-weight:bold;
        }
        ol {
            margin-left:50px;
        }
   </style> 
</head>
<body style="color:black">
    <div class="page21">
    <form id="form1" runat="server">
                <div style="text-align:center;padding-top:25px ;">
             <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                <a href="absconding.aspx" id="btnPrint" type="button"  onclick="goBack()"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>

            <asp:Button ID="btnpdf"  CssClass="btn btn-success" runat="server" Text="PDF" OnClick="pdf_Click" />
                     <asp:Button ID="btnemail" CssClass="btn btn-success" runat="server" Text="Gmail" OnClick="btnemail_Click" Visible="false" />
        <br /><br />
                </div>
    <div>
        <asp:Panel ID="Panel1" runat="server">
         <table width="100%">
                <tr>
                    <td>
                       <%-- <img id="img1" alt="/" src="sign board.jpg" width="1250px" height="120px" />--%>
                     <%--    <img id="img1" runat="server" src="~/sign board.png" width="1250px" height="120px" />--%>
                       <%-- <asp:Image ID="Image1" ImageUrl="~/sign board.jpg" Width="1250px" Height="120px" runat="server" />
                    --%></td>
                </tr>
            </table>
             <br /><br />

       <p>Date: 
        <asp:Label ID="lbldate" runat="server" Text=""></asp:Label></p>
        <p>To &nbsp;</p>
           <p> <asp:Label ID="gen" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblname" runat="server" Text=""></asp:Label></p>
        <p></p>
        <p><asp:Label ID="lbldesign" runat="server" Text=""></asp:Label></p>
            <p><asp:Label ID="lblemail" runat="server" Text=""></asp:Label></p>
        <br />
        <br />
        <br />
        <p>Sub. – Absconding from duties</p>
        <br />
        <br />
        <p>Dear &nbsp;&nbsp;<asp:Label ID="gen1" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblname2" runat="server" Text=""></asp:Label></p>
        <br />
        <p style="text-align:justify">It has been observed that you have proceeded on leave without prior permission of the concerned authorities, resulting in willful insubordination and gross negligence of duties, in your capacity as Designation.</p>
        <br />
        <p style="text-align:justify">Absenting yourself from duties without prior intimation is a misconduct for which you are making yourself liable for necessary action.</p>
        <br />
        <p style="text-align:justify">You are hereby warned to refrain from such activities; failure to do so shall invoke appropriate action.</p>
        <br />
        <p style="text-align:justify">You are further advised to submit a written explanation on your unauthorized leave as soon as you receive this letter or as soon as you resume duties.</p>
        <br />
        <p>Please treat this as very urgent.</p>
        <br />
        <br />
        <p>For Cherla Health Care (P) Ltd</p>
        <br />
        <br />
        <br />
        <br />
        
        <p>Authorized Signatory,</p>
            </asp:Panel>
    </div>
    </form>
        </div>
</body>
</html>
