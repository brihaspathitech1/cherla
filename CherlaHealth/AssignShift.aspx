﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AssignShift.aspx.cs" Inherits="CherlaHealth.AssignShift" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .style1 {
            color: #FF0000;
        }

        .style2 {
            color: #0066FF;
        }

        .style3 {
            color: #9966FF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Assign Shift</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                   <div class="col-md-12 padbot">
                    <div class="col-sm-4">
                        Company
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>

                                <asp:DropDownList ID="DropDownList1" class="form-control" runat="server"
                                    AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownList1"
                                    ErrorMessage="Please Select Comapny" InitialValue="0" Style="color: #FF0000"></asp:RequiredFieldValidator>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="DropDownList1" />
                            </Triggers>

                        </asp:UpdatePanel>
                    </div>
                    <div class="col-sm-4">
                        Department
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="Department" runat="server" class="form-control"
                            AutoPostBack="True" OnSelectedIndexChanged="Department_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Department"
                            ErrorMessage="Please Select Department" InitialValue="0" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Department" />
                    </Triggers>
                </asp:UpdatePanel>


                    </div>
                    <div class="col-sm-4">
                        Employee
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server" class="form-control"
                            AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownList2"
                            ErrorMessage="Please Select Employee" InitialValue="0" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DropDownList2" />
                    </Triggers>
                </asp:UpdatePanel>

                    </div>
                </div>
               <div class="col-sm-4">
                </div>
                <div class="col-md-12">

                    <div class="col-sm-4">
                        Assign Shift

                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="DropDownList3" runat="server" class="form-control"
                            AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownList3"
                            ErrorMessage="Please Assign Shift" InitialValue="0" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DropDownList3" />
                    </Triggers>
                </asp:UpdatePanel>


                    </div>
                    <div class="col-sm-4">
                        Start Date
              <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox><cc1:CalendarExtender ID="CalendarExtender1"
                  TargetControlID="TextBox1" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox1"
                            ErrorMessage="Select Start Date" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-sm-4 ">
                        End Date
              <asp:TextBox ID="TextBox2" class="form-control" runat="server"></asp:TextBox>

                        <cc1:CalendarExtender ID="CalendarExtender2"
                            TargetControlID="TextBox2" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBox2"
                            ErrorMessage="Select End Date" Style="color: #FF0000"></asp:RequiredFieldValidator>

                        <div class="col-md-12 btn1 ">
                            <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Assign" OnClick="Button1_Click" />

                            &nbsp;<asp:Label ID="Label6" runat="server" Style="color: #FF0000"></asp:Label>

                        </div>

                    </div>
                </div>
            </div>
        </div>
</asp:Content>
