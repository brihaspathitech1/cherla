﻿using inventory.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class EmployeeLeaves : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["isApply"] = "true";
                BindStudy();
            }
            if (!IsPostBack)
            {
                BindStudy4();
            }
        }
        private string getStatus()
        {
            string status;
            DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + Session["name"].ToString() + "'");
            if (ds.Tables[0].Rows.Count != 0)
            {
                status = "OnHRDesk";
            }
            else
            {
                status = "OnManagerDesk";
            }
            return status;
        }
        private Double getbalLeaves()
        {
            Double count = DataQueries.SelectCount("select sum(CL) from LeavesStatus where EmployeedID='" + Employee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + Begintime.Text + "')");
            count = 12 - count;
            return count;
        }
        string str = "";
        private void BindStudy()
        {
                        if (Session["Category"].ToString() == "Individual")
                        {
                            string str = "SELECT EmployeeId,EmployeName FROM Employees where EmployeeId='" + Session["name"].ToString() + "'";
                        }
                        else if (Session["Category"].ToString() == "Manager")
                        {
                             string str = "SELECT EmployeeId,EmployeName FROM Employees where DeptId=(Select DeptId from Employees where EmployeeId='" + Session["name"].ToString() + "')";
                        }

                        else if (Session["Category"].ToString() == "Administrator")
                        {
                              string str = "SELECT EmployeeId,EmployeName FROM Employees";
                        }
                        DataSet ds = DataQueries.SelectCommon(str);
                        Employee.DataSource = ds;
                        Employee.DataValueField = "EmployeeId";
                        Employee.DataTextField = "EmployeName";
                        Employee.DataBind();
                        Employee.Items.Insert(0, new ListItem("", "0"));
                    }
        private void BindStudy4()
        {
            string sqlCmd = "SELECT LeaveTypeId,LeaveTypeCode FROM LeaveTypes";
            DataSet ds = DataQueries.SelectCommon(sqlCmd);
            Leave.DataSource = ds;
            Leave.DataValueField = "LeaveTypeId";
            Leave.DataTextField = "LeaveTypeCode";
            Leave.DataBind();
            Leave.Items.Insert(0, new ListItem("", "0"));
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            int LOP, CL;
            int apply = Convert.ToInt16(Duration.Text);
            Double AlreadyApplied = DataQueries.SelectCount("select sum(CL) from LeavesStatus where EmployeedID='" + Employee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + Begintime.Text + "')");
            int month = DateTime.Parse(EndTime.Text).Month;
            int canapply = month - (int)AlreadyApplied;
            if (apply > canapply)
            {
                LOP = apply - canapply;
                CL = canapply;
            }
            else
            {
                CL = apply;
                LOP = 0;
            }
            DataQueries.InsertCommon("insert into LeavesStatus values('" + Employee.SelectedValue + "','" + System.DateTime.Now.ToShortDateString() + "','" + Begintime.Text + "','" + EndTime.Text + "','" + Duration.Text + "','" + getbalLeaves() + "','" + Reason.Text + "','" + getStatus() + "','','" + System.DateTime.Now.ToShortDateString() + "','" + CL + "','" + LOP + "')");
            Response.Write("<script>alert('Data inserted successfully')</script>");
            Response.Redirect("ApproveLeave.aspx");
        }
        protected void Employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Employee.SelectedIndex == 0) { return; }
            DataSet ds = DataQueries.SelectCommon("select CellNo from Employees where EmployeeId='" + Employee.SelectedValue + "'");
            Contact.Text = ds.Tables[0].Rows[0][0].ToString();
            Double count = getbalLeaves();
            txtBalLeaves.Text = count.ToString() + "/12 for Year " + (DateTime.Parse(Begintime.Text).Year).ToString();
            if (Convert.ToInt16(Duration.Text) > count)
            {
                string str;
                str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate";
                str += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
                str += " where l.EmployeedId='" + Employee.SelectedValue + "' order by l.LeaveId";
                Session["isApply"] = "false";
                Button2.Enabled = false;
                Employee.SelectedIndex = 0;
                pnlReadOnly.Visible = true;
                DataSet dsro1 = DataQueries.SelectCommon(str);
                lblReadOnlyListOfLeaves.Text = "Sorry..! You can Apply Only 12 Casual Leave Per Year..!";
                gvListofROLeaves.DataSource = dsro1;
                gvListofROLeaves.DataBind();
                return;
            }
            string s;
            s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate";
            s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
            s += " where l.EmployeedId='" + Employee.SelectedValue + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk')";

            DataSet dsro = DataQueries.SelectCommon(s);
            if (dsro.Tables[0].Rows.Count != 0)
            {
                Session["isApply"] = "false";
                Button2.Enabled = false;
                Employee.SelectedIndex = 0;
                pnlReadOnly.Visible = true;
                lblReadOnlyListOfLeaves.Text = "Already Pending Leave,You can't Apply..!";
                gvListofROLeaves.DataSource = dsro;
                gvListofROLeaves.DataBind();
                return;
            }
            else
            {
                Button2.Enabled = true;
                pnlReadOnly.Visible = false;
            }
        }
        protected void EndTime_TextChanged(object sender, EventArgs e)
        {
            string ErrorMessage = "";
            if (Begintime.Text != "" && EndTime.Text != "")
            {
                //Storing input Dates  
                DateTime FromYear = Convert.ToDateTime(Begintime.Text);
                DateTime staringdate = FromYear;
                DateTime ToYear = Convert.ToDateTime(EndTime.Text);
                DateTime endingdate = ToYear;
                endingdate = endingdate.AddDays(1);
                TimeSpan objTimeSpan = ToYear - FromYear;
                int duration = 0;
                //TotalDays  
                double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
                if (Days > 0)
                {
                    if (FromYear.Year != ToYear.Year)
                    {
                        ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                        //  Response.Write("<script>alert('Staring and Ending Date of leave Should have same Year..!')</script>");

                    }
                    else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                    {
                        ErrorMessage += "Wrong selection of Leave Date..!";
                        //Response.Write("<script>alert('Wrong selection of Leave Date..!')</script>");
                    }
                    else
                    {
                        DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + Begintime.Text + "' and '" + EndTime.Text + "'");

                        while (staringdate != endingdate)
                        {
                            if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                            {
                                if (Holidays.Tables[0].Rows.Count == 0)
                                { duration++; }
                                else
                                {
                                    for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                    {
                                        string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                        if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                        {
                                            duration++;
                                        }
                                    }
                                }
                            }
                            staringdate = staringdate.AddDays(1);
                        }
                        Duration.Text = Convert.ToString(duration);
                    }
                }
                else
                {
                    ErrorMessage += "Wrong selection of Leave Ending Date..!";
                }
            }
            lblErrorMessage.Text = ErrorMessage;
            if (ErrorMessage != "")
            {
                Employee.Enabled = false;
                Button2.Enabled = false;
            }
            else
            {
                Employee.Enabled = true;
                Button2.Enabled = true;
            }
        }
    }
}