﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Locdisplay.aspx.cs" Inherits="CherlaHealth.Locdisplay" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        
        <!-- /.box-header -->
        <div class="box-body">
              
                  <div class="col-md-12" style="padding-top: 15px">

                <div class="table table-responsive" style="overflow: scroll; height: 420px">
                    <asp:GridView ID="GridView1" runat="server" class="table table-bordered table-striped table-condensed" AutoGenerateColumns="false" OnRowDeleting="GridView1_RowDeleting" DataKeyNames="id" >

                        <Columns>
                            <asp:BoundField DataField="EmployeName" HeaderText="EmployeeName" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="in_time" HeaderText="Check in" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="addres" HeaderText="Check_in Address" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            
                            <asp:BoundField DataField="out_time" HeaderText="CheckOut" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                             <asp:BoundField DataField="outaddress" HeaderText="Check_Out Address" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" />
                            <asp:CommandField CausesValidation="false" ShowDeleteButton="true" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Red"  />

                        </Columns>
                    </asp:GridView>

                  
                </div>
            </div></div></div>
</asp:Content>
