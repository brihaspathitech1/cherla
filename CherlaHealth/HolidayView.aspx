﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HolidayView.aspx.cs" Inherits="CherlaHealth.HolidayView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            color: #FF0000;
        }
    </style>
    <style type="text/css">
        .tableBackground {
            background-color: silver;
            opacity: 0.7;
        }

        .tpadleft {
            padding-left: 20px;
        }

        .tpadright {
            padding-right: 20px;
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .form-control1 {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 20px">View Holidays</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
              
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="col-lg-6" style="padding-top: 20px; padding-bottom: 20px">

                            <asp:TextBox
                                ID="TextBox1"
                                AutoPostBack="True" runat="server" class="form-control" OnTextChanged="TextBox1_TextChanged" placeholder="Search........"></asp:TextBox>
                        </div>
                        <div class="col-lg-4" style="padding-top:20px">
                          <asp:Button ID="Button1" runat="server" Text="Add Holidays" PostBackUrl="~/AddHolidays.aspx" class="btn btn-success" Style="margin-left: 25px"></asp:Button>
                        </div>
                            <section id="no-more-tables">
                            <div class="col-lg-12" style="overflow-y: scroll; height: 400px">
                                <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" EmptyDataText=" No Rows Found" AutoGenerateColumns="false" DataKeyNames="HolidayId"
                                    runat="server" OnRowDeleting="GridView1_RowDeleting">
                                    <Columns>

                                        <asp:BoundField DataField="HolidayName" HeaderText="Name" />
                                        <asp:BoundField DataField="DATE" HeaderText="Date" />
                                        <asp:BoundField DataField="HolidayType" HeaderText="Type" />
                                        <%-- <asp:BoundField DataField="Dercripation" HeaderText="Holiday Name" />  --%>

                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" ForeColor="Green" class="glyphicon glyphicon-pencil" OnClick="lnkEdit_Click" runat="server"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField CausesValidation="false" ShowDeleteButton="true" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" />
                                    </Columns>

                                </asp:GridView>
                            </div>
                        </section>
                        <asp:Label ID="lblmsg" runat="server" />
                        <asp:Button ID="modelPopup" runat="server" Style="display: none" />

                        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup" PopupControlID="updatePanel"
                            CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                        </cc1:ModalPopupExtender>

                        <asp:Panel ID="updatePanel" runat="server" BackColor="White" Height="350px" Width="357px" Style="display: none">
                            <table width="100%" cellspacing="4">
                                <tr style="background-color: #DC7A01">
                                    <td colspan="2" align="center">
                                        <h3 style="color: #fff">Edit Holiday</h3>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="right" style="width: 45%"></td>
                                    <td>
                                        <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left " class="style3 tpadleft">Name
                                    </td>
                                    <td class="tpadright">
                                        <asp:TextBox ID="txtstor_address" runat="server" class="form-control" />

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" class="style3 tpadleft">Holiday Date
                                    </td>
                                    <td class="tpadright">
                                        <asp:TextBox ID="txtcity" runat="server" class="form-control" />
                                            <cc1:CalendarExtender ID="CalendarExtender3" Format="yyyy-MM-dd" TargetControlID="txtcity" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="style3 tpadleft">Holiday Type
                                    </td>
                                    <td class="tpadright">
                                        <asp:TextBox ID="txtstate" runat="server" class="form-control" />
                                    </td>

                                </tr>
                                <tr>

                                    <td align="left" class="style3 tpadleft">Description
                                    </td>
                                    <td class="tpadright">
                                        <asp:TextBox ID="txtstate1" runat="server" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tpadleft" style="padding: 10px 0px 10px 15px">
                                        <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update Data" OnClick="btnModity_Click" class="btn btn-success" />
                                    </td>
                                    <td style="padding: 10px 0px 10px 0px">
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>


                </div>

            </div>
        </div>
   
</asp:Content>

