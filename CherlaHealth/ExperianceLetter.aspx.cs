﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace CherlaHealth
{
    public partial class ExperianceLetter : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Employee_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Desigantion,DOJ from Employees where EmployeeId='" + dropemp.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                
                DateTime date = DateTime.Parse(dr["DOJ"].ToString());
                txtdate.Text = date.ToString("yyyy-MM-dd");
            }
            conn.Close();
        }

        protected void Submit_Click(object sender,EventArgs e)
        {
            Session["employid"] = dropemp.SelectedValue;
            //Session["reason"] = txtreason.Text;
            Session["conduct"] = txtconduct.Text;
            Session["leavedate"] = txtleave.Text;
            Session["jdate"] = txtdate.Text;
            Response.Redirect("~/ExperianceLetterPrint.aspx");
        }
    }
}