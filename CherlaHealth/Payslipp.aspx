﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Payslipp.aspx.cs" Inherits="CherlaHealth.Payslipp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size:24px">Payslip</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                        <div class="row mt">

              <%--  <div class=" col-lg-12 panel" style="padding: 20px">--%>
                    <%--<h3 style="color: Red; font-weight: bold;">
                        <label>Payslips</label></h3>--%>
                  <%--  <hr />--%>

                    <div class=" col-lg-12">
                        <div class=" col-lg-12" style="padding-top: 17px">

                            <div class=" col-lg-12">
                                <div class="col-md-3" style="padding-top: 20px;">
                                  
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server"
                                                 CssClass="inline-rb" RepeatDirection="Horizontal" AutoPostBack="true">
                                                <asp:ListItem Value="1">NPF &nbsp &nbsp  &nbsp  &nbsp  &nbsp</asp:ListItem>
                                                <asp:ListItem Value="2">PF &nbsp &nbsp  &nbsp  &nbsp  &nbsp</asp:ListItem>
                                                <asp:ListItem Value="3">ALL</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Required" ControlToValidate="RadioButtonList1" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                               

                                
                                <div class="col-md-3" style="padding-top: 17px">
  <asp:Button ID="Button1" runat="server" class="btn btn-danger" Text="Submit" OnClick="Button1_Click" />
                                    <asp:Label ID="label1" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="label2" runat="server" Text="" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class=" col-lg-12" style="padding-top: 17px">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" AutoGenerateColumns="false" 
                                    runat="server">
                                    <Columns>
                                        <asp:BoundField DataField="Payslip" HeaderText="Payslip" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />

                                        <asp:BoundField DataField="Basic" HeaderText="Basic" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="PF" HeaderText="PF" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="ESI" HeaderText="ESI" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Total" HeaderText="Total" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                                        <asp:TemplateField HeaderText="delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbl34" runat="server" OnClick="lbl34_Click" CssClass="fa fa-trash"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>



                                        


                                        
                                      <%--  <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White">
                                            <ItemTemplate>
                                                <asp:Button ID="Edit" runat="server" Text="Edit" OnClick="Edit_click" CommandArgument='<%# Eval("Payslip") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                    </Columns>

                                </asp:GridView>

                            </div>
                        </div>
                        <%--<div class=" col-lg-12" style="padding-top:15px">
     <div class="col-md-3">
        <asp:Button ID="Button2" class="btn btn-success" runat="server" Text="Export"  />
    </div>
    </div>--%><asp:Label ID="del" runat="server" Visible="false"></asp:Label>
                    </div>
             <%--   </div>--%>
            </div>
            </div>
         </div>
</asp:Content>
