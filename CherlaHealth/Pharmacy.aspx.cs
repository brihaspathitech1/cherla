﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class Pharmacy : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                candid();
            }
        }
        
        protected void candid()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeName,EmployeeId from Employees order by EmployeeId desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropempp.DataSource = dr;
            dropempp.DataTextField = "EmployeName";
            dropempp.DataValueField = "EmployeeId";
            dropempp.DataBind();
            dropempp.Items.Insert(0, new ListItem("", "0"));
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            Session["fromdate1"] = fromdate.Text;
            Session["emp1"] = dropempp.SelectedItem.Text;
            Session["address1"] = address.Text;
            Session["age1"] = age.Text;
            Session["occupation1"] = occupation.Text;
            Session["todate1"] = todate.Text;
            Session["gender1"] = gender.SelectedItem.Text;
            Session["yrs1"] = Dropdownlist1.SelectedItem.Text;
            Session["father1"] = fathername.Text;
            Session["salary1"] = salary.Text;
            Response.Redirect("Pharmacyprint.aspx");
        }
    }
}