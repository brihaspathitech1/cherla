﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="hourlypayroll.aspx.cs" Inherits="CherlaHealth.hourlypayroll" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCalendarShown() {

            var cal = $find("calendar1");
            //Setting the default mode to month
            cal._switchMode("months", true);

            //Iterate every month Item and attach click event to it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }

        function onCalendarHidden() {
            var cal = $find("calendar1");
            //Iterate every month Item and remove click event from it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }

        }
        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "month":
                    var cal = $find("calendar1");
                    cal._visibleDate = target.date;
                    cal.set_selectedDate(target.date);
                    cal._switchMonth(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged();
                    break;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Hourly Payroll</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    <asp:RadioButtonList ID="RadioButtonList1" RepeatDirection="Horizontal" runat="server" OnSelectedIndexChanged="Pf_Change" AutoPostBack="true">
                        <asp:ListItem>NPF</asp:ListItem>
                        <asp:ListItem>PF</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="RadioButtonList1" ForeColor="Red" runat="server" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
             <div class="col-md-3">
                <div class="form-group">
                    Employee Name <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="dropemp" ForeColor="Red" runat="server" ErrorMessage="Required"></asp:RequiredFieldValidator>

                    </div>
                 </div>
            <div class="col-md-3">
                <div class="form-group">
                   Month <asp:TextBox ID="txtmonth" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtmonth" ForeColor="Red" runat="server" ErrorMessage="Required"></asp:RequiredFieldValidator>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="yyyy-MM-dd" runat="server" TargetControlID="txtmonth" OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                            Enabled="True" />
                    </div>
                </div>
             <div class="col-md-3" style="padding-top:18px">
                <div class="form-group">
                    <asp:Button ID="btnclick" CssClass="btn btn-success" runat="server" Text="Click" OnClick="Click" />
                    </div>
                 </div>
                </div>
             <div class="col-md-3">
                <div class="form-group">
                    Absent <asp:TextBox ID="txtabsent" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
               <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtabsent" FilterType="Numbers" />

                    </div>
                 </div>
            <div class="col-md-3">
                <div class="form-group">
                    Holiday <asp:TextBox ID="txtholiday" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
                               <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtholiday" FilterType="Numbers" />

                    </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    Present <asp:TextBox ID="txtpresent" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
           <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtpresent" FilterType="Numbers" />

                    </div>
                </div>
              <div class="col-md-3">
                <div class="form-group">
                    Present(NOP)  <asp:TextBox ID="txtnop" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
                 <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtnop" FilterType="Numbers" />
  
                    </div>
                  </div>
            <div class="col-md-3">
                <div class="form-group">
                    Week Off <asp:TextBox ID="txtwoff" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
 <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtwoff" FilterType="Numbers" />
                    </div>
                </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    Salary Per Hour <asp:TextBox ID="txtsal" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
             <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtsal" FilterType="Numbers" />
                    </div>
                </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    Total Hours Worked<asp:TextBox ID="txthrs" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
                    
                    </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    Incentives<asp:TextBox ID="txtincentives" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtincentives" FilterType="Numbers" />

                    </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    Tax Deductions <asp:TextBox ID="txttax" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txttax" FilterType="Numbers" />

                    </div>
                </div>
            <div class="col-md-3" style="padding-top:18px">
                <div class="form-group">
                    <asp:Button ID="btncal" CssClass="btn btn-success" runat="server" Text="Calculate Salary" OnClick="salary_click" />
                    </div>
                </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div>
                    <div class="col-md-3">
        Salary Per Hour</div>
                    <div class="col-md-3">
                    <asp:Label ID="lblsalary" runat="server" Text="0"></asp:Label>

                    </div>
                     <div class="col-md-3">
            Tax Deduction
                         </div>
                     <div class="col-md-3">
                        <asp:Label ID="lbltax" runat="server" Text="0"></asp:Label>
                         </div>
                    </div>
                <div>
                     <div class="col-md-3">
            Total Hours Worked
                         </div>
                     <div class="col-md-3">
                         <asp:Label ID="lblhrs" runat="server" Text="0"></asp:Label>
                         </div>
                     <div class="col-md-3">
            Incentives
                         </div>
                     <div class="col-md-3">
                         <asp:Label ID="lblincentives" runat="server" Text="0"></asp:Label>
                         </div>
                    </div>
                <div>
                     <div class="col-md-3">
            Net Salary
                         </div>
                     <div class="col-md-3">
                         <asp:Label ID="lblnet" runat="server" Text="0"></asp:Label>
                         </div>
                     <div class="col-md-3">
            In Words 
                         </div>
                     <div class="col-md-3">
                          <asp:Label ID="lblwords" runat="server" Text="0"></asp:Label>
                         </div>
                    </div>
                </div>
            
            <div class="col-md-3" style="padding-top:18px">
                <div class="form-group">
                     <asp:Label ID="lblStatus" runat="server" Text="" ForeColor="red"></asp:Label>
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                </div>
            </div>
            <asp:Label ID="lbljoin" runat="server" Text="" Visible="false"></asp:Label>
          <asp:Label ID="lblacno" runat="server" Text="" Visible="false"></asp:Label>        
                <asp:Label ID="lbldesignation" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Label ID="lblpfno" runat="server" Text="" Visible="false"></asp:Label>

                        <asp:Label ID="lblbank" runat="server" Text="" Visible="false"></asp:Label>

                        <asp:Label ID="lbldept" runat="server" Text="" Visible="false"></asp:Label>

                        <asp:Label ID="lblpf" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="lbltot" runat="server" Text="" Visible="false"></asp:Label>

        </div>
        </div>
</asp:Content>
