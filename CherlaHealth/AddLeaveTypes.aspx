﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddLeaveTypes.aspx.cs" Inherits="CherlaHealth.AddLeaveTypes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Add Leave Type</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div class="col-md-3" runat="server" visible="false">
                    <div class="form-group">
                        Date <span style="color:red">*</span> <asp:TextBox ID="txtdate" CssClass="form-control" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdate" />
                        
                    </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    Select Type <span style="color:red">*</span><asp:DropDownList ID="droptype" CssClass="form-control" runat="server">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="2">Medicare</asp:ListItem>
                        <asp:ListItem Value="1">Non-Medicare</asp:ListItem>
                                                                </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="droptype" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>
                <div class="col-md-3">
                    <div class="form-group">
                         Leave Name <span style="color:red">*</span> <asp:TextBox ID="txtleave" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtleave" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
             <div class="col-md-3">
                    <div class="form-group">
                        Short Name <span style="color:red">*</span> <asp:TextBox ID="txtshort" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtshort" ForeColor="Red"></asp:RequiredFieldValidator>
 
                    </div>
                 </div>
              <div class="col-md-3">
                    <div class="form-group">
                        No Of Days Per Year <span style="color:red">*</span> <asp:TextBox ID="txtdays" CssClass="form-control" runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtdays" ForeColor="Red"></asp:RequiredFieldValidator>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtdays" FilterType="Numbers" />
                    </div>
                  </div>
              <div class="col-md-3">
                    <div class="form-group">
                        Description <asp:TextBox ID="txtdescription" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                  </div>
           
             <div class="col-md-3" style="padding-top:18px">
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                        </div>
                 </div>

            </div>
        </div>
</asp:Content>
