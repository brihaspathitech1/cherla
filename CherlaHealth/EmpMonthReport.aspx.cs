﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;

namespace CherlaHealth
{
    public partial class EmpMonthReport : System.Web.UI.Page
    {
        float count;
        float count1;
        float count2;
        float count3;
        float count4;
        float count5;
        float count6;
        float count7;
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Bindvalues();
            }
        }

        protected void Bindvalues()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            if (Session["name"].ToString() != "admin")
            {
                SqlCommand cmd = new SqlCommand("Select e.CompanyId,e.DeptId,e.EmployeeId,e.EmployeName,c.CompanyName from Employees e inner join Companies c on e.CompanyId=c.CompanyId where e.EmployeeId='" + Session["name"].ToString() + "'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    lblcompanyid.Text = dr["CompanyId"].ToString();
                    lblcompanyname.Text = dr["CompanyName"].ToString();
                    lbldeptid.Text = dr["DeptId"].ToString();
                    lblemployeeid.Text = dr["EmployeeId"].ToString();
                }
            }
        }
        private void BindGrid1()
        {
            if (Session["name"].ToString() != "admin")
            {
                DataTable dt = new DataTable();
                String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;


                SqlConnection conn1 = new SqlConnection(strConnString);
                SqlDataAdapter sda = new SqlDataAdapter();
                SqlCommand comm1 = new SqlCommand("Proc_Monthlydates");
                comm1.CommandType = CommandType.StoredProcedure;
                comm1.Parameters.AddWithValue("@Basic", ViewState["Filter"].ToString());
                comm1.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text.Trim();

                if (lblcompanyid.Text.ToString() == "")
                {
                    comm1.Parameters.Add("@Company", SqlDbType.VarChar).Value = DBNull.Value;
                }
                else
                {
                    comm1.Parameters.Add("@Company", SqlDbType.VarChar).Value = lblcompanyid.Text;
                }

                if (lbldeptid.Text.ToString() == "")
                {
                    comm1.Parameters.Add("@Department", SqlDbType.VarChar).Value = DBNull.Value;
                }
                else
                {
                    comm1.Parameters.Add("@Department", SqlDbType.VarChar).Value = lbldeptid.Text;
                }

                if (lblemployeeid.Text.ToString() == "")
                {


                    comm1.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = DBNull.Value;
                }
                else
                {
                    comm1.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = lblemployeeid.Text;
                }



                comm1.Connection = conn1;
                try
                {
                    conn1.Open();
                    GridView1.EmptyDataText = "No Records Found";

                    GridView1.DataSource = comm1.ExecuteReader();
                    GridView1.DataBind();

                    DropDownList ddlCountry =
                        (DropDownList)GridView1.HeaderRow.FindControl("ddlCountry");
                }
                catch (Exception ex1)
                {
                    throw ex1;
                }
                finally
                {

                    conn1.Close();
                    conn1.Dispose();

                }
                foreach (GridViewRow dr in GridView1.Rows)
                {
                    foreach (TableCell td in dr.Cells)
                    {
                        if (td.Text == "P" || td.Text == "H/D" || td.Text == "WOP" || td.Text == "P(NOP)")
                            count++;
                        else if (td.Text == "A")
                            count1++;

                    }
                }
                foreach (GridViewRow dr2 in GridView1.Rows)
                {
                    foreach (TableCell td2 in dr2.Cells)
                    {
                        if (td2.Text == "H/D")
                            count3++;


                    }
                }
                foreach (GridViewRow dr1 in GridView1.Rows)
                {
                    foreach (TableCell td1 in dr1.Cells)
                    {
                        if (td1.Text == "P" || td1.Text == "A" || td1.Text == "H/D" || td1.Text == "WOP" || td1.Text == "L" || td1.Text == "H" || td1.Text == "P(NOP)")
                            count2++;


                    }
                }
                foreach (GridViewRow dr1 in GridView1.Rows)
                {
                    foreach (TableCell td1 in dr1.Cells)
                    {
                        if (td1.Text == "WOP")
                            count4++;


                    }
                }
                foreach (GridViewRow dr1 in GridView1.Rows)
                {
                    foreach (TableCell td1 in dr1.Cells)
                    {
                        if (td1.Text == "P(NOP)")
                            count7++;


                    }
                }
                foreach (GridViewRow dr1 in GridView1.Rows)
                {
                    foreach (TableCell td1 in dr1.Cells)
                    {
                        if (td1.Text == "L")
                            count5++;


                    }
                }
                foreach (GridViewRow dr1 in GridView1.Rows)
                {
                    foreach (TableCell td1 in dr1.Cells)
                    {
                        if (td1.Text == "H")
                            count6++;
                    }
                }
                Label2.Text = Convert.ToDouble(count).ToString();
                Label3.Text = Convert.ToDouble(count1).ToString();
                Label1.Text = Convert.ToDouble(count2).ToString();
                Label5.Text = Convert.ToDouble(count3).ToString();
                Label6.Text = Convert.ToDouble(count4).ToString();
                Label7.Text = Convert.ToDouble(count5).ToString();
                Label8.Text = Convert.ToDouble(count6).ToString();
                Label9.Text = Convert.ToDouble(count7).ToString();
                Double a = Convert.ToDouble(Label1.Text);
                Double b = Convert.ToDouble(Label2.Text);
                Double c = Convert.ToDouble(Label3.Text);
                //decimal d = Convert.ToDecimal(Label4.Text);

                Double d = Math.Round(Convert.ToDouble((b / a) * 100), 2);
                //Double ans = d * 100;
                Label4.Text = Convert.ToDouble(d).ToString();
                //Label4.Text = Convert.ToDecimal(d).ToString();
                //d = (b / a) * 100;

                //Label34.Text=((Label2.Text) /(Label1.Text)).ToString("P");
            }

        }

        protected void Month_Change(object sender,EventArgs e)
        {
            divmsg.Style["display"] = "block";
            ViewState["Filter"] = "Detailed Summary Attendance";
            //Bindvalues();
            BindGrid1();
        }

        protected void Button6_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
            "attachment;filename='" + lblcompanyname.Text + "'.doc");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-word ";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            GridView1.AllowPaging = false;

            GridView1.RenderControl(hw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition",
            "attachment;filename='" + lblcompanyname.Text + "'.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            GridView1.AllowPaging = false;


            //Change the Header Row back to white color
            GridView1.HeaderRow.Style.Add("background-color", "#FFFFFF");

            //Apply style to Individual Cells
            GridView1.HeaderRow.Cells[0].Style.Add("background-color", "green");
            GridView1.HeaderRow.Cells[1].Style.Add("background-color", "green");
            GridView1.HeaderRow.Cells[2].Style.Add("background-color", "green");
            GridView1.HeaderRow.Cells[3].Style.Add("background-color", "green");


            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                GridViewRow row = GridView1.Rows[i];

                //Change Color back to white
                row.BackColor = System.Drawing.Color.White;

                //Apply text style to each Row
                row.Attributes.Add("class", "textmode");

                //Apply style to Individual Cells of Alternating Row
                if (i % 2 != 0)
                {
                    row.Cells[0].Style.Add("background-color", "#C2D69B");
                    row.Cells[1].Style.Add("background-color", "#C2D69B");
                    row.Cells[2].Style.Add("background-color", "#C2D69B");
                    row.Cells[3].Style.Add("background-color", "#C2D69B");
                }
            }
            GridView1.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }



        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition",
             "attachment;filename='" + lblcompanyname.Text + "'.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            GridView1.AllowPaging = false;

            GridView1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename='" + lblcompanyname.Text + "'.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sBuilder = new System.Text.StringBuilder();
            for (int index = 0; index < GridView1.Columns.Count; index++)
            {
                sBuilder.Append(GridView1.Columns[index].HeaderText + ',');
            }
            sBuilder.Append("\r\n");
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                for (int k = 0; k < GridView1.HeaderRow.Cells.Count; k++)
                {
                    sBuilder.Append(GridView1.Rows[i].Cells[k].Text.Replace(",", "") + ",");
                }
                sBuilder.Append("\r\n");
            }
            Response.Output.Write(sBuilder.ToString());
            Response.Flush();
            Response.End();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}