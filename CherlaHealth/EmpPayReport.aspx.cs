﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class EmpPayReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = Session["Payslip"].ToString();
            payslip();
        }

        private void payslip()
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con = new SqlConnection(connstrg);
            con.Open();
            SqlCommand cmd = new SqlCommand("Select Month,Year,Name,EmpNo,PFNo,Departrment,Workingdays,BankAc,CONVERT(CHAR(10), DOB, 101) as DOB,paybledays,Designation,CONVERT(CHAR(10), DOJ, 101) as DOJ,Basic,HRA,DA,EA,PF,MobileBill,CA,ESI,ASa,IT,travell,paydayd,Deducation,Total,Amountwords,ChildAllowance,TTotal from Payslip where Id='" + Label1.Text + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                //EName.Text = reader["Name"].ToString();
                //ENumber.Text = reader["EmpNo"].ToString();
                //PFnumber.Text = reader["PFNo"].ToString();

                //Department.Text = reader["Departrment"].ToString();
                //DOB.Text = reader["DOB"].ToString();
                //WorkingDays.Text = reader["Workingdays"].ToString();
                //Designation.Text = reader["Designation"].ToString();
                //DOJ.Text = reader["DOJ"].ToString();
                //Label2.Text = reader["Month"].ToString();
                //Label3.Text = reader["Year"].ToString();

                //PayableDays.Text = reader["paybledays"].ToString();
                //Basic.Text = reader["Basic"].ToString();
                //HAllowance.Text = reader["HRA"].ToString();
                //DAllowances.Text = reader["DA"].ToString();
                //PF.Text = reader["PF"].ToString();
                //CAlowance.Text = reader["CA"].ToString();
                //ESI.Text = reader["ESI"].ToString();
                //EAllowance.Text = reader["EA"].ToString();
                //ASalary.Text = reader["ASa"].ToString();
                //IT.Text = reader["IT"].ToString();
                //LPay.Text = reader["paydayd"].ToString();
                //Total.Text = reader["Total"].ToString();
                //Earning.Text = reader["Deducation"].ToString();
                //NPay.Text = reader["TTotal"].ToString();
                //AWords.Text = reader["Amountwords"].ToString();
                lblemployee.Text = reader["Name"].ToString();
                lblempid.Text = reader["EmpNo"].ToString();
                lblpfno.Text = reader["PFNo"].ToString();
                lbldoj.Text = reader["DOJ"].ToString();
                lbldays.Text = reader["Workingdays"].ToString();
                lbldesignation.Text = reader["Designation"].ToString();
                month.Text = reader["Month"].ToString();
                month1.Text = reader["Month"].ToString();
                year.Text = reader["Year"].ToString();
                year1.Text = reader["Year"].ToString();
                lblpay.Text = reader["paybledays"].ToString();
                lblacno.Text = reader["BankAc"].ToString();
                lblbasic.Text = reader["Basic"].ToString();
                lblpf.Text = reader["PF"].ToString();
                lblhr.Text = reader["HRA"].ToString();
                lblesi.Text = reader["ESI"].ToString();
                lblconveynce.Text = reader["CA"].ToString();
                lblmobile1.Text = reader["MobileBill"].ToString();
                lbladvance.Text = reader["ASa"].ToString();
                lblitax.Text = reader["IT"].ToString();
                lbltravell.Text = reader["travell"].ToString();
                lbltotal.Text = reader["Total"].ToString();
                lbldedtot.Text = reader["Deducation"].ToString();
                lblnet.Text = reader["TTotal"].ToString();
                lblwords.Text = reader["Amountwords"].ToString();
                lblchild.Text = reader["ChildAllowance"].ToString();
            }

            reader.Close();

            con.Close();
        }
    }
}