﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace CherlaHealth
{
    public partial class AddLeaveTypes : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            txtdate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Insert Into LeaveTypes(LeaveTypeName,LeaveTypeCode,Days,Description,Date,type,typeid) values('" + txtleave.Text + "','" + txtshort.Text + "','" + txtdays.Text + "','" + txtdescription.Text + "','"+txtdate.Text+"','"+droptype.SelectedItem.Text+"','"+droptype.SelectedValue+"')", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("ViewLeaveTypes.aspx");
        }
    }
}