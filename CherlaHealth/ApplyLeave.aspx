﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee.Master" AutoEventWireup="true" CodeBehind="ApplyLeave.aspx.cs" Inherits="CherlaHealth.ApplyLeave" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Apply Leave</h2>

            <hr />

             <div class="col-md-12">
            <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
            <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </ContentTemplate></asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel9" runat="server"><ContentTemplate>
            <asp:Label ID="lblmsg" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                </ContentTemplate></asp:UpdatePanel>
                 </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3" runat="server" visible="false"   id="div1">
                <div class="form-group">
                    <asp:UpdatePanel ID="update" runat="server"><ContentTemplate>
                    Select Type <span style="color:red">*</span><asp:DropDownList ID="droptype" CssClass="form-control" OnSelectedIndexChanged="type_Change" AutoPostBack="true" runat="server">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="2">Medicare</asp:ListItem>
                        <asp:ListItem Value="1">Non-Medicare</asp:ListItem>
                                </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="droptype" ForeColor="red"></asp:RequiredFieldValidator>
                </ContentTemplate></asp:UpdatePanel>
                        </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-4">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    Leave Type<span style="color:red">*</span> <asp:DropDownList ID="dropleave" CssClass="form-control" runat="server"></asp:DropDownList>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropleave" ForeColor="red" InitialValue="0"></asp:RequiredFieldValidator>
 </ContentTemplate></asp:UpdatePanel>
                </div>
            </div>
              <div class="col-md-4">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                    Begin Date<span style="color:red">*</span> <asp:TextBox ID="txtbegin" CssClass="form-control" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtbegin" Format="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtbegin" ForeColor="red"></asp:RequiredFieldValidator>
             </ContentTemplate></asp:UpdatePanel>
                </div>
                  </div>
             <div class="col-md-4">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                    End Date<span style="color:red">*</span> <asp:TextBox ID="txtend" CssClass="form-control" runat="server" OnTextChanged="Date_change" AutoPostBack="true"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtend" Format="dd/MM/yyyy" />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtend" ForeColor="red"></asp:RequiredFieldValidator>
          </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="col-md-4">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                    Duration <asp:TextBox ID="txtduration" CssClass="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                        </ContentTemplate></asp:UpdatePanel>
                    </div>
                </div> 
            
            <div class="col-md-4">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
                    Employee Name <span style="color:red">*</span><asp:DropDownList ID="dropemployee" CssClass="form-control" runat="server" OnSelectedIndexChanged="Employee_Change" AutoPostBack="true"></asp:DropDownList>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="dropemployee" ForeColor="red"></asp:RequiredFieldValidator>
 </ContentTemplate></asp:UpdatePanel>
                     </div>
                  </div>
             <div class="col-md-4">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server"><ContentTemplate>
                    Balance Leaves <asp:TextBox ID="txtbalance" CssClass="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                        </ContentTemplate></asp:UpdatePanel>
                    </div>
                 </div>
            <div class="clearfix"></div>
             <div class="col-md-4">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server"><ContentTemplate>
                    Reason<span style="color:red">*</span> <asp:TextBox ID="txtreason" CssClass="form-control" runat="server"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ControlToValidate="txtreason" ForeColor="red"></asp:RequiredFieldValidator>
           </ContentTemplate></asp:UpdatePanel>
                </div>
                 </div>

            <div class="col-md-3" style="padding-top:18px">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit" />
                    </div>
                </div>


            </div>
        </div>
</asp:Content>
