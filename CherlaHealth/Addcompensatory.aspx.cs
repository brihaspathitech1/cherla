﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace CherlaHealth
{
    public partial class Addcompensatory : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'",conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void btnSubmit_Click(object sender,EventArgs e)
        {
            DateTime date = System.DateTime.Now;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Insert into tbl_compensatoryoff(date,EmployeeId,EmployeeName,Worked_date,description,CL) values ('" + date.ToString("yyyy-MM-dd") + "','" + dropemp.SelectedValue + "','" + dropemp.SelectedItem.Text + "','" + txtdate.Text + "','" + txtdescription.Text + "','1')",conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("ViewCompensatory.aspx");
        }

    }
}