﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs" Inherits="CherlaHealth.AddCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            color: #FF0000;
        }
    </style>
    <script type="text/javascript">
        function ValidateCheckBox(sender, args) {
            if (document.getElementById("<%=Weekoff1.ClientID %>").checked == true) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }

    function ValidateCheckBox1(sender, args) {
        if (document.getElementById("<%=weekoff2.ClientID %>").checked == true) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox2(sender, args) {
        if (document.getElementById("<%=CHDW.ClientID %>").checked == true) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox3(sender, args) {
        if (document.getElementById("<%=cawdil.ClientID %>").checked == true) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox4(sender, args) {
        if (document.getElementById("<%=MHDL.ClientID %>").checked == true) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox5(sender, args) {
        if (document.getElementById("<%=MHDEGB.ClientID %>").checked == true) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBoxList(sender, args) {
        var checkBoxList = document.getElementById("<%=weekoff2list.ClientID %>");
        var checkboxes = checkBoxList.getElementsByTagName("input");
        var isValid = false;
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                isValid = true;
                break;
            }
        }
        args.IsValid = isValid;
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Add Category</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           <div class="col-md-12 padbot">
            
                <div class="col-md-12">

                    <div class="col-sm-3">
                        Category 
            <span class="style1">*</span><asp:TextBox ID="Category" class="form-control" runat="server" placeholder="Category" ></asp:TextBox>

                        <asp:Label ID="lblStatus" runat="server"
                            Style="color: #FF0000"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Category" runat="server" Style="color: #FF0000" ErrorMessage="Required"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-sm-3">
                        Short Name<span class="style1">*</span>
                        <asp:TextBox ID="Shortname" class="form-control" runat="server" placeholder="Short Name "  ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="Shortname" runat="server" Style="color: #FF0000" ErrorMessage="Required"></asp:RequiredFieldValidator>
           
                    </div>
                    
                <div class="col-md-3">
                    OT Formula

            <asp:DropDownList ID="OTformula" runat="server" class="form-control">
                <asp:ListItem>(Out Punch)-(Shift End Time)</asp:ListItem>
                <asp:ListItem>(Total Duration)+(Shift Hours)</asp:ListItem>
                <asp:ListItem>(Early Going)+(Late Coming)</asp:ListItem>
                <asp:ListItem>OT Not Applicable</asp:ListItem>
            </asp:DropDownList>

                </div>
                <div class="col-md-3">
                    Min OT
                    <asp:TextBox ID="Minot" runat="server" class="form-control" Text="0"></asp:TextBox>
                </div>
                    <div class="clearfix"></div>
                        <div class="col-md-3">
                        Grace Time for Late Login

                        <asp:TextBox ID="GRT" runat="server" class="form-control" Text="0"></asp:TextBox>


                    </div>
                    <div class="col-md-2" style="padding-top:24px">
                        Minutes
                    </div>
                          <div class="col-md-3">
                        Grace Time for Early Logout
                        <asp:TextBox ID="NglLstpunch1" runat="server" class="form-control" Text="0"></asp:TextBox>
                    </div>

                    <div class="col-md-2" style="padding-top:24px">
                        Minutes
                    </div>

                    <div class="clearfix"></div>
                    <br />
                       <div class="col-md-2 padtop15" style="width: 11.666667%">
                        <span class="style1">*</span>
                        <asp:CheckBox ID="Weekoff1" runat="server" Text="Weekly off 1"></asp:CheckBox>
                        <asp:CustomValidator ID="CustomValidator1" Style="color: #FF0000" runat="server" ErrorMessage="Required" ClientValidationFunction="ValidateCheckBox"></asp:CustomValidator>
                    </div>
                    <div class="col-md-6" style="width: 38%">

                        <asp:DropDownList ID="Weeloff11" runat="server" class="form-control" Width="63%">
                            <asp:ListItem>Sunday</asp:ListItem>
                            <asp:ListItem>Monday</asp:ListItem>
                            <asp:ListItem>Tuesday</asp:ListItem>
                            <asp:ListItem>Wednesday</asp:ListItem>
                            <asp:ListItem>Thursday</asp:ListItem>
                            <asp:ListItem>Friday</asp:ListItem>
                            <asp:ListItem>Saturday</asp:ListItem>
                        </asp:DropDownList>

                    </div>

                    <div class="col-md-2 padtop15" style="width: 11.666667%">
                        
                        <asp:CheckBox ID="weekoff2" runat="server" Text="Weekly off 2"></asp:CheckBox>
                        
                    </div>
                    <div class="col-md-6" style="width: 38%">
                        <asp:DropDownList ID="Weekoff22" runat="server" class="form-control" Width="55%">
                            <asp:ListItem>Sunday</asp:ListItem>
                            <asp:ListItem>Monday</asp:ListItem>
                            <asp:ListItem>Tuesday</asp:ListItem>
                            <asp:ListItem>Wednesday</asp:ListItem>
                            <asp:ListItem>Thursday</asp:ListItem>
                            <asp:ListItem>Friday</asp:ListItem>
                            <asp:ListItem>Saturday</asp:ListItem>
                        </asp:DropDownList>
                    </div>




                    <div class="col-md-2">
                        <asp:CheckBoxList ID="weekoff2list" runat="server" Visible="false"
                            RepeatDirection="Horizontal" Height="16px" Width="274px">
                            <asp:ListItem style="padding: 5px">1st  </asp:ListItem>
                            <asp:ListItem style="padding: 5px">2nd</asp:ListItem>
                            <asp:ListItem style="padding: 5px">3rd</asp:ListItem>
                            <asp:ListItem style="padding: 5px">4th</asp:ListItem>
                        </asp:CheckBoxList>
                        
                    </div>
                    <div class="clearfix"></div>
                        <div class="col-md-5">
                        <span class="style1">*</span>
                        <asp:CheckBox ID="CHDW" runat="server"
                            Text="Calculate Half Day if Work Duration is less than"></asp:CheckBox>
                        <asp:CustomValidator ID="CustomValidator3" Style="color: #FF0000" runat="server" ErrorMessage="Required" ClientValidationFunction="ValidateCheckBox2"></asp:CustomValidator>
                    </div>

                    <div class="col-md-5">
                        <asp:TextBox ID="CHWD1" runat="server" class="form-control" Text="0"></asp:TextBox>
                    </div>
                    <div class="col-md-2 padtop15">
                        Minutes
                    </div>
                     <div class="clearfix"></div>
                    <br />
                             <div class="col-md-5">
                    <span class="style1">*</span>
                    <asp:CheckBox ID="cawdil" runat="server"
                        Text="Calculate Absent if Work Duration is less than"></asp:CheckBox>
                    <asp:CustomValidator ID="CustomValidator4" Style="color: #FF0000" runat="server" ErrorMessage="Required" ClientValidationFunction="ValidateCheckBox3"></asp:CustomValidator>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="CAW" runat="server" class="form-control" Text="0"></asp:TextBox>
                </div>

                <div class="col-md-2 padtop15">
                    Minutes
                </div>
                 
                     <div class="clearfix"></div>
                     <br />
                             <div class="col-md-5">
                <span class="style1">*</span>
                <asp:CheckBox ID="MHDL" runat="server" Text="Mark Half Day If Late By"></asp:CheckBox>
                <asp:CustomValidator ID="CustomValidator5" Style="color: #FF0000" runat="server" ErrorMessage="Required" ClientValidationFunction="ValidateCheckBox4"></asp:CustomValidator>
            </div>
            <div class="col-md-5">

                <asp:TextBox ID="MHDL1" runat="server" class="form-control" Text="0"></asp:TextBox>
            </div>
            <div class="col-md-2 padtop15">
                Minutes
            </div>
                   
                     <div class="clearfix"></div>
                     <br />
                       <div class="col-md-5">
            <span class="style1">*</span>
            <asp:CheckBox ID="MHDEGB" runat="server" Text="Mark Half Day If Early Going By"></asp:CheckBox>
            <asp:CustomValidator ID="CustomValidator6" Style="color: #FF0000" runat="server" ErrorMessage="Required" ClientValidationFunction="ValidateCheckBox5"></asp:CustomValidator>
        </div>
        <div class="col-md-5">
            <asp:TextBox ID="MHDEGB1" runat="server" class="form-control" Text="0"></asp:TextBox>
        </div>
        <div class="col-md-2 padtop15">
            Minutes
        </div>
                     <div class="clearfix"></div>

                </div>
               
    
 
   
                 <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="Submit" Style="margin-left: 24px" class=" btn btn-success" type="btn" />
  
    
    </div>
            </div>
        </div>
</asp:Content>
