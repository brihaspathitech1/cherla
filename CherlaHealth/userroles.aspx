﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="userroles.aspx.cs" Inherits="CherlaHealth.userroles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper site-min-height">
            <div class="row mt">
                <div class=" col-lg-12 panel" style="padding: 5px; margin-top: -30px">
                    <div class=" col-lg-12">

                        <asp:scriptmanager id="ScriptManager1" runat="server"> </asp:scriptmanager>
                        <div class=" col-lg-12">
                            <h2>User Logins<span style="float: right"></span></h2>
                            &nbsp;<hr />
                        </div>
                        <br />
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-3 ">
                                    Name
            <asp:dropdownlist id="name" runat="server" class="form-control" autopostback="true" onselectedindexchanged="name_SelectedIndexChanged">
            </asp:dropdownlist>

                                    <asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" errormessage="Select name"
                                        forecolor="Red" controltovalidate="Name"></asp:requiredfieldvalidator>
                                </div>
                                <div class="col-md-3 ">
                                    Department
          <asp:dropdownlist id="dropdesignation" runat="server" class="form-control">
                                        </asp:dropdownlist>
                                    <asp:textbox id="Designation" runat="server" class="form-control" visible="false"></asp:textbox>

                                    <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" errormessage="Select name"
                                        forecolor="Red" controltovalidate="Name"></asp:requiredfieldvalidator>
                                </div>

                                <div class="col-md-3 ">
                                    Roles
                                    <asp:dropdownlist id="Roles" runat="server" class="form-control" onselectedindexchanged="designation_click" autopostback="true">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>Administrator</asp:ListItem>
                                        <asp:ListItem>Manager</asp:ListItem>
                                        <asp:ListItem>Individual</asp:ListItem>
                                    </asp:dropdownlist>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" errormessage="Select name"
                                        forecolor="Red" controltovalidate="Roles"></asp:requiredfieldvalidator>
                                </div>
                                <div class="col-md-3 ">
                                    Password
                                 <asp:textbox id="Password" runat="server" class="form-control"></asp:textbox>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" errormessage="Enter password"
                                        forecolor="Red" controltovalidate="Password"></asp:requiredfieldvalidator>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <asp:button id="Submit" runat="server" text="submit" class="btn btn-info" onclick="Submit_Click" />
                                <asp:label id="Label1" runat="server" text="Duplicate Entry....!" forecolor="Red" font-bold="true" visible="false"></asp:label>
                            </div>
                            <asp:label id="lbldesignation" runat="server" text="" visible="false"></asp:label>
                            <asp:label id="lbladmin" runat="server" text="" visible="false"></asp:label>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</asp:Content>

