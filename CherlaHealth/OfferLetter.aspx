﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="OfferLetter.aspx.cs" Inherits="CherlaHealth.OfferLetter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Offer Letter</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
       
        <div class="box-body">
             <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
                  Salutation<span style="color:red">*</span> <asp:DropDownList ID="salut"  CssClass="form-control" runat="server">
                      <asp:ListItem>Mr.</asp:ListItem>
                      <asp:ListItem>Mrs.</asp:ListItem>
                      <asp:ListItem>Ms.</asp:ListItem>      </asp:DropDownList>
                       </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
             <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="update" runat="server"><ContentTemplate>
                  Select Candidate<span style="color:red">*</span> <asp:DropDownList ID="dropcandidate" OnSelectedIndexChanged="candidates_Change" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="dropcandidate" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                 </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
             <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="em" runat="server"><ContentTemplate>
                         Select Email<span style="color:red">*</span><asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required" ControlToValidate="TextBox1" ForeColor="Red" ></asp:RequiredFieldValidator>
                                                             </ContentTemplate>

                     </asp:UpdatePanel>
                     </div></div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                         Designation <span style="color:red">*</span> <asp:TextBox ID="txtdesignation" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtdesignation" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
             <div class="clearfix"></div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel7" runat="server"><ContentTemplate>
                         Note: <span style="color:red">*</span> <asp:TextBox ID="TextBox2" CssClass="form-control" runat="server"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required" ControlToValidate="TextBox2" ForeColor="Red"></asp:RequiredFieldValidator>
                        --%>     </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                         Joining Date<span style="color:red">*</span> <asp:TextBox ID="joindate" CssClass="form-control" runat="server"></asp:TextBox>
                         <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="joindate" runat="server" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="joindate" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="Updatepanel25" runat="server"><ContentTemplate>
                        Time<span style="color:red">*</span><asp:TextBox ID="jointime" CssClass="form-control" runat="server" placeholder="eg-10:30am"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator25" runat="server" ErrorMessage="Required" ControlToValidate="jointime" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                       </ContentTemplate></asp:UpdatePanel>
                </div>
            </div>
                 <div class="col-md-3">
                     <div class="form-group"><asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                         Address <span style="color:red">*</span> <asp:DropDownList ID="dropaddress" CssClass="form-control" runat="server">
                              <asp:ListItem></asp:ListItem>
                             <asp:ListItem>Hyderabad</asp:ListItem>
                             <asp:ListItem>Balkampet,Hyderabad</asp:ListItem>
                              <asp:ListItem>Yousufguda,Hyderabad</asp:ListItem>
                              <asp:ListItem>Gachibowli,Hyderabad</asp:ListItem>
                             <asp:ListItem>Bandlaguda,Hyderabad</asp:ListItem>
                                                                  </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ControlToValidate="dropaddress" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                         </ContentTemplate></asp:UpdatePanel>
                         </div>
                 </div>

            <div class="clearfix"></div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                         Probation Period <span style="color:red">*</span> <asp:TextBox ID="txtperiod" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="txtperiod" ForeColor="Red"></asp:RequiredFieldValidator>
                         <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtperiod" FilterType="Numbers" />
                         </ContentTemplate></asp:UpdatePanel>
                     </div>
                </div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
                         Salary per Month<span style="color:red">*</span><asp:TextBox ID="txtsal" Placeholder="Enter only digits" CssClass="form-control" runat="server" OnTextChanged="Salary_Change" AutoPostBack="true"></asp:TextBox>

                         <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtsal" FilterType="Numbers" />
                          <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtsal" ForeColor="Red"></asp:RequiredFieldValidator>
                          --%>   </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>
            <div class="col-md-6" style="padding-top:18px">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel6" runat="server"><ContentTemplate>
                             
                         Salary In Rupees: <asp:Label ID="lblrupees" runat="server" Text="" Font-Italic="true" ForeColor="#3333ff" Font-Size="Larger" Font-Bold="true"></asp:Label>
                             </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>
            <div class="clearfix"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                </div>
            </div>
            </div>
        </div>

</asp:Content>
