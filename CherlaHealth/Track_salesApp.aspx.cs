﻿
using inventory.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class Track_salesApp : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        string latitude = string.Empty;
        string longitude = string.Empty;
        string time = string.Empty;
        #region Navigator
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod == "POST")
            {
                try
                {
                    string request = Request.Params["request"].ToString();
                    switch (request)
                    {
                        case "login":
                            login();
                            break;
                        case "checkin_state":
                            state();
                            break;
                        case "check_in":
                            checkin();
                            break;
                        case "check_out":
                            checkout();
                            break;
                        case "coordinate":
                            co_ordinates();
                            break;
                        case "list_coordinates":
                            list_coordinates();
                            break;
                        case "add_client":
                            add_client();
                            break;
                        case "update_client":
                            update_client();
                            break;
                        case "status":
                            status();
                            break;
                        case "update_status":
                            updatestatus();
                            break;
                        case "list_clients":
                            list_clients();
                            break;
                        case "list_status":
                            list_status();
                            break;
                        case "meeting_status":
                            list_entirestatus();
                            break;
                        case "admin_checkin":
                            admin_checkin();
                            break;
                        case "list_chechin":
                            list_checkin();
                            break;
                        case "meetings":
                            meetings();
                            break;
                        case "meeting_coordinates":
                            meetingdetails();
                            break;
                        case "online":
                            onlinedata();
                            break;
                        
                            //case "broadcast":
                            //    broadcast();
                            //    break;

                    }

                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\": true,\"result\":\"Server Error=" + ex.Message.ToString() + "\"}");
                }
            }
        }

        #endregion

        #region webservices

        protected void login()
        {
            string employee_id = Request.Params["employee_id"].ToString();
            string password = Request.Params["password"].ToString();
            //string mac_id = Request.Params["mac_id"].ToString();

            DataSet ds = DataQueries.SelectCommon("select a.EmployeeId,a.DOJ,a.EmployeName,a.DeptId,a.mac_id,a.cellno,a.Desigantion,a.CompanyEmail,b.Department from Employees a inner join Department b on a.DeptId=b.DeptId  where a.EmployeeId='" + employee_id + "' and a.password='" + password + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                //string dbmac_id = ds.Tables[0].Rows[0]["mac_id"].ToString();
                string employe_name = ds.Tables[0].Rows[0]["EmployeName"].ToString();

                //if (dbmac_id == mac_id)
                //{
                string name = ds.Tables[0].Rows[0]["EmployeName"].ToString().Trim();
                string phone = ds.Tables[0].Rows[0]["cellno"].ToString().Trim();
                string email = ds.Tables[0].Rows[0]["CompanyEmail"].ToString().Trim();
                string department = ds.Tables[0].Rows[0]["department"].ToString().Trim();
                string designation = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string doj = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim();
                string dept_id = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"phone\":\"" + phone + "\",\"email\":\"" + email + "\",\"department\":\"" + department + "\",\"designation\":\"" + designation + "\",\"doj\":\"" + doj + "\",\"deptid\":" + dept_id + "}");

                //  Response.Write("{\"error\":\"false\",\"message\":\"login successfully\"}");
                //}
                //else
                //{
                //    Response.Write("{\"error\":true,\"message\":\"login failed\"}");
                //}


            }
            else
            {
                Response.Write("{\"error\":true,\"message\":\"login failed\"}");
            }
        }

        
        protected void state()
        {
            string value = string.Empty;
            string employee_id = Request.Params["employee_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select * from tblcheckin where (status=1 or status=0) and convert(date,in_time)=convert(date,getdate()) and employee_id='" + employee_id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                value = ds.Tables[0].Rows[0]["status"].ToString();
            }
            if (value == "")
            {
                Response.Write("{\"error\": false,\"state\":" + 0 + "}");
            }
            else if (value == "1")
            {
                Response.Write("{\"error\": false,\"state\":" + 1 + "}");
            }
            else if (value == "0")
            {
                Response.Write("{\"error\": false,\"state\":" + 2 + "}");
            }
        }

        protected void checkin()
        {
            string in_lat = Request.Params["in_latitude"].ToString();
            string in_lon = Request.Params["in_longitude"].ToString();
            string employee_id = Request.Params["employee_id"].ToString();
            string address = Request.Params["address"].ToString();
            DateTime dt = DateTime.Now;
            string in_time = dt.ToString("yyyy - MM - dd hh:mm:ss");

            int empID = Convert.ToInt32(employee_id);

            //string in_time = Request.Params["in_time"].ToString();
            //string out_time = Request.Params["out_time"].ToString();

            DataSet ds = DataQueries.SelectCommon("select * from tblcheckin where employee_id='" + employee_id + "' and convert(date,in_time)=convert(date,getdate())");

            if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("{\"error\": true,\"message\":\" You Checkin\"}");
                //Hr push notifications
                string emp_name = employee_name(empID);
                string hr_title = "Check-In";
                string hr_body = emp_name + "Checked In";
                string hr_token = token(1);
                notification(hr_token, hr_title, hr_body);


            }
            else
            {
                DataQueries.InsertCommon("insert into tblcheckin (in_latitude,in_longitude,in_time,employee_id,Status,addres) values('" + in_lat + "','" + in_lon + "','" + in_time + "','" + employee_id + "','1','"+address+"')");
                Response.Write("{\"error\": false,\"message\":\"Checkin Successsfully\"}");
            }
            DataSet ds1 = DataQueries.SelectCommon("select * from tblattendance where UserId='" + employee_id + "' and convert(date,DateOfTransaction)=convert(date,getdate())");

            if (ds1.Tables[0].Rows.Count > 0)
            {
               // DataQueries.InsertCommon("update tblattendance set DateOfTransaction='" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "' where UserId='" + employee_id + "'and convert(date,DateOfTransaction)=convert(date,getdate())");

            }
            else
            {
                DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','OutDoorEntry')");

            }

        }

        protected void checkout()
        {
            string out_lat = Request.Params["out_latitude"].ToString();
            string out_long = Request.Params["out_longitude"].ToString();
            string employee_id = Request.Params["employee_id"].ToString();
            string address = Request.Params["address"].ToString();
            DateTime dt = DateTime.Now;
            string out_time = dt.ToString("yyyy-MM-dd hh:mm:ss");

            int empID = Convert.ToInt32(employee_id);

            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Update tblcheckin set out_latitude='" + out_lat + "',out_longitude='" + out_long + "',out_time='" + out_time + "',status='0',outaddress='"+address+"' where employee_id='" + employee_id + "' and convert(date,in_time)=convert(date,getdate()) and status='1'", conn);
            conn.Open();
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            if (i == 1)
            {
                Response.Write("{\"error\": false,\"message\":\"Checkout Successsfully\"}");

                //Hr push notifications
                string emp_name = employee_name(empID);
                string hr_title = "Check-Out";
                string hr_body = emp_name + "Checked Out";
                //string hr_token = token(1);
               // notification(hr_token, hr_title, hr_body);
            }
            else
            {
                Response.Write("{\"error\": true,\"message\":\"Please Check in first\"}");
            }



            //outpunch
            //string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            //DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employee_id + "' and convert(nvarchar(10),Time,126)='" + date + "' and status='1'");
            //if (ds.Tables[0].Rows.Count > 0)
            //{
               // DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','OutDoorEntry')");
              //  Response.Write("{\"error\": false,\"message\":\"Out Punch Request Submitted Successsfully\"}");
//}
           // else
           // {
               // DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','OutDoorEntry')");
              //  Response.Write("{\"error\": false,\"message\":\"Out Punch Request Submitted Successsfully\"}");
           // }
            
        }

        protected void list_coordinates()
        {
            string eid = Request.Params["employee_id"].ToString();
            string date = Request.Params["date"].ToString();

            DataSet ds = DataQueries.SelectCommon("select * from tblcoordinates where employee_id='" + eid + "' and convert(date,time)='" + date + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void add_client()
        {
            string name = Request.Params["name"].ToString();
            string address = Request.Params["address"].ToString();
            string email = Request.Params["email"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            string eid = Request.Params["employee_id"].ToString();

            DataQueries.InsertCommon("Insert into tblclient(name,address,email,mobileno,employee_id) values('" + name + "','" + address + "','" + email + "','" + mobile + "','" + eid + "' )");
            DataSet ds = DataQueries.SelectCommon("select * from tblclient where name='" + name + "' and mobileno='" + mobile + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["id"].ToString();
                Response.Write("{\"error\": false,\"id\":" + id + "}");
            }
        }

        protected void update_client()
        {
            string name = Request.Params["name"].ToString();
            string address = Request.Params["address"].ToString();
            string email = Request.Params["email"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            string cid = Request.Params["client_id"].ToString();
            DataQueries.UpdateCommon("update tblclient set name='" + name + "',address='" + address + "',email='" + email + "',mobileno='" + mobile + "' where id='" + cid + "'");
            Response.Write("{\"error\": false,\"message\":\"Updated Successfully\"}");
        }
        protected void status()
        {
            string client_id = Request.Params["client_id"].ToString();
            string eid = Request.Params["employee_id"].ToString();
            string status = Request.Params["status"].ToString();
            string mtime = Request.Params["meeting_time"].ToString();
            DateTime dt = DateTime.Now;
            string date = dt.ToString("yyyy-MM-dd");
            //string mdate = dt.ToString();

            DataSet ds = DataQueries.SelectCommon("Select * from tblstatus where client_id='" + client_id + "' and employee_id='" + eid + "' and status=0");
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("{\"error\": true,\"message\":\"You already set the meeting time\"}");
            }
            else
            {

                DataQueries.InsertCommon("Insert into tblstatus(date,client_id,employee_id,status,meeting_time,normaldate) values('" + date + "','" + client_id + "','" + eid + "','" + status + "','" + mtime + "','" + date + "')");
                Response.Write("{\"error\": false,\"message\":\"Success\"}");
            }

        }

        protected void updatestatus()
        {
            string id = Request.Params["id"].ToString();
            //string client_id = Request.Params["client_id"].ToString();
            //string eid = Request.Params["employee_id"].ToString();
            string status = Request.Params["status"].ToString();
            //string date = Request.Params["date"].ToString();
            string description = Request.Params["description"].ToString();
            string latitude = Request.Params["latitude"].ToString();
            string longitude = Request.Params["longitude"].ToString();
            DateTime dt = DateTime.Now;
            string date = dt.ToString("yyyy-MM-dd");

            DataQueries.UpdateCommon("update tblstatus set status='" + status + "',date='" + date + "',description='" + description + "',latitude='" + latitude + "',longitude='" + longitude + "' where id='" + id + "'");
            Response.Write("{\"error\": false,\"message\":\"Updated Successfully\"}");
        }

        protected void list_clients()
        {
            string eid = Request.Params["employee_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select id,name,address,email,mobileno as mobile from tblclient where employee_id='" + eid + "' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void list_status()
        {
            string eid = Request.Params["employee_id"].ToString();
            string date = Request.Params["date"].ToString();
            DataSet ds = DataQueries.SelectCommon("select c.id,c.name,c.address,c.mobileno as mobile,c.email,s.status,s.description,s.date from tblstatus s inner join tblclient c on c.id=s.client_id where s.employee_id='" + eid + "' and convert(date,s.date)='" + date + "' and s.status!=0 order by c.id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void list_entirestatus()
        {
            string eid = Request.Params["employee_id"].ToString();
            string cid = Request.Params["client_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("select id,status,description,meeting_time,date,normaldate,latitude,longitude from tblstatus where employee_id='" + eid + "' and client_id='" + cid + "' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void admin_checkin()
        {
            string eid = Request.Params["employee_id"].ToString();
            DateTime dt = DateTime.Now;
            string date = dt.ToShortDateString();
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("update tblcheckin set status='1',out_time='',out_latitude='',out_longitude='' where employee_id='" + eid + "' and status='0' and convert(date,in_time)='" + date + "'", conn);
            conn.Open();
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            if (i == 1)
            {
                //DataQueries.UpdateCommon("");

                Response.Write("{\"error\": false,\"message\":\"Updated Successsfully\"}");
            }
            else
            {
                Response.Write("{\"error\": true,\"message\":\"Please Checkin first\"}");
            }
        }

        protected void list_checkin()
        {
            string date = Request.Params["date"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select e.employename as name,e.EmployeeId as employee_id,c.in_latitude,c.in_longitude,c.in_time,c.out_time,c.status,c.out_latitude,c.out_longitude,(select count(id) from tblstatus t where convert(date,date)='" + date + "' AND employee_id = c.employee_id AND t.status != 0 ) as count from tblcheckin c inner join Employees e on e.EmployeeId=c.employee_id where convert(date,c.in_time)='" + date + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
            
        }

        protected void meetings()
        {
            string eid = Request.Params["employee_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("select s.date,c.name,c.id,c.mobileno as mobile,c.email,c.address,s.meeting_time from tblstatus s inner join tblclient c on c.id=s.client_id and c.employee_id=s.employee_id where s.employee_id='" + eid + "' and s.status='0' order by s.meeting_time desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void co_ordinates()
        {

            string data = Request.Params["data"].ToString();
            string eid = Request.Params["employee_id"].ToString();
            List<Coordinate> myDeserializedObjList = (List<Coordinate>)Newtonsoft.Json.JsonConvert.DeserializeObject(data, typeof(List<Coordinate>));
            
            foreach (Coordinate p in myDeserializedObjList)
            {
                float latitude = p.latitude;
                float longitude = p.longitude;
                string time = p.time;

                DataQueries.InsertCommon("insert into tblcoordinates(employee_id,latitude,longitude,time) values('" + eid + "','" + latitude + "','" + longitude + "','" + time + "')");

            }
            //string arr = str.ToString();

            //JObject json = JObject.Parse(data);

            //DataQueries.InsertCommon("insert into tblcoordinates(employee_id,latitude,longitude,time) values('"+eid+"','"+latitude+"','"+longitude+"','"+time+"')");
            Response.Write("{\"error\": false,\"message\":\"Added Successsfully\"}");
        }

        protected void meetingdetails()
        {
            string eid = Request.Params["employee_id"].ToString();
            string date = Request.Params["date"].ToString();
            DataSet ds = DataQueries.SelectCommon("select c.name,s.description,o.out_latitude,o.out_longitude,o.out_time from tblcheckin o inner join tblstatus s on o.employee_id=s.employee_id inner join tblclient c on c.employee_id=o.employee_id where c.employee_id='" + eid + "' and s.status='0' and convert(date,o.out_time)='" + date + "' order by o.out_time desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void onlinedata()
        {
            DataSet ds = DataQueries.SelectCommon("SELECT s.EmployeeId as employee_id, s.EmployeName as name,l.latitude,l.longitude,l.time FROM(select employee_id, max(id) as first from tblcoordinates group by employee_id) foo INNER JOIN Employees s ON foo.employee_id = s.EmployeeId inner join tblcoordinates l on l.id = foo.first where(GETDATE() - l.time) < '1900-01-01 00:15:00.000'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }


        #endregion

        #region localmethod
        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }

        public class Coordinate
        {
            public string time;
            public float latitude;
            public float longitude;
        }

        public string employee_name(int employee_id)
        {
            DataSet ds = DataQueries.SelectCommon("select EmployeName from Employees where employeeid='" + employee_id + "'");
            return (ds.Tables[0].Rows[0]["EmployeName"].ToString());
        }

        public string token(int employee_id)
        {
            DataSet ds = DataQueries.SelectCommon("select token from Employees where employeeid='" + employee_id + "'");
            return (ds.Tables[0].Rows[0]["token"].ToString());
        }


        public string hr_token()
        {
            DataSet ds = DataQueries.SelectCommon("select token from Employees where employeeid='1'");
            return (ds.Tables[0].Rows[0]["token"].ToString());
        }
        public void notification(string emp_token, string title, string body)
        {
            Notification notification = new Notification();
            notification.title = title;
            notification.body = body;
            //notification.image = path;
            notification.code = 13;
            Message msg = new Message();
            msg.sendSingleNotification(emp_token, notification);
        }
        //private void hr_tokens(Notification notification, string empid)
        //{
        //    DataSet tokens = DataQueries.SelectCommon("select * from Employees where status='1' and EmployeeId1!='" + empid + "'");

        //    //Empty array
        //    string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

        //    //loopcounter
        //    for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
        //    {
        //        //assign dataset values to array
        //        arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
        //    }
        //    //return arrvalues;


        //    Message msg = new Message();
        //    msg.sendMultipleNotification(arrvalues, notification);
        //}

        //protected void broadcast()
        //{
        //    string employeeid = Request.Params["employee_id"].ToString().Trim();
        //    string title = Request.Params["title"].ToString().Trim();
        //    string description = Request.Params["description"].ToString().Trim();
        //    DateTime dt = DateTime.Now;
        //    string date = dt.ToString("yyyy-MM-dd HH:mm:ss.000");
        //    DataQueries.InsertCommon("Insert into notifications(date,title,description,employee_id) values ('" + date + "','" + title + "','" + description + "','" + employeeid + "')");
        //    //DataSet ds = DataQueries.SelectCommon("Select type,deptId from Employees where EmployeeId1='" + employeeid + "'");
        //    //if (ds.Tables[0].Rows.Count > 0)
        //    //{
        //    //    string type = ds.Tables[0].Rows[0]["type"].ToString();
        //    //    string deptid = ds.Tables[0].Rows[0]["deptId"].ToString();
        //    //    Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        //    //    //EmployeeOperation operation = new EmployeeOperation();
        //    //    //EmployeeItem employee = operation.getEmployee(employeeid);
        //    //    if (deptid != "8")
        //    //    {
        //            Notification notification = new Notification();

        //            notification.title = title;
        //            notification.body = description;
        //            //notification.image = path;
        //            notification.code = 13;
        //            hr_tokens(notification, employeeid);
        //        }


        /*string hr_title = "Request Accepted";
        string hr_body = "Your Leave Request Accepted";
        string emp_token = token(empID);
        notification(emp_token, hr_title, hr_body);*/


        #endregion
    }
}
