﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CherlaHealth
{
    public partial class AddCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Categories WHERE CategoryName = @Name", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name", this.Category.Text.Trim());

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.lblStatus.Text = "Duplicate Entry";
                        }
                        else
                        {
                            if (weekoff2.Checked == true)
                            {
                                SqlConnection conn1 = new SqlConnection(connstrg);

                                string str = "insert into Categories (CategoryName,CategorySName,OTFormula,MinOT,GraceTimeForLateComing,GracetimeForEarlyGoing,IsWeeklyOff1,WeeklyOff1Day,IsWeeklyOff2,WeeklyOff2Day,WeeklyOff2days,IsCHalfForLessD,CHalfForLessDMins,IsCAbsentDisLessThan,CAbsentDisLessThanMins,IsMarkHalfDay_LateBy,MarkHalfDay_LateByMins,IsMarkHalfDay_EarlyGoing,MarkHalfDay_EarlyGoing) values ('" + Category.Text + "','" + Shortname.Text + "','" + OTformula.Text + "','" + Minot.Text + "','" + GRT.Text + "','" + NglLstpunch1.Text + "','" + Weekoff1.Checked + "','" + Weeloff11.SelectedItem.Text + "','" + weekoff2.Checked + "','" + Weekoff22.SelectedItem.Text + "','NULL','" + CHDW.Checked + "','" + CHWD1.Text + "','" + cawdil.Checked + "','" + CAW.Text + "','" + MHDL.Checked + "','" + MHDL1.Text + "','" + MHDEGB.Checked + "','" + MHDEGB1.Text + "')";

                                //(CategoryName,CategorySName,OTFormula,MinOT,IsMaxOT,MaxOT,IsConsiderFirstAndLastPunch,GraceTimeForLateComing,GracetimeForEarlyGoing,IsNeglectLastInPunch,IsWeeklyOff1,WeeklyOff1Day,IsWeeklyOff2,WeeklyOff2Day,WeeklyOff2days,IsConsiderEarlyComingPunch,IsConsiderLateGoingPunch,IsDeductBreakHourFromD,IsCHalfForLessD,CHalfForLessDMins,IsCAbsentDisLessThan,CAbsentDisLessThanMins,IsCHalfForLessD_P,CHalfForLessDMins_P,IsCAbsentDisLessThan_P,CAbsentDisLessThanMins_P,IsPrefixMarkWOAndHAbsent,IsSuffixMarkWOAndHAbsent,IsBothMarkWOAndHAbsent,IsMark,AbsentType,LateDay,IsMarkHalfDay_LateBy,MarkHalfDay_LateByMins,IsMarkHalfDay_EarlyGoing,MarkHalfDay_EarlyGoing,RecordStatus,ForSinglePunch,ConsiderWP_HPInOT,IsCalculateHalfCompOff,IsCalculateNoCompOff,CalculateHalfCompOffMins,CalculateNoCompOffMins)
                                SqlCommand comm1 = new SqlCommand(str, conn1);
                                conn1.Open();
                                comm1.ExecuteNonQuery();
                                conn1.Close();
                                Response.Write("<script>alert('Data inserted successfully')</script>");
                                Response.Redirect("~/CategoriesList.aspx");
                            }
                            if(weekoff2.Checked==false)
                            {
                                SqlConnection conn1 = new SqlConnection(connstrg);

                                string str = "insert into Categories (CategoryName,CategorySName,OTFormula,MinOT,GraceTimeForLateComing,GracetimeForEarlyGoing,IsWeeklyOff1,WeeklyOff1Day,IsCHalfForLessD,CHalfForLessDMins,IsCAbsentDisLessThan,CAbsentDisLessThanMins,IsMarkHalfDay_LateBy,MarkHalfDay_LateByMins,IsMarkHalfDay_EarlyGoing,MarkHalfDay_EarlyGoing) values ('" + Category.Text + "','" + Shortname.Text + "','" + OTformula.Text + "','" + Minot.Text + "','" + GRT.Text + "','" + NglLstpunch1.Text + "','" + Weekoff1.Checked + "','" + Weeloff11.SelectedItem.Text + "','" + CHDW.Checked + "','" + CHWD1.Text + "','" + cawdil.Checked + "','" + CAW.Text + "','" + MHDL.Checked + "','" + MHDL1.Text + "','" + MHDEGB.Checked + "','" + MHDEGB1.Text + "')";

                                //(CategoryName,CategorySName,OTFormula,MinOT,IsMaxOT,MaxOT,IsConsiderFirstAndLastPunch,GraceTimeForLateComing,GracetimeForEarlyGoing,IsNeglectLastInPunch,IsWeeklyOff1,WeeklyOff1Day,IsWeeklyOff2,WeeklyOff2Day,WeeklyOff2days,IsConsiderEarlyComingPunch,IsConsiderLateGoingPunch,IsDeductBreakHourFromD,IsCHalfForLessD,CHalfForLessDMins,IsCAbsentDisLessThan,CAbsentDisLessThanMins,IsCHalfForLessD_P,CHalfForLessDMins_P,IsCAbsentDisLessThan_P,CAbsentDisLessThanMins_P,IsPrefixMarkWOAndHAbsent,IsSuffixMarkWOAndHAbsent,IsBothMarkWOAndHAbsent,IsMark,AbsentType,LateDay,IsMarkHalfDay_LateBy,MarkHalfDay_LateByMins,IsMarkHalfDay_EarlyGoing,MarkHalfDay_EarlyGoing,RecordStatus,ForSinglePunch,ConsiderWP_HPInOT,IsCalculateHalfCompOff,IsCalculateNoCompOff,CalculateHalfCompOffMins,CalculateNoCompOffMins)
                                SqlCommand comm1 = new SqlCommand(str, conn1);
                                conn1.Open();
                                comm1.ExecuteNonQuery();
                                conn1.Close();
                                Response.Write("<script>alert('Data inserted successfully')</script>");
                                Response.Redirect("~/CategoriesList.aspx");
                            }
                        }
                    }

                }


            }
        }
    }
}