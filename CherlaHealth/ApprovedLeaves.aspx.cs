﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class ApprovedLeaves : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Bindleaveslist();
            }
        }

        protected void Bindleaveslist()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            if (Session["name"].ToString() == "ADMIN")
            {
                SqlCommand cmd = new SqlCommand("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.empreplace,l.LeaveType,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID where l.EmployeedID='" + Session["EmpId"].ToString() + "'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvlistofLeaves.DataSource = ds;
                    gvlistofLeaves.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    gvlistofLeaves.DataSource = ds;
                    gvlistofLeaves.DataBind();
                    int columncount = gvlistofLeaves.Rows[0].Cells.Count;
                    gvlistofLeaves.Rows[0].Cells.Clear();
                    gvlistofLeaves.Rows[0].Cells.Add(new TableCell());
                    gvlistofLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                    gvlistofLeaves.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
            else
            {               
                SqlCommand cmd = new SqlCommand("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,l.empreplace,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID where l.EmployeedID='" + Session["name"].ToString() + "'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvlistofLeaves.DataSource = ds;
                    gvlistofLeaves.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    gvlistofLeaves.DataSource = ds;
                    gvlistofLeaves.DataBind();
                    int columncount = gvlistofLeaves.Rows[0].Cells.Count;
                    gvlistofLeaves.Rows[0].Cells.Clear();
                    gvlistofLeaves.Rows[0].Cells.Add(new TableCell());
                    gvlistofLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                    gvlistofLeaves.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
    }
}