﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class ViewTravelAllowance : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                //bind();
            }
        }

        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("select EmployeeId,EmployeeName,Month,Condition,Kms,Amount,ExtraCharges,sum(convert(int,ExtraCharges)+convert(float,Amount)) AS total from travel_allowance group by Amount,ExtraCharges,EmployeeId,EmployeeName,Month,Condition,Kms  ", conn);


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvtravel.DataSource = ds;
                gvtravel.DataBind();
            }

            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvtravel.DataSource = ds;
                gvtravel.DataBind();
                int columncount = gvtravel.Rows[0].Cells.Count;
                gvtravel.Rows[0].Cells.Clear();
                gvtravel.Rows[0].Cells.Add(new TableCell());
                gvtravel.Rows[0].Cells[0].ColumnSpan = columncount;
                gvtravel.Rows[0].Cells[0].Text = "No Records Found";
            }
            

        }
        
        protected void Month_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select * from travel_allowance where Month='" + txtmonth.Text + "' order by id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvtravel.DataSource = ds;
                gvtravel.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvtravel.DataSource = ds;
                gvtravel.DataBind();
                int columncount = gvtravel.Rows[0].Cells.Count;
                gvtravel.Rows[0].Cells.Clear();
                gvtravel.Rows[0].Cells.Add(new TableCell());
                gvtravel.Rows[0].Cells[0].ColumnSpan = columncount;
                gvtravel.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}