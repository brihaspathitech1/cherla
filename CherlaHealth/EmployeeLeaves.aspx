﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EmployeeLeaves.aspx.cs" Inherits="CherlaHealth.EmployeeLeaves" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Company List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             
    <asp:UpdatePanel ID="UpdatePanel45" runat="server">
                <ContentTemplate>
    <div class="form-panel col-sm-12">
        <h2>
            Leave Entry</h2> <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
        <hr />
        
        <div class="col-md-12 padbot">
            <div class="col-sm-12">
                <div class="col-sm-4">
                    Leave Type<span class="style1">*</span>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="Leave" class="form-control" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="Leave" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Leave" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="col-sm-4">
                    Begin Date <span class="style1">*</span>
                    <asp:TextBox ID="Begintime" class="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Begintime"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="Begintime" runat="server">
                    </cc1:CalendarExtender>
                </div>
                <div class="col-sm-4">
                    End Date <span class="style1">*</span>
                    <asp:TextBox ID="EndTime" class="form-control" runat="server" AutoPostBack="True"
                        OnTextChanged="EndTime_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="EndTime"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                    <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="EndTime" runat="server">
                    </cc1:CalendarExtender>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4">
                    Duration&nbsp;
                    <asp:TextBox ID="Duration" class="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-4">
                    Employee <span class="style1">*</span>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="Employee" class="form-control" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="Employee_SelectedIndexChanged">
                            </asp:DropDownList>
                        
                  
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="Employee" InitialValue="0" Display="Dynamic"
                        runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                </div>
                <div class="col-sm-4">
                    Balance Leaves
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtBalLeaves" class="form-control" runat="server"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4">
                    Reason
                    <asp:TextBox ID="Reason" class="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-4">
                    Contact Number
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="Contact" class="form-control" runat="server"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                
            </div>
            <div class="col-sm-4" style="margin-left: 11px; margin-top: 12px">
                <asp:Button ID="Button2" runat="server" Text="Apply" class="btn btn-Primary" OnClick="Button2_Click" BackColor="#68dff0" />
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlReadOnly" runat="server" Visible="false">
        <div class="form-panel col-sm-12">
            <asp:UpdatePanel ID="panel12" runat="server">
                <ContentTemplate>
                    <h4>
                        Pending Leaves:-
                        <asp:Label ID="lblReadOnlyListOfLeaves" runat="server" Text="" ForeColor="#DC7A01"></asp:Label>
                    </h4>
                    <div style="height: 500px; overflow-y: scroll; overflow-x: scroll">
                        <asp:GridView ID="gvListofROLeaves" class="table table-bordered table-striped table-condensed"
                            runat="server" AutoGenerateColumns="True">
                        </asp:GridView>
                    </div>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                       
</div> </asp:Panel>

 </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
            </div>
</asp:Content>


