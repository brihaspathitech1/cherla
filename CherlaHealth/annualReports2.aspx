﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="annualReports2.aspx.cs" Inherits="CherlaHealth.annualReports2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">ToDate Report</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="row mt">
                <div class=" col-lg-12">

    <div class=" col-sm-12">
      
        <div class="row padbot">
            <div class="col-sm-4">
                Company
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="DropDownList2" class="form-control" runat="server" AutoPostBack="True"
                            OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-sm-4">
                Department
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="Department" runat="server" class="form-control" AutoPostBack="True"
                            OnSelectedIndexChanged="Department_SelectedIndexChanged">
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Department" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div class="col-sm-4">
                Employee
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="DropDownList3" runat="server" class="form-control" AutoPostBack="True">
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DropDownList3" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <br />
        <div class="row padbot">
            <div class="col-sm-4">
                Type
                <asp:DropDownList ID="DropDownList1" class="form-control" runat="server" >
                    <%--<asp:ListItem>Basic</asp:ListItem>
                    <asp:ListItem>Detailed Attendance</asp:ListItem>
                    <asp:ListItem>Summary Attendance report</asp:ListItem>--%>
                    <asp:ListItem>Detailed Summary Attendance </asp:ListItem>
                    <%--<asp:ListItem>In-Out Duration</asp:ListItem>
                    <asp:ListItem>Present</asp:ListItem>
                    <asp:ListItem>Absent</asp:ListItem>
                    <asp:ListItem>EarlyGoes</asp:ListItem>
                    <asp:ListItem>LateComes</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
            <div class="col-sm-4">
                Start Years
                <asp:DropDownList ID="DropDownList4" class="form-control" runat="server">
                    <asp:ListItem Value="2015-01-01">2015</asp:ListItem>
                    <asp:ListItem Value="2016-01-01">2016</asp:ListItem>
                    <asp:ListItem Value="2017-01-01">2017</asp:ListItem>
                    <asp:ListItem Value="2018-01-01">2018</asp:ListItem>
                    <asp:ListItem Value="2019-01-01">2019</asp:ListItem>
                    <asp:ListItem Value="2020-01-01">2020</asp:ListItem>
                    <asp:ListItem Value="2021-01-01">2021</asp:ListItem>
                    <asp:ListItem Value="2022-01-01">2022</asp:ListItem>
                    <asp:ListItem Value="2023-01-01">2023</asp:ListItem>
                    <asp:ListItem Value="2024-01-01">2024</asp:ListItem>
                    <asp:ListItem Value="2025-01-01">2025</asp:ListItem>
                    <asp:ListItem Value="2015-01-01">2026</asp:ListItem>
                    <asp:ListItem Value="2027-01-01">2027</asp:ListItem>
                    <asp:ListItem Value="2028-01-01">2028</asp:ListItem>
                    <asp:ListItem Value="2029-01-01">2029</asp:ListItem>
                    <asp:ListItem Value="2030-01-01">2030</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-4 centered">
                <br />
                <asp:Button ID="Button1" class="btn btn-success btn-block" runat="server" Text="Search" OnClick="Button1_Click" />
            </div>
        </div>
        <div style="display: none;" runat="server" id="divmsg">
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="overflowy">
                        <section id="no-more-tables">
                       <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed" runat="server">
                       
                      

    </asp:GridView>
 </section>
                    </div>
                    Total Days :<asp:Label ID="Label1" runat="server" Style="background-color: #FFFF00;
                        font-weight: 700"></asp:Label>
                    &nbsp;, Presents:
                    <asp:Label ID="Label2" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>
                    &nbsp;, Absents:
                    <asp:Label ID="Label3" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>
                    &nbsp;, Presents(No Out Punch):
                    <asp:Label ID="Label9" Style="background-color: #FFFF00; font-weight: 700" runat="server"></asp:Label>
                    &nbsp;, Percentage Persents:
                    <asp:Label ID="Label4" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;%,
                    HalfDays:
                    <asp:Label ID="Label5" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
                    Week of Present:
                    <asp:Label ID="Label6" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
                    Leaves:
                    <asp:Label ID="Label7" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;,
                    Holidays:
                    <asp:Label ID="Label8" runat="server" Style="background-color: #FFFF00; font-weight: 700"></asp:Label>&nbsp;
                </div>
                <div class="col-md-12">
                    <br />
                    <div class="col-md-3">
                        <p>
                            A:-Absent</p>
                        <p>
                            P:-Present</p>
                        <p>
                            H:-Holiday</p>
                        <p>
                            L:-Leave</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            HOP:-Holiday Present</p>
                        <p>
                            HO:-Half Day</p>
                        <p>
                            WO:-Week Off</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            WOP:-Week Off Present</p>
                        <p>
                            P(NOP):-Present But No Out Punch
                        </p>
                        <p>
                            OT:-Over Time</p>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button6" runat="server" class="btn btn-success" Text="Export Ms-Word"
                OnClick="Button6_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button7" runat="server" class="btn btn-success" Text="Export Excel"
                OnClick="Button7_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button8" runat="server" class="btn btn-success" Text="Export Pdf"
                OnClick="Button8_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button9" runat="server" class="btn btn-success" Text="Export CSV"
                OnClick="Button9_Click" />
        </div>
    </div>
    </div>
            </div>


            </div>
        </div>
</asp:Content>
