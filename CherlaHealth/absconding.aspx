﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="absconding.aspx.cs" Inherits="CherlaHealth.absconding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Absconding Letter</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
                  Salutation<span style="color:red">*</span> <asp:DropDownList ID="salut"  CssClass="form-control" runat="server">
                      <asp:ListItem>Mr.</asp:ListItem>
                      <asp:ListItem>Mrs.</asp:ListItem>
                      <asp:ListItem>Ms.</asp:ListItem>      </asp:DropDownList>
                       </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
                <div class="col-md-3">
                    <div class="form-group">
                        Select Employee<asp:DropDownList ID="dropemp" CssClass="form-control" runat="server" OnSelectedIndexChanged="Employee_Change" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
              <div class="col-md-3">
                    <div class="form-group">
                        Designation <asp:TextBox ID="txtdesign" CssClass="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                        </div>
                  </div>
            <div class="col-md-3">
                    <div class="form-group">
                        Email <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                  </div>
            <div class="col-md-3" style="padding-top:18px">
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                        </div>
                </div>
            </div>
        </div>
   

</asp:Content>
