﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calender123.aspx.cs" Inherits="CherlaHealth.Calender123" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cherla</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

   <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
        rel="stylesheet" />
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <script src="https://use.fontawesome.com/21d83371f2.js"></script>
    <script src="assets/js/chart-master/Chart.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
            border-radius: 12px;
            padding: 0;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }
        .modalPopup .body
        {
            padding: 10px;
            min-height: 50px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .footer
        {
            padding: 6px;
        }
        .modalPopup .yes, .modalPopup .no
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            border-radius: 4px;
        }
        .modalPopup .yes
        {
            background-color: #2FBDF1;
            border: 1px solid #0DA9D0;
        }
        .modalPopup .no
        {
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
   



    <section id="container">
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg  bg-gradient-9">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="Dashboard.aspx" class="logo"><b>Cherla</b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                <%--     <li>  <asp:LinkButton ID="LinkButton1" class="logout "  CausesValidation="false" runat="server" 
                       onclick="LinkButton1_Click">Logout</asp:LinkButton></li>--%>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
         <li class="mt">
        <asp:DataList id="DataList1"   runat="server" RepeatColumns="1">
         <ItemTemplate>
        <asp:Image ID="Image1"  class="img-circle" width="80" Height="80"  ImageUrl='<%# "~/OTEmp.ashx?EmployeeId=" + Eval("EmployeeId")%>' runat="server" />
       </ItemTemplate>
        </asp:DataList>
        <asp:Label ID="lblLogInName"  
                            runat="server"  Font-Bold="True" ForeColor="Red"></asp:Label>
        </li>
              	  	
                  <li class="mt">
                      <a class="active" href="HrDashboard.aspx">
                          <i class="fa fa-desktop"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

               
               <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-list"></i>
                          <span>Sourcing</span>
                      </a>
              
                      <ul class="sub">
                       <li><a  href="SourcingCandidates.aspx">Sourcing Candidates</a></li>
                          <li><a  href="ViewCandidates.aspx">View Candidates</a></li>
                         
                       
  
                      </ul>
                     
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-list"></i>
                          <span>Employee Master</span>
                      </a>
              
                      <ul class="sub">
                       <li><a  href="AddNewEmp.aspx">Add Employee</a></li>
                         <%-- <li><a  href="Categorieslist.aspx">Add Catagories</a></li>

                          <li><a  href="Department.aspx">Add Department</a></li>
                           <li><a  href="Shift.aspx">Add Shift</a></li>--%>
                       
  
                      </ul>
                     
                  </li>
             
              <li class="sub-menu">
                      <a href="javascript:;" >
                      <i class="fa fa-mobile" aria-hidden="true"></i>
                          
                          <span>Deductions</span>
                      </a>
              
                      <ul class="sub">
                      
                            <a href="MobileBills.aspx">Mobile Deductions</a>
                              <a href="MobileBillsView.aspx">Mobile bills View</a>
                        </li>
   
                       
                      </ul>
                      <li class="sub-menu">
                      <a href="javascript:;" >
                         <i class="fa fa-envelope" aria-hidden="true"></i>
                          <span>Letters</span>
                      </a>
              
                      <ul class="sub">
                      
                            <li><a  href="Letters.aspx">Appointment & Hike</a></li>
                        </li>
  
                       
  
                      </ul>
               
             
               <li class="sub-menu">
                      <a href="javascript:;" >
                         <i class="fa fa-money" aria-hidden="true"></i>
                          <span>Pay-Roll</span>
                      </a>
              
                      <ul class="sub">
                       <li>
                            <a href="SalariesListEmployee.aspx">Pf/Non-Pf Details</a>
                        </li>
  <li>
                            <a href="pfdetails.aspx">Master data</a>
                        </li>
                       <li>
                            <a href="Payslip.aspx"> Pay-Slip</a>
                        </li>
                        <li>
                            <a href="EmployeeDetails.aspx"> Employee-Details</a>
                        </li>


                        <li>
                            <a href="Rules.aspx"> Rules</a>
                        </li>
                        
                      </ul>
                     
                  </li>
                  
               <li class="sub-menu">
                      <a href="javascript:;" >
                         <i class="fa fa-money" aria-hidden="true"></i>
                          <span>Add Information</span>
                      </a>
              
                      <ul class="sub">
                       <li>
                            <a href="AddNotificationHr.aspx">Add Notification</a>
                        </li>
                           <li>
                            <a href="EventPage.aspx">Add Events</a>
                        </li>
                         <li>
                            <a href="Calender123.aspx">Calender Events</a>
                        </li>
                      </ul>
                     
                  </li>
                      
               <li class="sub-menu">
                      <a href="javascript:;" >
                         <i class="fa fa-money" aria-hidden="true"></i>
                          <span>Leave tracker</span>
                      </a>
              
                      <ul class="sub">
                      <li>
                            <a href="HolidayView.aspx">Add Holodays</a>
                        </li>
                       <li>
                            <a href="EmployeeLeaves.aspx">Applyleave</a>
                        </li>
   <li>
                            <a href="ApproveLeave.aspx">Leve Approvals</a>
                        </li>
                        
                      </ul>
                     
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                      <i class="fa fa-mobile" aria-hidden="true"></i>
                          
                          <span>Technician OT's</span>
                      </a>
              
                      <ul class="sub">
                      
                            <a href="OT.aspx">OT</a>
                              <a href="OTApproval.aspx">View OT's</a> 
                        </li>

              </ul>
          
          </div>
      </aside>
      <!--sidebar end-->
      
     
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <%-- <section class="co  ntent-header">
      <h1>
        Calendar
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calendar</li>
      </ol>
    </section>--%>

    <!-- Main content -->
    <br />
    <br />
    <br />
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h4 class="box-title">Draggable Events</h4>
            </div>
            <div class="box-body">
              <!-- the events -->
              <div id="external-events">
             
                <div class="external-event bg-green">Lunch</div>
                <div class="external-event bg-yellow">Leave</div>
                <div class="external-event bg-aqua">Meeting</div>
                <div class="external-event bg-light-blue">Holiday</div>
                <div class="external-event bg-red">BTBOMB</div>
                <div class="checkbox">
                  <label for="drop-remove">
                    <input type="checkbox" id="drop-remove"/>
                    remove after drop
                  </label>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Create Event</h3>
            </div>
            <div class="box-body">
              <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                <ul class="fc-color-picker" id="color-chooser">
                  <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                </ul>
              </div>
              <!-- /btn-group -->
              <div class="input-group">
                <input id="new-event" type="text" class="form-control" placeholder="Event Title">

                <div class="input-group-btn">
                  <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
                </div>
                <!-- /btn-group -->
              </div>
              <!-- /input-group -->
            </div>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar" style="color:#000 !important"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- /.tab-pane -->
    
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->
<script>
    $(function () {

        /* initialize the external events
        -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function () {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }

        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
        -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            //Random default events
            events: [
        {
            title: 'All Day Event',
            start: new Date(y, m, 1),
            backgroundColor: "#f56954", //red
            borderColor: "#f56954" //red
        }

      ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!

            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported

                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");
                var dt = new Date(copiedEventObject.start);


                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)

                var qrystr = copiedEventObject.title + "-" + $.datepicker.formatDate('mm/dd/yy', dt) + "-" + copiedEventObject.backgroundColor + "-" + copiedEventObject.borderColor;
                var sendevent;
                sendevent = null;
                if (window.XMLHttpRequest) {
                    sendevent = new XMLHttpRequest();
                }
                sendevent.open("GET", "DragEvents.aspx?id=2&methodname=" + qrystr, true);
                sendevent.send();

                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list

                    $(this).remove();

                }

            }
        });
        //        ------------------------Filling from Database---------------------------------------
        // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);

        // assign it the date that was reported
        var xmlHttp7;
        xmlHttp7 = null;

        if (window.XMLHttpRequest) {
            //for new browsers    
            xmlHttp7 = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            var strName7 = "Msxml2.XMLHTTP"
            if (navigator.appVersion.indexOf("MSIE 5.5") >= 0) {
                strName7 = "Microsoft.XMLHTTP"
            }
            try {
                xmlHttp7 = new ActiveXObject(strName7);
            }
            catch (e) {
                alert("Error. Scripting for ActiveX might be disabled")
                return false;
            }
        }

        xmlHttp7.open("GET", "DragEvents.aspx?id=1", true);
        xmlHttp7.send();
        if (xmlHttp7 != null) {
            xmlHttp7.onreadystatechange = function () {

                if (xmlHttp7.readyState == 4 && xmlHttp7.status == 200) {
                    var string7 = xmlHttp7.responseText;

                    var temp7 = new Array();
                    temp7 = string7.split(",");
                    var i7, j7 = 0;

                    for (i7 = 0; i7 < temp7.length; i7++) {

                        var parts7 = temp7[i7].split('-');
                        copiedEventObject.title = parts7[0];
                        copiedEventObject.start = new Date(parts7[1], parts7[2], parts7[3]);
                        copiedEventObject.allDay = true;
                        copiedEventObject.backgroundColor = parts7[4];
                        copiedEventObject.borderColor = parts7[5]
                        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    }
                }
            }


        }


        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)




        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({ "background-color": currColor, "border-color": currColor });
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create events
            var event = $("<div />");
            event.css({ "background-color": currColor, "border-color": currColor, "color": "#fff" }).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality049.4
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
    });
</script>
  <!-- js placed at the end of the document so the pages load faster -->
  
    
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>
    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
 
    </form>
</body>
</html>

