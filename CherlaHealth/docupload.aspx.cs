﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;

namespace CherlaHealth
{
    public partial class docupload : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where Status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd1 = new SqlCommand("Select * from Empdocuments where EmployeeId='" + dropemp.SelectedValue + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Response.Write("<script>alert('Duplicate Entry')</script>");
            }
            else
            {
                string respath = string.Empty;
                string deg1path = string.Empty;
                string deg2path = string.Empty;
                string deg3path = string.Empty;
                string deg4path = string.Empty;
                string deg5path = string.Empty;
                string mcipath = string.Empty;
                string scipath = string.Empty;
                string oth1path = string.Empty;
                string oth2path = string.Empty;
                string oth3path = string.Empty; string oth4path = string.Empty; string oth5path = string.Empty;
                if (fileresume.HasFile)
                {
                    //InsuranceFileUpload.SaveAs(folderPath + Path.GetFileName(InsuranceFileUpload.FileName));
                    string fileName1 = Path.GetFileName(fileresume.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    fileresume.PostedFile.SaveAs(Server.MapPath(path1));
                    respath = path1;
                }
                if (filedeg1.HasFile)
                {
                    string fileName1 = Path.GetFileName(filedeg1.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    filedeg1.PostedFile.SaveAs(Server.MapPath(path1));
                    deg1path = path1;
                }
                if (filedeg2.HasFile)
                {
                    string fileName1 = Path.GetFileName(filedeg2.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    filedeg2.PostedFile.SaveAs(Server.MapPath(path1));
                    deg2path = path1;
                }
                if (filedeg3.HasFile)
                {
                    string fileName1 = Path.GetFileName(filedeg3.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    filedeg3.PostedFile.SaveAs(Server.MapPath(path1));
                    deg3path = path1;
                }
                if (filedeg4.HasFile)
                {
                    string fileName1 = Path.GetFileName(filedeg4.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    filedeg4.PostedFile.SaveAs(Server.MapPath(path1));
                    deg4path = path1;
                }
                if (filedeg5.HasFile)
                {
                    string fileName1 = Path.GetFileName(filedeg5.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    filedeg5.PostedFile.SaveAs(Server.MapPath(path1));
                    deg5path = path1;
                }
                if (filelicense1.HasFile)
                {
                    string fileName1 = Path.GetFileName(filelicense1.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    filelicense1.PostedFile.SaveAs(Server.MapPath(path1));
                    mcipath = path1;
                }
                if (filelicence2.HasFile)
                {
                    string fileName1 = Path.GetFileName(filelicence2.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    filelicence2.PostedFile.SaveAs(Server.MapPath(path1));
                    scipath = path1;
                }
                if (fileother1.HasFile)
                {
                    string fileName1 = Path.GetFileName(fileother1.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    fileother1.PostedFile.SaveAs(Server.MapPath(path1));
                    oth1path = path1;
                }
                if (fileother2.HasFile)
                {
                    string fileName1 = Path.GetFileName(fileother2.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    fileother2.PostedFile.SaveAs(Server.MapPath(path1));
                    oth2path = path1;
                }
                if (fileother3.HasFile)
                {
                    string fileName1 = Path.GetFileName(fileother3.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    fileother3.PostedFile.SaveAs(Server.MapPath(path1));
                    oth3path = path1;
                }
                if (fileother4.HasFile)
                {
                    string fileName1 = Path.GetFileName(fileother4.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    fileother4.PostedFile.SaveAs(Server.MapPath(path1));
                    oth4path = path1;
                }
                if (fileother5.HasFile)
                {
                    string fileName1 = Path.GetFileName(fileother5.PostedFile.FileName);
                    string path1 = "~/documents/" + fileName1;
                    fileother5.PostedFile.SaveAs(Server.MapPath(path1));
                    oth5path = path1;
                }
                SqlCommand cmd = new SqlCommand("Insert into Empdocuments(EmployeeId,resume,degree1,degree2,degree3,degree4,degree5,MCI,SCI,others1,others2,others3,others4,others5,SCI_Expire,MCI_Expire) values ('" + dropemp.SelectedValue + "','" + respath + "','" + deg1path + "','" + deg2path + "','" + deg3path + "','" + deg4path + "','" + deg5path + "','" + mcipath + "','" + scipath + "','" + oth1path + "','" + oth2path + "','" + oth3path + "','" + oth4path + "','" + oth5path + "','" + txtsci.Text + "','" + txtmci.Text + "')", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                dropemp.SelectedValue = null;
                txtmci.Text = "";
                txtsci.Text = "";
                Response.Write("<script>alert('Data inserted successfully')</script>");
            }
        }
    }
}