﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace CherlaHealth
{
    public partial class EmpHourSlip : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Bindgrid();
                BindEmployee();
            }
        }

        protected void Bindgrid()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from hourpayslip where Month(Date)=Month(getdate())", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void BindEmployee()
        {
            SqlConnection sqlConn = new SqlConnection(connstrg);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandText = "Select EmployeeId,EmployeName  from Employees where Status='1' and DeptId='7'";
            sqlCmd.Connection = sqlConn;
            sqlConn.Open();
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dropemp.DataSource = dt;
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataTextField = "EmployeName";
            dropemp.DataBind();
            sqlConn.Close();
            dropemp.Items.Insert(0, new ListItem("", "0"));
        }

        protected void Submit(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd;
            SqlDataAdapter da=new SqlDataAdapter();
            if (txtmonth.Text!=""&&dropemp.SelectedItem.Text!="")
            {
                cmd = new SqlCommand("Select * from hourpayslip where EmpNo='" + dropemp.SelectedValue + "' and Month(Date)=Month('" + txtmonth.Text + "') and Year=Year('" + txtmonth.Text + "')", conn);
                da = new SqlDataAdapter(cmd);
            }
            else if(txtmonth.Text!=""&&dropemp.SelectedItem.Text=="")
            {
                cmd = new SqlCommand("Select * from hourpayslip where Month(Date)=Month('" + txtmonth.Text + "') and Year=Year('" + txtmonth.Text + "')", conn);
                da = new SqlDataAdapter(cmd);
            }
            else if(txtmonth.Text==""&&dropemp.SelectedItem.Text!="")
            {
                cmd = new SqlCommand("Select * from hourpayslip where EmpNo='" + dropemp.SelectedValue + "'", conn);
                da = new SqlDataAdapter(cmd);
            }
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Report_click(object sender,EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            Session["payid"] = id.ToString();
            Response.Redirect("~/HourPaySlip.aspx");
        }
    }
}