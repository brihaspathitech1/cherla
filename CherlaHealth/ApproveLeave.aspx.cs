﻿using inventory.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace CherlaHealth
{
    public partial class ApproveLeave : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //FillgvLeavesList();
                Bindleaveslist();
            }
        }
        private void FillgvLeavesList()
        {
            DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + Session["name"].ToString() + "'");
            if (ds.Tables[0].Rows.Count != 0)
            {
                pnlApprove.Visible = true;
                string s;
                if (ds.Tables[0].Rows[0][0].ToString() == "102")
                {
                    s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                    s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                    s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnHRDesk'";
                }
                else
                {
                    s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                    s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                    s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where d.ManagerId='" + Session["name"].ToString() + "' and l.Status='OnManagerDesk'";
                }
                DataSet ds1 = DataQueries.SelectCommon(s);
                gvlistofLeaves.DataSource = ds1;
                gvlistofLeaves.DataBind();
                if (ds1.Tables[0].Rows.Count == 0)
                {
                    lblListDetail.Text = "No Leaves Pending";
                }
                else
                {
                    lblListDetail.Text = "Leaves Pending = " + ds1.Tables[0].Rows.Count.ToString();
                }
            }
            else
            {
                pnlReadOnly.Visible = true;
                string s;
                s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate as UpdateOn";
                s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
                s += " where l.EmployeedId='" + Session["name"].ToString() + "' order by l.LeaveId desc";

                DataSet dsro = DataQueries.SelectCommon(s);

                gvListofROLeaves.DataSource = dsro;
                gvListofROLeaves.DataBind();

                Panel1.Visible = true;
                string y;
                y = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate as UpdateOn";
                y += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
                y += " where l.EmployeedId='" + Session["name"].ToString() + "' and YEAR(l.AppliedDate)=YEAR(GETDATE()) order by l.LeaveId desc";
                DataSet dsro1 = DataQueries.SelectCommon(y);
                GridView1.DataSource = dsro1;
                GridView1.DataBind();
            }
        }
        protected void Bindresult()
        {

            //string arr[] = TextBox1.Text.Split("/");
            string[] arr = TextBox1.Text.Split('/');
            string month = arr[0].ToString();

            DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + Session["name"].ToString() + "'");
            if (ds.Tables[0].Rows.Count != 0)
            {
                pnlApprove.Visible = true;
                string s;
                if (ds.Tables[0].Rows[0][0].ToString() == "102")
                {
                    s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                    s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                    s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnHRDesk' and Month(l.AppliedDate)='" + month + "'";
                }
                else
                {
                    s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                    s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                    s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where d.ManagerId='" + Session["name"].ToString() + "' and l.Status='OnManagerDesk' Month(l.AppliedDate)='" + month + "'";
                }


                DataSet ds1 = DataQueries.SelectCommon(s);
                gvlistofLeaves.DataSource = ds1;
                gvlistofLeaves.DataBind();
                if (ds1.Tables[0].Rows.Count == 0)
                {
                    lblListDetail.Text = "No Leaves Pending";
                }
                else
                {
                    lblListDetail.Text = "Leaves Pending = " + ds1.Tables[0].Rows.Count.ToString();
                }
            }
            else
            {
                pnlReadOnly.Visible = true;
                string s;
                s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate as UpdateOn";
                s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
                s += " where l.EmployeedId='" + Session["name"].ToString() + "' and Month(l.AppliedDate)='" + month + "' order by l.LeaveId desc";

                DataSet dsro = DataQueries.SelectCommon(s);
                if (dsro.Tables[0].Rows.Count > 0)
                {
                    gvListofROLeaves.DataSource = dsro;
                    gvListofROLeaves.DataBind();
                }
                else
                {
                    dsro.Tables[0].Rows.Add(dsro.Tables[0].NewRow());
                    gvListofROLeaves.DataSource = dsro;
                    gvListofROLeaves.DataBind();
                    int columncount = gvListofROLeaves.Rows[0].Cells.Count;
                    gvListofROLeaves.Rows[0].Cells.Clear();
                    gvListofROLeaves.Rows[0].Cells.Add(new TableCell());
                    gvListofROLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                    gvListofROLeaves.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }

        protected void BindResult2()
        {
            DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + Session["name"].ToString() + "'");
            if (ds.Tables[0].Rows.Count != 0)
            {
                pnlApprove.Visible = true;
                string s;
                if (ds.Tables[0].Rows[0][0].ToString() == "102")
                {
                    s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                    s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                    s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnHRDesk' and YEAR(l.AppliedDate)='" + DropDownList1.SelectedItem.Text + "'";
                }
                else
                {
                    s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                    s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                    s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where d.ManagerId='" + Session["name"].ToString() + "' and l.Status='OnManagerDesk' and YEAR(l.AppliedDate)='" + DropDownList1.SelectedItem.Text + "'";
                }


                DataSet ds1 = DataQueries.SelectCommon(s);
                gvlistofLeaves.DataSource = ds1;
                gvlistofLeaves.DataBind();
                if (ds1.Tables[0].Rows.Count == 0)
                {
                    lblListDetail.Text = "No Leaves Pending";
                }
                else
                {
                    lblListDetail.Text = "Leaves Pending = " + ds1.Tables[0].Rows.Count.ToString();
                }
            }
            else
            {
                Panel1.Visible = true;
                string y;
                y = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate as UpdateOn";
                y += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
                y += " where l.EmployeedId='" + Session["name"].ToString() + "' and YEAR(l.AppliedDate)='" + DropDownList1.SelectedItem.Text + "' order by l.LeaveId desc";
                DataSet dsro1 = DataQueries.SelectCommon(y);
                if (dsro1.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = dsro1;
                    GridView1.DataBind();
                }
                else
                {
                    dsro1.Tables[0].Rows.Add(dsro1.Tables[0].NewRow());
                    GridView1.DataSource = dsro1;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
        protected void month_changed(object sender, EventArgs e)
        {
            Bindresult();
        }
        protected void year1_changed(object sender, EventArgs e)
        {
            BindResult2();
        }

        protected void Bindleaveslist()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            DataSet ds1 = DataQueries.SelectCommon("Select d.DeptId from DeptMaster d  inner join Employees e on e.EmployeeId=d.ManagerId");
            string deptid = ds1.Tables[0].Rows[0]["DeptId"].ToString();
            if (ds1.Tables[0].Rows.Count>0)
            {
                if(deptid=="4")
                {
                    SqlCommand cmd = new SqlCommand("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID inner join DeptMaster d on d.DeptId=e.DeptId where l.Status='OnHRDesk'", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvlistofLeaves.DataSource = ds;
                        gvlistofLeaves.DataBind();
                    }
                    else
                    {
                        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                        gvlistofLeaves.DataSource = ds;
                        gvlistofLeaves.DataBind();
                        int columncount = gvlistofLeaves.Rows[0].Cells.Count;
                        gvlistofLeaves.Rows[0].Cells.Clear();
                        gvlistofLeaves.Rows[0].Cells.Add(new TableCell());
                        gvlistofLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                        gvlistofLeaves.Rows[0].Cells[0].Text = "No Records Found";
                    }
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID inner join DeptMaster d on d.DeptId=e.DeptId where l.Status='OnManagerDesk' and d.ManagerId='"+ Session["EmpId"].ToString() + "'", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvlistofLeaves.DataSource = ds;
                        gvlistofLeaves.DataBind();
                    }
                    else
                    {
                        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                        gvlistofLeaves.DataSource = ds;
                        gvlistofLeaves.DataBind();
                        int columncount = gvlistofLeaves.Rows[0].Cells.Count;
                        gvlistofLeaves.Rows[0].Cells.Clear();
                        gvlistofLeaves.Rows[0].Cells.Add(new TableCell());
                        gvlistofLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                        gvlistofLeaves.Rows[0].Cells[0].Text = "No Records Found";
                    }
                }
            }
            
        }

        protected void Approve_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            string LeaveId = btn.CommandArgument;
            string sqlstr = "";

            DataSet ds = DataQueries.SelectCommon("Select Status,BalanceLeaves,Duration from LeavesStatus where LeaveId='" + LeaveId.Trim() + "'");
            string status = ds.Tables[0].Rows[0][0].ToString().Trim();
            int bal = int.Parse(ds.Tables[0].Rows[0][1].ToString());
            int LeaveDuration = int.Parse(ds.Tables[0].Rows[0][2].ToString());

            if (status == "OnManagerDesk")
            {
                sqlstr = "update LeavesStatus set Status='OnHRDesk',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + LeaveId.Trim() + "'";
            }
            else if (status == "OnHRDesk")
            {
                sqlstr = "update LeavesStatus set Status='Approved',BalanceLeaves='" + (bal - LeaveDuration) + "',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + LeaveId.Trim() + "'";
            }

            DataQueries.UpdateCommon(sqlstr);
            //FillgvLeavesList();
            Bindleaveslist();
        }

        protected void Reject_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            string LeaveId = btn.CommandArgument;
            string sqlstr = "update LeavesStatus set Status='Rejected',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + LeaveId.Trim() + "'";
            DataQueries.UpdateCommon(sqlstr);
            FillgvLeavesList();
        }
    }
}