﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewCandidates.aspx.cs" Inherits="CherlaHealth.ViewCandidates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .style1
        {
            height: 26px;
        }
        .tpadleft
        {
            padding-left: 20px;
        }
        .tpadright
        {
            padding-right: 20px;
        }
        .form-control1
        {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
        th{
            text-align:center !important;
        }
            .auto-style1 {
                left: 0px;
                top: 0px;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">View Candidates</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row mt">
                <div class=" col-lg-12 panel" style="padding: 20px">

                    <div class=" col-lg-12">
                       
                      <%--  <h4 style="color: green; font-weight: bold;">
                            <strong>View Candidates</strong></h4>
                        <hr />--%>

                        <div class="col-md-4">

                            <div class="input-group margin">
                   <asp:DropDownList ID="DropDownList2" runat="server" class="form-control" CssClass="auto-style1">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem>Selected</asp:ListItem>
                                    <asp:ListItem>Rejected</asp:ListItem>
                                    <asp:ListItem>Hold</asp:ListItem>
                                </asp:DropDownList>
                                <span class="input-group-btn">
                                    <asp:Button ID="Button1" runat="server" Text="Go!" class="btn btn-info btn-flat" OnClick="Button1_Click"></asp:Button>
                                </span>
                            </div>
                        </div>
                        <%--<button type="button" class="btn btn-info btn-flat">Go!</button>--%>
                    </div>



                    <br />
                    <br />
                    <div class="col-md-12 ">
                      <div class="table table-responsive" style="overflow: scroll; height: 420px">
                        <asp:GridView ID="GridView1" class="table table-bordered table-striped" GridLines="none" runat="server"
                            AutoGenerateColumns="false" DataKeyNames="Id">

                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="" Visible="false" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />

                                <asp:BoundField DataField="JPosition" HeaderText="Job Position" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                <asp:BoundField DataField="Branch" HeaderText="Branch" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                <asp:BoundField DataField="Email" HeaderText="Email" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                <asp:BoundField DataField="SalaryExp" HeaderText="Expected CTC" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                <asp:BoundField DataField="Mobile" HeaderText="Mobile" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />

                                <asp:BoundField DataField="Avg" HeaderText="Performance" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" />
                                <asp:TemplateField HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd">
                                    <HeaderTemplate>
                                        Update Status
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <itemstyle horizontalalign="Center" />
                                        <h5 class=" text-center">
                                            <asp:LinkButton ID="lnkEdit" CssClass="center glyphicon glyphicon-pencil" OnClick="Click1"
                                                runat="server"></asp:LinkButton></h5>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd" Visible="false">
                                    <HeaderTemplate>
                                        Skills
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <h5 class=" text-center">
                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SSS" class="glyphicon glyphicon-plus"
                                                CommandArgument='<%# Eval("Id") %>'></asp:LinkButton></h5>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#21badd">
                                    <HeaderTemplate>
                                        View Details
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <h5 class=" text-center">
                                            <asp:LinkButton ID="LinkButton2" class="glyphicon glyphicon-eye-open" runat="server"
                                                OnClick="Details" CommandArgument='<%# Eval("Id") %>'></asp:LinkButton></h5>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--   <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    Resume(s)
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkView" runat="server" class="glyphicon glyphicon-eye-open"
                        OnClick="View" CommandArgument='<%# Eval("Id") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:Button ID="modelPopup" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup"
                        PopupControlID="updatePanel" CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="updatePanel" runat="server">
                        <table width="180%" cellspacing="4" style="background: #efefef; margin-top: -70px; color: #000 !important; padding: 20px">
                            <tr style="background-color: green">
                                <td colspan="2" align="center">
                                    <h3 style="color: white">Candidate Details</h3>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="style2"></td>
                                <td>
                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Full Name
                                </td>
                                <td class="style1">
                                    <asp:Label ID="FName" runat="server"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Job Position
                                </td>
                                <td class="style1">
                                    <asp:Label ID="JPosition" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Previous Company
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="CCompany" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Previous Experience
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="lblexperiance" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Expected CTC
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Exp_CTC" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Interview Held on
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Interview_date" runat="server"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Reffered by
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Referby" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Communication&nbsp;
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Communication" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Technical
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Technical" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Screening
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Screening" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Status
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="status" runat="server"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td align="left" class="style3 tpadleft">Reason 
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Reason" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Branch
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="lblbranch" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Mobile No
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="Mobile" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Email Id
                                </td>
                                <td class="style1">
                                    <asp:Label ID="Email" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style3 tpadleft">Address
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="address" runat="server"></asp:Label>
                                </td>
                                </tr>
                            <tr>
                                 <td align="left" class="style3 tpadleft">Remarks
                                </td>
                                <td class="style1 tpadright">
                                    <asp:Label ID="lblremarks" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <%-- <tr>
                    <td align="left" class="style3 tpadleft">
                        Blood Group
                    </td>
                    <td class="style1 tpadright">
                        <asp:TextBox ID="txtstate9" runat="server" class="form-control1" />
                    </td>
                </tr>--%>
                            <tr>
                                <td align="left" class="style2 tpadleft" style="padding: 10px 0px 10px 15px"></td>
                                <td>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div class="col-md-12" align="right">
                        <asp:Literal ID="ltEmbed" runat="server" />
                    </div>
                    <asp:Button ID="Button5" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button5"
                        PopupControlID="Panel2" CancelControlID="Button3" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="Panel2" runat="server" BackColor="White" Style="display: none; border: 2px solid #ceceec; padding-bottom: 20px" Width="500px">
                        <div class="col-md-12">
                            <h3 class="text-center">Update Status</h3>
                            <hr />
                            <div class="col-md-6 control-group text-center" style="padding-bottom: 20px !important">
                                <asp:DropDownList ID="DropDownList1" class="form-control" runat="server">
                                    <asp:ListItem>Select</asp:ListItem>
                                    <asp:ListItem>Selected</asp:ListItem>
                                    <asp:ListItem>Rejected</asp:ListItem>
                                    <asp:ListItem>Hold</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px !important">
                                <asp:TextBox ID="txtremarks" CssClass="form-control" placeholder="Remarks....." runat="server"></asp:TextBox>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />
                        <div class="control-group text-center" style="padding-top: 20px !important">
                            <asp:Button ID="Button2" runat="server" class="btn btn-success" OnClick="buttn2"
                                Text="Submit"></asp:Button>
                            <%-- <asp:Button ID="Button4" runat="server"  Text="Not "></asp:Button>--%>
                            <asp:Button ID="Button3" runat="server" class="btn btn-danger" Text="Cancel"></asp:Button>
                        </div>
                    </asp:Panel>

                    <asp:Label ID="Label2" runat="server" Text="Label" Visible="false"></asp:Label>
                </div>
            </div>
            </div>
        </div>
</asp:Content>
