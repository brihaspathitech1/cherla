﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="LeavesList.aspx.cs" Inherits="CherlaHealth.LeavesList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Applied Leaves List</h2>

            <hr />
            <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <asp:GridView ID="gvLeaveslist" CssClass="table table-responsive" GridLines="None" AutoGenerateColumns="false" runat="server">
                    <Columns>
                        <asp:BoundField DataField="EmployeName" HeaderText="Employee Name" />
                        <asp:BoundField DataField="AppliedDate" HeaderText="Applied Date" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:BoundField DataField="leaveType" HeaderText="Leave Type"/>
                        <asp:BoundField DataField="FromDate" HeaderText="From Date" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:BoundField DataField="ToDate" HeaderText="To Date" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="CL" HeaderText="CL" />
                        <asp:BoundField DataField="SL" HeaderText="SL" />
                        <asp:BoundField DataField="LOP" HeaderText="LWP" />
                        <asp:BoundField DataField="BalanceLeaves" HeaderText="Balance Leaves" />
                        <asp:BoundField DataField="ReasontoApply" HeaderText="Reason" />
                    </Columns>
                </asp:GridView>
            </div>
            </div>
        </div>
</asp:Content>
