﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace CherlaHealth
{
    public partial class AddHolidays : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Holidays WHERE HolidayDate = @Name", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name", this.TextBox2.Text.Trim());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.lblStatus.Text = "Duplicate Entry";
                        }
                        else
                        {
                            SqlConnection conn1 = new SqlConnection(connstrg);

                            SqlCommand com1 = new SqlCommand("insert into Holidays(HolidayName,HolidayDate,HolidayType,Dercripation) values ('" + TextBox1.Text + "','" + TextBox2.Text + "','" + DropDownList1.SelectedItem.Text + "','" + TextBox3.Text + "')", conn1);
                            conn1.Open();
                            com1.ExecuteNonQuery();
                            conn1.Close();
                            Response.Write("<script>alert('Data inserted successfully')</script>");
                            Response.Redirect("~/HolidayView.aspx");

                        }
                    }
                }

            }
        }
    }
}