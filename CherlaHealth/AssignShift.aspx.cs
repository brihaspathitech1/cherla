﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class AssignShift : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindStudy();
            }

            if (!IsPostBack)
            {
                BindStudy4();
            }
        }
        private void BindStudy()
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT CompanyID,CompanyName FROM Companies";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList1.DataSource = dt;
                        DropDownList1.DataValueField = "CompanyID";
                        DropDownList1.DataTextField = "CompanyName";
                        DropDownList1.DataBind();
                        sqlConn.Close();


                        DropDownList1.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }

        }

        private void BindStudy4()
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT ShiftId,ShiftName from Shifts ";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList3.DataSource = dt;
                        DropDownList3.DataValueField = "ShiftId";
                        DropDownList3.DataTextField = "ShiftName";
                        DropDownList3.DataBind();
                        sqlConn.Close();


                        DropDownList3.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }

        }


        protected void Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT EmployeeId,EmployeName FROM Employees where  CompanyId='" + DropDownList1.SelectedValue + "' and DeptId='" + Department.SelectedValue + "' ";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        DropDownList2.DataSource = dt;
                        DropDownList2.DataValueField = "EmployeeId";
                        DropDownList2.DataTextField = "EmployeName";
                        DropDownList2.DataBind();
                        sqlConn.Close();


                        DropDownList2.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "SELECT DeptId,Department FROM Department where CompanyId='" + DropDownList1.SelectedValue + "' ";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        Department.DataSource = dt;
                        Department.DataValueField = "DeptId";
                        Department.DataTextField = "Department";
                        Department.DataBind();
                        sqlConn.Close();


                        Department.Items.Insert(0, new ListItem("", "0"));




                    }
                }
            }
            catch { }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM EmployeeShift WHERE ShiftId = @Name and EmployeeId=@name1 and ShiftStartDate=@name2 and ShiftEndDate=@name3 ", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name", this.DropDownList3.SelectedValue.Trim());
                        cmd.Parameters.AddWithValue("@Name1", this.DropDownList2.SelectedValue.Trim());
                        cmd.Parameters.AddWithValue("@Name2", this.TextBox1.Text.Trim());
                        cmd.Parameters.AddWithValue("@Name3", this.TextBox2.Text.Trim());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.Label6.Text = "Shift Already Assigned";
                        }
                        else
                        {
                            SqlConnection con1 = new SqlConnection(connstrg);

                            String insert = "insert into EmployeeShift(ShiftId,EmployeeId,ShiftStartDate,ShiftEndDate,ShiftName,EmployeeName)values('" + DropDownList3.SelectedValue + "','" + DropDownList2.SelectedValue + "','" + TextBox1.Text + "','" + TextBox2.Text + "','" + DropDownList3.SelectedItem.Text + "','" + DropDownList2.SelectedItem.Text + "')";
                            SqlCommand comm1 = new SqlCommand(insert, con);

                            string update = "Update Employees set shift='" + DropDownList3.SelectedValue + "' where EmployeeId='" + DropDownList2.SelectedValue + "'";
                            SqlCommand cmd12 = new SqlCommand(update, con);

                            con1.Open();
                            comm1.ExecuteNonQuery();
                            cmd12.ExecuteNonQuery();
                            con1.Close();
                            Response.Write("<script>alert('Data inserted successfully')</script>");
                            Response.Redirect("~/EmpShiftDetails.aspx");

                        }

                    }
                }
            }
        }
        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    using (SqlConnection sqlConn = new SqlConnection(connstrg))
            //    {
            //        using (SqlCommand sqlCmd = new SqlCommand())
            //        {
            //            sqlCmd.CommandText = "SELECT EmpId,MachineId FROM Employee where EmpId='" + DropDownList2.SelectedValue + "' ";
            //            sqlCmd.Connection = sqlConn;
            //            sqlConn.Open();
            //            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            //            DataTable dt = new DataTable();
            //            da.Fill(dt);
            //            DropDownList6.DataSource = dt;
            //            DropDownList6.DataValueField = "EmpId";
            //            DropDownList6.DataTextField = "MachineId";
            //            DropDownList6.DataBind();
            //            sqlConn.Close();







            //        }
            //    }
            //}
            //catch { }




        }
    }
}