﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.DataAccessLayer;
namespace CherlaHealth
{
    public partial class userroles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if
                (!IsPostBack)
            {
                GetEmployee();
                designation();
            }
        }

        private void GetEmployee()
        {
            DataSet ds=DataQueries.SelectCommon("select * from [Employees]  where status='1'");
            name.DataSource = ds;
            name.DataTextField = "EmployeName";
            name.DataValueField = "EmployeeId";
            name.DataBind();
            name.Enabled = true;
            name.Items.Insert(0, new ListItem("", "0"));
        }
        protected void designation()
        {
            DataSet ds=DataQueries.SelectCommon("with cte as (select Id,Roles,ROW_NUMBER() over (partition by Roles order by id desc ) as rn from [Stock].[dbo].[register12] where Roles is not null and Roles!='Vijayawada') select * from cte where rn=1");
            
            dropdesignation.DataSource = ds;
            dropdesignation.DataTextField = "Roles";
            dropdesignation.DataValueField = "Id";
            dropdesignation.DataBind();
            dropdesignation.Items.Insert(0, new ListItem("", "0"));
        }
        protected void designation_click(object sender, EventArgs e)
        {
            DataSet ds= DataQueries.SelectCommon("select repassword from register12 where roles='" + dropdesignation.SelectedItem.Text + "' and Email='" + Roles.SelectedItem.Text + "' order by id desc");
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                lbldesignation.Text = ds.Tables[0].Rows[0]["repassword"].ToString();
            }
        }
        
        public void insert()
        {
            DataSet ds= DataQueries.SelectCommon("select * from register12 where Name='" + name.SelectedItem.Value + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                Label1.Visible = true;
            }
            else
            {
                if (lbldesignation.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select Valid Role')", true);
                }
                else
                {
                    string comms1 = "insert into [Stock].[dbo].[register12](Name,Admin,Email,[Designation],[Password],[Repassword],[Roles]) values('" + name.SelectedItem.Value + "','" + lbladmin.Text + "','" + Roles.SelectedItem.Text + "','" + Designation.Text + "','" + Password.Text + "','" + lbldesignation.Text + "','" + dropdesignation.SelectedItem.Text + "')";
                    DataQueries.InsertCommon(comms1);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Login Created Successfully')", true);
                    dropdesignation.SelectedItem.Text = "";
                    name.SelectedItem.Text = "";
                    Password.Text = "";
                    Roles.SelectedItem.Text = "";
                }

            }
        }
        protected void name_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds=DataQueries.SelectCommon("select * from [Employees] where [EmployeeId]='" + name.SelectedValue + "' ");
            if (ds.Tables[0].Rows. Count >0)
            {
                Designation.Text = ds.Tables[0].Rows[0]["Desigantion"].ToString();
                lbladmin.Text = ds.Tables[0].Rows[0]["CompanyEmail"].ToString();
            }
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            insert();
        }
    }
}