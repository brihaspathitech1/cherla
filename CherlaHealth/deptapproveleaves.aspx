﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee.Master" AutoEventWireup="true" CodeBehind="deptapproveleaves.aspx.cs" Inherits="CherlaHealth.deptapproveleaves" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Approve Leaves</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
                    <div class="row" runat="server" visible="false">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Monthly</a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Yearly</a></li>

                                    
                                </ul>

                                
                            </div>


                        </div>
            
          	<div class="row mt">
          		<div class="col-lg-12 panel">
 <asp:Panel ID="pnlApprove" runat="server" Visible="true">
        <div class="form-panel col-sm-12">
            <h4>
                List of Pending Leaves:-
                <asp:Label ID="lblListDetail" runat="server" Text="" ForeColor="#DC7A01"></asp:Label>
            </h4>
            <div style="height: 700px; overflow-y: scroll; overflow-x: scroll">
                <asp:GridView ID="gvlistofLeaves" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="LeaveID">
                    <Columns>
                        <asp:BoundField DataField="EmployeedID" HeaderText="Employee Id" />
                        <asp:BoundField DataField="EmployeName" HeaderText="Employee" />
                        <asp:BoundField DataField="LeaveId" HeaderText="LeaveId" Visible="False" />
                        <asp:BoundField DataField="AppliedDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Applied Date" />
                        <asp:BoundField DataField="CL" HeaderText="Leave Type" />
                        <asp:BoundField DataField="SL" HeaderText="SL" />
                        <asp:BoundField DataField="Pl" HeaderText="PL/EL" />
                        <asp:BoundField DataField="COF" HeaderText="Compensatory Off" />
                        <asp:BoundField DataField="LOP" HeaderText="LWP" />
                        <asp:BoundField DataField="LeaveType" HeaderText="Leave Type" />
                        <asp:BoundField DataField="FromDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="From Date" />
                        <asp:BoundField DataField="ToDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="To Date" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="BalanceLeaves" HeaderText="Balance Leaves" />
                        <asp:BoundField DataField="ReasontoApply" HeaderText="Reason to Apply" />
                       <%-- <asp:BoundField DataField="Status" HeaderText="Status" />--%>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <div class="btn-group" style="float: right">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                        Actions <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu " role="menu">
                                        <li>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" CommandArgument='<%# Eval("LeaveId") %>'
                                                OnClick="Approve_Click">Approve</asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandArgument='<%# Eval("LeaveId") %>'
                                                OnClick="Reject_Click">Reject</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                                <%--Text="Approve" ForeColor="White" BackColor="#DC7A01" />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

                      <div class="tab-content">

                            <!-----Customer Panel Starts----->
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>

                                    <div class="col-md-3" style="z-index:999" runat="server" visible="false">
                                 Month:<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" OnTextChanged="month_changed" AutoPostBack="true" style="z-index:9999"></asp:TextBox>
             <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"  TargetControlID="TextBox1" OnClientHidden="onCalendarHidden"  OnClientShown="onCalendarShown" BehaviorID="calendar1" 
    Enabled="True"  />
                                        </div>
                                        <div class="clearfix"></div>
                                        </ContentTemplate>
                                </asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
    <asp:Panel ID="pnlReadOnly" runat="server" Visible="false">
      
           
              

            <div style="height: 700px;" class="col-md-12">
                 <h4>
                List of Leaves:-
                <asp:Label ID="lblReadOnlyListOfLeaves" runat="server" Text="" ForeColor="#DC7A01"></asp:Label>
            </h4>
                <asp:GridView ID="gvListofROLeaves" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="True">
                </asp:GridView>
            </div>
      
    </asp:Panel>
                                        </ContentTemplate>
    </asp:UpdatePanel>
                                        
                                </div>
                           <div role="tabpanel" class="tab-pane" id="profile">
                              <%--<asp:TextBox ID="TextBox2" runat="server" placeholder="Type year only.." OnTextChanged="year_changed" AutoPostBack="true"></asp:TextBox>--%>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                         <div class="col-md-3">
                               Select Year:<asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="year1_changed" CssClass="form-control" AutoPostBack="true">
                           <asp:ListItem></asp:ListItem>
                           <asp:ListItem>2016</asp:ListItem>
                           <asp:ListItem>2017</asp:ListItem>
                           <asp:ListItem>2018</asp:ListItem>
                           <asp:ListItem>2019</asp:ListItem>
                           <asp:ListItem>2020</asp:ListItem>
                           <asp:ListItem>2021</asp:ListItem>
                           <asp:ListItem>2022</asp:ListItem>
                           <asp:ListItem>2023</asp:ListItem>
                           <asp:ListItem>2024</asp:ListItem>
                           <asp:ListItem>2025</asp:ListItem>
                           <asp:ListItem>2026</asp:ListItem>
                           <asp:ListItem>2027</asp:ListItem>
                           <asp:ListItem>2028</asp:ListItem>
                           <asp:ListItem>2029</asp:ListItem>
                           <asp:ListItem>2030</asp:ListItem>
             
                       </asp:DropDownList>
                                             </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                               <asp:Panel ID="Panel1" runat="server" Visible="false">
        <div class="form-panel col-sm-12">
            <h4>
                List of Leaves:-
                <asp:Label ID="Label1" runat="server" Text="" ForeColor="#DC7A01"></asp:Label>
            </h4>
                  
            <div style="height: 700px;">
                <asp:GridView ID="GridView1" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="True">
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
                                        </ContentTemplate>
                                   </asp:UpdatePanel>
                               </div>
                          </div>

                      <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" CancelControlID="btnclose" PopupControlID="Panel2"></ajaxToolkit:ModalPopupExtender>
                      <asp:Panel ID="Panel2" runat="server" BackColor="White" Height="200px" Width="500px"
                        Style="display: none" BorderStyle="Groove">
                          <h3 style="text-align:center">Replace Person</h3>
                          <hr />
                          <div class="col-md-6">
                              <div class="form-group">
                                  Employee Name <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server"></asp:DropDownList>
                              </div>
                          </div>
                           <div class="col-md-3" style="padding-top:18px">
                              <div class="form-group">
                                  <asp:HiddenField ID="HiddenField1" runat="server" />
                                  <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Approve" />
                                  </div>
                               </div>
                           <div class="col-md-3" style="padding-top:18px">
                              <div class="form-group">
                                  <asp:Button ID="btnclose" CssClass="btn btn-danger" runat="server" Text="Close" />
                                  </div>
                               </div>
                      </asp:Panel>

                      <asp:Label ID="lblleaveid" Visible="false" runat="server" Text="Label"></asp:Label>

    </div>
    </div>
            </div>
        </div>

</asp:Content>
