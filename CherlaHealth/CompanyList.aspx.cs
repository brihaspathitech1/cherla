﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class CompanyList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindStudy();
            if (!IsPostBack)
            {
                BindStudy();
            }
        }

        private void BindStudy()
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Companies", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompany(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
            SqlDataAdapter adp = new SqlDataAdapter(" SELECT CompanyName FROM Companies where CompanyName like'" + prefixText + "%'", con);


            DataTable dt = new DataTable();
            adp.Fill(dt);
            List<string> Mobilenumber = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Mobilenumber.Add(dt.Rows[i]["CompanyName"].ToString());
            }
            return Mobilenumber;
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            txtstor_address.Text = gRow.Cells[10].Text.Replace("&nbsp;", ""); ;
            txtcity.Text = gRow.Cells[3].Text.Replace("&nbsp;", "");
            txtstate.Text = gRow.Cells[4].Text.Replace("&nbsp;", "");
            txtadrs.Text = gRow.Cells[5].Text.Replace("&nbsp;", "");
            txtemail.Text = gRow.Cells[6].Text.Replace("&nbsp;", "");
            txtstate2.Text = gRow.Cells[7].Text.Replace("&nbsp;", "");
            txtstate3.Text = gRow.Cells[8].Text.Replace("&nbsp;", "");

            txtstate5.Text = gRow.Cells[9].Text.Replace("&nbsp;", "");
            txtstate13.Text = gRow.Cells[11].Text.Replace("&nbsp;", "");
            txtstate6.Text = gRow.Cells[12].Text.Replace("&nbsp;", "");
            txtstate7.Text = gRow.Cells[13].Text.Replace("&nbsp;", "");
            txtstate8.Text = gRow.Cells[14].Text.Replace("&nbsp;", "");
            txtstate9.Text = gRow.Cells[15].Text.Replace("&nbsp;", "");
            txtstate10.Text = gRow.Cells[16].Text.Replace("&nbsp;", "");
            txtstate11.Text = gRow.Cells[17].Text.Replace("&nbsp;", "");
            txtstate12.Text = gRow.Cells[18].Text.Replace("&nbsp;", "");
            txtlat.Text = gRow.Cells[19].Text.Replace("&nbsp;", "");
            txtlong.Text = gRow.Cells[20].Text.Replace("&nbsp;", "");
            this.ModalPopupExtender2.Show();
        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn33 = new SqlConnection(connstrg);
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lbldeleteid = (Label)row.FindControl("CompanyId");
            conn33.Open();
            SqlCommand cmd3 = new SqlCommand("delete FROM Companies where CompanyId='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'", conn33);
            cmd3.ExecuteNonQuery();
            conn33.Close();
            BindStudy();

        }
        protected void btnModity_Click(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            string DeptId = lblstor_id.Text;
            conn44.Open();
            SqlCommand cmd = new SqlCommand("update Companies set CompanyCode='" + txtstor_address.Text + "',CompanyName='" + txtcity.Text + "',Name='" + txtstate.Text + "',Email='" + txtemail.Text + "',Address1='" + txtadrs.Text + "',Phone='" + txtstate2.Text + "',WebsiteURL='" + txtstate3.Text + "',MobileNo='" + txtstate5.Text + "', FaxNo='" + txtstate13.Text + "',RegNo='" + txtstate6.Text + "',RegDate='" + txtstate7.Text + "',Tanno='" + txtstate8.Text + "',PanNo='" + txtstate9.Text + "',TinNo='" + txtstate10.Text + "',PhoneSecond='" + txtstate11.Text + "',Desiganation='" + txtstate12.Text + "',latitude='"+txtlat.Text+ "',longitude='"+txtlong.Text+"' where CompanyId='" + lblstor_id.Text+"'", conn44);
            cmd.ExecuteNonQuery();
            conn44.Close();
            //
            lblmsg.Text = "Data Updated...";
            lblmsg.ForeColor = System.Drawing.Color.Green;
            BindStudy();
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Updated...')", true);
        }
        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);

            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Companies where CompanyName like '%'+@date1+'%'  ", conn44);
            cmd.Parameters.Add("@date1", SqlDbType.VarChar).Value = TextBox1.Text.Trim();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}