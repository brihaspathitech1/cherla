﻿using inventory.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["name"].ToString()=="")
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection conn = new SqlConnection(connstrg);
                SqlCommand cmd = new SqlCommand("Select * from Login where Username='" + Session["name"].ToString() + "' and Password='" + Session["Password"].ToString() + "'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if(dr.Read())
                {
                    Label1.Text = dr["Name"].ToString();
                }
                conn.Close();
                FillgvLeavesList();
            }
        }

        private void FillgvLeavesList()
        {
            if (Session["name"].ToString() == "admin")
            {
                try
                {
                    DataSet ds = DataQueries.SelectCommon("Select deptid from DeptMaster where ManagerId='" + Session["EmpId"].ToString() + "'");
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        string s=string.Empty;
                        if (ds.Tables[0].Rows[0][0].ToString() == "4")
                        {
                            s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                            s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                            s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnHRDesk'";
                        }                      
                        DataSet ds1 = DataQueries.SelectCommon(s);
                        int count = ds1.Tables[0].Rows.Count;

                        lblLeavesCount.Text = "You have " + count.ToString() + " Leaves Pending";
                        lblcount.Text = count.ToString();

                    }
                    else
                    {
                        string s;

                        s = "select LeaveId from LeavesStatus where EmployeedID='" + Session["name"].ToString().Trim() + "' and Status='Approved' and StatusDate=convert(date,getdate())";

                        DataSet ds1 = DataQueries.SelectCommon(s);
                        int count = ds1.Tables[0].Rows.Count;

                        //lblLeavesCount.Text = "Congrats,Your " + count.ToString() + " Leave Approved";
                        lblcount.Text = count.ToString();
                    }

                }
                catch
                {
                    Response.Redirect("Default.aspx");
                }
            }
            

        }
    }
}