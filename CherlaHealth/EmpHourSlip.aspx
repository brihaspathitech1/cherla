﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EmpHourSlip.aspx.cs" Inherits="CherlaHealth.EmpHourSlip" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCalendarShown() {

            var cal = $find("calendar1");
            //Setting the default mode to month
            cal._switchMode("months", true);

            //Iterate every month Item and attach click event to it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }

        function onCalendarHidden() {
            var cal = $find("calendar1");
            //Iterate every month Item and remove click event from it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }

        }
        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "month":
                    var cal = $find("calendar1");
                    cal._visibleDate = target.date;
                    cal.set_selectedDate(target.date);
                    cal._switchMonth(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged();
                    break;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Hourly Payroll</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                       Month      <asp:TextBox ID="txtmonth" CssClass="form-control" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtmonth" OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                            Enabled="True" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Employee <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server"></asp:DropDownList>

                    </div>
                </div>
            <div class="col-md-3" style="padding-top:18px">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit" />
                    </div>
                </div>
            <div class="col-md-12">
                <asp:GridView ID="GridView1" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None" runat="server">
                    <Columns>
                       <asp:BoundField DataField="Name" HeaderText="Employee Name" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="hrsalary" HeaderText="Salary Per Hour" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="hours" HeaderText="No.Of Hours" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="incentives" HeaderText="Incentives" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Taxded" HeaderText="Tax Deduction" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="gross" HeaderText="Total" HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White" />
                         <asp:TemplateField HeaderStyle-BackColor="#428bca" HeaderStyle-ForeColor="White">
                                            <HeaderTemplate>
                                                Payslip
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <h5 class=" text-center">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" class="glyphicon glyphicon-print"
                                                        CommandArgument='<%# Eval("Id") %>' OnClick="Report_click" CausesValidation="false"></asp:LinkButton></h5>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            </div>
        </div>
</asp:Content>
