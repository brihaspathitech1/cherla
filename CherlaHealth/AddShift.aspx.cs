﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class AddShift : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;


            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Shifts WHERE ShiftName = @Name", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name", this.shiftname.Text.Trim());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.lblStatus.Text = "Duplicate Entry";
                        }
                        else
                        {
                            SqlConnection conn1 = new SqlConnection(connstrg);
                            string str = "insert into Shifts values('" + shiftname.Text + "', '" + shiftcode.Text + "', '" + start.Text + "','" + end.Text + "')";
                            SqlCommand comm1 = new SqlCommand(str, conn1);
                            conn1.Open();
                            comm1.ExecuteNonQuery();
                            conn1.Close();
                        }
                    }
                }
            }

            shiftname.Text = "";
            shiftcode.Text = "";

            start.Text = "";
            end.Text = "";

            Response.Write("<script>alert('Data inserted successfully')</script>");
            Response.Redirect("~/ShiftsList.aspx");
            //  BindGridview();



        }
    }
}