﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

namespace CherlaHealth
{
    public partial class DashBoard : System.Web.UI.Page
    {
        SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AppendHeader("Refresh", "180");

            Label5.Text = DateTime.Now.ToString("yyyy-MM-dd");
            var yese = DateTime.Now.AddDays(-1);
            Label6.Text = yese.ToString("yyyy-MM-dd");
            if (!Page.IsPostBack)
            {
                BindDataList();
            }
            if (!Page.IsPostBack)
            {
                BindDataList2();
            }
            if (!Page.IsPostBack)
            {
                BindDataList3();
            }
            if (!Page.IsPostBack)
            {
                BindDataList4();
            }


        }
        protected void BindDataList()
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);


            using (conn44)
            {
                SqlCommand command = new SqlCommand("Proc_ModifiedDashBoard", conn44);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = Label5.Text;
                conn44.Open();
                SqlDataReader reader = command.ExecuteReader();

                DataList1.DataSource = reader;
                DataList1.DataBind();





            }

        }




        protected void BindDataList2()
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);


            using (conn44)
            {
                SqlCommand command = new SqlCommand("Proc_ModifiedDashBoardAbsent", conn44);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = Label5.Text;
                conn44.Open();
                SqlDataReader reader = command.ExecuteReader();

                DataList2.DataSource = reader;
                DataList2.DataBind();





            }

        }

        protected void BindDataList3()
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);


            using (conn44)
            {
                SqlCommand command = new SqlCommand("Proc_ModifiedDashBoardEarlygoes", conn44);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = Label6.Text;
                conn44.Open();
                SqlDataReader reader = command.ExecuteReader();

                DataList3.DataSource = reader;
                DataList3.DataBind();





            }

        }

        protected void BindDataList4()
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);


            using (conn44)
            {
                SqlCommand command = new SqlCommand("Proc_ModifiedDashBoardTodayEarlyCome", conn44);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = Label5.Text;
                conn44.Open();
                SqlDataReader reader = command.ExecuteReader();

                DataList4.DataSource = reader;
                DataList4.DataBind();





            }

        }

      



        [WebMethod]
        public static List<countrydetails> GetChartData()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection("Data Source=103.233.79.22;Initial Catalog=Cherla;User ID=CherlaHos;Password=Brihaspathi@123"))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select name,total=Days from PieChart order by total desc", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                con.Close();
            }
            List<countrydetails> dataList = new List<countrydetails>();
            foreach (DataRow dtrow in dt.Rows)
            {
                countrydetails details = new countrydetails();
                details.Countryname = dtrow[0].ToString();
                details.Total = Convert.ToInt32(dtrow[1]);
                dataList.Add(details);
            }
            return dataList;
        }
    }
    public class countrydetails
    {
        public string Countryname { get; set; }
        public int Total { get; set; }
    }
}