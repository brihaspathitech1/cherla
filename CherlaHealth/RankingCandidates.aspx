﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="RankingCandidates.aspx.cs" Inherits="CherlaHealth.RankingCandidates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padt25
        {
            padding-top: 20px;
        }
        .Empty
        {
            background: url("../Empty.gif") no-repeat right top;
        }
        .Empty:hover
        {
            background: url("../Filled.gif") no-repeat right top;
        }
        .Filled
        {
            background: url("../Filled.gif") no-repeat right top;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus
        {
            padding: 6px 50px;
            background: #77a6ff;
            color: #fff;
        }
        .nav-tabs > li > a
        {
            padding: 6px 50px;
            background: rgba(255, 235, 175, 0.55);
        }
        
        .padb
        {
            padding-bottom: 5px;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link href="assets/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />
    <script src="assets/js/star-rating.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
            padding: 0;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
            margin-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
     
        </div>
        <!-- /.box-header -->
        <div class="box-body">
              <div class="row mt">
                <div class="col-lg-12 panel" style="margin-top: -30px !important; min-height: 540px;">
                    <h4 style="color: green; font-weight: bold; text-align: center">
                        <br />
                        <strong>Candidate Performance</strong></h4>


                    <br />

                    <div class=" col-md-12">


                        <div class=" col-md-12">
                            <div class="row">
                                <div class=" col-md-12">
                                    <div class="col-md-6">

                                        <h4><span style="color: #000">Name Of The Candidate: </span>
                                            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>
                                            <strong>
                                                <asp:Label ID="Label2" runat="server" Text="" Style="color: Red; font-weight: bold;"></asp:Label>
                                            </strong>
                                        </h4>


                                    </div>
                                    <div class="col-md-6">
                                        <h4><span style="color: #000">Designation: </span>
                                            <strong>
                                                <asp:Label ID="Label5" runat="server" Text="" Style="color: #018b9e; font-weight: bold;" /></strong></h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr />
                                <div class=" col-md-12" style="padding-bottom: 10px">

                                    <div class="col-md-3">

                                        <label>
                                            <b>Communication Skills</b></label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox type="text" class="rating rating-loading" value="" data-size="xs" title=""
                                            runat="server" ID="Com_skill"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3">

                                        <asp:TextBox type="text" class="form-control" Placeholder="Reported By"
                                            runat="server" ID="TextBox1"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class=" col-md-12">
                            <div class="row">
                                <div class=" col-md-12" style="padding-bottom: 10px">
                                    <div class="col-md-3">
                                        <label>
                                            <b>Technical Skills</b></label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox type="text" class="rating rating-loading" data-size="xs" title=""
                                            runat="server" ID="Tech_skill"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3">

                                        <asp:TextBox type="text" class="form-control" Placeholder="Reported By"
                                            runat="server" ID="TextBox2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class=" col-md-12">
                            <div class="row">
                                <div class=" col-md-12" style="padding-bottom: 10px">
                                    <div class="col-md-3">
                                        <label>
                                            <b>Screening Test</b></label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox type="text" class="rating rating-loading" value="" data-size="xs" title=""
                                            runat="server" ID="Screen_Test"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3">

                                        <asp:TextBox type="text" class="form-control" Placeholder="Reported By"
                                            runat="server" ID="TextBox3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>





                        </div>
                        <div class=" col-md-12">
                            <div class="row">
                                <div class=" col-md-12">
                                    <div class=" col-md-12">
                                        <asp:Button ID="Submit" runat="server" class="btn btn-success" Text="Submit" OnClick="Update" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>

                </div>
            </div>
                   <cc1:ModalPopupExtender ID="ModalPopupExtender1" BehaviorID="mpe" runat="server"
                PopupControlID="pnlPopup" TargetControlID="lnkDummy" BackgroundCssClass="modalBackground" CancelControlID="btnHide">
            </cc1:ModalPopupExtender>
            <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
            <div class=" col-md-12">
            </div>
            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none; height: 350px">
                <div class="col-md-12">
                    <h3 style="color: #0087ff; text-align: center">Status Updated</h3>
                    <hr />
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12 text-center">

                                <asp:Label ID="Label4" runat="server" Text="Label" Visible="false"></asp:Label>

                            </div>

                            <div class="col-md-12 text-center">
                                <hr />
                                <asp:Button ID="btnHide" runat="server" Text="Ok" class="btn btn-success" />
                            </div>

                        </div>
                    </div>
                </div>

            </asp:Panel>
            </div>
        </div>
      <script type="text/javascript">

        function Decide(option) {
            var temp = "";
            document.getElementById('lblRate').innerText = "";
            if (option == 1) {
                document.getElementById('Rating1').className = "Filled";
                document.getElementById('Rating2').className = "Empty";
                document.getElementById('Rating3').className = "Empty";
                document.getElementById('Rating4').className = "Empty";
                document.getElementById('Rating5').className = "Empty";
                temp = "1-Poor";
            }
            if (option == 2) {
                document.getElementById('Rating1').className = "Filled";
                document.getElementById('Rating2').className = "Filled";
                document.getElementById('Rating3').className = "Empty";
                document.getElementById('Rating4').className = "Empty";
                document.getElementById('Rating5').className = "Empty";
                temp = "2-Ok";

            }
            if (option == 3) {
                document.getElementById('Rating1').className = "Filled";
                document.getElementById('Rating2').className = "Filled";
                document.getElementById('Rating3').className = "Filled";
                document.getElementById('Rating4').className = "Empty";
                document.getElementById('Rating5').className = "Empty";
                temp = "3-Fair";
            }
            if (option == 4) {
                document.getElementById('Rating1').className = "Filled";
                document.getElementById('Rating2').className = "Filled";
                document.getElementById('Rating3').className = "Filled";
                document.getElementById('Rating4').className = "Filled";
                document.getElementById('Rating5').className = "Empty";
                temp = "4-Good";
            }
            if (option == 5) {
                document.getElementById('Rating1').className = "Filled";
                document.getElementById('Rating2').className = "Filled";
                document.getElementById('Rating3').className = "Filled";
                document.getElementById('Rating4').className = "Filled";
                document.getElementById('Rating5').className = "Filled";
                temp = "5-Nice";
            }
            document.getElementById('lblRate').innerText = temp;
            return false;
        }

    </script>
</asp:Content>
