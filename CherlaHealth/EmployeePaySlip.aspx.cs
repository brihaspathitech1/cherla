﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class EmployeePaySlip : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BindData()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            if (Session["name"].ToString() == "admin")
            {
                SqlCommand cmd1 = new SqlCommand("select  Id,Payslip,Name,PFNo,Basic,HRA,Total,GrossSalary,Year,CA,PF,ESI,MobileBill,Month,TTotal,CPF,CESI,Admin,convert(Bigint ,Present) +(convert (float, NOP)) AS SUM,(convert(bigint,PF)+isnull (convert(float,MobileBill),0)+convert(float,ESI)) as sum1,convert(bigint,CPF)+convert(bigint,CESI)+convert(bigint,Admin) as company from  Payslip where date='" + txtmonth.Text + "'  ORDER BY Id asc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn.Open();

                SqlDataReader reader = cmd1.ExecuteReader();
               
                if (reader.Read())
                {
                    label1.Text = reader["Month"].ToString();
                    label2.Text = reader["Year"].ToString();
                }
                reader.Close();
                conn.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GridView1.DataSource = ds;
                    GridView1.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
            else
            {
                SqlCommand cmd = new SqlCommand("Select PFStatus,EmployeName from Employees where EmployeeId='" + Session["name"].ToString() + "'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    lblpf.Text = dr["PFStatus"].ToString();
                    lblname.Text = dr["EmployeName"].ToString();
                }
                conn.Close();


                SqlCommand cmd1 = new SqlCommand("select  Id,Payslip,Name,PFNo,Basic,HRA,Total,GrossSalary,Year,CA,PF,ESI,MobileBill,Month,TTotal,CPF,CESI,Admin,convert(Bigint ,Present) +(convert (float, NOP)) AS SUM,(convert(bigint,PF)+isnull (convert(float,MobileBill),0)+convert(float,ESI)) as sum1,convert(bigint,CPF)+convert(bigint,CESI)+convert(bigint,Admin) as company from  Payslip where date='" + txtmonth.Text + "' AND Name='" + lblname.Text + "' and PFStatus='" + lblpf.Text + "' ORDER BY Id asc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                DataSet ds = new DataSet();
                da.Fill(ds);

                conn.Open();
                SqlDataReader reader = cmd1.ExecuteReader();
                
                if (reader.Read())
                {
                    label1.Text = reader["Month"].ToString();
                    label2.Text = reader["Year"].ToString();
                }
                reader.Close();
                conn.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GridView1.DataSource = ds;
                    GridView1.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }

        }

        protected void SSS(object sender, EventArgs e)
        {
            int Id = int.Parse((sender as LinkButton).CommandArgument);


            Session["Payslip"] = Id;
            Response.Redirect("~/EmpPayReport.aspx");

        }

        protected void Month_Change(object sender,EventArgs e)
        {
            BindData();
        }
    }
}