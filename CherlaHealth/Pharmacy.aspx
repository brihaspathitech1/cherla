﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Pharmacy.aspx.cs" Inherits="CherlaHealth.Pharmacy" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Pharmacist Bond</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                          Date<span style="color:red">*</span> <asp:TextBox ID="fromdate" CssClass="form-control" runat="server"></asp:TextBox>
                         <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="fromdate" runat="server" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="fromdate" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>
            
            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                  Employee Name<span style="color:red">*</span><asp:DropDownList ID="dropempp"  runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropempp" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                 </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
              <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel9" runat="server"><ContentTemplate>
                         Father Name <asp:TextBox ID="fathername" CssClass="form-control" runat="server"></asp:TextBox>
                             
                         
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                         Address <asp:TextBox ID="address" CssClass="form-control" runat="server"></asp:TextBox>
                             
                         
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
             <div class="clearfix"></div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel10" runat="server"><ContentTemplate>
                        Salary <asp:TextBox ID="salary" CssClass="form-control" runat="server"></asp:TextBox>
                             
                         
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel7" runat="server"><ContentTemplate>
                         Gender <asp:dropdownlist ID="gender" CssClass="form-control" runat="server">
                             <asp:ListItem>male</asp:ListItem>
                             <asp:ListItem>female</asp:ListItem>
                             
                         </asp:dropdownlist>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
           
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
                         years <asp:dropdownlist ID="Dropdownlist1" CssClass="form-control" runat="server">
                             <asp:ListItem>1</asp:ListItem>
                             <asp:ListItem>2</asp:ListItem>
                             
                         </asp:dropdownlist>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                         Age <span style="color:red">*</span> <asp:TextBox ID="age" CssClass="form-control"  runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="age" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
             <div class="clearfix"></div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
                       Occupation <span style="color:red">*</span> <asp:TextBox ID="occupation" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ControlToValidate="occupation" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
             <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel6" runat="server"><ContentTemplate>
                         To Date<span style="color:red">*</span> <asp:TextBox ID="todate" CssClass="form-control" runat="server"></asp:TextBox>
                         <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="todate" runat="server" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="todate" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>
            
           
            <div class="clearfix"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                </div>
            </div>
            </div>
        </div>

</asp:Content>

