﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace CherlaHealth
{
    public partial class Employeesrelieved : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindStudy();
        }
        private void BindStudy()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select Company,Desigantion1,EmployeName,EmployeeID1,convert(varchar(10),DOB,110) as DOB,convert(varchar(10),DOJ,110) as DOJ,Gender,Salary,Desigantion,EmployeeType,CompanyEmail,CellNo,Address,BloodGroup,EmployeeId,Password,Emptype from  Employees where status='0'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Update Employees set status='1' where EmployeeId='" + lblid.Text + "'", conn44);

            cmd.ExecuteNonQuery();
            conn44.Close();
            BindStudy();
        }
    }
}