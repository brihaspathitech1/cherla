﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Warning.aspx.cs" Inherits="CherlaHealth.Warning" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px; color: green; font-weight: bold;">Warning Letter</h2>
            <hr />

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
                  Salutation<span style="color:red">*</span> <asp:DropDownList ID="salut"  CssClass="form-control" runat="server">
                      <asp:ListItem>Mr.</asp:ListItem>
                      <asp:ListItem>Mrs.</asp:ListItem>
                      <asp:ListItem>Ms.</asp:ListItem>      </asp:DropDownList>
                       </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
            <div class="col-md-3">
                <div class="form-group">
                    Select Employee<span style="color: red">*</span>
                    <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server" OnSelectedIndexChanged="dropemp_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="dropemp" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
               --%> </div>
            </div>
           
            <div class="col-md-3">
                <div class="form-group">
                    Reason<span style="color: red">*</span>
                    <asp:TextBox ID="txtreason" CssClass="form-control" runat="server"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtreason" ForeColor="Red"></asp:RequiredFieldValidator>--%>

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Suggestion <span style="color: red">*</span>
                    <asp:TextBox ID="txtsug" CssClass="form-control" runat="server"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtsug" ForeColor="Red"></asp:RequiredFieldValidator>
            --%>    </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Explanation Held On<span style="color: red">*</span>
                    <asp:TextBox ID="txtdate" CssClass="form-control" runat="server"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtdate" ForeColor="Red"></asp:RequiredFieldValidator>--%>

                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdate" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    Email<asp:TextBox ID="txtmail" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
