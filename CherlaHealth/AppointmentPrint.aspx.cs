﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace CherlaHealth
{
    public partial class AppointmentPrint : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lbldate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
                BindValues();
                salutation.Text = Session["salute"].ToString();
                Label2.Text = Session["salute"].ToString();
            }
        }

        protected void BindValues()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from appointment where id='" + Session["id"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblname.Text = dr["name"].ToString();
                lblname2.Text= dr["name"].ToString();
                lblname3.Text= dr["name"].ToString();
                lbladdress.Text = dr["address"].ToString();
                lbldesig.Text = dr["designation"].ToString();
                lbldesignation.Text = dr["designation"].ToString();
                Label1.Text = dr["company_address"].ToString();
                DateTime date = DateTime.Parse(dr["apoint_date"].ToString());
                lbldate3.Text = date.ToString("dd MMMM yyyy");
                lblmonth.Text =(Math.Round(Convert.ToDouble(dr["monthsal"].ToString()),2)).ToString();
                lblyear.Text = (Math.Round(Convert.ToDouble(dr["yearsal"].ToString()), 2)).ToString();
                lblmbasic.Text = (Math.Round(Convert.ToDouble(dr["mbasic"].ToString()), 2)).ToString();
                lblmhra.Text = (Math.Round(Convert.ToDouble(dr["mhra"].ToString()), 2)).ToString();
                lblmconveyance.Text = (Math.Round(Convert.ToDouble(dr["mconveyance"].ToString()), 2)).ToString();
                lblmcha.Text = (Math.Round(Convert.ToDouble(dr["mcha"].ToString()), 2)).ToString();
                lblmspecial.Text = (Math.Round(Convert.ToDouble(dr["mspecial"].ToString()), 2)).ToString();
                lblmtotal.Text = (Math.Round(Convert.ToDouble(dr["msubtotal1"].ToString()), 2)).ToString();
                lblmsta.Text = (Math.Round(Convert.ToDouble(dr["mstatutory"].ToString()), 2)).ToString();
                lblmesi.Text = (Math.Round(Convert.ToDouble(dr["mesi"].ToString()), 2)).ToString();
                lblmpf.Text = (Math.Round(Convert.ToDouble(dr["mpf"].ToString()), 2)).ToString();
                lblmtotal1.Text = (Math.Round(Convert.ToDouble(dr["msubtotal2"].ToString()), 2)).ToString();
                lblmctc.Text = (Math.Round(Convert.ToDouble(dr["mtotCTC"].ToString()), 2)).ToString();
                lblybasic.Text = (Math.Round(Convert.ToDouble(dr["ybasic"].ToString()), 2)).ToString();
                lblyhra.Text = (Math.Round(Convert.ToDouble(dr["yhra"].ToString()), 2)).ToString();
                lblyconvey.Text = (Math.Round(Convert.ToDouble(dr["yconveyance"].ToString()), 2)).ToString();
                lblycha.Text = (Math.Round(Convert.ToDouble(dr["ycha"].ToString()), 2)).ToString();
                lblyspecial.Text = (Math.Round(Convert.ToDouble(dr["yspecial"].ToString()), 2)).ToString();
                lblytotal.Text = (Math.Round(Convert.ToDouble(dr["ysubtotal1"].ToString()), 2)).ToString();
                lblysta.Text = (Math.Round(Convert.ToDouble(dr["ystatutory"].ToString()), 2)).ToString();
                lblyesi.Text = (Math.Round(Convert.ToDouble(dr["yesi"].ToString()), 2)).ToString();
                lblypf.Text = (Math.Round(Convert.ToDouble(dr["ypf"].ToString()), 2)).ToString();
                lblytotal1.Text = (Math.Round(Convert.ToDouble(dr["ysubtotal2"].ToString()), 2)).ToString();
                lblyctc.Text = (Math.Round(Convert.ToDouble(dr["ytotCTC"].ToString()), 2)).ToString();
                lblappoint.Text = dr["apoint_date"].ToString();
                lblsuperior.Text = dr["superior"].ToString();
                lblcity.Text = dr["city"].ToString();
                lblstate.Text = dr["state"].ToString();
                lblstate3.Text = dr["state"].ToString();
            }
            conn.Close();

            SqlCommand cmd1 = new SqlCommand("Select * from shifts where ShiftId='" + Session["shift"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr1 = cmd1.ExecuteReader();
            if(dr1.Read())
            {
                //lblstart.Text = dr1["BeginTime"].ToString();
               // lblend.Text = dr1["EndTime"].ToString();
            }

            conn.Close();
        }

        protected void pdf_Click(object sender, EventArgs e)
        {
            
           // Image1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Appointment Letter.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //string imagePath = Server.MapPath("sign board.jpg") + "";
            //for (int i = 0; i < 1; i++)
            //{
            //    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            //    image.Alignment = Element.ALIGN_TOP;

            //    image.SetAbsolutePosition(40, 700);

            //    image.ScaleToFit(500f, 400f);
            //    image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            //    pdfDoc.Add(image);
                htmlparser.Parse(sr);
           // }
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
    }
}