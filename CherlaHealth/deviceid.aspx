﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="deviceid.aspx.cs" Inherits="CherlaHealth.deviceid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Remove DeviceId</h2>
            <hr />
        
    <div class="box-body">
               <div class="col-md-12">
                <div class="col-md-6" >
                    <asp:TextBox ID="TextBox1" AutoPostBack="True" runat="server"  OnTextChanged="TextBox1_TextChanged" class="form-control" 
                         placeholder="Search By Employee Name or Designation or Mobile No........"></asp:TextBox>

                </div>
            </div>
        </div>
          <div class="col-md-12" style="padding-top: 15px">
         <asp:gridview id="GridView1" class="table table-bordered table-striped table-condensed cf"
             autogeneratecolumns="False" datakeynames="EmployeeId"
             runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
                        <Columns>
                            
                            
                            <asp:BoundField DataField="EmployeeID1" Visible="false" />
                            <asp:TemplateField HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" HeaderText="Employee Photo">
                                <ItemTemplate>
                                    <asp:Image ID="Image1" class="img-circle" Width="80px" Height="80px" ImageUrl='<%# "profile.ashx?EmployeeID1=" + Eval("EmployeeID1")%>'
                                        runat="server" />
                                </ItemTemplate>

<HeaderStyle BackColor="#12B5DA" ForeColor="White"></HeaderStyle>
                            </asp:TemplateField>
                           <asp:BoundField DataField="EmployeName" HeaderText="Employee Name" HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" >
<HeaderStyle BackColor="#12B5DA" ForeColor="White"></HeaderStyle>
                            </asp:BoundField>
                            <asp:TemplateField  HeaderStyle-BackColor="#12b5da" HeaderStyle-ForeColor="White" HeaderText="Remove DeviceID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" class="glyphicon glyphicon-pencil" ForeColor="Green" OnClick="lnkEdit_Click" HeaderStyle-BackColor="#12b5da"
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            </Columns>
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
                    </asp:gridview>
                <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                       
       </div>
         </div>
    </div>

</asp:Content>
