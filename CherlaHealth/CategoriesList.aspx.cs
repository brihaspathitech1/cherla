﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class CategoriesList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindStudy();
            if (!IsPostBack)
            {
                BindStudy();
            }
        }
        private void BindStudy()
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Categories", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {

                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }

        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn33 = new SqlConnection(connstrg);
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lbldeleteid = (Label)row.FindControl("CategoryId");
            conn33.Open();
            SqlCommand cmd3 = new SqlCommand("delete FROM Categories where CategoryId='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'", conn33);
            cmd3.ExecuteNonQuery();
            conn33.Close();
            BindStudy();

        }
        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            txtstor_address.Text = gRow.Cells[0].Text.Replace("&nbsp;", "");
            txtcity.Text = gRow.Cells[1].Text.Replace("&nbsp;", "");
            txtstate.SelectedItem.Text = gRow.Cells[2].Text.Replace("&nbsp;", "");
            txtstate33.Text = gRow.Cells[3].Text.Replace("&nbsp;", "");
            txtstor_address1.Text = gRow.Cells[4].Text.Replace("&nbsp;", "");
            txtcity1.Text = gRow.Cells[5].Text.Replace("&nbsp;", "");
            txtstate1.Text = gRow.Cells[6].Text.Replace("&nbsp;", "");
            txtstor_address2.Text = gRow.Cells[7].Text.Replace("&nbsp;", "");
            txtcity2.Text = gRow.Cells[8].Text.Replace("&nbsp;", "");
            txtstate2.Text = gRow.Cells[9].Text.Replace("&nbsp;", "");
            txtstor_address01.Text = gRow.Cells[10].Text.Replace("&nbsp;", "");
            txtcity02.Text = gRow.Cells[11].Text.Replace("&nbsp;", "");
            txtstate03.Text = gRow.Cells[12].Text.Replace("&nbsp;", "");
            txtstor_address11.Text = gRow.Cells[13].Text.Replace("&nbsp;", "");
            txtstor_address111.Text = gRow.Cells[14].Text.Replace("&nbsp;", "");
            txtcity11.Text = gRow.Cells[15].Text.Replace("&nbsp;", "");
            txtstate11.Text = gRow.Cells[16].Text.Replace("&nbsp;", "");
            txtstor_address21.Text = gRow.Cells[17].Text.Replace("&nbsp;", "");
            txtcity21.Text = gRow.Cells[18].Text.Replace("&nbsp;", "");

            this.ModalPopupExtender2.Show();
        }
        protected void btnModity_Click(object sender, EventArgs e)
        {   
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            string CategoryId = lblstor_id.Text;
            conn44.Open();
            
            SqlCommand cmd = new SqlCommand("update Categories set CategoryName='" + txtstor_address.Text + "', CategorySName='" + txtcity.Text + "', OTFormula='" + txtstate.SelectedItem.Text + "',MinOT='" + txtstate33.Text + "', GraceTimeForLateComing='" + txtstor_address1.Text + "', GracetimeForEarlyGoing='" + txtcity1.Text + "',IsWeeklyOff1='" + txtstate1.Text + "', WeeklyOff1Day='" + txtstor_address2.Text + "', IsWeeklyOff2='" + txtcity2.Text + "',WeeklyOff2Day='" + txtstate2.Text + "', WeeklyOff2days='" + txtstor_address01.Text + "', IsCHalfForLessD='" + txtcity02.Text + "',CHalfForLessDMins='" + txtstate03.Text + "', IsCAbsentDisLessThan='" + txtstor_address11.Text + "', CAbsentDisLessThanMins='" + txtstor_address111.Text + "',IsMarkHalfDay_LateBy='" + txtcity11.Text + "', MarkHalfDay_LateByMins='" + txtstate11.Text + "', IsMarkHalfDay_EarlyGoing='" + txtstor_address21.Text + "',MarkHalfDay_EarlyGoing='" + txtcity21.Text + "' where CategoryId=" + lblstor_id.Text, conn44);
            cmd.ExecuteNonQuery();
            conn44.Close();
            lblmsg.Text = "Data Updated...";
            BindStudy();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Categories where CategoryName like '%'+@date1+'%'", conn44);
            cmd.Parameters.Add("@date1", SqlDbType.VarChar).Value = TextBox1.Text.Trim();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
            //BindStudy();
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompany(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
            SqlDataAdapter adp = new SqlDataAdapter(" SELECT CategoryName FROM Categories where CategoryName like'" + prefixText + "%'", con);


            DataTable dt = new DataTable();
            adp.Fill(dt);
            List<string> Mobilenumber = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Mobilenumber.Add(dt.Rows[i]["CategoryName"].ToString());
            }
            return Mobilenumber;
        }
    }
}