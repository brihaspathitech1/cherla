﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Attendance.aspx.cs" Inherits="CherlaHealth.Attendance" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Attendance Entry</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-lg-12">
       
            <div class="col-lg-3">
                   Date
                <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="TextBox1" runat="server" />
            </div>
            <div class="col-lg-4" style="padding-top:18px">
                <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Search" OnClick="Button1_Click" />
                <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
            </div>
        </div>
        <br />
            <div class="clearfix"></div>
        <div class="col-lg-12" style="padding-top:10px">
            <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label>
            <section id="no-more-tables">
                <div class=" table-responsive overflow" style="height:500px; overflow-y:scroll">
                    <asp:GridView ID="GridView1" AutoGenerateColumns="false" DataKeyNames="AttendanceLogsID" HeaderStyle-HorizontalAlign="Center" class="table table-bordered table-striped table-condensed" runat="server">
                        <HeaderStyle HorizontalAlign="Right" />

                        <Columns>
                            <asp:TemplateField HeaderText="Employee Photo" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:Image ID="Image1" class="img-circle" Width="80" Height="80" ImageUrl='<%# "~/wb5.ashx?ImID=" + Eval("EmployeeID")%>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="EmployeeName" HeaderText="Name" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />



                            <asp:BoundField DataField="EmployeeID" HeaderText="Employee Id" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />

                            <asp:BoundField DataField="InTime" HeaderText="In Time" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />

                            <asp:BoundField DataField="OutTime" HeaderText="Out Time" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Duration" HeaderText="Duration" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EarlyComes" ControlStyle-ForeColor="Green" HeaderText="Early Login" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="LateComes" HeaderText="Late Login" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />

                            <asp:BoundField DataField="LateGoes" HeaderText="Early Logout" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="EarlyGoes" HeaderText="Late Logout" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />

                            <asp:BoundField DataField="OT" HeaderText="OT" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />

                            <asp:BoundField DataField="IsAbsent" HeaderText="Status" HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White" />
                            <asp:TemplateField HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" class="glyphicon glyphicon-pencil" OnClick="Click1" runat="server"></asp:LinkButton>


                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#20a9f4" HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:LinkButton ID="exit" Text="exit time" OnClick="exit_Click" runat="server"></asp:LinkButton>


                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:Button ID="Button5" runat="server" Style="display: none" />
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button5" PopupControlID="Panel1"
                    CancelControlID="Button3" BackgroundCssClass="tableBackground">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" Width="400px" BackColor="White" Style="display: none; border: 1px solid #ceceec; padding-bottom: 20px">

                    <div class="col-md-12">

                        <h3 class="text-center">Attendance Entry</h3>
                        <hr />

                        <div class="col-md-12">

                            <div class="form-group">
                            <asp:TextBox ID="TextBox2" class="form-control" runat="server" Text="" Style="padding-top: 13px; width: 100%"></asp:TextBox>
                            </div>
                        </div>        
                     <div class="col-md-12">

                            <div class="form-group">
                        <asp:Button ID="Button2" runat="server" class="btn btn-success" OnClick="buttn2" Text="Submit"></asp:Button>
                        <%-- <asp:Button ID="Button4" runat="server"  Text="Not "></asp:Button>--%>
                        <asp:Button ID="Button3" runat="server" class="btn btn-danger" Text="Cancel"></asp:Button>

                    </div>


                         </div>



                    <%--  <script type="text/javascript" src="./jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="../js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
        <script type="text/javascript">
            $('.form_datetime').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            });
            $('.form_date').datetimepicker({
                language: 'fr',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0
            });
            $('.form_time').datetimepicker({
                language: 'fr',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 1,
                minView: 0,
                maxView: 1,
                forceParse: 0
            });
</script>--%>
                </asp:Panel>
            </section>
        </div>
            </div>
        </div>
</asp:Content>
