﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="docagrmnt.aspx.cs" Inherits="CherlaHealth.docagrmnt" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">DOCTOR AGREEMENT</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                          Date<span style="color:red">*</span> <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>
                         <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="TextBox1" runat="server" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="TextBox1" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                         </div>
                </div>
             <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="update" runat="server"><ContentTemplate>
                  To<span style="color:red">*</span><asp:DropDownList ID="dropcandid" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="dropcandid" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                 </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                  Dear<span style="color:red">*</span> <asp:DropDownList ID="dropemp" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropemp" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                 </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
             
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                         Appointment will commence from <span style="color:red">*</span> <asp:TextBox ID="TextBox2" CssClass="form-control" runat="server"></asp:TextBox>
                             <ajaxToolkit:CalendarExtender ID="CalendarExtender3" TargetControlID="TextBox2" runat="server" />
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="TextBox2" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="clearfix"></div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                         Timings <span style="color:red">*</span> <asp:TextBox ID="TextBox3" CssClass="form-control" placeholder="eg-10:00 am " runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="TextBox3" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
                       Current Work Location <span style="color:red">*</span> <asp:TextBox ID="TextBox4" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ControlToValidate="TextBox4" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
               
            <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel6" runat="server"><ContentTemplate>
                       MCI Number<span style="color:red">*</span> <asp:TextBox ID="TextBox5" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required" ControlToValidate="TextBox5" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
             <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel7" runat="server"><ContentTemplate>
                       Specialization <asp:TextBox ID="TextBox6" CssClass="form-control" runat="server"></asp:TextBox>
                         
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
             <div class="clearfix"></div>
             <div class="col-md-3">
                     <div class="form-group">
                         <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
                       Qualification<span style="color:red">*</span> <asp:TextBox ID="TextBox7" CssClass="form-control" runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required" ControlToValidate="TextBox7" ForeColor="Red"></asp:RequiredFieldValidator>
                             </ContentTemplate></asp:UpdatePanel>
                     </div>
                 </div>
                 
           
            <div class="col-md-3">
                     <div class="form-group"><asp:UpdatePanel ID="UpdatePanel9" runat="server"><ContentTemplate>
                         Place <span style="color:red">*</span> <asp:DropDownList ID="dropaddress" CssClass="form-control" runat="server">
                              <asp:ListItem></asp:ListItem>
                             <asp:ListItem>Hyderabad</asp:ListItem>
                             <asp:ListItem>Balkampet,Hyderabad</asp:ListItem>
                              <asp:ListItem>Yousufguda,Hyderabad</asp:ListItem>
                              <asp:ListItem>Gachibowli,Hyderabad</asp:ListItem>
                             <asp:ListItem>Bandlaguda,Hyderabad</asp:ListItem>
                                                                  </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required" ControlToValidate="dropaddress" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                         </ContentTemplate></asp:UpdatePanel>
                         </div>
                 </div>
            
           
            <div class="clearfix"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                </div>
            </div>
            </div>
        </div>

</asp:Content>

