﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class EmpPayslip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                binding();

            }

        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedValue == "1")
            {
                employeee();
            }
            else
            {
                employeeewithpf();
            }

        }


        //without pf
        private void employeee()
        {


            string strcon = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection connection = new SqlConnection(strcon);
            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT EmployeeId,EmployeName FROM Employees where PFStatus='NPF' and status='1'", connection);
            SqlDataReader dr = cmd.ExecuteReader();
            Employee.DataSource = dr;
            Employee.Items.Clear();

            Employee.DataTextField = "EmployeName";
            Employee.DataValueField = "EmployeeId";
            Employee.DataBind();
            connection.Close();


            Employee.Items.Insert(0, new ListItem("", "0"));





        }
        //with pf
        private void employeeewithpf()
        {


            string strcon = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection connection = new SqlConnection(strcon);
            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT EmployeeId,EmployeName FROM Employees where PFStatus='PF' and status='1'", connection);
            SqlDataReader dr = cmd.ExecuteReader();
            Employee.DataSource = dr;
            Employee.Items.Clear();

            Employee.DataTextField = "EmployeName";
            Employee.DataValueField = "EmployeeId";
            Employee.DataBind();
            connection.Close();


            Employee.Items.Insert(0, new ListItem("", "0"));





        }
        private void binding()
        {
            string today = DateTime.Now.AddDays(DateTime.Now.Day + 1 - (DateTime.Now.Day * 2)).ToString("yyyy - MM - dd");
            //string today = DateTime.Now.ToString("yyyy-MM-dd");
            //Date.Text = today;
            //DateTime oDate = Convert.ToDateTime(Date.Text);

            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;

            SqlConnection conn44 = new SqlConnection(connstrg);
            conn44.Open();
            //SqlCommand cmd = new SqlCommand("select  Id,Payslip,Name,PFNo,Basic,HRA,Total,GrossSalary,Year,CA,PF,ESI,MobileBill,Month,TTotal,CPF,CESI,Admin,convert(Bigint ,Present) +(convert (float, NOP)) AS SUM,(convert(bigint,PF)+isnull (convert(bigint,MobileBill),0)+convert(bigint,ESI)) as sum1,convert(bigint,CPF)+convert(bigint,CESI)+convert(bigint,Admin) as company from  Payslip where date='" + today + "' and date is not null ORDER BY Id asc", conn44);
            SqlCommand cmd1 = new SqlCommand("select Id,Payslip,Name,PFNo,Basic,HRA,Total,GrossSalary,Year,CA,PF,ESI,MobileBill,Month,TTotal,CPF,CESI,Admin,convert(Bigint ,Present) +(convert (float, NOP)) AS SUM,(convert(bigint,PF)+isnull (convert(float,MobileBill),0)+convert(float,ESI)) as sum1,convert(bigint,CPF)+convert(bigint,CESI)+convert(bigint,Admin) as company from  Payslip where date='" + today+ "' and PFStatus='PF' ORDER BY Id asc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        private void bindingindate()
        {
            if (Employee.SelectedItem.Text == ""&& RadioButtonList1.SelectedValue == "2" && Date.Text!="")
            {
                string connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection conn441 = new SqlConnection(connstrg1);
                conn441.Open();
                SqlCommand cmd1 = new SqlCommand("select Id,Payslip,Name,PFNo,Basic,HRA,Total,GrossSalary,Year,CA,PF,ESI,MobileBill,Month,TTotal,CPF,CESI,Admin,convert(Bigint ,Present) +(convert (float, NOP)) AS SUM,(convert(bigint,PF)+isnull (convert(float,MobileBill),0)+convert(float,ESI)) as sum1,convert(bigint,CPF)+convert(bigint,CESI)+convert(bigint,Admin) as company from  Payslip where date='" + Date.Text.Trim() + "' and PFStatus='PF' ORDER BY Id asc", conn441);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataSet ds1 = new DataSet();
                da1.Fill(ds1);

                SqlDataReader reader1 = cmd1.ExecuteReader();
                if (reader1.Read())
                {
                    label1.Text = reader1["Month"].ToString();
                    label2.Text = reader1["Year"].ToString();
                }
                reader1.Close();

                if (ds1.Tables[0].Rows.Count > 0)
                {

                    GridView1.DataSource = ds1;
                    GridView1.DataBind();

                }
                else
                {
                    ds1.Tables[0].Rows.Add(ds1.Tables[0].NewRow());
                    GridView1.DataSource = ds1;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
                conn441.Close();
            }
            else if(Employee.SelectedItem.Text == ""&& RadioButtonList1.SelectedValue=="1" && Date.Text!= "")
            {
                string connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection conn441 = new SqlConnection(connstrg1);
                conn441.Open();
                SqlCommand cmd1 = new SqlCommand("select Id,Payslip,Name,PFNo,Basic,HRA,Total,GrossSalary,Year,CA,PF,ESI,MobileBill,Month,TTotal,CPF,CESI,Admin,convert(Bigint ,Present) +(convert (float, NOP)) AS SUM,(convert(bigint,PF)+isnull (convert(float,MobileBill),0)+convert(float,ESI)) as sum1,convert(bigint,CPF)+convert(bigint,CESI)+convert(bigint,Admin) as company from  Payslip where date='" + Date.Text.Trim() + "' and PFStatus='NPF' ORDER BY Id asc", conn441);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataSet ds1 = new DataSet();
                da1.Fill(ds1);

                SqlDataReader reader1 = cmd1.ExecuteReader();
                if (reader1.Read())
                {
                    label1.Text = reader1["Month"].ToString();
                    label2.Text = reader1["Year"].ToString();
                }
                reader1.Close();

                if (ds1.Tables[0].Rows.Count > 0)
                {

                    GridView1.DataSource = ds1;
                    GridView1.DataBind();

                }
                else
                {
                    ds1.Tables[0].Rows.Add(ds1.Tables[0].NewRow());
                    GridView1.DataSource = ds1;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
                conn441.Close();
            }
            //
            else
            {
                string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;

                SqlConnection conn44 = new SqlConnection(connstrg);
                conn44.Open();
                SqlCommand cmd = new SqlCommand("select  Id,Payslip,Name,PFNo,Basic,HRA,Total,GrossSalary,Year,CA,PF,ESI,MobileBill,Month,TTotal,CPF,CESI,Admin,convert(Bigint ,Present) +(convert (float, NOP)) AS SUM,(convert(bigint,PF)+isnull (convert(float,MobileBill),0)+convert(float,ESI)) as sum1,convert(bigint,CPF)+convert(bigint,CESI)+convert(bigint,Admin) as company from  Payslip where date='" + Date.Text.Trim() + "' AND Name='" + Employee.SelectedItem.Text + "'  ORDER BY Id asc", conn44);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);


                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    label1.Text = reader["Month"].ToString();
                    label2.Text = reader["Year"].ToString();
                }
                reader.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {

                    GridView1.DataSource = ds;
                    GridView1.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
                conn44.Close();
            }
        }
        protected void SSS(object sender, EventArgs e)
        {
            int Id = int.Parse((sender as LinkButton).CommandArgument);


            Session["Payslip"] = Id;
            Response.Redirect("~/PayReports.aspx");

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bindingindate();

        }

        //protected void Edit_click(object sender, EventArgs e)
        //{
        //    Button details = sender as Button;
        //    GridViewRow grow = (GridViewRow)details.NamingContainer;
        //    string name = grow.Cells[0].Text;

        //    string id = details.CommandArgument;
        //    Response.Redirect("Payslipedit.aspx?Name=" + name.ToString() + "&id=" + id.ToString());
        //}
    }
}