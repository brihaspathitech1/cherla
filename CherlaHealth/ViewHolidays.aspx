﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewHolidays.aspx.cs" Inherits="CherlaHealth.ViewHolidays" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Holidays List</h2>
            <hr />
        </div>
       
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    Search By Year <asp:DropDownList ID="dropyear" CssClass="form-control" OnSelectedIndexChanged="Year_Change" AutoPostBack="true" runat="server">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                        <asp:ListItem>2022</asp:ListItem>
                        <asp:ListItem>2023</asp:ListItem>
                        <asp:ListItem>2024</asp:ListItem>
                        <asp:ListItem>2025</asp:ListItem>
                                   </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3" style="float:right">
                <div class="form-group">
                    <asp:Button ID="btnholiday" CssClass="btn btn-success" runat="server" Text="Add Holiday" OnClick="Holiday_Click" />
                    </div>
                </div>
            <div class="col-md-3" style="float:right">
                <div class="form-group">
            <button style="margin-bottom:10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">  
                        <i class="fa fa-plus-circle"></i> Import Excel  
                    </button> </div>
                </div>
            <div class="modal fade" id="myModal">  
                        <div class="modal-dialog">  
                            <div class="modal-content">  
                                <div class="modal-header">  
                                    <h4 class="modal-title">Import Excel File</h4>  
                                    <button type="button" class="close" data-dismiss="modal">×</button>  
                                </div>  
                                <div class="modal-body">  
                                    <div class="row">  
                                        <div class="col-md-12">  
                                            <div class="form-group">  
                                                <label>Choose excel file</label>  
                                                <div class="input-group">  
                                                    <div class="custom-file">  
                                                        <asp:FileUpload ID="FileUpload1" CssClass="custom-file-input" runat="server" />  
                                                        <label class="custom-file-label"></label>  
                                                    </div>  
                                                    <label id="filename"></label>  
                                                    <div class="input-group-append">  
                                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-success" Text="Upload" OnClick="btnUpload_Click" />  
                                                    </div>  
                                                </div>  
                                                <asp:Label ID="lblMessage" runat="server"></asp:Label>  
                                            </div>  
                                        </div>  
                                    </div>  
                                </div>  
                                <div class="modal-footer">  
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
                                </div>  
                            </div>  
                        </div>  
                    </div>  
            <div class="col-md-12">
                <asp:UpdatePanel ID="update" runat="server"><ContentTemplate>
            <asp:GridView ID="gvHolidays" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None" runat="server">
                <Columns>
                    <asp:BoundField DataField="HolidayDate" HeaderText="Holiday Date" DataFormatString="{0:dd-MM-yyyy}" />
                    <asp:BoundField DataField="HolidayName" HeaderText="Holiday Name" />
                    <asp:BoundField DataField="HolidayType" HeaderText="Type" />
                    <asp:BoundField DataField="Dercripation" HeaderText="Description" />
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:Button ID="btnedit" CssClass="btn btn-primary btn-xs" runat="server" Text="Edit" CommandArgument='<%# Eval("HolidayId") %>' OnClick="Edit_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:Button ID="btndelete" CssClass="btn btn-danger btn-xs" runat="server" Text="Delete" CommandArgument='<%# Eval("HolidayId") %>' OnClick="Delete" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                    </ContentTemplate></asp:UpdatePanel>
                </div>

            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1" CancelControlID="btnclose" TargetControlID="HiddenField1"></ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" BackColor="White" Height="250px" Width="800px" Style="display: none;" BorderColor="#0000ff">
                <h4 style="text-align:center">Edit Holiday Details</h4>
                <hr />
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                        Holiday Date <asp:TextBox ID="txtdate" CssClass="form-control" runat="server"></asp:TextBox>
                         <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtdate" runat="server"></cc1:CalendarExtender>
                            </ContentTemplate></asp:UpdatePanel>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                        Holiday Name <asp:TextBox ID="txtname" CssClass="form-control" runat="server"></asp:TextBox>
                            </ContentTemplate></asp:UpdatePanel>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                        Type <asp:DropDownList ID="droptype" CssClass="form-control" runat="server">
                             <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="1">National Holiday</asp:ListItem>
                    <asp:ListItem Value="2">Common Festival Holiday</asp:ListItem>
                    <asp:ListItem Value="3">Discretionary</asp:ListItem>
                             </asp:DropDownList>
                            </ContentTemplate></asp:UpdatePanel>
                    </div>
                </div>
                  <div class="col-md-3">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                        Description <asp:TextBox ID="txtdescription" CssClass="form-control" runat="server"></asp:TextBox>
                            </ContentTemplate></asp:UpdatePanel>
                        </div>
                      </div>
                 <div class="col-md-3">
                    <div class="form-group">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="Update" />
                        </div>
                     </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:Button ID="btnclose" CssClass="btn btn-danger" runat="server" Text="Cancel" />
                        </div>
                    </div>
            </asp:Panel>
            <asp:Label ID="lblid" Visible="false" runat="server" Text="Label"></asp:Label>
            </div>
        </div>

    <script type="text/javascript">  
        $(document).ready(function () {  
            $("#GridView1").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();  
        });  
</script>  
</asp:Content>
