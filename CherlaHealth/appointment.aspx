﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="appointment.aspx.cs" Inherits="CherlaHealth.appointment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Appointment Letter</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel11" runat="server"><ContentTemplate>
                  Salutation<span style="color:red">*</span> <asp:DropDownList ID="salut"  CssClass="form-control" runat="server">
                      <asp:ListItem>Mr.</asp:ListItem>
                      <asp:ListItem>Mrs.</asp:ListItem>
                      <asp:ListItem>Ms.</asp:ListItem>      </asp:DropDownList>
                       </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    Select Candidate <span style="color:red">*</span> <asp:DropDownList ID="dropcandidate" CssClass="form-control" runat="server" OnSelectedIndexChanged="Candidate_Change" AutoPostBack="true"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="dropcandidate" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </ContentTemplate></asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                    Address<span style="color:red">*</span>  <asp:TextBox ID="txtaddress" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtaddress"></asp:RequiredFieldValidator>
                        </ContentTemplate></asp:UpdatePanel>
                </div>
                </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
                    Designation<span style="color:red">*</span>  <asp:TextBox ID="txtdesignation" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtdesignation"></asp:RequiredFieldValidator>
                    </ContentTemplate></asp:UpdatePanel>
                </div>
                </div><div class="clearfix"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
                    Company Address<span style="color:red">*</span>  <asp:DropDownList ID="dropaddress" CssClass="form-control" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="dropaddress" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </ContentTemplate></asp:UpdatePanel>
                </div>
                </div>
            
             <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
                    City<span style="color:red">*</span>  <asp:TextBox ID="txtcity" CssClass="form-control" runat="server"></asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtcity"></asp:RequiredFieldValidator>     
                        </ContentTemplate></asp:UpdatePanel>
                </div>
                 </div>
             <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server"><ContentTemplate>
                       State<span style="color:red">*</span>  <asp:TextBox ID="txtstate" CssClass="form-control" runat="server"></asp:TextBox>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtstate"></asp:RequiredFieldValidator>     
                        </ContentTemplate></asp:UpdatePanel>
                </div>
                 </div>
              <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server"><ContentTemplate>
                    Joining Date<span style="color:red">*</span>  <asp:TextBox ID="txtjoin" CssClass="form-control" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtjoin"></asp:RequiredFieldValidator>     
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtjoin" />
                        </ContentTemplate></asp:UpdatePanel>
                </div>
                  </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
                    Salary Per Annum<span style="color:red">*</span>  <asp:TextBox ID="txtsal" CssClass="form-control" runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtsal"></asp:RequiredFieldValidator>     
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtsal" FilterType="Numbers" />
                        </ContentTemplate></asp:UpdatePanel>
                </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server"><ContentTemplate>
                    Supervisor  <span style="color:red">*</span> <asp:DropDownList ID="dropemployee" CssClass="form-control" runat="server"></asp:DropDownList>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="dropemployee" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>     
      </ContentTemplate></asp:UpdatePanel>
                </div>
                </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server"><ContentTemplate>
                    Shift <span style="color:red">*</span> <asp:DropDownList ID="dropshift" CssClass="form-control" runat="server"></asp:DropDownList>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="dropshift" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>     
                        </ContentTemplate></asp:UpdatePanel>
                    </div>
                </div>
             <div class="col-md-3" style="padding-top:18px">
                <div class="form-group">
                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                    </div>
                 </div>
            <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
            </div>
        </div>
</asp:Content>
