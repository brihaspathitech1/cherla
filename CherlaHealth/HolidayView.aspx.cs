﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.DataAccessLayer;

namespace CherlaHealth
{
    public partial class HolidayView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            BindStudy();
            if (!IsPostBack)
            {
                BindStudy();
            }
        }
        private void BindStudy()
        {
            DataSet ds=DataQueries.SelectCommon("select HolidayId,HolidayName,convert(varchar(10),HolidayDate,101) AS DATE,Week,HolidayType,Dercripation from  Holidays");
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
           
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lbldeleteid = (Label)row.FindControl("HolidayId");
            DataQueries.DeleteCommon("delete FROM Holidays where HolidayId='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'");
            BindStudy();
        }
        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            txtstor_address.Text = gRow.Cells[0].Text.Replace("&nbsp;", "");
            txtcity.Text = gRow.Cells[1].Text.Replace("&nbsp;", "");
            txtstate.Text = gRow.Cells[2].Text.Replace("&nbsp;", "");
            txtstate1.Text = gRow.Cells[3].Text.Replace("&nbsp;", "");

            this.ModalPopupExtender2.Show();
        }
        protected void btnModity_Click(object sender, EventArgs e)
        {
            string DeptId = lblstor_id.Text;
            DataQueries.UpdateCommon("update Holidays set HolidayName='" + txtstor_address.Text + "', HolidayDate='" + txtcity.Text + "', HolidayType='" + txtstate.Text + "',Dercripation='" + txtstate1.Text + "' where HolidayId=" + lblstor_id.Text);
            
            lblmsg.Text = "Data Updated...";
            BindStudy();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            DataSet ds= DataQueries.SelectCommon("select HolidayId,HolidayName,convert(varchar(10),HolidayDate,101) AS DATE,Week,HolidayType,Dercripation from  Holidays where HolidayName='"+TextBox1.Text+"'   ");
          
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
           
        }
    }
}
