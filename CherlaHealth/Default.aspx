﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CherlaHealth.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
     <meta name="author" content="Brihaspathi">
    <meta name="keyword" content="">

    <title>Cherla Health</title>
  
 
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="vendor/iCheck/all.css" /> 

        <!-- Icons -->
        <link href="resources/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="resources/icons/themify-icons/themify-icons.css" rel="stylesheet">

        <!-- Theme style -->
        <link rel="stylesheet" href="resources/css/main-style.min.css">
        <link rel="stylesheet" href="resources/css/skins/all-skins.min.css">

        <link rel="stylesheet" href="resources/css/demo.css">

    
</head>
<body class="skin-yellow login-page" style="background: url(health-care.jpg) center center / cover !important;">
    <div class="box-login">
            <div class="box-login-body">
	  	
		     <form id="form1" class="form-login" runat="server">
		        <h2 class="form-login-heading">sign in</h2>
		        <div class="login-wrap">
                <asp:TextBox ID="username" runat="server" class="form-control" placeholder="user name" >
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                  ErrorMessage="Please Enter User Name" ControlToValidate="username" 
                                                  style="color: #FF0000"></asp:RequiredFieldValidator>
		            
		            <br>
                    <asp:TextBox ID="password" runat="server" class="form-control" TextMode="Password"
                        placeholder="password"  ></asp:TextBox>
		               <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                  ErrorMessage="Please Enter Password" ControlToValidate="password" 
                                                  style="color: #FF0000"></asp:RequiredFieldValidator>
		            <label class="checkbox">
		                <span class="pull-right">
		                  <%--  <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>--%>
		
		                </span>
		            </label>
                    <asp:Button ID="Button2" class="btn btn-success btn-block" runat="server" 
                        Text="LogIn" onclick="Button2_Click" />
                    
		            
		            
		         <%--   <div class="login-social-link centered">
		            <p>or you can sign in via your social network</p>
      <%--                  <asp:Button ID="Button3" runat="server" Text="Facebook"  PostBackUrl = "/https://www.facebook.com/brihaspathi"/ >
		                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
		                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>--%>
                         <%--<a data-toggle="modal"  lass="btn btn-facebook" href="https://www.facebook.com/brihaspathi"> <i class="fa fa-facebook"></i> Facebook</a>
                            <a data-toggle="modal"  lass="btn btn-facebook" href="https://twitter.com/Brihaspathitec"> <i class="fa fa-facebook"></i> Twitter</a>
		            </div>--%>
		            
		
		        </div>
		
		          <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Enter your e-mail address below to reset your password.</p>
                                  <asp:TextBox ID="TextBox1" placeholder="Email" runat="server" class="form-control placeholder-no-fix"></asp:TextBox>
<asp:Label ID="Label1" runat="server"  style="color: #FF0000" ></asp:Label>
		
		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                  <asp:Button class="btn btn-theme" ID="Button1" runat="server"  OnClick="Button1_Click" Text="Submit"  />
		                        
		                      </div>
		                  </div>
		              </div>
		          </div>
		          <!-- modal -->
		
		      </form>	  	
	  	
	  	</div>
	  </div>
       <script src="vendor/jQuery/jquery-2.2.3.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="resources/js/pages/jquery-icheck.js"></script>
        <script src="vendor/fastclick/fastclick.min.js"></script>
        <script src="resources/js/demo.js"></script>
    
</body>
</html>
