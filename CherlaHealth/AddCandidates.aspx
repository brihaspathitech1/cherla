﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddCandidates.aspx.cs" Inherits="CherlaHealth.AddCandidates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Add Candidate</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row mt">
                <div class=" col-lg-12 panel" style="padding: 20px">

                    <div class=" col-lg-12">


                        

                        <br />
                        <div class="col-md-12 ">
                            <div class="row">
                                <div class="col-md-3 ">
                                    <label>
                                        Full Name<span style="color:red">*</span></label>
                                    <asp:TextBox ID="candidate" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="candidate"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-3 ">
                                    <label>
                                        MailId<span style="color:red">*</span></label>
                                    <asp:TextBox ID="MailId" class="form-control" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="MailId"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="MailId" ErrorMessage="Invalid Email Format" ForeColor="Red"></asp:RegularExpressionValidator>

                                </div>
                                <div class="col-md-2">
                                    <label>
                                        Branch<span style="color:red">*</span></label>
                                    <asp:DropDownList ID="Branch" runat="server" class="form-control">
                                     <%--   <asp:ListItem>Select</asp:ListItem>
                                        <asp:ListItem>Hyderabad</asp:ListItem>--%>
                                        <%--<asp:ListItem>Vijayawada</asp:ListItem>
                                        <asp:ListItem>Kurnool</asp:ListItem>
                                        <asp:ListItem>Kakinada</asp:ListItem>
                                        <asp:ListItem>Vizag</asp:ListItem>--%>

                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Branch" Display="Dynamic" InitialValue="0"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-1" style="padding-top: 25px">
                <div class="form-group">
                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal2">+</button>
                </div>
            </div>
                                <div class="col-md-3 ">
                                    <label>
                                        Job Position<span style="color:red">*</span></label>
                                    <asp:TextBox ID="JPosition" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="JPosition"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 ">
                            <br />
                            <div class="row">
                                <div class="col-md-3" runat="server" visible="true" id="div2">
                                    <%--<label>
                    Notice Period(days)</label>--%>

                                    <label>
                                        Education</label>
                                    <asp:DropDownList ID="Education" runat="server" OnSelectedIndexChanged="Education_Change" AutoPostBack="true" class="form-control">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>MBBS</asp:ListItem>
                                        <asp:ListItem>MBBS MD</asp:ListItem>
                                        <asp:ListItem>MBBS MS</asp:ListItem>
                                        <asp:ListItem>MD DM</asp:ListItem>
                                        <asp:ListItem>D.Pharmacy</asp:ListItem>
                                        <asp:ListItem>B.Pharmacy</asp:ListItem>
                                        <asp:ListItem>B.com</asp:ListItem>
                                        <asp:ListItem>MBA</asp:ListItem>
                                        <asp:ListItem>Others</asp:ListItem>
                                      
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Education" InitialValue="0" Display="Dynamic"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>

                                    <asp:TextBox ID="Notice" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                </div>
                                <div class="col-md-3" runat="server" visible="false" id="div1">
                                    <label>Education</label>
                                    <asp:TextBox ID="txtedu" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-3 ">
                                    <label>
                                        Referred by</label>
                                    <asp:TextBox ID="Reference" class="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-3 ">
                                    <label>
                                        Permanent/Contract<span style="color:red">*</span></label>
                                    <asp:DropDownList ID="Mode_Emp" class="form-control" runat="server">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>Permanent</asp:ListItem>
                                        <asp:ListItem>Contract</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="Mode_Emp"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <%-- <asp:TextBox ID="Perminant" class="form-control" runat="server"></asp:TextBox>--%>
                                </div>
                                <div class="col-md-3 ">
                                    <label>
                                        Expected Salary per Month<span style="color:red">*</span></label>
                                    <asp:TextBox ID="Salary" class="form-control" runat="server"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                        TargetControlID="Salary" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="Salary"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 ">
                            <br />
                            <div class="row">
                                <div class="col-md-3 ">
                                    <label>
                                        Reason for Change</label>
                                    <asp:TextBox ID="Reason" class="form-control" runat="server"></asp:TextBox>
                                </div>


                                <div class="col-md-3">
                                    <label>
                                        Experience</label>
                                    <asp:DropDownList ID="Exp" runat="server" class="form-control">
                                        <asp:ListItem>--Select Experience--</asp:ListItem>
                                        <asp:ListItem>0 Years</asp:ListItem>
                                        <asp:ListItem>6 months</asp:ListItem>
                                        <asp:ListItem>< 1 Years</asp:ListItem>
                                        <asp:ListItem>1-2 Years</asp:ListItem>
                                        <asp:ListItem>2-3 Years</asp:ListItem>
                                        <asp:ListItem>> 3 Years</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-3">
                                    <label>
                                        Current Company</label>
                                    <asp:TextBox ID="Current_cmpny" class="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-3">
                                    <label>
                                        Mobile<span style="color:red">*</span></label>
                                    <asp:TextBox ID="Mobile" class="form-control" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Mobile"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 ">
                            <br />
                            <div class="row">
                                
                                <div class="col-md-3 ">
                                    <label>
                                       Present Address</label>
                                    <asp:TextBox ID="Address" class="form-control" runat="server" Visible="false"></asp:TextBox>

                                    <%--                  <input id="autocomplete" class="form-control zin"  type="text" placeholder="Address" textarea="" name="Name" onFocus="geolocate()"/>--%>
                                    <textarea id="autocomplete" class="form-control zin" type="text" placeholder="Address" name="Name" onfocus="geolocate()"></textarea>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="Address"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-3 ">
                                    <label>
                                      Permanent Address<span style="color:red">*</span></label>
                                    <asp:TextBox ID="txtaddress" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtaddress"
                                        ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                <div class="col-md-3">
                                    <label>
                                        Resume</label>
                                    <asp:FileUpload ID="Resume" runat="server" />
                               
                        </div>
                        <div class="col-md-12">
                            <br />
                            <div class="row">
                                <div class="col-md-2">
                                    <label>
                                        Interview Schedule<span style="color:red">*</span></label>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="Interview" class="form-control" runat="server" AutoPostBack="true"
                                                OnTextChanged="Interview_TextChanged"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="Interview" runat="server"></cc1:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="Interview"
                                                ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>


                                <div class=" col-md-2" style="padding-top: 30px;">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="LinkButton11" runat="server" CssClass="label label-danger" Text="10:30 AM" autopostback="true"
                                                OnClick="link1"></asp:LinkButton>
                                            &nbsp; 
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class=" col-md-2" style="padding-top: 30px;">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="LinkButton12" runat="server" CssClass="label label-danger" Text="12:30 PM" autopostback="true"
                                                OnClick="link2"></asp:LinkButton>
                                            &nbsp;
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class=" col-md-2" style="padding-top: 30px;">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="LinkButton13" runat="server" CssClass="label label-danger" Text="02:30 PM" autopostback="true"
                                                OnClick="link3"></asp:LinkButton>
                                            &nbsp;
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class=" col-md-2" style="padding-top: 35px;">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="LinkButton14" runat="server" CssClass="label label-danger" Text="05:00 PM" autopostback="true"
                                                OnClick="link4"></asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-2" style="padding-top: 10px">
                                    <h3 style="font-size: 20px !important"><span>
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <label>
                                                    Time</label>
                                                <asp:Label ID="Time" runat="server" Text="00:00"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </span>
                                    </h3>
                                </div>


                            </div>



                        </div>
                        <div class="col-md-12">
                            <br />
                            <div class="row">
                                <div class=" col-md-4">
                                    <asp:Button ID="Submit" runat="server" class="btn btn-success" Text="Submit" OnClick="Submit_Click" CausesValidation="true" />
                                </div>
                            </div>
                        </div>

                      <div class="modal fade" id="myModal2" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Branch</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:TextBox ID="txtbranch" runat="server" class="form-control" />
                            </p>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="Button1" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Branch_Click" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>


                    </div>
                </div>
            </div>
            </div>
        </div>
    
                        <script>
                            // This example displays an address form, using the autocomplete feature
                            // of the Google Places API to help users fill in the information.

                            // This example requires the Places library. Include the libraries=places
                            // parameter when you first load the API. For example:
                            // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                            var placeSearch, autocomplete;
                            var componentForm = {
                                street_number: 'short_name',
                                route: 'long_name',
                                locality: 'long_name',
                                administrative_area_level_1: 'short_name',
                                country: 'long_name',
                                postal_code: 'short_name'
                            };

                            function initAutocomplete() {
                                // Create the autocomplete object, restricting the search to geographical
                                // location types.
                                autocomplete = new google.maps.places.Autocomplete(
                                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                                   { types: ['geocode'] });

                                // When the user selects an address from the dropdown, populate the address
                                // fields in the form.
                                autocomplete.addListener('place_changed', fillInAddress);
                            }

                            function fillInAddress() {
                                // Get the place details from the autocomplete object.
                                var place = autocomplete.getPlace();

                                for (var component in componentForm) {
                                    document.getElementById(component).value = '';
                                    document.getElementById(component).disabled = false;
                                }

                                // Get each component of the address from the place details
                                // and fill the corresponding field on the form.
                                for (var i = 0; i < place.address_components.length; i++) {
                                    var addressType = place.address_components[i].types[0];
                                    if (componentForm[addressType]) {
                                        var val = place.address_components[i][componentForm[addressType]];
                                        document.getElementById(addressType).value = val;
                                    }
                                }
                            }

                            // Bias the autocomplete object to the user's geographical location,
                            // as supplied by the browser's 'navigator.geolocation' object.
                            function geolocate() {
                                if (navigator.geolocation) {
                                    navigator.geolocation.getCurrentPosition(function (position) {
                                        var geolocation = {
                                            lat: position.coords.latitude,
                                            lng: position.coords.longitude
                                        };
                                        var circle = new google.maps.Circle({
                                            center: geolocation,
                                            radius: position.coords.accuracy
                                        });
                                        autocomplete.setBounds(circle.getBounds());
                                    });
                                }
                            }
                        </script>
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAx3SkfmXNS1y1gAehn9F_gilSHQ0uZs4U&libraries=places&callback=initAutocomplete"
                            async defer></script>
</asp:Content>
