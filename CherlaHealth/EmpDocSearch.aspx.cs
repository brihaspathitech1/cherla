﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace CherlaHealth
{
    public partial class EmpDocSearch : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindEmployee();
                BinGrid();
            }
        }
        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where Status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
        }

        protected void BinGrid()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("SElect d.EmployeeId,e.EmployeName,e.Desigantion1,d.resume,d.degree1,d.degree2,d.degree3,d.degree4,d.degree5,d.MCI,d.SCI,d.MCI_Expire,d.SCI_Expire,d.others1,d.others2,d.others3,d.others4,d.others5 from Empdocuments d inner join Employees e on d.employeeId=e.EmployeeId", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                gvDocuments.DataSource = ds;
                gvDocuments.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvDocuments.DataSource = ds;
                gvDocuments.DataBind();
                int columncount = gvDocuments.Rows[0].Cells.Count;
                gvDocuments.Rows[0].Cells.Clear();
                gvDocuments.Rows[0].Cells.Add(new TableCell());
                gvDocuments.Rows[0].Cells[0].ColumnSpan = columncount;
                gvDocuments.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Resume_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Resume,RIGHT(Resume,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["Resume"].ToString();
            }
            conn.Close();
            if(filename==".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Degree1_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select degree1,RIGHT(degree1,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["degree1"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Degree2_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select degree2,RIGHT(degree2,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["degree2"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Degree3_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select degree3,RIGHT(degree3,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["degree3"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Degree4_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select degree4,RIGHT(degree4,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["degree4"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Degree5_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select degree5,RIGHT(degree5,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["degree5"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void MCI_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select MCI,RIGHT(MCI,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["MCI"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void SCI_click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select SCI,RIGHT(SCI,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["SCI"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Other1_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select others1,RIGHT(others1,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["others1"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Other2_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select others2,RIGHT(others2,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["others2"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Other3_Click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select others3,RIGHT(others3,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["others3"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Other4_click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select others4,RIGHT(others4,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["others4"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void Other5_click(object sender,EventArgs e)
        {
            string filename = string.Empty;
            string filepath = string.Empty;
            LinkButton btn = sender as LinkButton;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select others5,RIGHT(others5,4) as path from Empdocuments where EmployeeId='" + id + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                filename = dr["path"].ToString();
                filepath = dr["others5"].ToString();
            }
            conn.Close();
            if (filename == ".pdf")
            {
                Session["FileName"] = filepath;
                string url = "fileview.aspx";
                string s = "window.open('" + url + "', 'popup_window', 'width=1000,height=500,left=100,top=100,resizable=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            }
            else
            {
                Response.Write("<script>alert('Invalid Document')</script>");
            }
        }

        protected void dropemp_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("SElect d.EmployeeId,e.EmployeName,e.Desigantion1,d.resume,d.degree1,d.degree2,d.degree3,d.degree4,d.degree5,d.MCI,d.SCI,d.MCI_Expire,d.SCI_Expire,d.others1,d.others2,d.others3,d.others4,d.others5 from Empdocuments d inner join Employees e on d.employeeId=e.EmployeeId where d.EmployeeId='" + dropemp.SelectedValue + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvDocuments.DataSource = ds;
                gvDocuments.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvDocuments.DataSource = ds;
                gvDocuments.DataBind();
                int columncount = gvDocuments.Rows[0].Cells.Count;
                gvDocuments.Rows[0].Cells.Clear();
                gvDocuments.Rows[0].Cells.Add(new TableCell());
                gvDocuments.Rows[0].Cells[0].ColumnSpan = columncount;
                gvDocuments.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Update_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select MCI,SCI,MCI_Expire,SCI_Expire from Empdocuments where EmployeeId='" + lblid.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                lblmci.Text = dr["MCI"].ToString();
                lblsci.Text = dr["SCI"].ToString();
                lblmcidate.Text = dr["MCI_Expire"].ToString();
                lblscidate.Text = dr["SCI_Expire"].ToString();
            }
            conn.Close();
            ModalPopupExtender1.Show();
        }

        protected void Update(object sender,EventArgs e)
        {
            string mcipath = string.Empty;
            string scipath = string.Empty;
            if (filemci.HasFile)
            {
                string fileName1 = Path.GetFileName(filemci.PostedFile.FileName);
                string path1 = "~/documents/" + fileName1;
                filemci.PostedFile.SaveAs(Server.MapPath(path1));
                mcipath = path1;
            }
            if (filesci.HasFile)
            {
                string fileName1 = Path.GetFileName(filesci.PostedFile.FileName);
                string path1 = "~/documents/" + fileName1;
                filesci.PostedFile.SaveAs(Server.MapPath(path1));
                scipath = path1;
            }
            if(mcipath=="")
            {
                mcipath = lblmci.Text;
            }
            if(scipath=="")
            {
                scipath = lblsci.Text;
            }
            if(txtmci.Text=="")
            {
                txtmci.Text = lblmcidate.Text;
            }
            if(txtsci.Text=="")
            {
                txtsci.Text = lblscidate.Text;
            }
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Update Empdocuments set MCI='" + mcipath + "',SCI='" + scipath + "',MCI_Expire='" + txtmci.Text + "',SCI_Expire='" + txtsci.Text + "' where EmployeeId='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BinGrid();
        }
    }
}