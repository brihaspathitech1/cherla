﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Text;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Net;

namespace CherlaHealth
{
    public partial class AddCandidates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CalendarExtender1.StartDate = DateTime.Now;
            if (!IsPostBack)
            {
                LinkButton11.Visible = false;
                LinkButton12.Visible = false;
                LinkButton13.Visible = false;
                LinkButton14.Visible = false;
            }
            if(!IsPostBack)
            {
                BindBranch();
            }
        }
        protected void Interview_TextChanged(object sender, EventArgs e)
        {
            if (Interview.Text != "")
            {
                LinkButton14.Visible = true;
                LinkButton13.Visible = true;
                LinkButton12.Visible = true;
                LinkButton11.Visible = true;
            }
            else
            {
                LinkButton11.Visible = false;
                LinkButton12.Visible = false;
                LinkButton13.Visible = false;
                LinkButton14.Visible = false;
            }
            String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;



            SqlConnection con = new SqlConnection(strConnString);

            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Candidates where InterviewShedule='" + Interview.Text + "' ", con);

            cmd.CommandType = CommandType.Text;

            cmd.Connection = con;


            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (LinkButton11.Text == dr["Time"].ToString())
                {
                    LinkButton11.Enabled = false;
                    LinkButton11.Attributes.Add("style", "text-decoration: line-through;");

                }
                if (LinkButton12.Text == dr["Time"].ToString())
                {
                    LinkButton12.Enabled = false;
                    LinkButton12.Attributes.Add("style", "text-decoration: line-through;");

                }
                if (LinkButton13.Text == dr["Time"].ToString())
                {
                    LinkButton13.Enabled = false;
                    LinkButton13.Attributes.Add("style", "text-decoration: line-through;");

                }
                if (LinkButton14.Text == dr["Time"].ToString())
                {
                    LinkButton14.Enabled = false;
                    LinkButton14.Attributes.Add("style", "text-decoration: line-through;");

                }
            }
            dr.Close();
            con.Close();
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            string name1 = Request.Form["Name"];
            Address.Text = name1;
            string education = string.Empty;
            if(txtedu.Text=="")
            {
                education = Education.SelectedItem.Text;
            }
            else
            {
                education = txtedu.Text;
            }
            int length = Resume.PostedFile.ContentLength;
            byte[] imgbyte = new byte[length];
            HttpPostedFile img = Resume.PostedFile;
            img.InputStream.Read(imgbyte, 0, length);


            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg);
            string str = "insert into [Candidates] (Name,Notice,SalaryExp,InterviewShedule,Reason,Ccompany,Reference,Modeof_emp,Mobile,Address,Time,JPosition,Email,Experiance,Education,Resume,Branch,Permanent_Address) values ('" + candidate.Text + "','" + Notice.Text + "','" + Salary.Text + "','" + Interview.Text + "','" + Reason.Text + "','" + Current_cmpny.Text + "','" + Reference.Text + "','" + Mode_Emp.SelectedItem.Text + "','" + Mobile.Text + "','" + Address.Text + "','" + Time.Text + "','" + JPosition.Text + "','" + MailId.Text + "','" + Exp.SelectedItem.Text + "','" + education + "',@img1,'" + Branch.SelectedItem.Text + "','"+txtaddress.Text+"')";
            SqlCommand comm1 = new SqlCommand(str, conn1);
            comm1.Parameters.Add("@img1", SqlDbType.Image).Value = imgbyte;

            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();

            Sendmail();

            Response.Write("<script>alert('Candidate Added')</script>");
            Response.Redirect(Request.Url.AbsoluteUri);
        }

        protected void Sendmail()
        {
            string address = string.Empty;
            if(Address.Text=="")
            {
                address = txtaddress.Text;
            }
            else
            {
                address = Address.Text;
            }
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();
                    {
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Date: " + System.DateTime.Now.ToString("yyyy-MM-dd") + "</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + candidate.Text + "</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + address.ToString() + "</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Dear Mr./Ms." + candidate.Text + ", </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>This has reference to your application, indicating interest in seeking employment with Cherla Health Pvt Ltd. We thank you for the same.</p>");
                        sb.Append("<p><br></p>");
                        //sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>We would like to personally meet you for a discussion on " + Interview.Text + " at " + Time.Text + "  at the following address: Cherla Health Pvt Ltd 1st floor, Optimus Prime, Plot 124, Lumbini Avenue, Landmark: Lane right opposite to Care Hospital, Gachibowli, Hyderabad</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>We would like to personally meet you for a discussion on " + Interview.Text + " at " + Time.Text + "</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>We hope this time and venue is suitable to you. If it is not, we request you to get in touch with us indicating suitable date and time for the same. </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>To help us co-ordinate this meeting effectively, we request you to confirm to us your availability for this meeting either by email or on phone. Our email is Hr@cherlahealth.com and our telephone no. is 9959006006.  Kindly ask for Mr. Avinash Reddy Somu who will be your contact person throughout the process.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>We look forward to meeting you.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Kind Regards,</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>HR</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Cherla Health Pvt. Ltd</p>");

                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            string path = Server.MapPath("Files");
                            string filename = path + "/" + "Interview Call Letter" + ".Pdf";

                            PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();
                            htmlparser.Parse(sr);


                            /*string imagePath = Server.MapPath("topp-hyd.jpg") + "";

                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                            image.Alignment = Element.ALIGN_TOP;

                            image.SetAbsolutePosition(30, 750);

                            image.ScaleToFit(550f, 550f);
                            image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

                            pdfDoc.Add(image);*/

                            PdfContentByte content = writer.DirectContent;
                            Rectangle rectangle = new Rectangle(pdfDoc.PageSize);
                            rectangle.Left += pdfDoc.LeftMargin;
                            rectangle.Right -= pdfDoc.RightMargin;
                            rectangle.Top -= pdfDoc.TopMargin;
                            rectangle.Bottom += pdfDoc.BottomMargin;
                            content.SetColorStroke(BaseColor.BLACK);
                            //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                            content.Stroke();
                            pdfDoc.Close();



                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();

                            MailMessage mm = new MailMessage();
                            mm.From = new MailAddress("hr.cherlahealth@gmail.com", "Interview Letter");
                            mm.To.Add(MailId.Text);
                            //.CC.Add("md@brihaspathi.com");
                            //mm.CC.Add("hr@brihaspathi.com");
                           // mm.CC.Add("hra@brihaspathi.com");

                            mm.IsBodyHtml = true;
                            mm.Subject = "Interview Call Letter";
                            mm.Body = "Kindly find the below attached Interview Call Letter <br /><br />Regards<br/><br/>HR<br/><br /><b>Thank you.</b>";
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Interview Call Letter.pdf"));
                            mm.Priority = MailPriority.High;
                            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                            //smtp.UseDefaultCredentials = true;
                            smtp.Credentials = new System.Net.NetworkCredential("hr.cherlahealth@gmail.com", "HR@cherla");
                            smtp.EnableSsl = true;
                            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.Send(mm);
                        }
                    }
                }
            }
            //Working

            /*string result = "";
            WebRequest request = null;
            HttpWebResponse response = null;

            string pp = Mobile.Text;
            //string pp = Empmobile.Text;
            string pp1 = candidate.Text;




            String userid = "BTRAK";
            String passwd = "841090";

            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=TechBT&to=91" + pp + "&text=As Discussed, Your Interview has been scheduled with Cherla Health Pvt Ltd on "+Interview.Text+" at "+Time.Text+ ". Interview Venue: Diamond Hills, Lumbini Avenue, Gachibowly, Hyderabad. Land Mark: HDFC Bank, Opp to Care Hospital.Regards HR"; request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            Encoding ec = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader red = new System.IO.StreamReader(stream, ec);
            result = red.ReadToEnd();
            Console.WriteLine(result);
            red.Close();
            stream.Close();*/

        }
        protected void link1(object sender, EventArgs e)
        {
            //    string a = Interview.Text;
            //    string b = Label1.Text;
            //a+b= LinkButton11.Text;
            Time.Text = LinkButton11.Text;
        }
        protected void link2(object sender, EventArgs e)
        {
            Time.Text = LinkButton12.Text;
        }
        protected void link3(object sender, EventArgs e)
        {
            Time.Text = LinkButton13.Text;
        }
        protected void link4(object sender, EventArgs e)
        {
            Time.Text = LinkButton14.Text;
        }
        protected void BindBranch()
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select Id,BranchName from Branch", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            Branch.DataSource = dr;
            Branch.DataTextField = "BranchName";
            Branch.DataValueField = "Id";
            Branch.DataBind();
            Branch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "0"));
            conn.Close();
        }
        protected void Branch_Click(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select * from Branch where BranchName='"+txtbranch.Text+"'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                Response.Write("<script>alert('Duplicate Entry')</script>");
            }
            else
            {
                if(txtbranch.Text!="")
                {
                    SqlCommand cmd1 = new SqlCommand("Insert Into Branch(BranchName) values ('" + txtbranch.Text + "')", conn);
                    conn.Open();
                    cmd1.ExecuteNonQuery();
                    conn.Close();
                    BindBranch();
                }
            }
        }

        protected void Education_Change(object sender,EventArgs e)
        {
            if(Education.SelectedItem.Text== "Others")
            {
                div1.Visible = true;
                div2.Visible = false;
            }
            else
            {
                div1.Visible = false;
                div2.Visible = true;
            }
        }
    }
}