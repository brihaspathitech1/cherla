﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace CherlaHealth
{
    public partial class Warning : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }
       
        protected void Submit_Click(object sender,EventArgs e)
        {
            Session["employid"] = dropemp.SelectedValue;
            Session["employname"] = dropemp.SelectedItem.Text;
            Session["reason"] = txtreason.Text;
            Session["sugg"] = txtsug.Text;
            Session["date"] = txtdate.Text;
            Session["CompanyEmail"] = txtmail.Text;
            Session["salute"] = salut.SelectedItem.Text;
            Response.Redirect("~/WarningPrint.aspx");
        }

        protected void dropemp_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select CompanyEmail from Employees where EmployeeId='" + dropemp.SelectedValue + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
               
                txtmail.Text = dr["CompanyEmail"].ToString();

            }
            conn.Close();
        }
    }
}