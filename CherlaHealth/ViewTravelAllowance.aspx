﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewTravelAllowance.aspx.cs" Inherits="CherlaHealth.ViewTravelAllowance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script type="text/javascript">
    function onCalendarShown() {

        var cal = $find("calendar1");
        //Setting the default mode to month
        cal._switchMode("months", true);

        //Iterate every month Item and attach click event to it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }
    }

    function onCalendarHidden() {
        var cal = $find("calendar1");
        //Iterate every month Item and remove click event from it
        if (cal._monthsBody) {
            for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                var row = cal._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                }
            }
        }

    }
    function call(eventElement) {
        var target = eventElement.target;
        switch (target.mode) {
            case "month":
                var cal = $find("calendar1");
                cal._visibleDate = target.date;
                cal.set_selectedDate(target.date);
                cal._switchMonth(target.date);
                cal._blur.post(true);
                cal.raiseDateSelectionChanged();
                break;
        }
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">View Travel Allowance</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    Select By Month<asp:TextBox ID="txtmonth" CssClass="form-control" runat="server" OnTextChanged="Month_Change" AutoPostBack="true"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtmonth" OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown" BehaviorID="calendar1"
                                    Enabled="True" />
                        </ContentTemplate></asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-12">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                <asp:GridView ID="gvtravel" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None" runat="server">
                    <Columns>
                        <asp:BoundField DataField="Month" HeaderText="Date" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:BoundField DataField="EmployeeId" HeaderText="Employee Id" />
                        <asp:BoundField DataField="EmployeeName" HeaderText="Employee Name" /> 
                        <asp:BoundField DataField="Condition" HeaderText="Vehicle" />
                        <asp:BoundField DataField="Kms" HeaderText="kilometers" />
                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                        <asp:BoundField DataField="ExtraCharges" HeaderText="Extra Charges" />
                        <asp:BoundField DataField="total" HeaderText="Total" />
                    </Columns>
                </asp:GridView>
                    </ContentTemplate></asp:UpdatePanel>
            </div>
            </div>
        </div>
</asp:Content>
