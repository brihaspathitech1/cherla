﻿using inventory.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class deptapproveleaves : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Bindleaveslist();
            }
        }

        protected void month_changed(object sender, EventArgs e)
        {
            //Bindresult();
        }
        protected void year1_changed(object sender, EventArgs e)
        {
            //BindResult2();
        }

        
        protected void Bindleaveslist()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            DataSet ds1 = DataQueries.SelectCommon("Select d.DeptId from DeptMaster d  inner join Employees e on e.EmployeeId=d.ManagerId where d.ManagerId='" + Session["name"].ToString() + "'");
            string deptid = ds1.Tables[0].Rows[0]["DeptId"].ToString();
            if (ds1.Tables[0].Rows.Count > 0)
            {
                //if (deptid == "4")
                //{
                //    SqlCommand cmd = new SqlCommand("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID where l.Status='OnHRDesk'", conn);
                //    SqlDataAdapter da = new SqlDataAdapter(cmd);
                //    DataSet ds = new DataSet();
                //    da.Fill(ds);
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        gvlistofLeaves.DataSource = ds;
                //        gvlistofLeaves.DataBind();
                //    }
                //    else
                //    {
                //        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                //        gvlistofLeaves.DataSource = ds;
                //        gvlistofLeaves.DataBind();
                //        int columncount = gvlistofLeaves.Rows[0].Cells.Count;
                //        gvlistofLeaves.Rows[0].Cells.Clear();
                //        gvlistofLeaves.Rows[0].Cells.Add(new TableCell());
                //        gvlistofLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                //        gvlistofLeaves.Rows[0].Cells[0].Text = "No Records Found";
                //    }
                //}
                //else
                //{
                    SqlCommand cmd = new SqlCommand("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID inner join [DeptMaster] d on d.[DeptId]=e.deptId where l.Status='OnManagerDesk' and d.DeptId='"+deptid+"'", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvlistofLeaves.DataSource = ds;
                        gvlistofLeaves.DataBind();
                    }
                    else
                    {
                        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                        gvlistofLeaves.DataSource = ds;
                        gvlistofLeaves.DataBind();
                        int columncount = gvlistofLeaves.Rows[0].Cells.Count;
                        gvlistofLeaves.Rows[0].Cells.Clear();
                        gvlistofLeaves.Rows[0].Cells.Add(new TableCell());
                        gvlistofLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                        gvlistofLeaves.Rows[0].Cells[0].Text = "No Records Found";
                    }
                //}
            }

        }

        protected void Approve_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            string LeaveId = btn.CommandArgument;
            lblleaveid.Text= btn.CommandArgument;
            string sqlstr = "";

            DataSet ds = DataQueries.SelectCommon("Select Status,BalanceLeaves,Duration from LeavesStatus where LeaveId='" + LeaveId.Trim() + "'");
            string status = ds.Tables[0].Rows[0][0].ToString().Trim();
            int bal = int.Parse(ds.Tables[0].Rows[0][1].ToString());
            int LeaveDuration = int.Parse(ds.Tables[0].Rows[0][2].ToString());

            if (status == "OnManagerDesk")
            {
                //sqlstr = "update LeavesStatus set Status='OnHRDesk',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + LeaveId.Trim() + "'";
                BindEmployee();
                ModalPopupExtender1.Show();
            }
            else if (status == "OnHRDesk")
            {
                sqlstr = "update LeavesStatus set Status='Approved',BalanceLeaves='" + (bal - LeaveDuration) + "',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + LeaveId.Trim() + "'";
                DataQueries.UpdateCommon(sqlstr);
                Bindleaveslist();
            }
        }

        protected void Reject_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            string LeaveId = btn.CommandArgument;
            string sqlstr = "update LeavesStatus set Status='Rejected',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + LeaveId.Trim() + "'";
            DataQueries.UpdateCommon(sqlstr);
            Bindleaveslist();
        }

        protected void BindEmployee()
        {
            DataSet ds1 = DataQueries.SelectCommon("Select d.DeptId from DeptMaster d  inner join Employees e on e.EmployeeId=d.ManagerId where d.ManagerId='" + Session["name"].ToString() + "'");
            string deptid = ds1.Tables[0].Rows[0]["DeptId"].ToString();
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where Status='1' and deptid='"+deptid+"'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
        }

        protected void Approve(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("update LeavesStatus set Status='OnHRDesk',empreplace='"+dropemp.SelectedItem.Text+"',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + lblleaveid.Text + "'",conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Bindleaveslist();
        }
    }
}