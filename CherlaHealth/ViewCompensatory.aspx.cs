﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace CherlaHealth
{
    public partial class ViewCompensatory : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindGrid();
                BindEmployee();
            }
        }

        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select * from tbl_compensatoryoff order by id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                gvCompensatory.DataSource = ds;
                gvCompensatory.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvCompensatory.DataSource = ds;
                gvCompensatory.DataBind();
                int columncount = gvCompensatory.Rows[0].Cells.Count;
                gvCompensatory.Rows[0].Cells.Clear();
                gvCompensatory.Rows[0].Cells.Add(new TableCell());
                gvCompensatory.Rows[0].Cells[0].ColumnSpan = columncount;
                gvCompensatory.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'",conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));

        }

        protected void Employee_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select * from tbl_compensatoryoff where EmployeeId='"+dropemp.SelectedValue+"' order by id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvCompensatory.DataSource = ds;
                gvCompensatory.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvCompensatory.DataSource = ds;
                gvCompensatory.DataBind();
                int columncount = gvCompensatory.Rows[0].Cells.Count;
                gvCompensatory.Rows[0].Cells.Clear();
                gvCompensatory.Rows[0].Cells.Add(new TableCell());
                gvCompensatory.Rows[0].Cells[0].ColumnSpan = columncount;
                gvCompensatory.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}