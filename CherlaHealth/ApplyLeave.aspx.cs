﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using inventory.DataAccessLayer;

namespace CherlaHealth
{
    public partial class ApplyLeave : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        string message = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                //BindLeaves();
                BindLeaves();
                BindEmployee();
            }
        }

        protected void BindLeaves()
        {
            if (Session["name"].ToString() == "ADMIN")
            {
                div1.Visible = true;
            }
            else
            {
                SqlConnection conn = new SqlConnection(strConnString);
                SqlCommand cmd1 = new SqlCommand("Select * from Employees where DateAdd(yy,1,DOJ)<GETDATE() and EmployeeId='" + Session["name"].ToString() + "'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    SqlCommand cmd = new SqlCommand("Select l.LeaveTypeId,l.LeaveTypeName from LeaveTypes l inner join Employees e on e.emptypeid=l.typeid where e.EmployeeId='" + Session["name"].ToString() + "'", conn);
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropleave.DataSource = dr;
                    dropleave.DataTextField = "LeaveTypeName";
                    dropleave.DataValueField = "LeaveTypeId";
                    dropleave.DataBind();
                    dropleave.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("Select l.LeaveTypeId,l.LeaveTypeName from LeaveTypes l inner join Employees e on e.emptypeid=l.typeid where l.LeaveTypeName!='PL/EL Leaves' and e.EmployeeId='" + Session["name"].ToString() + "'", conn);
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropleave.DataSource = dr;
                    dropleave.DataTextField = "LeaveTypeName";
                    dropleave.DataValueField = "LeaveTypeId";
                    dropleave.DataBind();
                    dropleave.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        protected void type_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select LeaveTypeId,LeaveTypeName from LeaveTypes where typeid='"+droptype.SelectedValue+"'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropleave.DataSource = dr;
            dropleave.DataTextField = "LeaveTypeName";
            dropleave.DataValueField = "LeaveTypeId";
            dropleave.DataBind();
            dropleave.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }
        //protected void BindLeaves()
        //{
            

        //}

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            if (Session["name"].ToString() == "ADMIN")
            {
                SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dropemployee.DataSource = dr;
                dropemployee.DataTextField = "EmployeName";
                dropemployee.DataValueField = "EmployeeId";
                dropemployee.DataBind();
                dropemployee.Items.Insert(0, new ListItem("", "0"));
                conn.Close();
            }
            else
            {
                SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1' and EmployeeId='" + Session["name"].ToString() + "'", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dropemployee.DataSource = dr;
                dropemployee.DataTextField = "EmployeName";
                dropemployee.DataValueField = "EmployeeId";
                dropemployee.DataBind();
                dropemployee.Items.Insert(0, new ListItem("", "0"));
                conn.Close();
            }
        }

        protected void Date_change(object sender,EventArgs e)
        {
            string ErrorMessage = "";
            //if (droptype.SelectedValue == "1")
            //{
                if (dropleave.SelectedItem.Text == "Casual Leaves")
                {
                    if (txtbegin.Text != "" && txtend.Text != "")
                    {
                    //Storing input Dates  
                    DateTime FromYear = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                    DateTime ToYear = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                   // DateTime FromYear = Convert.ToDateTime(txtbegin.Text);
                        DateTime staringdate = FromYear;
                     //   DateTime ToYear = Convert.ToDateTime(txtend.Text);
                        DateTime endingdate = ToYear;
                        endingdate = endingdate.AddDays(1);
                        TimeSpan objTimeSpan = ToYear - FromYear;
                        int duration = 0;
                        //TotalDays  
                        double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
                        if (Days > 0)
                        {
                            if (FromYear.Year != ToYear.Year)
                            {

                                ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                                //  Response.Write("<script>alert('Staring and Ending Date of leave Should have same Year..!')</script>");

                            }
                            else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                            {
                                ErrorMessage += "Wrong selection of Leave Date..!";
                                //Response.Write("<script>alert('Wrong selection of Leave Date..!')</script>");

                            }
                            else
                            {
                            DateTime start = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                            DateTime end = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                                while (staringdate != endingdate)
                                {
                                    if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        if (Holidays.Tables[0].Rows.Count == 0)
                                        { duration++; }
                                        else
                                        {
                                            for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                            {
                                                string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                                DateTime s1 = DateTime.Parse(s.ToString());
                                                DateTime s2 = DateTime.Parse(s.ToString());
                                                if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                                {
                                                    duration++;
                                                }

                                            }
                                        }
                                    }
                                    staringdate = staringdate.AddDays(1);
                                }
                                staringdate = FromYear;
                                for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                {
                                    string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                    DateTime s1 = DateTime.Parse(s.ToString());
                                    DateTime s2 = DateTime.Parse(s.ToString());
                                    s1 = s1.AddDays(-1);
                                    s2 = s2.AddDays(+1);
                                    if (s1 == staringdate)
                                    {
                                        lblmsg.Text = "true";

                                    }
                                    if (s2 == staringdate)
                                    {
                                        lblmsg.Text = "true";
                                    }

                                }
                                txtduration.Text = Convert.ToString(duration);
                            }
                        }
                        else
                        {

                            ErrorMessage += "Wrong selection of Leave Ending Date..!";
                            // Response.Write("<script>alert('Wrong selection of Leave Ending Date..!')</script>");

                        }
                    }
                }
                else if (dropleave.SelectedItem.Text == "Sick Leaves")
                {
                    if (txtbegin.Text != "" && txtend.Text != "")
                    {
                    //Storing input Dates  
                    DateTime FromYear = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                    DateTime ToYear = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                  //  DateTime FromYear = Convert.ToDateTime(txtbegin.Text);
                        DateTime staringdate = FromYear;
                     //   DateTime ToYear = Convert.ToDateTime(txtend.Text);
                        DateTime endingdate = ToYear;
                        endingdate = endingdate.AddDays(1);
                        TimeSpan objTimeSpan = ToYear - FromYear;
                        int duration = 0;
                        //TotalDays  
                        double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
                        if (Days > 0)
                        {
                            if (FromYear.Year != ToYear.Year)
                            {

                                ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                                //  Response.Write("<script>alert('Staring and Ending Date of leave Should have same Year..!')</script>");

                            }
                            //else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                            //{
                            //    ErrorMessage += "Wrong selection of Leave Date..!";
                            //    //Response.Write("<script>alert('Wrong selection of Leave Date..!')</script>");

                            //}
                            else
                            {
                            DateTime start = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                            DateTime end = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                                while (staringdate != endingdate)
                                {
                                    if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        if (Holidays.Tables[0].Rows.Count == 0)
                                        { duration++; }
                                        else
                                        {
                                            for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                            {
                                                string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                                if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                                {
                                                    duration++;
                                                }

                                            }
                                        }
                                    }
                                    staringdate = staringdate.AddDays(1);
                                }
                                txtduration.Text = Convert.ToString(duration);
                            }
                        }
                        else
                        {

                            ErrorMessage += "Wrong selection of Leave Ending Date..!";
                            // Response.Write("<script>alert('Wrong selection of Leave Ending Date..!')</script>");

                        }
                    }
                }
                else if (dropleave.SelectedItem.Text == "PL/EL Leaves")
                {
                    if (txtbegin.Text != "" && txtend.Text != "")
                    {
                    //Storing input Dates  
                    DateTime FromYear = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                    DateTime ToYear = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                   // DateTime FromYear = Convert.ToDateTime(txtbegin.Text);
                        DateTime staringdate = FromYear;
                    //    DateTime ToYear = Convert.ToDateTime(txtend.Text);
                        DateTime endingdate = ToYear;
                        endingdate = endingdate.AddDays(1);
                        TimeSpan objTimeSpan = ToYear - FromYear;
                        int duration = 0;
                        //TotalDays  
                        double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
                        if (Days > 0)
                        {
                            if (FromYear.Year != ToYear.Year)
                            {

                                ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                                //  Response.Write("<script>alert('Staring and Ending Date of leave Should have same Year..!')</script>");

                            }
                            else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                            {
                                ErrorMessage += "Wrong selection of Leave Date..!";
                                //Response.Write("<script>alert('Wrong selection of Leave Date..!')</script>");

                            }
                            else
                            {
                            DateTime start = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                            DateTime end = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                                while (staringdate != endingdate)
                                {
                                    if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        if (Holidays.Tables[0].Rows.Count == 0)
                                        { duration++; }
                                        else
                                        {
                                            for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                            {
                                                string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                                DateTime s1 = DateTime.Parse(s.ToString());
                                                DateTime s2 = DateTime.Parse(s.ToString());
                                                s1 = s1.AddDays(-1);
                                                s2 = s2.AddDays(+1);
                                                if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                                {
                                                    duration++;
                                                }
                                                if (s1 == staringdate)
                                                {
                                                    //lblmsg.Text = "true";
                                                    duration++;
                                                }
                                                if (s2 == staringdate)
                                                {
                                                    //lblmsg.Text = "true";
                                                    duration++;
                                                }
                                            }
                                        }
                                    }
                                    staringdate = staringdate.AddDays(1);
                                }

                                txtduration.Text = Convert.ToString(duration);
                            }
                        }
                        else
                        {

                            ErrorMessage += "Wrong selection of Leave Ending Date..!";
                            // Response.Write("<script>alert('Wrong selection of Leave Ending Date..!')</script>");

                        }
                    }
                }
                else if (dropleave.SelectedItem.Text == "Compensatory Off")
                {
                    if (txtbegin.Text != "" && txtend.Text != "")
                    {
                    DateTime FromYear = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                    DateTime ToYear = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                   // DateTime FromYear = Convert.ToDateTime(txtbegin.Text);
                        DateTime staringdate = FromYear;
                      //  DateTime ToYear = Convert.ToDateTime(txtend.Text);
                        DateTime endingdate = ToYear;
                        endingdate = endingdate.AddDays(1);
                        TimeSpan objTimeSpan = ToYear - FromYear;
                        int duration = 0;
                        //TotalDays  
                        double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
                        if (Days > 0)
                        {
                            if (FromYear.Year != ToYear.Year)
                            {

                                ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                                //  Response.Write("<script>alert('Staring and Ending Date of leave Should have same Year..!')</script>");

                            }
                            else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                            {
                                ErrorMessage += "Wrong selection of Leave Date..!";
                                //Response.Write("<script>alert('Wrong selection of Leave Date..!')</script>");

                            }
                            else
                        {
                            DateTime start = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                            DateTime end = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                                while (staringdate != endingdate)
                                {
                                    if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        if (Holidays.Tables[0].Rows.Count == 0)
                                        { duration++; }
                                        else
                                        {
                                            for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                            {
                                                string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                                DateTime s1 = DateTime.Parse(s.ToString());
                                                DateTime s2 = DateTime.Parse(s.ToString());
                                                if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                                {
                                                    duration++;
                                                }

                                            }
                                        }
                                    }
                                    staringdate = staringdate.AddDays(1);
                                }
                                staringdate = FromYear;
                                txtduration.Text = Convert.ToString(duration);
                            }
                        }
                        else
                        {

                            ErrorMessage += "Wrong selection of Leave Ending Date..!";
                            // Response.Write("<script>alert('Wrong selection of Leave Ending Date..!')</script>");

                        }
                    }
                }
                else if (dropleave.SelectedItem.Text == "Loss Without Pay")
                {
                    if (txtbegin.Text != "" && txtend.Text != "")
                    {
                       // DateTime FromYear = Convert.ToDateTime(txtbegin.Text);
                         DateTime FromYear = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                         DateTime staringdate = FromYear;
                   //     DateTime ToYear = Convert.ToDateTime(txtend.Text);
                    DateTime ToYear = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                    DateTime endingdate = ToYear;
                        endingdate = endingdate.AddDays(1);
                        TimeSpan objTimeSpan = ToYear - FromYear;
                        int duration = 0;
                        //TotalDays  
                        double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
                        if (Days > 0)
                        {
                            if (FromYear.Year != ToYear.Year)
                            {

                                ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                                //  Response.Write("<script>alert('Staring and Ending Date of leave Should have same Year..!')</script>");

                            }
                            else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                            {
                                ErrorMessage += "Wrong selection of Leave Date..!";
                                //Response.Write("<script>alert('Wrong selection of Leave Date..!')</script>");

                            }
                            else
                            {
                                DateTime start = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
                            DateTime end = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                                while (staringdate != endingdate)
                                {
                                    if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        if (Holidays.Tables[0].Rows.Count == 0)
                                        { duration++; }
                                        else
                                        {
                                            for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                            {
                                                string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                                DateTime s1 = DateTime.Parse(s.ToString());
                                                DateTime s2 = DateTime.Parse(s.ToString());
                                                if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                                {
                                                    duration++;
                                                }

                                            }
                                        }
                                    }
                                    staringdate = staringdate.AddDays(1);
                                }
                                staringdate = FromYear;
                                txtduration.Text = Convert.ToString(duration);
                            }
                        }
                        else
                        {

                            ErrorMessage += "Wrong selection of Leave Ending Date..!";
                            // Response.Write("<script>alert('Wrong selection of Leave Ending Date..!')</script>");

                        }
                    }
                }
            //}
           // else if(droptype.SelectedValue=="2")
           // {
           //     ErrorMessage = "We will Implement soon";
           // }
            lblErrorMessage.Text = ErrorMessage;
        }

        protected Double getbalcl() 
        {
            Double count = DataQueries.SelectCount("Select Sum(cl) from LeavesStatus where EmployeedID='"+dropemployee.SelectedValue+ "' and Status='Approved' and Year(FromDate)='"+txtbegin.Text+"'");
            count = 12 - count;
            return count;
        }

        protected Double getbalSl()
        {
            Double count1 = DataQueries.SelectCount("Select Sum(Sl) from LeavesStatus where EmployeedID='" + dropemployee.SelectedValue + "' and Status='Approved' and Year(FromDate)='" + txtbegin.Text + "'");
            count1 = 12 - count1;
            return count1;
        }

        protected Double getballeaves()
        {
           // DateTime start = DateTime.Parse(txtbegin.Text);
            DateTime start = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);

            Double count=0;
            Double days = DataQueries.SelectCount("Select Days from LeaveTypes where LeaveTypeId='" + dropleave.SelectedValue + "'");
            if(dropleave.SelectedItem.Text== "Casual Leaves")                      
            {
                count = DataQueries.SelectCount("Select Sum(cl) from LeavesStatus where EmployeedID='" + dropemployee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + start.ToString("yyyy-MM-dd") + "')");
                count = days - count;
                //return count;
            }
            else if(dropleave.SelectedItem.Text== "Sick Leaves")
            {
                count = DataQueries.SelectCount("Select Sum(Sl) from LeavesStatus where EmployeedID='" + dropemployee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + start.ToString("yyyy-MM-dd") + "')");
                count = days - count;          
            }
            else if (dropleave.SelectedItem.Text == "PL/EL Leaves")
            {
                count = DataQueries.SelectCount("Select Sum(PL) from LeavesStatus where EmployeedID='" + dropemployee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + start.ToString("yyyy-MM-dd") + "')");
                count = days - count;
            }
            else if(dropleave.SelectedItem.Text== "Compensatory Off")
            {
                count = DataQueries.SelectCount("select sum(CL) from tbl_compensatoryoff where EmployeeId='" + dropemployee.SelectedValue + "' and YEAR(Worked_date)=YEAR('" + start.ToString("yyyy-MM-dd") + "') and Worked_date>= DateAdd(DAy,-30,Getdate())");
                //count = days - count;

                double count1 = DataQueries.SelectCount("Select Sum(COF) from LeavesStatus where EmployeedID='" + dropemployee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + start.ToString("yyyy-MM-dd") + "') and FromDate>=DateAdd(DAy,-30,Getdate())");
                if (count1 == 0)
                {
                    return count;
                }
                else
                {
                    count = count1 - count;
                }
            }
            else if(dropleave.SelectedItem.Text== "Loss Without Pay")
            {
                count = 0;
            }
            return count;
        }

        protected void Employee_Change(object sender,EventArgs e)
        {
            Double days = DataQueries.SelectCount("Select Days from LeaveTypes where LeaveTypeId='" + dropleave.SelectedValue + "'");
            if (dropemployee.SelectedIndex == 0) { return; }
            Double count = getballeaves();
            txtbalance.Text = count.ToString(); /*+ "/" + days + " for Year " + (DateTime.Parse(txtbegin.Text).Year).ToString();*/
            if (dropleave.SelectedItem.Text == "Casual Leaves")
            {
                if (Convert.ToInt16(txtduration.Text) > count)
                {
                    string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and l.LeaveType='Casual Leaves' order by l.LeaveId";
                    Session["isApply"] = "false";
                    DataSet ds = DataQueries.SelectCommon(str);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        lblErrorMessage.Text = "Sorry..! You can Apply Only "+days+" Casual Leaves Per Year..!";
                    }
                }

                string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='Casual Leaves'";
                DataSet ds1 = DataQueries.SelectCommon(s);
                if (ds1.Tables[0].Rows.Count != 0)
                {
                    lblErrorMessage.Text = "Already Pending Leave,You can't Apply..!";
                }
            }
            else if(dropleave.SelectedItem.Text == "Sick Leaves")
            {
                if (Convert.ToInt16(txtduration.Text) > count)
                {
                    string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.SL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and l.LeaveType='Sick Leaves' order by l.LeaveId";
                    Session["isApply"] = "false";
                    DataSet ds = DataQueries.SelectCommon(str);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        lblErrorMessage.Text = "Sorry..! You can Apply Only "+days+ " Sick Leaves Per Year..!";
                    }
                }

                string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.SL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='Sick Leaves'";
                DataSet ds1 = DataQueries.SelectCommon(s);
                if (ds1.Tables[0].Rows.Count != 0)
                {
                    lblErrorMessage.Text = "Already Pending Leave,You can't Apply..!";
                }
            }
            else if (dropleave.SelectedItem.Text == "PL/EL Leaves")
            {
                if (Convert.ToInt16(txtduration.Text) > count)
                {
                    string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.PL,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and l.LeaveType='PL/EL Leaves' order by l.LeaveId";
                    Session["isApply"] = "false";
                    DataSet ds = DataQueries.SelectCommon(str);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        lblErrorMessage.Text = "Sorry..! You can Apply Only "+days+ " PL/EL Leaves Per Year..!";
                    }
                }

                string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.PL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='PL/EL Leaves'";
                DataSet ds1 = DataQueries.SelectCommon(s);
                if (ds1.Tables[0].Rows.Count != 0)
                {
                    lblErrorMessage.Text = "Already Pending Leave,You can't Apply..!";
                }
                
            }

            else if(dropleave.SelectedItem.Text== "Compensatory Off")
            {
                txtbalance.Text = count.ToString();/* + " for Year "+ (DateTime.Parse(txtbegin.Text).Year).ToString();*/
                if (Convert.ToInt16(txtduration.Text) > count)
                {
                    string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.PL,l.COF,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and l.LeaveType='Compensatory Off' order by l.LeaveId";
                    Session["isApply"] = "false";
                    DataSet ds = DataQueries.SelectCommon(str);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        lblErrorMessage.Text = "Sorry..! You can Apply Only " + count.ToString() + " Compensatory Off's Per Year..!";
                    }
                    else
                    {
                        lblErrorMessage.Text = "You have only "+count.ToString()+" compensatory off's.You don't have permissions to apply more than that.";
                    }
                }

                string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.PL,l.COF,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + dropemployee.SelectedValue + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='Compensatory Off'";
                DataSet ds1 = DataQueries.SelectCommon(s);
                if (ds1.Tables[0].Rows.Count != 0)
                {
                    lblErrorMessage.Text = "Already Pending Leave,You can't Apply..!";
                }
            }
        }

        private string getStatus()
        {
            string status=string.Empty;
            if (Session["name"].ToString() == "ADMIN")
            {
                DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + Session["EmpId"].ToString() + "'");
                if (ds.Tables[0].Rows.Count != 0)
                {
                    status = "OnHRDesk";
                }
                else
                {
                    status = "OnManagerDesk";
                }
            }
            else
            {
                DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + Session["name"].ToString() + "'");
                if (ds.Tables[0].Rows.Count != 0)
                {
                    status = "OnHRDesk";
                }
                else
                {
                    status = "OnManagerDesk";
                }
            }
            return status;
        }

        protected void Submit(object sender,EventArgs e)
        {
            
            int LOP=0, CL=0;
            Double AlreadyApplied=0;
            int apply = Convert.ToInt16(txtduration.Text);
            //DateTime start = DateTime.Parse(txtbegin.Text);
           // DateTime end = DateTime.Parse(txtend.Text);
            DateTime start = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null);
            DateTime end = DateTime.ParseExact(txtend.Text, "dd/MM/yyyy", null);
            DataSet ds = DataQueries.SelectCommon("Select emptype from Employees where employeeid='" + dropemployee.SelectedValue + "'");
            string type = string.Empty;
            if (droptype.SelectedItem.Text=="")
            {
               type  = ds.Tables[0].Rows[0]["emptype"].ToString();
            }
            else
            {
                type = droptype.SelectedItem.Text;
            }
            if (dropleave.SelectedItem.Text== "Casual Leaves")
            {
                AlreadyApplied = DataQueries.SelectCount("select sum(CL) from LeavesStatus where EmployeedID='" + dropemployee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + start.ToString("yyyy-MM-dd") + "')");
            }
            else if(dropleave.SelectedItem.Text == "Sick Leaves")
            {
                AlreadyApplied = DataQueries.SelectCount("select sum(SL) from LeavesStatus where EmployeedID='" + dropemployee.SelectedValue + "' and Status='Approved' and Year(FromDate)=Year('" + start.ToString("yyyy-MM-dd") + "')");
            }
            if (dropleave.SelectedItem.Text == "Casual Leaves" || dropleave.SelectedItem.Text == "Sick Leaves")
            {
               // int month = DateTime.Parse(txtend.Text).Month;
                int month = DateTime.ParseExact(txtbegin.Text, "dd/MM/yyyy", null).Month;
                int canapply = month - (int)AlreadyApplied;
                if (lblmsg.Text == "true")
                {
                    CL = 0;
                    LOP = int.Parse(txtduration.Text);
                }
                else
                {
                    if (apply > canapply)
                    {
                        LOP = apply - canapply;
                        CL = canapply;
                    }
                    else if (apply > 3)
                    {
                        //CL = apply;
                        LOP = apply - 3;
                        CL = apply - LOP;
                    }
                    else
                    {
                        CL = apply;
                        LOP = 0;
                    }
                }
            }
            else if(dropleave.SelectedItem.Text== "PL/EL Leaves")
            {
                int month = DateTime.Parse(txtend.Text).Month;
                if(apply>21)
                {
                    LOP = apply - 21;
                    CL = apply - LOP;
                }
                else
                {
                    CL = apply;
                    LOP = 0;
                }
            }
            else if(dropleave.SelectedItem.Text == "Compensatory Off")
            {
                CL = apply;
            }
            else if(dropleave.SelectedItem.Text== "Loss Without Pay")
            {
                LOP = apply;
            }
            if (dropleave.SelectedItem.Text == "Casual Leaves")
            {
                DataQueries.InsertCommon("insert into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values('" + dropemployee.SelectedValue + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start.ToString("yyyy-MM-dd") + "','" + end.ToString("yyyy-MM-dd") + "','" + txtduration.Text + "','" + getballeaves() + "','" + txtreason.Text + "','"+ getStatus() + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + CL + "','" + LOP + "','','0','"+dropleave.SelectedItem.Text+"','0','0','"+ type + "')");
                Response.Write("<script>alert('Data inserted successfully')</script>");

                Response.Redirect("ApplyLeave.aspx");
            }
            else if (dropleave.SelectedItem.Text == "Sick Leaves")
            {
                DataQueries.InsertCommon("insert into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,COF,type,PL) values('" + dropemployee.SelectedValue + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start.ToString("yyyy-MM-dd") + "','" + end.ToString("yyyy-MM-dd") + "','" + txtduration.Text + "','" + getballeaves() + "','" + txtreason.Text + "','" + getStatus() + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','"+CL+"','"+dropleave.SelectedItem.Text+"','0','"+ type + "','0')");
                Response.Write("<script>alert('Data inserted successfully')</script>");

                Response.Redirect("ApplyLeave.aspx");
            }
            else if(dropleave.SelectedItem.Text== "PL/EL Leaves")
            {
                DataQueries.InsertCommon("Insert Into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values ('" + dropemployee.SelectedValue + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start.ToString("yyyy-MM-dd") + "','" + end.ToString("yyyy-MM-dd") + "','" + txtduration.Text + "','" + getballeaves() + "','" + txtreason.Text + "','" + getStatus() + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','0','" + dropleave.SelectedItem.Text + "','" + CL + "','0','"+ type + "')");
                Response.Write("<script>alert('Data inserted successfully')</script>");

                Response.Redirect("ApplyLeave.aspx");
            }
            else if(dropleave.SelectedItem.Text== "Compensatory Off")
            {
                DataQueries.InsertCommon("Insert Into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values ('" + dropemployee.SelectedValue + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start.ToString("yyyy-MM-dd") + "','" + end.ToString("yyyy-MM-dd") + "','"+txtduration.Text+"','" + getballeaves() + "','" + txtreason.Text + "','" + getStatus() + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','0','" + dropleave.SelectedItem.Text + "','0','" + CL + "','"+ type + "')");
                Response.Write("<script>alert('Data inserted successfully')</script>");

                Response.Redirect("ApplyLeave.aspx");
            }
            else if(dropleave.SelectedItem.Text== "Loss Without Pay")
            {
                DataQueries.InsertCommon("Insert Into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values ('" + dropemployee.SelectedValue + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start.ToString("yyyy-MM-dd") + "','" + end.ToString("yyyy-MM-dd") + "','" + txtduration.Text + "','" + getballeaves() + "','" + txtreason.Text + "','" + getStatus() + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','0','" + dropleave.SelectedItem.Text + "','0','0','"+ type + "')");
                Response.Write("<script>alert('Data inserted successfully')</script>");
                Response.Redirect("ApplyLeave.aspx");
            }
        }
    }
}