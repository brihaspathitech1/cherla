﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class Attendance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindStudy();
            }
            Label1.Text = DateTime.Now.ToString("yyyy-MM-dd");

            String conns = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(conns);
            SqlCommand comm1 = new SqlCommand();
            comm1.CommandType = CommandType.StoredProcedure;
            comm1.CommandText = "Proc_fullattendance";
            comm1.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = Label1.Text.Trim();
            
            comm1.Connection = conn1;
            try
            {
                conn1.Open();
                GridView1.EmptyDataText = "No Records Found";

                GridView1.DataSource = comm1.ExecuteReader();
                GridView1.DataBind();
                foreach (GridViewRow row in GridView1.Rows)
                {
                    foreach (TableCell cell in row.Cells)
                    {
                        cell.Attributes.CssStyle["text-align"] = "center";
                    }
                }
            }
            catch (Exception ex1)
            {
                throw ex1;
            }
            finally
            {

                conn1.Close();
                conn1.Dispose();

            }
        }

        protected void Click1(object sender, EventArgs e)
        {
            // string id = GridView11.SelectedRow.Cells[0].Text;
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            // Label13.Text = GridView11.DataKeys[gRow.RowIndex].Value.ToString();
            Label2.Text = gRow.Cells[2].Text.Replace("&nbsp;", "");


            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string aa = " 09:30";

            //double Premam = Convert.ToDouble(TextBox1.Text);
            //double c = Premam + 1;
            String a = date + aa;

            TextBox2.Text = a;


            ModalPopupExtender1.Show();


        }

        protected void buttn2(object sender, EventArgs e)
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String insert = "insert into tblattendance(UserId,DateOfTransaction,OuiDoor)values('" + Label2.Text + "','" + TextBox2.Text + "','OutDoorEntry')";
            SqlCommand comm = new SqlCommand(insert, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("Attendance.aspx");
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            String conns = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(conns);
            SqlCommand comm1 = new SqlCommand();
            comm1.CommandType = CommandType.StoredProcedure;
            comm1.CommandText = "Proc_fullattendance";
            comm1.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = TextBox1.Text.Trim();
            comm1.Connection = conn1;
            try
            {
                conn1.Open();
                GridView1.EmptyDataText = "No Records Found";
                GridView1.DataSource = comm1.ExecuteReader();
                GridView1.DataBind();
            }
            catch (Exception ex1)
            {
                throw ex1;
            }
            finally
            {
                conn1.Close();
                conn1.Dispose();

            }
        }

        protected void exit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            // Label13.Text = GridView11.DataKeys[gRow.RowIndex].Value.ToString();
            Label2.Text = gRow.Cells[2].Text.Replace("&nbsp;", "");


            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string aa = " 16:30";

            //double Premam = Convert.ToDouble(TextBox1.Text);
            //double c = Premam + 1;
            String a = date + aa;

            TextBox2.Text = a;


            ModalPopupExtender1.Show();
        }
    }
}