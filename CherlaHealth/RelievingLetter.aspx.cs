﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.DataAccessLayer;

namespace CherlaHealth
{
    public partial class RelievingLetter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label2.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            DateTo.Text = Request.QueryString["dateto"];
            Name1.Text = Request.QueryString["employeename"];
            Datefrom.Text = Request.QueryString["datefrom"];
            Relieving();
        }
        private void Relieving()
        {
            DataSet ds=DataQueries.SelectCommon("select [Employees].[CompanyEmail],Name,Empno,[Designation],Salary_Anum,Salary_Words,Salary_HikeDate from [EmployeeDetails] inner join [Employees] on [EmployeeDetails].id=[Employees].[EmployeeId] where Id='" + Session["Relieve"] + "' ");
            
            if (ds.Tables[0].Rows.Count > 0)
            {

                Email.Text = ds.Tables[0].Rows[0]["CompanyEmail"].ToString();
            }
        }
        protected void btn_relievemail(object sender, EventArgs e)
        {
            if (Email.Text == "" || Email.Text == null)
            {
                Response.Write("<script>alert('No valid email');</script>");

            }
            else
            {
                SendPDFEmail();
            }

        }
        protected void SendPDFEmail()
        {


            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();

                    DateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");

                    sb.Append("<div style='margin-right:80px;'><b>Date:" + DateTo.Text + "</b></div>");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<div style='margin-right:80px;'><b>" + Name1.Text + "</b></div>");
                    sb.Append("Hyderabad.");
                    sb.Append("<div style='text-align:center;font-size:20px'><b><u>Relieving Letter</u></b></div>");
                    sb.Append("<table width='100%'  border='0' cellpadding='7'>");
                    sb.Append("<tr><td>Dear " + Name1.Text + " </td> </tr>");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<p>");
                    sb.Append("<tr><td>We wish to inform you that your relieving is hereby accept and you are being relieved from the </td></tr>");
                    sb.Append("<tr><td>service of the company with effect from close of the office hours on" + DateTo.Text + ". </td></tr>");
                    sb.Append("</p>");
                    sb.Append("</table>");
                    sb.Append("<table width='100%' border='0' cellpadding='7'>");
                    sb.Append("<tr><td>Your contribution to the organisation and its success will always be appreciated. we at </td></tr>");
                    sb.Append("<tr><td>Brihaspathi Technologies Pvt Ltd., wish you all the best in your future endeavours.</td></tr>");
                    sb.Append("</table>");
                    sb.Append("<table width='100%' border='0' cellpadding='7'>");
                    sb.Append("<tr><td>Yours Sincerely,</td></tr>");
                    sb.Append("<tr><td><b>For Brihaspathi Technologies Pvt Ltd.,</b></td></tr>");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<tr><td><b>Hyma P,</b></td></tr>");
                    sb.Append("<tr><td><b>HR Executive.</b></td></tr>");
                    sb.Append("</table>");
                    StringReader sr = new StringReader(sb.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        //string path = Server.MapPath("Files");
                        //string filename = path + "/" + txtname.Text + ".Pdf";

                        //PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        htmlparser.Parse(sr);
                        string imagePath = Server.MapPath("top.jpg") + "";
                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);
                        image.Alignment = Element.ALIGN_TOP;
                        image.SetAbsolutePosition(30, 750);
                        image.ScaleToFit(550f, 550f);
                        image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;
                        pdfDoc.Add(image);
                        PdfContentByte content = writer.DirectContent;
                        Rectangle rectangle = new Rectangle(pdfDoc.PageSize);
                        rectangle.Left += pdfDoc.LeftMargin;
                        rectangle.Right -= pdfDoc.RightMargin;
                        rectangle.Top -= pdfDoc.TopMargin;
                        rectangle.Bottom += pdfDoc.BottomMargin;
                        content.SetColorStroke(BaseColor.BLACK);
                        //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                        content.Stroke();
                        content.Stroke();
                        pdfDoc.Close();
                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();
                        MailMessage mm = new MailMessage("hre@brihaspathi.com", Email.Text);
                        mm.Subject = "Relieving  Letter";
                        mm.Body = "Relieving Letter";
                        mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Experience.pdf"));
                        mm.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential();
                        NetworkCred.UserName = "trainee@brihaspathi.com";
                        NetworkCred.Password = "myself123";
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
            }
        }
    }
}