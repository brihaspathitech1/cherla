﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.DataAccessLayer;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Drawing;
using System.ComponentModel;

namespace CherlaHealth
{

    public partial class emp_app : System.Web.UI.Page
    {
        #region navigator
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod == "POST")
            {
                try
                {
                    string request = Request.Params["request"].ToString();

                    switch (request)
                    {
                        case "login":
                            login();
                            break;
                        case "daily":
                            daily();
                            break;
                        case "contacts":
                            contacts();
                            break;
                        case "monthreport":
                            monthreport();
                            break;
                        case "twodates":
                            twodates();
                            break;

                        case "attendance_status":
                            attendance_status();
                            break;

                        case "attendance_req":
                            attendance_req();
                            break;

                        case "request_data":
                            request_data();
                            break;

                        case "mark_attendance":
                            mark_attendance();
                            break;

                        case "leaves":
                            leaves();
                            break;

                        case "leavedetails":
                            leavedetails();
                            break;

                        case "update_token":
                            token();
                            break;
                        case "distance":
                            distance();
                            break;
                        case "paylist":
                            paylist();
                            break;
                        case "payslip":
                            payslip();
                            break;
                        case "leavetypes":
                            leavetypes();
                            break;
                        case "apply_leave":
                            applyleave();
                            break;
                        case "leavelist":
                            leaveslist();
                            break;
                        case "employee_list":
                            employeelist();
                            break;
                        case "mark_leave":
                            mark_leave();
                            break;
                        case "markout":
                            outpunch();
                            break;
                        case "profileimage":
                            profileimage();
                            break;
                        case "forgot_password":
                            forgot_password();
                            break;
                        case "otp_verification":
                            otp_verfication();
                            break;
                        case "create_password":
                            change_password();
                            break;
                        case "hrmark_leave":
                            hrmark_leave();
                            break;
                        case "broadcast":
                            broadcast();
                            break;
                        case "events":
                            events();
                            break;
                        
                        case "hrmark_leave1":
                            hrmark_leave1();
                            break;
                        case "req_price":
                            req_price();
                            break;
                        case "list_data":
                            list_data();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\":\"true\",\"message\":\"server_error=" + ex.Message.ToString() + "\"}");
                }
            }
        }
        #endregion

        #region webservices



        protected void login()
        {
            string employee_id = Request.Params["employee_id"].ToString();
            string password = Request.Params["password"].ToString();
            string device_id = Request.Params["device_id"].ToString();
            string status = string.Empty;

            DataSet ds1 = DataQueries.SelectCommon("SElect * from DeptMaster where ManagerId='" + employee_id + "'");
            if (ds1.Tables[0].Rows.Count > 0)
            {
                status = "yes";
            }
            else
            {
                status = "no";
            }
        found: DataSet ds = DataQueries.SelectCommon("select a.EmployeeId,a.DOJ,a.EmployeName,a.DeptId,a.device_id,a.cellno,a.Desigantion,a.CompanyEmail,b.Department from Employees a inner join Department b on a.DeptId=b.DeptId  where a.EmployeeId='" + employee_id + "' and a.password='" + password + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                string dbDevice_id = ds.Tables[0].Rows[0]["device_id"].ToString();
                string employe_name = ds.Tables[0].Rows[0]["EmployeName"].ToString();

                if (dbDevice_id == device_id)
                {
                    string name = ds.Tables[0].Rows[0]["EmployeName"].ToString().Trim();
                    string phone = ds.Tables[0].Rows[0]["cellno"].ToString().Trim();
                    string email = ds.Tables[0].Rows[0]["CompanyEmail"].ToString().Trim();
                    string department = ds.Tables[0].Rows[0]["department"].ToString().Trim();
                    string designation = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                    string doj = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim();
                    DateTime datejoin = DateTime.Parse(doj);
                    string dept_id = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();

                    Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"phone\":\"" + phone + "\",\"email\":\"" + email + "\",\"department\":\"" + department + "\",\"designation\":\"" + designation + "\",\"doj\":\"" + datejoin.ToString("yyyy-MM-dd") + "\",\"deptid\":" + dept_id + ",\"head\":" + status + "}");

                    //  Response.Write("{\"error\":\"false\",\"message\":\"login successfully\"}");
                }
                else if (device_id == "9a8174dbf36f8311" || device_id == "77227112e9593ad1" || device_id == "5b49309aefbd4271")
                {
                    string name = ds.Tables[0].Rows[0]["EmployeName"].ToString().Trim();
                    string phone = ds.Tables[0].Rows[0]["cellno"].ToString().Trim();
                    string email = ds.Tables[0].Rows[0]["CompanyEmail"].ToString().Trim();
                    string department = ds.Tables[0].Rows[0]["department"].ToString().Trim();
                    string designation = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                    string doj = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim();
                    DateTime datejoin = DateTime.Parse(doj);
                    string dept_id = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();

                    Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"phone\":\"" + phone + "\",\"email\":\"" + email + "\",\"department\":\"" + department + "\",\"designation\":\"" + designation + "\",\"doj\":\"" + datejoin.ToString("yyyy-MM-dd") + "\",\"deptid\":" + dept_id + ",\"head\":" + status + "}");

                }
                else if ((dbDevice_id == "") || (dbDevice_id == null))
                {
                    DataQueries.UpdateCommon("update Employees set device_id='" + device_id + "' where EmployeeId='" + employee_id + "'");
                    goto found;
                    // Response.Write("{\"error\":\"false\",\"message\":\"login failed\"}");
                }
                else
                {
                    Response.Write("{\"error\":\"false\",\"message\":\"Device in alredy use\"}");
                }

            }
            else
            {
                Response.Write("{\"error\":\"true\",\"message\":\"Invalid UserName Or Password\"}");
            }
        }

        protected void events()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
            SqlCommand cmd = new SqlCommand("select HolidayName,convert(varchar(50),HolidayDate,106) as HolidayDate from Holidays order by HolidayId desc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }

        protected void daily()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            double empid = double.Parse(employeeid);
            string MonthDate = Request.Params["MonthDate"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_Dailydates", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", MonthDate);
                cmd.Parameters.AddWithValue("@Basic", Type);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("MonthlyReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);

                if (dt.Rows.Count > 0)
                {

                    string Shift = dt.Rows[0]["Shift"].ToString();
                    string InTime = dt.Rows[0]["In Time"].ToString();
                    string Outtime = dt.Rows[0]["Out Time"].ToString();
                    string Duration = dt.Rows[0]["Duration"].ToString();
                    string ot = dt.Rows[0]["OT"].ToString();
                    string status = dt.Rows[0]["Status"].ToString();
                    Response.Write("{\"error\": false ,\"message\":\"Success\",\"Shift\":\"" + Shift + "\",\"InTime\":\"" + InTime + "\",\"OutTime\":\"" + Outtime + "\",\"Duration\":\"" + Duration + "\",\"OT\":\"" + ot + "\",\"Status\":\"" + status + "\"}");
                }
                else
                {
                    Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
                }
                 
            }
        }

        protected void contacts()
        {
            DataSet ds = DataQueries.SelectCommon("select [EmployeName],[CellNo],[CompanyEmail] from [Employees] where [status]='1'");
            DataTable dt = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(dt);

            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }


        protected void token()
        {
            string employee_id = Request.Params["employee_id"].ToString();
            string token = Request.Params["token"].ToString();
            DataQueries.UpdateCommon("update Employees set token='" + token + "' where EmployeeId='" + employee_id + "'");
            Response.Write("{\"error\": false ,\"message\":\"success\"}");

        }

        protected void forgot_password()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select a.EmployeeId,a.EmployeName,a.DeptId,a.cellno,a.Desigantion,a.CompanyEmail,b.Department  from Employees a inner join Department b on a.DeptId=b.DeptId where a.EmployeeId=652 and a.status='1'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string mobile = ds.Tables[0].Rows[0]["cellno"].ToString();
                Random random = new Random();
                int OTP = random.Next(100000, 999999);
                sendOTP(mobile, OTP);
                DataQueries.UpdateCommon("Update Employees set otp='" + OTP.ToString() + "' where employeeid='" + employeeid + "'");
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"mobile\":\"" + mobile + "\"}");
            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"fail\"}");
            }

        }

        protected void otp_verfication()
        {
            string employee_id = Request.Params["employee_id"].ToString();
            string otp = Request.Params["otp"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select * from Employees where Employeeid='" + employee_id + "' and otp='" + otp + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                //string name = ds.Tables[0].Rows[0]["firstname"].ToString();
                Response.Write("{\"error\":\"false\",\"message\":\"Mobile number verified successfully\"}");
                //send_sms(mobile, name);

            }
            else
            {
                Response.Write("{\"error\":\"true\",\"message\":\"Invalid OTP\"}");
            }
        }

        protected void change_password()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string password = Request.Params["new_password"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("Select * from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataQueries.UpdateCommon("Update Employees set password='" + password + "' where EmployeeId='" + employeeid + "'");
                Response.Write("{\"error\": false,\"message\":\"Success\"}");
            }
        }
        protected void monthreport()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            double empid = double.Parse(employeeid);
            string MonthDate = Request.Params["MonthDate"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_MonthlydatesAndroid", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", MonthDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("MonthlyReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);
                Response.Write("{\"error\": false,\"message\":\"Success\",\"data\":" + JSONresult + "}");
            }

            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }
        }


        protected void twodates()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string StartDate = Request.Params["StartDate"].ToString().Trim();
            string EndDate = Request.Params["EndDate"].ToString().Trim();
            double empid = double.Parse(employeeid);
            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {

                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_Twodates", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("TwoDatesReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
            }
            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }
        }

        //protected void attendance_req()
        //{

        //    string employeeid = Request.Params["employee_id"].ToString().Trim();
        //    string reason = Request.Params["reason"].ToString().Trim();
        //    string latitude = Request.Params["latitude"].ToString().Trim();
        //    string longitude = Request.Params["longitude"].ToString().Trim();
        //    string Image = Request.Params["image"].ToString();

        //    int empID = Convert.ToInt32(employeeid);
        //    int employid = 0;
        //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
        //    SqlCommand cmd = new SqlCommand("proc_distance", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@lat", latitude);
        //    cmd.Parameters.AddWithValue("@long", longitude);
        //    cmd.Parameters.AddWithValue("@empid", employeeid);
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    if (dt.Rows.Count != 0)
        //    {
        //        double distance = Convert.ToDouble(dt.Rows[0]["Distance"].ToString());
        //        //double meters = distance * 0.000621371;
        //        //if (distance < 1.737)
        //        //{
        //           string date = System.DateTime.Now.ToString("yyyy/MM/dd");
        //            string date12 = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
        //            string name = employeeid + "_" + date;
        //            string filename = name;
        //            string base64 = Image;
        //            byte[] bytes1 = Convert.FromBase64String(base64);
        //            File.WriteAllBytes(Server.MapPath("~/attendanceimages/") + filename + ".jpg", bytes1); //write to a temp location.
        //            string path = "http://cherla.bterp.in/attendanceimages/" + filename + ".jpg";

        //            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");
        //            if (ds.Tables[0].Rows.Count == 1)
        //            {
        //                Response.Write("{\"error\": true ,\"message\":\"Already Request Sent\"}");
        //            }
        //            else
        //            {
        //                // DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status) values ('" + reason + "','" + DateTime.Now + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0')");
        //                //DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status) values ('" + reason + "','" + System.DateTime.Now.ToString() + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0')");
        //                DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status,image) values ('" + reason + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0','" + path.ToString() + "')");

        //                DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','OutDoorEntry')");
        //                DataQueries.InsertCommon("update  RequestAttendance set status='1' where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");

        //                Response.Write("{\"error\": false,\"message\":\"Request Submitted Successsfully\"}");

        //                DataSet ds12 = DataQueries.SelectCommon("Select deptid from Employees where employeeid='" + employeeid + "'");
        //                if (ds12.Tables[0].Rows.Count > 0)
        //                {
        //                    string deptid = ds12.Tables[0].Rows[0]["deptid"].ToString();
        //                    DataSet ds1 = DataQueries.SelectCommon("Select ManagerId from DeptMaster where deptid='" + deptid + "'");
        //                    if (ds1.Tables[0].Rows.Count > 0)
        //                    {
        //                        string empid12 = ds1.Tables[0].Rows[0]["ManagerId"].ToString();
        //                        employid = int.Parse(empid12.ToString());
        //                    }
        //                }

        //                //Employee push notification
        //                string emp_title = "Attendance Request Sent";
        //                //string emp_body = "Your Request for Attendance Submitted Succesfully";
        //                string emp_body = "Your Attendance Request Accepted";
        //                string emp_token = token(empID);
        //                notification(emp_token, emp_title, emp_body);

        //                //Hr push notifications
        //                /* string emp_name = employee_name(empID);
        //                 string hr_title = "New Attendance Request";
        //                 string hr_body = emp_name + "Submitted Attendance Request";
        //                 string hr_token = token(1);
        //                 notification(hr_token, hr_title, hr_body);*/

        //                //department push notifications
        //                string emp_name1 = employee_name(empID);
        //                string dept_title = "New Attendance Request";
        //                //string dept_body = emp_name1 + "Submitted Attendance Request";
        //                string dept_body = emp_name1 + "Marked Attendance Request Succesfully";
        //                string dept_token = token(employid);
        //                notification(dept_token, dept_title, dept_body);

        //            }
        //       // }
        //        //else
        //        //{
        //        //    Response.Write("{\"error\": true,\"message\":\"your attendance request is not send\"}");
        //        //}
        //    }


        //}
        protected void attendance_req()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string reason = Request.Params["reason"].ToString().Trim();
            string latitude = Request.Params["latitude"].ToString().Trim();
            string longitude = Request.Params["longitude"].ToString().Trim();
            string Image = Request.Params["image"].ToString();
            string dateee = System.DateTime.Now.ToString("yyyy-MM-dd");
            int empID = Convert.ToInt32(employeeid);
            int employid = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
            SqlCommand cmd = new SqlCommand("proc_distance", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@lat", latitude);
            cmd.Parameters.AddWithValue("@long", longitude);
            cmd.Parameters.AddWithValue("@empid", employeeid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count != 0)
            {
                double distance = Convert.ToDouble(dt.Rows[0]["Distance"].ToString());
                //double meters = distance * 0.000621371;
                if (distance < 1.737)
                {
                DataSet dss = DataQueries.SelectCommon("select * from tblattendance where USERID='" + employeeid + "' and convert(nvarchar(10),DateOfTransaction,126)='" + dateee + "'");
                if (dss.Tables[0].Rows.Count == 1)
                {
                   // DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + date12 + "','OutDoorEntry')");
                    Response.Write("{\"error\": true,\"message\":\"Out Punch Request Submitted already\"}");
                }
                else
                {
                    string date = System.DateTime.Now.ToString("yyyy/MM/dd");
                    string date12 = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
                    string name = employeeid + "_" + date;
                    string filename = name;
                    string base64 = Image;
                    byte[] bytes1 = Convert.FromBase64String(base64);
                    File.WriteAllBytes(Server.MapPath("~/attendanceimages/") + filename + ".jpg", bytes1); //write to a temp location.
                    string path = "http://cherla.bterp.in/attendanceimages/" + filename + ".jpg";

                    DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        Response.Write("{\"error\": true ,\"message\":\"Already Request Sent\"}");
                    }
                    else
                    {
                        // DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status) values ('" + reason + "','" + DateTime.Now + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0')");
                        //DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status) values ('" + reason + "','" + System.DateTime.Now.ToString() + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0')");
                        DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status,image) values ('" + reason + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0','" + path.ToString() + "')");

                        DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','OutDoorEntry')");
                        DataQueries.InsertCommon("update  RequestAttendance set status='1' where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");

                        Response.Write("{\"error\": false,\"message\":\"Request Submitted Successsfully\"}");

                        DataSet ds12 = DataQueries.SelectCommon("Select deptid from Employees where employeeid='" + employeeid + "'");
                        if (ds12.Tables[0].Rows.Count > 0)
                        {
                            string deptid = ds12.Tables[0].Rows[0]["deptid"].ToString();
                            DataSet ds1 = DataQueries.SelectCommon("Select ManagerId from DeptMaster where deptid='" + deptid + "'");
                            if (ds1.Tables[0].Rows.Count > 0)
                            {
                                string empid12 = ds1.Tables[0].Rows[0]["ManagerId"].ToString();
                                employid = int.Parse(empid12.ToString());
                            }
                        }

                        //Employee push notification
                        string emp_title = "Attendance Request Sent";
                        //string emp_body = "Your Request for Attendance Submitted Succesfully";
                        string emp_body = "Your Attendance Request Accepted";
                        string emp_token = token(empID);
                        notification(emp_token, emp_title, emp_body);

                        //Hr push notifications
                        /* string emp_name = employee_name(empID);
                         string hr_title = "New Attendance Request";
                         string hr_body = emp_name + "Submitted Attendance Request";
                         string hr_token = token(1);
                         notification(hr_token, hr_title, hr_body);*/

                        //department push notifications
                        string emp_name1 = employee_name(empID);
                        string dept_title = "New Attendance Request";
                        //string dept_body = emp_name1 + "Submitted Attendance Request";
                        string dept_body = emp_name1 + "Marked Attendance Request Succesfully";
                        string dept_token = token(employid);
                        notification(dept_token, dept_title, dept_body);

                    }
                }
                }
                else
                {
                    Response.Write("{\"error\": true,\"message\":\"your attendance request is not send\"}");
                }

            }
        }
        protected void attendance_status()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");

            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {

                string reason = ds.Tables[0].Rows[0]["reason"].ToString().Trim();
                string time = ds.Tables[0].Rows[0]["time"].ToString().Trim();
                string status = ds.Tables[0].Rows[0]["status"].ToString().Trim();
                string latitude = ds.Tables[0].Rows[0]["latitude"].ToString().Trim();
                string longitude = ds.Tables[0].Rows[0]["longitude"].ToString().Trim();
                string image = ds.Tables[0].Rows[0]["image"].ToString().Trim();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"reason\":\"" + reason + "\",\"time\":\"" + time + "\",\"status\":\"" + status + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"image\":\"" + image + "\"}");
            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"No Records Found\"}");
            }
        }
        protected void request_data()
        {
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            //DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where  convert(nvarchar(10),Time,126)='" + date + "'");
            DataSet ds = DataQueries.SelectCommon("select a.employeeid as employee_id,a.desigantion,a.employename as name,a.deptid as department_id,b.Reason as reason,b.Time as time,b.Latitude as latitude,b.Longitude as longitude,b.Status as status,b.image,c.department from employees a inner join RequestAttendance b on a.employeeid=b.employee_id inner join department c on a.deptid=c.deptid where convert(nvarchar(10),Time,126)='" + date + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void mark_attendance()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string hr = Request.Params["hr"].ToString().Trim();
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");

            DataSet ds = DataQueries.SelectCommon("Select Time from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "' and Status='0'");
            DateTime date2 = DateTime.Parse(ds.Tables[0].Rows[0]["Time"].ToString());
            string date23 = date2.ToString("yyyy-MM-dd HH:mm:ss");

            int empID = Convert.ToInt32(employeeid);

            string date1 = DateTime.Now.ToString("yyyy-MM-dd");
            string aa = " 09:30";

            String a = date1 + aa;
            DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + date23 + "','OutDoorEntry')");
            DataQueries.InsertCommon("update  RequestAttendance set status='1', markby='" + hr + "' where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");
            Response.Write("{\"error\": false,\"message\":\"Marked Successsfully\"}");
            //Hr push notifications
            //string emp_name = employee_name(empID);
            string hr_title = "Request Accepted";
            string hr_body = "Your Attendance Request Accepted";
            string emp_token = token(empID);
            notification(emp_token, hr_title, hr_body);

        }
        //protected void outpunch()
        //{
        //    string employeeid = Request.Params["employee_id"].ToString().Trim();
        //    string latitude = Request.Params["latitude"].ToString().Trim();
        //    string longitude = Request.Params["longitude"].ToString().Trim();

        //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
        //    SqlCommand cmd = new SqlCommand("proc_distance", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@lat", latitude);
        //    cmd.Parameters.AddWithValue("@long", longitude);
        //    cmd.Parameters.AddWithValue("@empid", employeeid);
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    if (dt.Rows.Count != 0)
        //    {
        //        double distance = Convert.ToDouble(dt.Rows[0]["Distance"].ToString());
        //        //if (distance < 1.737)
        //        //{
        //            string date = System.DateTime.Now.ToString("yyyy/MM/dd");
        //            string date12 = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "' and status='1'");
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + date12 + "','OutDoorEntry')");
        //                Response.Write("{\"error\": false,\"message\":\"Out Punch Request Submitted Successsfully\"}");
        //            }
        //            else
        //            {
        //            //Response.Write("{\"error\": true,\"message\":\"your in punch is not send, firstly you send the in punch\"}");
        //            DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + System.DateTime.Now.ToString("yyyy - MM - dd HH: mm:ss") + "','OutDoorEntry')");
        //            Response.Write("{\"error\": false,\"message\":\"Out Punch Request Submitted Successsfully\"}");


        //        }
        //        //}
        //        //else
        //        //{
        //        //    Response.Write("{\"error\": true,\"message\":\"your Out punch request is not send\"}");
        //        //}
        //    }
        //}
        protected void outpunch()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string latitude = Request.Params["latitude"].ToString().Trim();
            string longitude = Request.Params["longitude"].ToString().Trim();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
            SqlCommand cmd = new SqlCommand("proc_distance", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@lat", latitude);
            cmd.Parameters.AddWithValue("@long", longitude);
            cmd.Parameters.AddWithValue("@empid", employeeid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count != 0)
            {
                double distance = Convert.ToDouble(dt.Rows[0]["Distance"].ToString());
                if (distance < 1.737)
                {
                    //  string date = System.DateTime.Now.ToString("yyyy/MM/dd");
                    string date = System.DateTime.Now.ToString("yyyy-MM-dd");
                    string date12 = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                  //  DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "' and status='1'");
                    DataSet ds = DataQueries.SelectCommon("select * from tblattendance where USERID='" + employeeid + "' and convert(nvarchar(10),DateOfTransaction,126)='" + date + "'");
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + date12 + "','OutDoorEntry')");
                        Response.Write("{\"error\": false,\"message\":\"Out Punch Request Submitted Successsfully\"}");
                    }
                    else
                    {
                        Response.Write("{\"error\": true,\"message\":\"your in punch is not send, firstly you send the in punch\"}");
                    }
                }
                else
                {
                    Response.Write("{\"error\": true,\"message\":\"your Out punch request is not send\"}");
                }
            }
            
        }
        protected void leaves()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
            SqlCommand cmd = new SqlCommand("Proc_LeavesAndroid", con);
            SqlCommand cmd1 = new SqlCommand("select AppliedDate,FromDate,ToDate,Duration,ReasontoApply,Status,StatusDate,cl,sl,pl,cof,lop from LeavesStatus where employeedid='" + employeeid + "'", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@employeedid", employeeid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable("Leaves");
            DataTable dt1 = new DataTable("LeaveRecords");
            da.Fill(dt);
            da1.Fill(dt1);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            string JSONresult1 = DataTableToJsonWithJsonNet(dt1);
            if (dt.Rows.Count > 0)
            {
                int BalanceLeaves = Convert.ToInt32(dt.Rows[0]["balanceleaves"].ToString());
                int Used = Convert.ToInt32(dt.Rows[0]["used"].ToString());
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"Remaining\": " + BalanceLeaves + ",\"Used\":" + Used + ",\"History\": " + JSONresult1 + "}");
            }
            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }

        }

        protected void leavedetails()
        {
            string employee_id = Request.Params["employee_id"].ToString().Trim();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ToString());
            SqlCommand cmd = new SqlCommand("select applieddate,fromdate,todate,duration,status,reasontoapply,LeaveType from LeavesStatus where employeedid='" + employee_id + "'and status='approved'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Leaves");
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

        }
        protected void distance()
        {
            string empid = Request.Params["employee_id"].ToString();
            string date = Request.Params["date"].ToString();
            string distance = Request.Params["distance"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select * from distance where employee_id='" + empid + "' and date='" + date + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataQueries.SelectCommon("Update distance set distance='" + distance + "' where employee_id='" + empid + "' and date='" + date + "'");
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            }
            else
            {
                DataQueries.InsertCommon("Insert into distance(date,employee_id,distance) values ('" + date + "','" + empid + "','" + distance + "')");
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            }
        }
        
        protected void paylist()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select a.Year,a.Month,month(cast(a.date as varchar)) as mdate,a.total,a.ttotal,a.present,a.workingdays,a.Name,b.EmployeeCodeInDevice from Payslip a left outer join Employees b on a.Name=b.EmployeName where EmployeeCodeInDevice='" + employeeid + "'");

            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }
        protected void payslip()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string month = Request.Params["month"].ToString().Trim();
            string year = Request.Params["year"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select top 1 *,convert(varchar,b.DOB,101) as DB,convert(varchar,b.DOJ,101) as DJ from Payslip a inner join Employees b on a.Name=b.EmployeName where EmployeeCodeInDevice='" + employeeid + "' and month(date)='" + month + "' and year='" + year + "' order by id desc ");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                string department = ds.Tables[0].Rows[0]["Departrment"].ToString().Trim();
                string designation = ds.Tables[0].Rows[0]["Designation"].ToString().Trim();
                string empnum = ds.Tables[0].Rows[0]["EmpNo"].ToString().Trim();
                string dob = ds.Tables[0].Rows[0]["DB"].ToString().Trim();
                string doJ = ds.Tables[0].Rows[0]["DJ"].ToString().Trim();
                string workingdays = ds.Tables[0].Rows[0]["Workingdays"].ToString().Trim();
                string paybledays = ds.Tables[0].Rows[0]["Paybledays"].ToString().Trim();
                string pfno = ds.Tables[0].Rows[0]["PFNo"].ToString().Trim();
                string basic = ds.Tables[0].Rows[0]["Basic"].ToString().Trim();
                string hra = ds.Tables[0].Rows[0]["HRA"].ToString().Trim();
                string da = ds.Tables[0].Rows[0]["DA"].ToString().Trim();
                string pf = ds.Tables[0].Rows[0]["PF"].ToString().Trim();
                string ca = ds.Tables[0].Rows[0]["CA"].ToString().Trim();
                string esi = ds.Tables[0].Rows[0]["ESI"].ToString().Trim();
                string ea = ds.Tables[0].Rows[0]["EA"].ToString().Trim();
                string asa = ds.Tables[0].Rows[0]["ASa"].ToString().Trim();
                string it = ds.Tables[0].Rows[0]["IT"].ToString().Trim();
                string paydayd = ds.Tables[0].Rows[0]["paydayd"].ToString().Trim();
                string total = ds.Tables[0].Rows[0]["Total"].ToString().Trim();
                string deducation = ds.Tables[0].Rows[0]["Deducation"].ToString().Trim();
                string ttotal = ds.Tables[0].Rows[0]["TTotal"].ToString().Trim();
                string amountwords = ds.Tables[0].Rows[0]["Amountwords"].ToString().Trim();
                string child = ds.Tables[0].Rows[0]["ChildAllowance"].ToString().Trim();
                string travel = ds.Tables[0].Rows[0]["travell"].ToString().Trim();
                string mobile = ds.Tables[0].Rows[0]["MobileBill"].ToString().Trim();
                string special = ds.Tables[0].Rows[0]["special"].ToString().Trim();
                string proftax = ds.Tables[0].Rows[0]["proftax"].ToString().Trim();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"department\":\"" + department + "\",\"designation\":\"" + designation + "\",\"empnum\":\"" + empnum + "\",\"dob\":\"" + dob + "\",\"doj\":\"" + doJ + "\",\"workingdays\":\"" + workingdays + "\",\"paybledays\":\"" + paybledays + "\",\"pfno\":\"" + pfno + "\",\"basic\":\"" + basic + "\",\"hra\":\"" + hra + "\",\"da\":\"" + da + "\",\"pf\":\"" + pf + "\",\"ca\":\"" + ca + "\",\"esi\":\"" + esi + "\",\"ea\":\"" + ea + "\",\"asa\":\"" + asa + "\",\"it\":\"" + it + "\",\"LoseofPay\":\"" + paydayd + "\",\"total\":\"" + total + "\",\"deducation\":\"" + deducation + "\",\"ttotal\":\"" + ttotal + "\",\"amountwords\":\"" + amountwords + "\",\"mobilebill\":\"" + mobile + "\",\"travel\":\"" + travel + "\",\"cha\":\"" + child + "\",\"s.a\":\"" + special + "\",\"pt\":\"" + proftax + "\"}");
            }

            else
            {
               Response.Write("{\"error\": True ,\"message\":\"No Records Found\"}");
            }
        }

        protected void leavetypes()
        {
            DataSet ds1;
            string empid = Request.Params["employee_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select * from Employees where DateAdd(yy,1,DOJ)<GETDATE() and EmployeeId='" + empid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                ds1 = DataQueries.SelectCommon("Select l.LeaveTypeName as leavename from LeaveTypes l inner join Employees e on e.emptypeid=l.typeid where e.EmployeeId='" + empid + "'");
            }
            else
            {
                ds1 = DataQueries.SelectCommon("Select l.LeaveTypeName as leavename from LeaveTypes l inner join Employees e on e.emptypeid=l.typeid where l.LeaveTypeName!='PL/EL Leaves' and e.EmployeeId='" + empid + "'");
            }
            DataTable table = ds1.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void applyleave()
        {
            string ErrorMessage = ""; string status; string noofdays = "";
            string lblmsg = string.Empty;
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string leave_type = Request.Params["leave_type"].ToString();
            string begin_date = Request.Params["begin_date"].ToString();
            string end_date = Request.Params["end_date"].ToString();
            string reason = Request.Params["reason"].ToString();
            string employee_id = Request.Params["employee_id"].ToString();
            int empID = Convert.ToInt32(employee_id);

            int employid = 0;
            DataSet ds1234 = DataQueries.SelectCommon("Select deptid from Employees where employeeid='" + employee_id + "'");
            if (ds1234.Tables[0].Rows.Count > 0)
            {
                string deptid = ds1234.Tables[0].Rows[0]["deptid"].ToString();
                DataSet ds1 = DataQueries.SelectCommon("Select ManagerId from DeptMaster where deptid='" + deptid + "'");
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    string empid12 = ds1.Tables[0].Rows[0]["ManagerId"].ToString();
                    employid = int.Parse(empid12.ToString());
                }
            }

            DataSet ds123 = DataQueries.SelectCommon("Select EmpType from Employees where EmployeeId='" + employee_id + "'");
            string type = ds123.Tables[0].Rows[0]["EmpType"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + employee_id + "'");
            if (ds.Tables[0].Rows.Count != 0)
            {
                status = "OnHRDesk";
            }
            else
            {
                status = "OnManagerDesk";
            }
            DateTime FromYear = Convert.ToDateTime(begin_date);
            DateTime staringdate = FromYear;
            DateTime ToYear = Convert.ToDateTime(end_date);
            DateTime endingdate = ToYear;
            endingdate = endingdate.AddDays(1);
            TimeSpan objTimeSpan = ToYear - FromYear;
            int duration = 0;
            //TotalDays  
            double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
            if (leave_type == "Casual Leaves")
            {
                if (begin_date != "" && end_date != "")
                {
                    if (Days > 0)
                    {
                        if (FromYear.Year != ToYear.Year)
                        {

                            ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        }
                        //else if ((FromYear < System.DateTime.Now || ToYear < System.DateTime.Now) && leave_type != "Sick leave")
                        //{
                        //    ErrorMessage += "Wrong selection of Leave Date..!";
                        //    Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        //}
                        else
                        {
                            DateTime start = DateTime.Parse(begin_date);
                            DateTime end = DateTime.Parse(end_date);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                            //while (staringdate != endingdate)
                            //{
                                if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    if (Holidays.Tables[0].Rows.Count == 0)
                                    { duration++; }
                                    else
                                    {
                                        for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                        {
                                            string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                            DateTime s1 = DateTime.Parse(s.ToString());
                                            DateTime s2 = DateTime.Parse(s.ToString());
                                            if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                            {
                                                duration++;
                                            }

                                        }
                                    }
                                }
                                staringdate = staringdate.AddDays(1);
                            //}
                            staringdate = FromYear;
                            for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                            {
                                string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                DateTime s1 = DateTime.Parse(s.ToString());
                                DateTime s2 = DateTime.Parse(s.ToString());
                                s1 = s1.AddDays(-1);
                                s2 = s2.AddDays(+1);
                                if (s1 == staringdate)
                                {
                                    lblmsg = "true";

                                }
                                if (s2 == staringdate)
                                {
                                    lblmsg = "true";
                                }

                            }
                            noofdays = Convert.ToString(duration);
                        }
                    }
                    else
                    {

                        ErrorMessage += "Wrong selection of Leave Ending Date..!";
                        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                    }
                }
            }


            else if (leave_type == "Sick Leave")
            {
                if (begin_date != "" && end_date != "")
                {
                    if (Days > 0)
                    {
                        if (FromYear.Year != ToYear.Year)
                        {

                            ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        }
                        //else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                        //{
                        //    ErrorMessage += "Wrong selection of Leave Date..!";
                        //    //Response.Write("<script>alert('Wrong selection of Leave Date..!')</script>");

                        //}
                        else
                        {
                            DateTime start = DateTime.Parse(begin_date);
                            DateTime end = DateTime.Parse(end_date);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                         //   while (staringdate != endingdate)
                          //  {
                                if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    if (Holidays.Tables[0].Rows.Count == 0)
                                    { duration++; }
                                    else
                                    {
                                        for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                        {
                                            string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                            if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                            {
                                                duration++;
                                            }

                                        }
                                    }
                                }
                                staringdate = staringdate.AddDays(1);
                           // }
                            noofdays = Convert.ToString(duration);
                        }
                    }
                    else
                    {

                        ErrorMessage += "Wrong selection of Leave Ending Date..!";
                        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                    }
                }
            }

            else if (leave_type == "PL/EL Leaves")
            {
                if (begin_date != "" && end_date != "")
                {
                    if (Days > 0)
                    {
                        if (FromYear.Year != ToYear.Year)
                        {

                            ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        }
                        //else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                        //{
                        //    ErrorMessage += "Wrong selection of Leave Date..!";
                        //    Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        //}

                        else
                        {
                            DateTime start = DateTime.Parse(begin_date);
                            DateTime end = DateTime.Parse(end_date);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                            while (staringdate != endingdate)
                            {
                                if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    if (Holidays.Tables[0].Rows.Count == 0)
                                    { duration++; }
                                    else
                                    {
                                        for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                        {
                                            string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                            DateTime s1 = DateTime.Parse(s.ToString());
                                            DateTime s2 = DateTime.Parse(s.ToString());
                                            s1 = s1.AddDays(-1);
                                            s2 = s2.AddDays(+1);
                                            if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                            {
                                                duration++;
                                            }
                                            if (s1 == staringdate)
                                            {
                                                //lblmsg.Text = "true";
                                                duration++;
                                            }
                                            if (s2 == staringdate)
                                            {
                                                //lblmsg.Text = "true";
                                                duration++;
                                            }
                                        }
                                    }
                                }
                                staringdate = staringdate.AddDays(1);
                            }

                            noofdays = Convert.ToString(duration);
                        }
                    }
                    else
                    {

                        ErrorMessage += "Wrong selection of Leave Ending Date..!";
                        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                    }
                }
            }
            else if (leave_type == "Compensatory Off")
            {
                if (begin_date != "" && end_date != "")
                {
                    if (Days > 0)
                    {
                        if (FromYear.Year != ToYear.Year)
                        {

                            ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        }
                        //else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                        //{
                        //    ErrorMessage += "Wrong selection of Leave Date..!";
                        //    Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        //}
                        else
                        {
                            DateTime start = DateTime.Parse(begin_date);
                            DateTime end = DateTime.Parse(end_date);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                            //while (staringdate != endingdate)
                            //{
                                if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    if (Holidays.Tables[0].Rows.Count == 0)
                                    { duration++; }
                                    else
                                    {
                                        for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                        {
                                            string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                            DateTime s1 = DateTime.Parse(s.ToString());
                                            DateTime s2 = DateTime.Parse(s.ToString());
                                            if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                            {
                                                duration++;
                                            }

                                        }
                                    }
                                }
                                staringdate = staringdate.AddDays(1);
                            //}
                            staringdate = FromYear;
                            noofdays = Convert.ToString(duration);
                        }
                    }
                    else
                    {

                        ErrorMessage += "Wrong selection of Leave Ending Date..!";
                        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                    }

                }
            }
            else if (leave_type == "Loss Without Pay")
            {
                if (begin_date != "" && end_date != "")
                {
                    if (Days > 0)
                    {
                        if (FromYear.Year != ToYear.Year)
                        {

                            ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        }
                        //else if (FromYear < System.DateTime.Now || ToYear < System.DateTime.Now)
                        //{
                        //    ErrorMessage += "Wrong selection of Leave Date..!";
                        //    Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                        //}
                        else
                        {
                            DateTime start = DateTime.Parse(begin_date);
                            DateTime end = DateTime.Parse(end_date);
                            DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + start.ToString("yyyy-MM-dd") + "' and '" + end.ToString("yyyy-MM-dd") + "'");

                            //while (staringdate != endingdate)
                           // {
                                if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    if (Holidays.Tables[0].Rows.Count == 0)
                                    { duration++; }
                                    else
                                    {
                                        for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                        {
                                            string s = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                            DateTime s1 = DateTime.Parse(s.ToString());
                                            DateTime s2 = DateTime.Parse(s.ToString());
                                            if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                            {
                                                duration++;
                                            }

                                        }
                                    }
                                }
                                staringdate = staringdate.AddDays(1);
//}
                            staringdate = FromYear;
                            noofdays = Convert.ToString(duration);
                        }
                    }
                    else
                    {

                        ErrorMessage += "Wrong selection of Leave Ending Date..!";
                        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                    }
                }
            }
            if (ErrorMessage == "")
            {
                DateTime start1 = DateTime.Parse(begin_date);
                Double count = 0;
                Double days = DataQueries.SelectCount("Select Days from LeaveTypes where LeaveTypeName='" + leave_type + "' and type='" + type + "'");
                if (leave_type == "Casual Leaves")
                {
                    count = DataQueries.SelectCount("Select Sum(cl) from LeavesStatus where EmployeedID='" + employee_id + "' and status='Approved' and Year(FromDate)=Year('" + start1.ToString("yyyy-MM-dd") + "')");
                    count = days - count;
                    //return count;
                }
                else if (leave_type == "Sick Leave")
                {
                    count = DataQueries.SelectCount("Select Sum(Sl) from LeavesStatus where EmployeedID='" + employee_id + "' and status='Approved'  and Year(FromDate)=Year('" + start1.ToString("yyyy-MM-dd") + "')");
                    count = days - count;
                }
                else if (leave_type == "PL/EL Leaves")
                {
                    count = DataQueries.SelectCount("Select Sum(PL) from LeavesStatus where EmployeedID='" + employee_id + "' and status='Approved' and Year(FromDate)=Year('" + start1.ToString("yyyy-MM-dd") + "')");
                    count = days - count;
                }
                else if (leave_type == "Compensatory Off")
                {
                    count = DataQueries.SelectCount("select sum(CL) from tbl_compensatoryoff where EmployeeId='" + employee_id + "' and YEAR(Worked_date)=YEAR('" + start1.ToString("yyyy-MM-dd") + "') and Worked_date>= DateAdd(DAy,-30,Getdate())");
                    //count = days - count;

                    double count1 = DataQueries.SelectCount("Select Sum(COF) from LeavesStatus where EmployeedID='" + employee_id + "' and Status='Approved' and Year(FromDate)=Year('" + start1.ToString("yyyy-MM-dd") + "') and FromDate>=DateAdd(DAy,-30,Getdate())");
                    if (count1 == 0)
                    {
                        count = count;
                    }
                    else
                    {
                        count = count1 - count;
                    }
                }
                else if (leave_type == "Loss Without Pay")
                {
                    count = 0;
                }

                //if (leave_type == "Casual Leaves")
                //{
                //    if (Convert.ToInt16(noofdays) > count)
                //    {
                //        string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and l.LeaveType='Casual Leaves' order by l.LeaveId";
                //        Session["isApply"] = "false";
                //        DataSet ds12 = DataQueries.SelectCommon(str);
                //        if (ds12.Tables[0].Rows.Count != 0)
                //        {
                //            ErrorMessage = "Sorry..! You can Apply Only " + days + " Casual Leaves Per Year..!";
                //            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //        }
                //    }

                //    string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='Casual Leaves'";
                //    DataSet ds1 = DataQueries.SelectCommon(s);
                //    if (ds1.Tables[0].Rows.Count != 0)
                //    {
                //        ErrorMessage = "Already Pending Leave,You can't Apply..!";
                //        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //    }
                //}
                //else if (leave_type == "Sick Leave")
                //{
                //    if (Convert.ToInt16(noofdays) > count)
                //    {
                //        string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.SL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and l.LeaveType='Sick Leaves' order by l.LeaveId";
                //        Session["isApply"] = "false";
                //        DataSet ds12 = DataQueries.SelectCommon(str);
                //        if (ds12.Tables[0].Rows.Count != 0)
                //        {
                //            ErrorMessage = "Sorry..! You can Apply Only " + days + " Sick Leave Per Year..!";
                //            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //        }
                //    }

                //    string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.SL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='Sick Leaves'";
                //    DataSet ds1 = DataQueries.SelectCommon(s);
                //    if (ds1.Tables[0].Rows.Count != 0)
                //    {
                //        ErrorMessage = "Already Pending Leave,You can't Apply..!";
                //        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //    }
                //}
                //else if (leave_type == "PL/EL Leaves")
                //{
                //    if (Convert.ToInt16(noofdays) > count)
                //    {
                //        string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.PL,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and l.LeaveType='PL/EL Leaves' order by l.LeaveId";
                //        Session["isApply"] = "false";
                //        DataSet ds2 = DataQueries.SelectCommon(str);
                //        if (ds2.Tables[0].Rows.Count != 0)
                //        {
                //            ErrorMessage = "Sorry..! You can Apply Only " + days + " PL/EL Leaves Per Year..!";
                //            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //        }
                //    }

                //    string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.PL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='PL/EL Leaves'";
                //    DataSet ds1 = DataQueries.SelectCommon(s);
                //    if (ds1.Tables[0].Rows.Count != 0)
                //    {
                //        ErrorMessage = "Already Pending Leave,You can't Apply..!";
                //        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //    }

                //}

                //else if (leave_type == "Compensatory Off")
                //{
                //    string balance = count.ToString() + " for Year " + (DateTime.Parse(begin_date).Year).ToString();
                //    if (Convert.ToInt16(noofdays) > count)
                //    {
                //        string str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.PL,l.COF,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and l.LeaveType='Compensatory Off' order by l.LeaveId";
                //        Session["isApply"] = "false";
                //        DataSet ds12 = DataQueries.SelectCommon(str);
                //        if (ds12.Tables[0].Rows.Count != 0)
                //        {
                //            ErrorMessage = "Sorry..! You can Apply Only " + count.ToString() + " Compensatory Off's Per Year..!";
                //            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //        }
                //        else
                //        {
                //            ErrorMessage = "You have only " + count.ToString() + " compensatory off's.You don't have permissions to apply more than that.";
                //            Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //        }
                //    }

                //    string s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.PL,l.COF,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate from Employees e inner join Department d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.EmployeedId='" + employee_id + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and l.LeaveType='Compensatory Off'";
                //    DataSet ds1 = DataQueries.SelectCommon(s);
                //    if (ds1.Tables[0].Rows.Count != 0)
                //    {
                //        ErrorMessage = "Already Pending Leave,You can't Apply..!";
                //        Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                //    }
                //}


                if (ErrorMessage == "")
                {
                    int LOP = 0, CL = 0;
                    Double AlreadyApplied = 0;
                    int apply = Convert.ToInt16(noofdays);
                    DateTime start12 = DateTime.Parse(begin_date);
                    DateTime end12 = DateTime.Parse(end_date);
                    if (leave_type == "Casual Leaves")
                    {
                        AlreadyApplied = DataQueries.SelectCount("select sum(CL) from LeavesStatus where EmployeedID='" + employee_id + "' and Status='Approved' and Year(FromDate)=Year('" + start12.ToString("yyyy-MM-dd") + "')");
                    }
                    else if (leave_type == "Sick Leave")
                    {
                        AlreadyApplied = DataQueries.SelectCount("select sum(SL) from LeavesStatus where EmployeedID='" + employee_id + "' and Status='Approved' and Year(FromDate)=Year('" + start12.ToString("yyyy-MM-dd") + "')");
                    }
                    if (leave_type == "Casual Leaves" || leave_type == "Sick Leave")
                    {
                        int month = DateTime.Parse(end_date).Month;
                        int canapply = month - (int)AlreadyApplied;
                        if (lblmsg == "true")
                        {
                            CL = 0;
                            LOP = int.Parse(noofdays);
                        }
                        else
                        {
                            if (apply > canapply)
                            {
                                LOP = apply - canapply;
                                CL = canapply;
                            }
                            else if (apply > 3)
                            {
                                //CL = apply;
                                LOP = apply - 3;
                                CL = apply - LOP;
                            }
                            else
                            {
                                CL = apply;
                                LOP = 0;
                            }
                        }
                    }
                    else if (leave_type == "PL/EL Leaves")
                    {
                        int month = DateTime.Parse(end_date).Month;
                        if (apply > 21)
                        {
                            LOP = apply - 21;
                            CL = apply - LOP;
                        }
                        else
                        {
                            CL = apply;
                            LOP = 0;
                        }
                    }
                    else if (leave_type == "Compensatory Off")
                    {
                        CL = apply;
                    }
                    else if (leave_type == "Loss Without Pay")
                    {
                        LOP = apply;
                    }
                    if (leave_type == "Casual Leaves")
                    {
                        DataQueries.InsertCommon("insert into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start12.ToString("yyyy-MM-dd") + "','" + end12.ToString("yyyy-MM-dd") + "','" + noofdays + "','" + count + "','" + reason + "','" + status + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + CL + "','" + LOP + "','','0','" + leave_type + "','0','0','" + type + "')");
                        //Response.Write("<script>alert('Data inserted successfully')</script>");
                        Response.Write("{\"error\": false,\"status\":\"leave applied successfully\"}");
                        //Response.Redirect("ApplyLeave.aspx");


                        //Employee push notification
                        string emp_title = "Leave Request Sent";
                        string emp_body = "Your Request for Leave Submitted Succesfully";
                        string emp_token = token(empID);
                        notification(emp_token, emp_title, emp_body);

                        //department push notifications
                        string emp_name1 = employee_name(empID);
                        string dept_title = "New Leave Request";
                        string dept_body = emp_name1 + "Submitted Leave Request";
                        string dept_token = token(employid);
                        notification(dept_token, dept_title, dept_body);

                    }
                    else if (leave_type == "Sick Leave")
                    {
                        DataQueries.InsertCommon("insert into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,COF,type,PL) values('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start12.ToString("yyyy-MM-dd") + "','" + end12.ToString("yyyy-MM-dd") + "','" + noofdays + "','" + count + "','" + reason + "','" + status + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','" + CL + "','" + leave_type + "','0','" + type + "','0')");
                        Response.Write("{\"error\": false,\"status\":\"leave applied successfully\"}");

                        //Employee push notification
                        string emp_title = "Leave Request Sent";
                        string emp_body = "Your Request for Leave Submitted Succesfully";
                        string emp_token = token(empID);
                        notification(emp_token, emp_title, emp_body);

                        //department push notifications
                        string emp_name1 = employee_name(empID);
                        string dept_title = "New Leave Request";
                        string dept_body = emp_name1 + "Submitted Leave Request";
                        string dept_token = token(employid);
                        notification(dept_token, dept_title, dept_body);
                    }
                    else if (leave_type == "PL/EL Leaves")
                    {
                        DataQueries.InsertCommon("Insert Into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values ('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start12.ToString("yyyy-MM-dd") + "','" + end12.ToString("yyyy-MM-dd") + "','" + noofdays + "','" + count + "','" + reason + "','" + status + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','0','" + leave_type + "','" + CL + "','0','" + type + "')");
                        Response.Write("{\"error\": false,\"status\":\"leave applied successfully\"}");
                        //Employee push notification
                        string emp_title = "Leave Request Sent";
                        string emp_body = "Your Request for Leave Submitted Succesfully";
                        string emp_token = token(empID);
                        notification(emp_token, emp_title, emp_body);

                        //department push notifications
                        string emp_name1 = employee_name(empID);
                        string dept_title = "New Leave Request";
                        string dept_body = emp_name1 + "Submitted Leave Request";
                        string dept_token = token(employid);
                        notification(dept_token, dept_title, dept_body);

                    }
                    else if (leave_type == "Compensatory Off")
                    {
                        DataQueries.InsertCommon("Insert Into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values ('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start12.ToString("yyyy-MM-dd") + "','" + end12.ToString("yyyy-MM-dd") + "','" + noofdays + "','" + count + "','" + reason + "','" + status + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','0','" + leave_type + "','0','" + CL + "','" + type + "')");
                        Response.Write("{\"error\": false,\"status\":\"leave applied successfully\"}");

                        //Employee push notification
                        string emp_title = "Leave Request Sent";
                        string emp_body = "Your Request for Leave Submitted Succesfully";
                        string emp_token = token(empID);
                        notification(emp_token, emp_title, emp_body);

                        //department push notifications
                        string emp_name1 = employee_name(empID);
                        string dept_title = "New Leave Request";
                        string dept_body = emp_name1 + "Submitted Leave Request";
                        string dept_token = token(employid);
                        notification(dept_token, dept_title, dept_body);
                    }

                    else if (leave_type == "Loss Without Pay")
                    {
                        DataQueries.InsertCommon("Insert Into LeavesStatus(EmployeedID,AppliedDate,FromDate,ToDate,Duration,BalanceLeaves,ReasontoApply,Status,StatusReason,StatusDate,CL,LOP,approvedby,SL,LeaveType,PL,COF,type) values ('" + employee_id + "','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + start12.ToString("yyyy-MM-dd") + "','" + end12.ToString("yyyy-MM-dd") + "','" + noofdays + "','" + count + "','" + reason + "','" + status + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','0','" + LOP + "','','0','" + leave_type + "','0','0','" + type + "')");
                        Response.Write("{\"error\": false,\"status\":\"leave applied successfully\"}");

                        //Employee push notification
                        string emp_title = "Leave Request Sent";
                        string emp_body = "Your Request for Leave Submitted Succesfully";
                        string emp_token = token(empID);
                        notification(emp_token, emp_title, emp_body);

                        //department push notifications
                        string emp_name1 = employee_name(empID);
                        string dept_title = "New Leave Request";
                        string dept_body = emp_name1 + "Submitted Leave Request";
                        string dept_token = token(employid);
                        notification(dept_token, dept_title, dept_body);
                    }
                }
            }
        }

        protected void leaveslist()
        {
            DataSet ds;
            string employee_id = Request.Params["employee_id"].ToString();
            string desk = Request.Params["Desk"].ToString();
            DataSet ds1 = DataQueries.SelectCommon("Select e.DeptId from DeptMaster d  inner join Employees e on e.EmployeeId=d.ManagerId where d.ManagerId='" + employee_id + "'");

            if (ds1.Tables[0].Rows.Count != 0)
            {
                string deptid = ds1.Tables[0].Rows[0]["DeptId"].ToString();

                // if (deptid == "4")
                if (desk == "OnHRDesk")
                {
                    ds = DataQueries.SelectCommon("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,l.empreplace as Replace,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID inner join DeptMaster d on d.DeptId=e.DeptId where l.Status='OnHRDesk'");

                }
                else
                {
                    ds = DataQueries.SelectCommon("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID inner join DeptMaster d on d.DeptId=e.DeptId where l.Status='OnManagerDesk' and d.ManagerId='" + employee_id + "'");
                }
                DataTable table = ds.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }
            else
            {
                DataSet ds12 = DataQueries.SelectCommon("Select l.LeaveId,l.EmployeedID,l.AppliedDate,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.Status,l.CL,l.LOP,l.SL,l.PL,l.COF,l.LeaveType,l.empreplace,e.EmployeName from LeavesStatus l inner join Employees e on e.EmployeeId=l.EmployeedID where l.EmployeedID='" + employee_id + "'");
                DataTable table = ds12.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }
        }
        protected void employeelist()
        {
            string employee_id = Request.Params["employee_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select deptid from Employees where EmployeeId='" + employee_id + "'");
            if (ds.Tables[0].Rows.Count != 0)
            {
                string deptid = ds.Tables[0].Rows[0]["deptid"].ToString();
                DataSet ds1 = DataQueries.SelectCommon("Select EmployeName as employeename from Employees where deptId='" + deptid + "' and employeeid='" + employee_id + "' and Status='1'");
                DataTable table = ds1.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                string result = "{\"EmployeeName\": " + JSONresult + "}";
                Response.Write(result);
            }
        }
        protected void mark_leave()
        {
            string sqlstr = "";
            string hr = Request.Params["hr"].ToString();
            string leave_id = Request.Params["leave_id"].ToString();
            string repempname = Request.Params["employee_name"].ToString();

            DataSet ds12 = DataQueries.SelectCommon("Select EmployeedID from LeavesStatus where LeaveId='" + leave_id + "'");
            string employee = ds12.Tables[0].Rows[0]["EmployeedID"].ToString();
            int empID = int.Parse(employee.ToString());

            DataSet ds123 = DataQueries.SelectCommon("Select EmployeeId from Employees where EmployeName='" + repempname + "'");
            string employee12 = ds123.Tables[0].Rows[0]["EmployeeId"].ToString();
            int empID12 = int.Parse(employee12.ToString());

            DataSet ds = DataQueries.SelectCommon("Select Status,BalanceLeaves,Duration from LeavesStatus where LeaveId='" + leave_id + "'");
            string status = ds.Tables[0].Rows[0][0].ToString().Trim();
            int bal = int.Parse(ds.Tables[0].Rows[0][1].ToString());
            int LeaveDuration = int.Parse(ds.Tables[0].Rows[0][2].ToString());

            if (status == "OnManagerDesk")
            {
                sqlstr = "update LeavesStatus set Status='OnHRDesk',empreplace='" + repempname + "',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + leave_id + "'";
                DataQueries.UpdateCommon(sqlstr);
                Response.Write("{\"error\": false,\"status\":\"your leave is on HR Desk\"}");

                DataSet approve = DataQueries.SelectCommon("Select l.LeaveId,convert(varchar,l.FromDate,113) as FromDate,convert(varchar,l.ToDate,113) as ToDate,e.EmployeName,s.BeginTime,s.EndTime from LeavesStatus l inner join Employees e on l.EmployeedID=e.EmployeeId inner join Shifts s on e.shift=s.ShiftId where l.empreplace='" + repempname + "' and l.Status='Approved' and l.LeaveId='" + leave_id + "' order by l.LeaveId desc");
                string fromdate = approve.Tables[0].Rows[0]["FromDate"].ToString();
                string todate = approve.Tables[0].Rows[0]["ToDate"].ToString();
                string employee_name1 = approve.Tables[0].Rows[0]["EmployeName"].ToString();
                string begindate = approve.Tables[0].Rows[0]["BeginTime"].ToString();
                string enddate = approve.Tables[0].Rows[0]["EndTime"].ToString();

                //Employee push notification
                string emp_title = "Leave Request Sent";
                string emp_body = "Your leave request is on HR desk";
                string emp_token = token(empID);
                notification(emp_token, emp_title, emp_body);

                // HR push notification
                string emp_name = employee_name(empID);
                string hr_title = "New Leave Request";
                string hr_body = emp_name + "leave request is on your desk";
                string hr_token = token(20180515);
                notification(hr_token, hr_title, hr_body);

                // Employee Replace notification

                string emp_title1 = "Employee Replace Order";
                string emp_body1 = "Mr/Ms." + repempname + " is on the leave,So you have to work in this following duration '" + fromdate + "' to '" + todate + "' with in the time span of '" + begindate + "' to '" + enddate + "'";
                string emp_token1 = emprep_token(empID12);
                notification(emp_token1, emp_title1, emp_body1);

            }
            //else if (status == "OnHRDesk")
            //{
            //    sqlstr = "update LeavesStatus set Status='Approved',BalanceLeaves='" + (bal - LeaveDuration) + "',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + leave_id + "'";
            //    DataQueries.UpdateCommon(sqlstr);
            //    Response.Write("{\"error\": false,\"status\":\"leave approved successfully\"}");

            //    // Employee Notification
            //    string hr_title = "Request Accepted";
            //    string hr_body = "Your Leave Request Accepted";
            //    string emp_token = token(empID);
            //    notification(emp_token, hr_title, hr_body);
            //}


        }
        //protected void updateleavestatusreject()
        //{
        //    string employee_id = Request.Params["hr"].ToString();
        //    string leave_id = Request.Params["leave_id"].ToString();
        //    DataSet ds = DataQueries.SelectCommon("Select EmployeedId,ReasontoApply from LeavesStatus where LeaveId='" + leave_id + "'");
        //    string Empployee = ds.Tables[0].Rows[0]["EmployeedId"].ToString();
        //    string reason = ds.Tables[0].Rows[0]["ReasontoApply"].ToString();
        //    string admin = Request.Params["admin"].ToString();
        //    int empID = Convert.ToInt32(Empployee);
        //    DataQueries.UpdateCommon("Update LeavesStatus set Status='Rejected',approvedby='" + employee_id + "' where LeaveId='" + leave_id + "'");
        //    Response.Write("{\"error\": false,\"status\":\"updated successfully\"}");
        //    EmployeeOperation operation = new EmployeeOperation();
        //    EmployeeItem employee = operation.getEmployee(empID);
        //    Notification notification1 = new Notification();
        //    notification1.title = "Leave Rejected successfully";
        //    notification1.body = reason;
        //    notification1.code = 13;
        //    emp_tokens(Empployee, notification1);

        //}
        protected void hrmark_leave1()
        {

            string sqlstr = "";
            string hr = Request.Params["hr"].ToString();
            string leave_id = Request.Params["leave_id"].ToString();
            DataSet ds12 = DataQueries.SelectCommon("Select EmployeedID from LeavesStatus where LeaveId='" + leave_id + "'");
            string employee = ds12.Tables[0].Rows[0]["EmployeedID"].ToString();
            int empID = int.Parse(employee.ToString());

            DataSet ds = DataQueries.SelectCommon("Select Status,BalanceLeaves,Duration from LeavesStatus where LeaveId='" + leave_id + "'");
            string status = ds.Tables[0].Rows[0][0].ToString().Trim();
            int bal = int.Parse(ds.Tables[0].Rows[0][1].ToString());
            int LeaveDuration = int.Parse(ds.Tables[0].Rows[0][2].ToString());


            sqlstr = "update LeavesStatus set Status='Rejected',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + leave_id + "'";
            DataQueries.UpdateCommon(sqlstr);
            Response.Write("{\"error\": false,\"status\":\"leave Rejected\"}");

            // Employee Notification
            string hr_title = "Request Rejected";
            string hr_body = "Your Leave Request Rejected";
            string emp_token = token(empID);
            notification(emp_token, hr_title, hr_body);

        }
        protected void hrmark_leave()
        {
            string sqlstr = "";
            string hr = Request.Params["hr"].ToString();
            string leave_id = Request.Params["leave_id"].ToString();
            DataSet ds12 = DataQueries.SelectCommon("Select EmployeedID from LeavesStatus where LeaveId='" + leave_id + "'");
            string employee = ds12.Tables[0].Rows[0]["EmployeedID"].ToString();
            int empID = int.Parse(employee.ToString());

            DataSet ds = DataQueries.SelectCommon("Select Status,BalanceLeaves,Duration from LeavesStatus where LeaveId='" + leave_id + "'");
            string status = ds.Tables[0].Rows[0][0].ToString().Trim();
            int bal = int.Parse(ds.Tables[0].Rows[0][1].ToString());
            int LeaveDuration = int.Parse(ds.Tables[0].Rows[0][2].ToString());

            if (status == "OnHRDesk")
            {
                sqlstr = "update LeavesStatus set Status='Approved',BalanceLeaves='" + (bal - LeaveDuration) + "',StatusDate='" + System.DateTime.Now.ToString("yyyy-MM-dd") + "' where LeaveId='" + leave_id + "'";
                DataQueries.UpdateCommon(sqlstr);
                Response.Write("{\"error\": false,\"status\":\"leave approved successfully\"}");

                // Employee Notification
                string hr_title = "Request Accepted";
                string hr_body = "Your Leave Request Accepted";
                string emp_token = token(empID);
                notification(emp_token, hr_title, hr_body);
            }

        }
        protected void profileimage()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select [Empimage] from employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
               byte[] imgBytes = (byte[])ds.Tables[0].Rows[0]["EmpImage"];

                 // If you want convert to a bitmap file
                 TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                 Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(imgBytes);
                 string imgString = Convert.ToBase64String(imgBytes);
                //string image = ds.Tables[0].Rows[0]["image"].ToString();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"Photo\":\"" + imgString + "\" }");

            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"No Image Found\"}");
            }
        }

        #endregion

        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }

        public string employee_name(int employee_id)
        {
            DataSet ds = DataQueries.SelectCommon("select EmployeName from Employees where employeeid='" + employee_id + "'");
            return (ds.Tables[0].Rows[0]["EmployeName"].ToString());
        }

        public string token(int employee_id)
        {
            DataSet ds = DataQueries.SelectCommon("select token from Employees where employeeid='" + employee_id + "'");
            //string token = "euvkXYdHrcI:APA91bGuFu_qYNfj8UXsJR9-6l3pqjQsYb0_XIRJ9LU5tWSGL1v7moICPjLKn19dJ4Q0vuSMdEDi3bSb46JYmIA47GafQt1GlZ3P-zKxfBkerUZh-dbBld1tBGAS0Bfp0tdbIBStwQ_Q";
            return (ds.Tables[0].Rows[0]["token"].ToString());
            //return (token.ToString());
        }


        public string hr_token()
        {
            DataSet ds = DataQueries.SelectCommon("select token from Employees where employeeid='20170908'");
            return (ds.Tables[0].Rows[0]["token"].ToString());
        }

        public string dept_token(int employee_id)
        {
            DataSet ds = DataQueries.SelectCommon("Select token from Employees where employeeid='" + employee_id + "'");
            return (ds.Tables[0].Rows[0]["token"].ToString());

        }

        public string emprep_token(int employee_id)
        {
            DataSet ds = DataQueries.SelectCommon("Select token from Employees where employeeid='" + employee_id + "'");
            return (ds.Tables[0].Rows[0]["token"].ToString());
        }

        public void notification(string emp_token, string title, string body)
        {
            Notification notification = new Notification();
            notification.title = title;
            notification.body = body;
            //notification.image = path;
            notification.code = 13;
            Message msg = new Message();
            msg.sendSingleNotification(emp_token, notification);
        }

        public void sendOTP(string mobile, int OTP)
        {
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "CHERLAHEALTH";
            String passwd = "215598";
            string smstext = "Welcome to Cherla Health Pvt Ltd..! Your OTP is :" + OTP.ToString();
            String url = "http://smsc.essms.com/smpp/?username=" + userid + "&password=" + passwd + "&from=CHERLA&to=91" + mobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
        }

        private void hr_tokens(Notification notification)
        {
            DataSet tokens = DataQueries.SelectCommon("select * from Employees where status='1' ");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;


            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }

        protected void broadcast()
        {
            string title = Request.Params["title"].ToString().Trim();
            string description = Request.Params["description"].ToString().Trim();
            DateTime dt = DateTime.Now;
            string date = dt.ToString("yyyy-MM-dd HH:mm:ss.000");
            DataQueries.InsertCommon("Insert into notifications(date,title,description) values ('" + date + "','" + title + "','" + description + "')");

            Notification notification = new Notification();

            notification.title = title;
            notification.body = description;
            notification.code = 13;
            hr_tokens(notification);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }

        protected void req_price()
        {
            string ptype = Request.Params["ptype"].ToString();
            string product = Request.Params["product"].ToString();
            string model = Request.Params["model"].ToString();
            string price = Request.Params["price"].ToString();
            string qnty = Request.Params["qty"].ToString();
            string empid = Request.Params["empid"].ToString();
            string custid = Request.Params["cid"].ToString();
            DataQueries.InsertCommon("insert into quote (ptype,product,model,price,qty,empid,cid) values('"+ptype+"','"+product+"','"+model+"','"+price+"','"+qnty+"','"+empid+"','"+custid+"')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void list_data()
        {
            string empid = Request.Params["empid"].ToString();
            string cid = Request.Params["cid"].ToString();
            string subtot = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select *,cast(cast(price as float)*cast(qty as float)  as varchar) as tot from quote where cid='" + cid+"' and empid='"+empid+"'");
            DataTable table = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(table);
            DataSet dss = DataQueries.SelectCommon("select sum(cast(price as float)*cast(qty as float)) as subtotal from quote where cid='" + cid + "' and empid='" + empid + "'");
            if(dss.Tables[0].Rows.Count>0)
            {
                subtot = dss.Tables[0].Rows[0]["subtotal"].ToString();
            }
            Response.Write("{\"subtotal\": \"" + subtot + "\",\"data\":"+json+"}");
            
        }
    }
}