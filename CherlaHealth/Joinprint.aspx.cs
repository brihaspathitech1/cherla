﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class Joinprint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // lblempname.Text = Session["custname"].ToString();
            lbljdate2.Text= Session["jdate"].ToString();
            lbljdate1.Text= Session["txt3"].ToString();
          //  lbljdate2.Text= Session["txt1"].ToString();
            Ltime.Text= Session["tme"].ToString();
            desc.Text= Session["des"].ToString();
            lbladd.Text = Session["txt2"].ToString();

        }
        protected void pdf_Click(object sender, EventArgs e)
        {
            //Image1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Joining Report.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            string imagePath = Server.MapPath("sign board.jpg") + "";
            //for (int i = 0; i < 1; i++)
            //{
            //    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            //    image.Alignment = Element.ALIGN_TOP;

            //    image.SetAbsolutePosition(40, 700);

            //    image.ScaleToFit(500f, 400f);
            //    image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            //    pdfDoc.Add(image);
                htmlparser.Parse(sr);
           // }
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
    }
}