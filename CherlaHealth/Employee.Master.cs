﻿using inventory.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CherlaHealth
{
    public partial class Employee : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["name"].ToString() == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                SqlConnection conn = new SqlConnection(connstrg);
                if (Session["name"].ToString() == "ADMIN")
                {
                    SqlCommand cmd = new SqlCommand("Select * from Login where Username='" + Session["name"].ToString() + "' and Password='" + Session["Password"].ToString() + "'", conn);
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        Label1.Text = dr["Name"].ToString();
                    }
                    conn.Close();
                    link12.Visible = false;
                }
                else
                {
                    
                    
                    SqlCommand cmd = new SqlCommand("Select * from Employees where EmployeeId='" + Session["name"].ToString() + "' and Password='" + Session["Password"].ToString() + "'", conn);
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        Label1.Text = dr["EmployeName"].ToString();
                    }
                    conn.Close();

                    DataSet ds1 = DataQueries.SelectCommon("Select d.DeptId from DeptMaster d  inner join Employees e on e.EmployeeId=d.ManagerId where d.ManagerId='" + Session["name"].ToString() + "'");
                    //string deptid = ds1.Tables[0].Rows[0]["DeptId"].ToString();
                    if(ds1.Tables[0].Rows.Count>0)
                    {
                        link1.Visible = true;
                        a2.Visible = true;
                        a1.Visible = false;
                    }
                    else
                    {
                        link1.Visible = false;
                        a1.Visible = true;
                        a2.Visible = false;
                    }
                }
                FillgvLeavesList();
                notifications();
            }
        }

        private void FillgvLeavesList()
        {
            if (Session["name"].ToString() == "ADMIN")
            {
                //try
                //{
                    DataSet ds = DataQueries.SelectCommon("Select deptid from DeptMaster where ManagerId='" + Session["EmpId"].ToString() + "'");
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        string s;
                        if (ds.Tables[0].Rows[0][0].ToString() == "4")
                        {
                            s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                            s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                            s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnHRDesk'";
                        }
                        else
                        {
                            //s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                            //s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                            //s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnManagerDesk'";

                            s = "with cte as (select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId,ROW_NUMBER() over (PARTITION by l.leaveId order by LeaveId desc) as rn from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnManagerDesk') select * from cte where rn=1";
                        }
                       

                        DataSet ds1 = DataQueries.SelectCommon(s);
                        int count = ds1.Tables[0].Rows.Count;

                        lblLeavesCount.Text = "You have " + count.ToString() + " Leaves Pending";
                        lblcount.Text = count.ToString();

                    }
                    else
                    {
                        string s;

                        s = "select LeaveId from LeavesStatus where EmployeedID='" + Session["name"].ToString().Trim() + "' and Status='Approved' and StatusDate=convert(date,getdate())";

                        DataSet ds1 = DataQueries.SelectCommon(s);
                        int count = ds1.Tables[0].Rows.Count;

                        lblLeavesCount.Text = "Congrats,Your " + count.ToString() + " Leave Approved";
                        lblcount.Text = count.ToString();
                    }

                //}
                //catch
                //{
                //    Response.Redirect("Default.aspx");
                //}
            }
            else
            {
                try
                {
                    DataSet ds = DataQueries.SelectCommon("Select deptid from DeptMaster where ManagerId='" + Session["name"].ToString() + "'");
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        string s;
                        if (ds.Tables[0].Rows[0][0].ToString() == "4")
                        {
                            s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                            s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                            s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnHRDesk'";
                        }
                        else if(ds.Tables[0].Rows[0][0].ToString() != "4")
                        {
                            s = "with cte as (select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId,ROW_NUMBER() over (PARTITION by l.leaveId order by LeaveId desc) as rn from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where l.Status='OnManagerDesk') select * from cte where rn=1";
                        }
                        else
                        {
                            
                                s = "select e.EmployeName,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.LeaveId";
                                s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId";
                                s += " inner join LeavesStatus l on l.EmployeedID = e.EmployeeId where d.ManagerId='" + Session["name"].ToString() + "' and l.Status='OnManagerDesk'";
                            
                        }
                        DataSet ds1 = DataQueries.SelectCommon(s);
                        int count = ds1.Tables[0].Rows.Count;

                        lblLeavesCount.Text = "You have " + count.ToString() + " Leaves Pending";
                        lblcount.Text = count.ToString();
                    }
                    else
                    {
                        string s;

                        s = "select LeaveId from LeavesStatus where EmployeedID='" + Session["name"].ToString().Trim() + "' and Status='Approved' and StatusDate=convert(date,getdate())";

                        DataSet ds1 = DataQueries.SelectCommon(s);
                        int count = ds1.Tables[0].Rows.Count;

                        lblLeavesCount.Text = "Congrats,Your " + count.ToString() + " Leave Approved";
                        lblcount.Text = count.ToString();

                      
                    }
                }
                catch
                {
                    Response.Redirect("Default.aspx");
                }
            }

        }

        protected void notifications()
        {
            string s1, s2;
            if (Session["name"].ToString() != "ADMIN")
            {
                s1 = "select EmployeName from Employees where EmployeeId='" + Session["name"].ToString().Trim() + "'";
                DataSet ds12 = DataQueries.SelectCommon(s1);
                string name = ds12.Tables[0].Rows[0]["EmployeName"].ToString();

                s2 = "Select * from LeavesStatus where empreplace='" + name + "' and Status='Approved'";
                DataSet ds123 = DataQueries.SelectCommon(s2);
                int count2 = ds123.Tables[0].Rows.Count;

                lblnotification.Text = "You have " + count2.ToString() + "  notifications";
                lblcount1.Text = count2.ToString();
            }
        }
    }
}