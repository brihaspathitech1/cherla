﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee.Master" AutoEventWireup="true" CodeBehind="ApprovedLeaves.aspx.cs" Inherits="CherlaHealth.ApprovedLeaves" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Applied Leaves</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <asp:GridView ID="gvlistofLeaves" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="LeaveID">
                    <Columns>
                        
                        <asp:BoundField DataField="EmployeName" HeaderText="Employee" />
                        <asp:BoundField DataField="LeaveId" HeaderText="LeaveId" Visible="False" />
                        <asp:BoundField DataField="AppliedDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Applied Date" />
                        <asp:BoundField DataField="CL" HeaderText="CL" />
                        <asp:BoundField DataField="SL" HeaderText="SL" />
                        <asp:BoundField DataField="Pl" HeaderText="PL/EL" />
                        <asp:BoundField DataField="COF" HeaderText="Compensatory Off" />
                        <asp:BoundField DataField="LOP" HeaderText="LWP" />
                        <asp:BoundField DataField="LeaveType" HeaderText="Leave Type" />
                        <asp:BoundField DataField="FromDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="From Date" />
                        <asp:BoundField DataField="ToDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="To Date" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="BalanceLeaves" HeaderText="Balance Leaves" />
                        <asp:BoundField DataField="ReasontoApply" HeaderText="Reason to Apply" />
                        <asp:BoundField DataField="Status" HeaderText="Status" />
                        <asp:BoundField DataField="empreplace" HeaderText="Replace Employee" />
                        </Columns>
                    </asp:GridView>
            </div>
            </div>
        </div>

</asp:Content>
