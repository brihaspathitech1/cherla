﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Leaveapproved.aspx.cs" Inherits="CherlaHealth.Leaveapproved" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            height: 26px;
        }

        .tpadleft {
            padding-left: 20px;
        }

        .tpadright {
            padding-right: 20px;
        }

        .form-control1 {
            display: block;
            width: 100%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Employee List</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
               <div class="col-md-12">
                
            </div>
                        <div class="col-md-12" style="padding-top: 15px">

                <div class="table table-responsive" style="overflow: scroll; height: 420px">

                 
                <asp:GridView ID="gvlistofLeaves" class="table table-bordered table-striped table-condensed"
                    runat="server" AutoGenerateColumns="False" DataKeyNames="LeaveID">
                    <Columns>
                        <asp:BoundField DataField="EmployeedID" HeaderText="Employee Id" />
                        <asp:BoundField DataField="EmployeName" HeaderText="Employee" />
                        
                        <asp:BoundField DataField="AppliedDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Applied Date" />
                        <asp:BoundField DataField="CL" HeaderText="Leave Type" />
                        <asp:BoundField DataField="SL" HeaderText="SL" />
                        <asp:BoundField DataField="Pl" HeaderText="PL/EL" />
                        <asp:BoundField DataField="COF" HeaderText="Compensatory Off" />
                        <asp:BoundField DataField="LOP" HeaderText="LWP" />
                        <asp:BoundField DataField="LeaveType" HeaderText="Leave Type" />
                        <asp:BoundField DataField="FromDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="From Date" />
                        <asp:BoundField DataField="ToDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="To Date" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="BalanceLeaves" HeaderText="Balance Leaves" />
                        <asp:BoundField DataField="ReasontoApply" HeaderText="Reason to Apply" />
                        <asp:BoundField DataField="Status" HeaderText="status" />

                      
                     
                               
                            
                          
                    </Columns>
                </asp:GridView>
                      </div>
            </div>
            </div></asp:Content>