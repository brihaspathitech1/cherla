﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmpPayReport.aspx.cs" Inherits="CherlaHealth.EmpPayReport" %>

<!DOCTYPE html>

<html  lang="en">

    <head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Brihaspathi -Biometric Application</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/biometric.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
  <link href="assets/js/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<link href="assets/js/fancybox/jquery.fancybox.css" rel="stylesheet" />
 <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-daterangepicker/daterangepicker.css" />



    <link href="assets/css/table-responsive.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

      <style>
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    padding: 10px 0;
    margin-left: 15px;
    margin-right: 15px;
    border-bottom: 3px solid #2fa3e6;
    border-left: 0;
    border-top: 0;
    border-right: 0;
    border-radius: 0;
}

.borderless td, .borderless th {
    border: none;
}

page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}

}
@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }


    .pagebreak {page-break-after: always;}

}
.borderless td, .borderless th {
    border: none;
}
.hritem{margin-top: 0px;margin-bottom: 9px;border: 0; border-top: 1px solid #d6f4ff;}
table {border: none;}
@media print {

.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
  float: left;
}
.col-sm-12 {
  width: 100%;
}
.col-sm-11 {
  width: 91.66666666666666%;
}
.col-sm-10 {
  width: 83.33333333333334%;
}
.col-sm-9 {
  width: 75%;
}
.col-sm-8 {
  width: 66.66666666666666%;
}
.col-sm-7 {
  width: 58.333333333333336%;
}
.col-sm-6 {
  width: 50%;
}
.col-sm-5 {
  width: 41.66666666666667%;
}
.col-sm-4 {
  width: 33.33333333333333%;
 }
 .col-sm-3 {
   width: 25%;
 }
 .col-sm-2 {
   width: 16.666666666666664%;
 }
 .col-sm-1 {
  width: 8.333333333333332%;
 }
 
 #btnPrint{  visibility:hidden}
table {border: none;}
 
 
 .vendorListHeading{
            background: #1a4567 !important; 
            color: white;}
       
		  
  .vendorListHeading th{
           color:white;
          }
		  
 }

 



  .vendorListHeading{
            background: #1a4567 !important; 
            color: white;
          }
		  
  .vendorListHeading th{
           color:white;
          }
		  
.dcprint p{font-size:14px !important}


.bordertab{
border-bottom: 1px solid #cecece;
    border-left: 1px solid #cecece;
    border-top: 1px solid #cecece;
    border-right: 1px solid #cecece;}
		  table h4{ margin-top:0px; margin-bottom:0px}
		  table p {
    margin-top:0px !important;
    margin-bottom:0px !important;
}
</style>
</head>

<body>
    <form id="form1" runat="server">
     <div class="text-center" style="padding-top: 5px">
        <input type="button" id="btnPrint" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
        <a href="EmployeePaySlip.aspx" id="btnPrint" type="button" onclick="goBack()"><strong style="background: #5cb85c;
            padding: 9px 8px; border-radius: 4px; color: #fff;">Go Back</strong></a>
    </div>
    <div class="container" >
        <div class="row mt ">
            <div class="col-lg-12 panel">
          
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-6 col-sm-6">
                            <%--<img src="logo.png" style="padding-bottom: 8px; height: 90px">--%>
                        </div>
                        <div class="col-md-6 col-sm-6" style="text-align: right">
                            <p>
                                <%--<b>Brihaspathi Technologies Pvt Ltd</b>
                                <br/>
                                #7-1-621/259, V Floor, Sahithi Arcade,
                                <br/>
                                Beside S R Nagar Police Station, S R Nagar
                                <br/>
                                Hyderabad Telangana
                                <br/>
                                India 9885888835--%>
                            <%--   <b> Cherla Health Clinic</b>--%>
                            </p>
                        </div>
                    </div>
                </div>
         <%--       <div class="row">
                    <div class="col-md-12 col-sm-12">
                       
                        <hr>
                    </div>
                </div>--%>
                <div class="row">
                    <div class="col-md-12 col-sm-12">


                        	<table  class="table table-striped bordertab" width="100%">
					<tbody>
						<tr style="height:50px;">
							<td style=" text-align:center;font-size:30px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=5>
								<nobr>CHERLA HEALTH PVT LTD</nobr>
							</td>
							 
						</tr>
						<tr style="height:23px;">
							<td style=" text-align:center;font-size:20px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=5>
								<nobr>Salary Statement for the Month of <asp:Label ID="month" runat="server" Text=""></asp:Label>,&nbsp;<asp:Label ID="year" runat="server" Text=""></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:17px;">
							<td style=" font-size:18px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Employee Name</nobr>
							</td>

                                
							<td style=" text-align:center;font-size:18px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=3>
								<nobr><asp:Label ID="lblemployee" runat="server" Text="" ForeColor="Blue" Font-Bold="true"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:18px;">
							<td style=" font-size:18px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Employee Number</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblempid" runat="server" Text=""></asp:Label></nobr>
							</td>
							<td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Joining Date</nobr>
							</td>
							<td style="font-family:Verdana;text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lbldoj" runat="server" Text=""></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:18px;">
							<td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Account Number</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblacno" runat="server" Text=""></asp:Label></nobr>
							</td>
							<td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Designation</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lbldesignation" runat="server" Text=""></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:17px;">
							<td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Days Present</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lbldays" runat="server" Text=""></asp:Label></nobr>
							</td>
							<%--<td style=" font-size:11px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Paid Leaves</nobr>
							</td>
							<td style=" text-align:center;font-size:11px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>--%>
                            <td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>P.F. No.</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;<asp:Label ID="lblpfno" runat="server" Text="Label"></asp:Label></nobr>
							</td>
							 
						</tr>
                        	<tr style="height:17px;">
							<td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Payble Days</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblpay" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<%--<td style=" font-size:11px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Paid Leaves</nobr>
							</td>
							<td style=" text-align:center;font-size:11px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>--%>
                            <td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Salary for the month</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="month1" runat="server" Text=""></asp:Label>'<asp:Label ID="year1" runat="server" Text=""></asp:Label></nobr>
							</td>
							 
						</tr>
						<%--<tr style="height:17px;">
							<td style=" font-size:11px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Arrears Days</nobr>
							</td>
							<td style=" text-align:center;font-size:11px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>
							
							 
						</tr>--%>
					<%--	<tr style="height:17px;">
                            <td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Payble Days</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr></nobr>
							</td>
							<td style=" font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Salary for the month</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr></nobr>
							</td>
							
							 
						</tr>--%>
						<tr style="height:17px;">
							<td style=" text-align:center;font-size:18px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=3 rowspan=2>
								<nobr>Earnings</nobr>
							</td>
							<td style=" text-align:center;font-size:18px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2 rowspan=2>
								<nobr>Deductions</nobr>
							</td>
							 
						</tr>
						<tr style="height:17px;">
							 
						</tr>
						<tr style="height:20px;">
							<td style=" text-align:center;font-size:12px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>&nbsp;</nobr>
							</td>
							<td style=" text-align:right;font-size:15px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Amount</nobr>
							</td>
							<td style="min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>
							<td style=" text-align:right;font-size:15px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Amount</nobr>
							</td>
							 
						</tr>
						<tr style="height:20px;">
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Basic Salary</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblbasic" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>E.Provident Fund</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblpf" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:20px;">
							<td style=" font-size:14px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>HRA</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblhr" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>ESI</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblesi" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:20px;">
							<td style=" font-size:14px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Conveyance</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblconveynce" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Professional Tax</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblptax" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:20px;">
							<td style=" font-size:14px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Children Education Allowance</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblchild" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>I. Tax</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblitax" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:20px;">
							<td style=" font-size:14px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Mobile Expenses</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblmobile1" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Loan </nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblloan" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:20px;">
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Special Allowance</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblspeacial" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Advance</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lbladvance" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:20px;">
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Travel Allowance</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lbltravell" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Misc. Deduction</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblmisc" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<%--<tr style="height:20px;">
							<td style=" font-size:12px;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Performance Incentives</nobr>
							</td>
							<td style=" text-align:right;font-size:12px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>
							<td style=" font-size:12px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Security Deposit</nobr>
							</td>
							<td style=" text-align:center;font-size:11px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>
							 
						</tr>--%>
						<%--<tr style="height:20px;">
							<td style=" font-size:12px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Arrears/ Other Ern.</nobr>
							</td>
							<td style=" text-align:right;font-size:12px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>
							<td style=" font-size:12px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>
							<td style=" text-align:right;font-size:12px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>&nbsp;</nobr>
							</td>
							 
						</tr>--%>
						<tr style="height:20px;">
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=2>
								<nobr>Total Earnings</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lbltotal" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Total Deductions</nobr>
							</td>
							<td style=" text-align:right;font-size:14px;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lbldedtot" runat="server" Text="0"></asp:Label></nobr>
							</td>
							 
						</tr>
						<tr style="height:33px;">
							<td style=" font-size:22px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr>Net Salary Rs.</nobr>
							</td>
							<td style=" text-align:right;font-size:22px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
								<nobr><asp:Label ID="lblnet" runat="server" Text="0"></asp:Label></nobr>
							</td>
							<td style=" font-size:22px;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=3>
								<nobr><asp:Label ID="lblwords" runat="server" Text=""></asp:Label> </nobr>
							</td>
							 
						</tr>
						 
						 
						
					</tbody>
				</table>


                        <!----------------------------->

















<%--                        <table class="table table-striped bordertab" width="100%">
                            <tr>
                                <td colspan="6">
                                    <h3 class="text-center">
                                        <b>CHERLA HEALTH PVT LTD</b>
                                    </h3>
                                </td>
                            </tr>
                      <tr>
                                <td  colspan="6">   <h3 class="text-center">
                            <b>Salary Statement for the Month of  <asp:Label ID="Label2" runat="server" ></asp:Label>
&nbsp;<asp:Label ID="Label3" runat="server" ></asp:Label></b>
                           
                        </h3>
                        </td>
                        </tr>
                            <tr>
                                <td>
                                    <h4>
                                        Name:</h4>
                                </td>
                                <td>
                                   <h4> <asp:Label ID="EName" runat="server" Text=""></asp:Label></h4>
                                </td>
                                <td>
                                    <h4>
                                        Employee Num:</h4>
                                </td>
                                <td>
                                   <h4> <asp:Label ID="ENumber" runat="server" Text=""></asp:Label></h4>
                                </td>
                                <td>
                                    <h4>
                                        PF Number</h4>
                                </td>
                                <td>
                                   <h4> <asp:Label ID="PFnumber" runat="server" Text=""></asp:Label></h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        Department:</h4>
                                </td>
                                <td>
                                    <h4> <asp:Label ID="Department" runat="server" Text=""></asp:Label></h4>
                                </td>
                                <td>
                                    <h4>
                                        Date of Birth</h4>
                                </td>
                                <td>
                                    <h4> <asp:Label ID="DOB" runat="server" Text=""></asp:Label></h4>
                                </td>
                                <td>
                                    <h4>
                                        Working Days:</h4>
                                </td>
                                <td>
                                    <h4> <asp:Label ID="WorkingDays" runat="server" Text=""></asp:Label></h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        Designation:</h4>
                                </td>
                                <td>
                                    <h4> <asp:Label ID="Designation" runat="server" Text=""></asp:Label></h4>
                                </td>
                                <td>
                                    <h4>
                                        Date of Joining</h4>
                                </td>
                                <td>
                                    <h4> <asp:Label ID="DOJ" runat="server" Text=""></asp:Label></h4>
                                </td>
                                <td>
                                    <h4>
                                        Payable Days:</h4>
                                </td>
                                <td>
                                   <h4> <asp:Label ID="PayableDays" runat="server" Text=""></asp:Label></h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>Account Number</h4>
                                </td>
                                <td>
                                    <h4>
                                        <asp:Label ID="lblaccount" runat="server" Text=""></asp:Label></h4>
                                </td>
                                <td>
                                    <h4>
                                        Salary for the month	

                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        
                                        <asp:Label ID="lblmonth" runat="server" Text="Label"></asp:Label>'&nbsp;<asp:Label ID="lblyear" runat="server" Text="Label"></asp:Label>
                                    </h4>
                                </td>
                            </tr>
                        </table>
                        <table class="table table-stripped dcprint bordertab" style="width: 100%; border-collapse: collapse;
                            border-bottom: 1px solid #cecece">
                            <thead class="thead-inverse">
                                <tr class="vendorListHeading border-none">
                                    <th>
                                        Earning<th>
                                            <th class="text-right">
                                                Amount
                                            </th>
                                            <th class="text-left">
                                                Deductions
                                            </th>
                                            <th class="text-right">
                                                Amount
                                            </th>
                                </tr>
                                <tbody>
                                    <tr>
                                        <td class="text-left">
                                            <p>
                                                
                                                <span id="DataList1_Label5_0">Basic</span></p>
                                        </td>
                                        <td scope="row ">
                                            <div class="row">
                                                <div class="col-md-11 col-sm-11">
                                                    <p>
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                            <asp:Label ID="Basic" runat="server" Text="basic"></asp:Label>
                                                
                                        </td>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">House Rent Allowance</span></p>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                             <asp:Label ID="HAllowance" runat="server" Text="HAllowance"></asp:Label>
                                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">Dearness Allowance</span></p>
                                        </td>
                                        <td scope="row ">
                                            <div class="row">
                                                <div class="col-md-11 col-sm-11">
                                                    <p>
                                                   
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                                  <asp:Label ID="DAllowances" runat="server" Text="DAllownces"></asp:Label></p>
                                        </td>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">PF</span></p>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                                 <asp:Label ID="PF" runat="server" Text="PF"></asp:Label></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Children Education Allowance	

                                        </td>
                                        <td>
                                            <asp:Label ID="lblchildren" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mobile Expenses	

                                        </td>
                                        <td>
                                            <asp:Label ID="lblmobile" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Special Allowance	
                                        </td>
                                        <td>
                                            <asp:Label ID="lblspecial" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Travel Allowance	

                                        </td>
                                        <td>
                                            <asp:Label ID="lbltravel" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">Conveyance Alowance</span></p>
                                        </td>
                                        <td scope="row ">
                                            <div class="row">
                                                <div class="col-md-11 col-sm-11">
                                                    <p>
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <p><asp:Label ID="CAlowance" runat="server" Text="CAlowance"></asp:Label>
                                                </p>
                                        </td>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">ESI</span></p>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                                <asp:Label ID="ESI" runat="server" Text="ESI"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">Education Allowance</span></p>
                                        </td>
                                        <td scope="row ">
                                            <div class="row">
                                                <div class="col-md-11 col-sm-11">
                                                    <p>
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                            <asp:Label ID="EAllowance" runat="server" Text="EAllowance"></asp:Label>
                                                
                                        </td>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">Advance Salary</span></p>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                            <asp:Label ID="ASalary" runat="server" Text="ASalary"></asp:Label>
                                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">INcomeTax</span></p>
                                        </td>
                                        <td scope="row ">
                                            <div class="row">
                                                <div class="col-md-11 col-sm-11">
                                                    <p>
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                            <asp:Label ID="IT" runat="server" Text="Income Tax"></asp:Label>
                                                
                                        </td>
                                        <td class="text-left">
                                            <p>
                                                <span id="DataList1_Label5_0">Loss of Pay</span></p>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                            <asp:Label ID="LPay" runat="server" Text="Loss of Pay"></asp:Label>
                                                
                                        </td>
                                    </tr>
                                    <tr style="border-top:1px solid #cecece">
                                        <td class="text-left">
                                            <p>
                                                <strong><span id="DataList1_Label5_0"><b>Total</b></span></strong></p>
                                        </td>
                                        <td scope="row ">
                                            <div class="row">
                                                <div class="col-md-11 col-sm-11">
                                                    <p>
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <p>
                                              <strong>  <asp:Label ID="Total" runat="server" Text="Total"></asp:Label></strong>
                                        </td>
                                        <td class="text-left">
                                            <p>
                                              <strong>  <span id="DataList1_Label5_0"><b>Deductions</b></span></strong></p>
                                        </td>
                                        <td class="text-right">
                                            <p></strong><asp:Label ID="Earning" runat="server" Text="Earning"></asp:Label><strong>
                                               
                                        </td>
                                    </tr>
                                     <tr style="border-top:1px solid #cecece">
                                    <td colspan="3">
                                        <h4>
                                            Net Pay Rs. 
                                            <asp:Label ID="NPay" runat="server" Text="Label"></asp:Label></h4>
                                        </br>
                                    </td>
                                    <td colspan="2">
                                        <h4>
                                            <asp:Label ID="AWords" runat="server" Text="Label"></asp:Label> </h4>
                                        </br>
                                    </td>
                                </tr>
                                </tbody>
                        </table>--%>
                    </div>
                </div>
              
                <div class="row">
                    <div class="col-md-12 ">
                        <table class="table  borderless">
                            <tbody>
                                <tr>
                                    <h5 class="text-center">
                                        ** System generated print out. No signature required**</h5>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
     0
    <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
    </form>
</body>
</html>
