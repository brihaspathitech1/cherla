﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

using System.Data.OleDb;
using System.Data.Common;

namespace CherlaHealth
{
    public partial class ViewHolidays : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("SElect * from Holidays order by HolidayId desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                gvHolidays.DataSource = ds;
                gvHolidays.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvHolidays.DataSource = ds;
                gvHolidays.DataBind();
                int columncount = gvHolidays.Rows[0].Cells.Count;
                gvHolidays.Rows[0].Cells.Clear();
                gvHolidays.Rows[0].Cells.Add(new TableCell());
                gvHolidays.Rows[0].Cells[0].ColumnSpan = columncount;
                gvHolidays.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Edit_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select * from Holidays where HolidayId='" + lblid.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                DateTime date = DateTime.Parse(dr["HolidayDate"].ToString());
                txtdate.Text = date.ToString("yyyy-MM-dd");
                txtname.Text = dr["HolidayName"].ToString();
                txtdescription.Text = dr["Dercripation"].ToString();
                droptype.SelectedItem.Text = dr["HolidayType"].ToString();
            }
            conn.Close();
            this.ModalPopupExtender1.Show();
        }

        protected void Delete(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("delete from Holidays where HolidayId='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
        }

        protected void Update(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Update Holidays set HolidayName='" + txtname.Text + "',HolidayDate='" + txtdate.Text + "',HolidayType='" + droptype.SelectedItem.Text + "',Dercripation='" + txtdescription.Text + "' where HolidayId='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
        }

        protected void Year_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("SElect * from Holidays where Year(HolidayDate)='"+dropyear.SelectedItem.Text+"' order by HolidayId desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvHolidays.DataSource = ds;
                gvHolidays.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvHolidays.DataSource = ds;
                gvHolidays.DataBind();
                int columncount = gvHolidays.Rows[0].Cells.Count;
                gvHolidays.Rows[0].Cells.Clear();
                gvHolidays.Rows[0].Cells.Add(new TableCell());
                gvHolidays.Rows[0].Cells[0].ColumnSpan = columncount;
                gvHolidays.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Holiday_Click(object sender,EventArgs e)
        {
            Response.Redirect("AddHoliday.aspx");
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (FileUpload1.PostedFile != null)
            {
                try
                {
                    string path = string.Concat(Server.MapPath("~/images/" + FileUpload1.FileName));
                    FileUpload1.SaveAs(path);
                    // Connection String to Excel Workbook  
                    string excelCS = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
                    using (OleDbConnection con = new OleDbConnection(excelCS))
                    {
                        OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", con);
                        con.Open();
                        // Create DbDataReader to Data Worksheet  
                        DbDataReader dr = cmd.ExecuteReader();
                        // SQL Server Connection String  
                        string CS = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
                        // Bulk Copy to SQL Server   
                        SqlBulkCopy bulkInsert = new SqlBulkCopy(CS);
                        bulkInsert.DestinationTableName = "Holidays";
                        bulkInsert.WriteToServer(dr);
                        BindGrid();
                        lblMessage.Text = "Your file uploaded successfully";
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                    }
                    BindGrid();
                }
                catch (Exception)
                {
                    lblMessage.Text = "Your file not uploaded";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            FileUpload1.Dispose();
        }
    }
}