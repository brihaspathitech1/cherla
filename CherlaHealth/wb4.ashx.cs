﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    /// <summary>
    /// Summary description for wb4
    /// </summary>
    public class wb4 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string imageid = context.Request.QueryString["ImID"];
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select EmpImage from dbo.AttendanceLogs1 where EmployeeID='" + imageid + "'", con);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                context.Response.BinaryWrite((byte[])dr[0]);
                con.Close();
                context.Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}