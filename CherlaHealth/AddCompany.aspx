﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddCompany.aspx.cs" Inherits="CherlaHealth.AddCompany" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size:24px">Add Company</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div class="col-md-4">
              <div class="form-group">

             
                Company Code
                <asp:TextBox ID="Code" class="form-control" runat="server"></asp:TextBox>
             </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                Company Name 
                <span class="style1">*</span> 
                <asp:TextBox ID="Company" class="form-control" runat="server"></asp:TextBox>
                <asp:Label ID="lblStatus" runat="server" style="color: #FF0000"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompany" runat="server" ControlToValidate="Company"
                    ErrorMessage="* Fill Company Name" ForeColor="Red"></asp:RequiredFieldValidator>
              </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                Reg.No
                <asp:TextBox ID="Regno" class="form-control" runat="server"></asp:TextBox>
              </div>
          </div>
        <div class="clearfix"></div>
         <div class="col-md-4">
              <div class="form-group">
                Reg Date
                <asp:TextBox ID="RegDate" class="form-control" runat="server"></asp:TextBox>
            
            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="RegDate" runat="server" />
            </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                TAN No
                <asp:TextBox ID="TAN" class="form-control" runat="server"></asp:TextBox>
              </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                Pan No
                <asp:TextBox ID="Pan" class="form-control" runat="server"></asp:TextBox>
             </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                TIN No
                <asp:TextBox ID="TIN" class="form-control" runat="server"></asp:TextBox>
             </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                Name
                <asp:TextBox ID="Name" class="form-control" runat="server"></asp:TextBox>
             </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                Designation
                <asp:TextBox ID="Desiganation" class="form-control" runat="server"></asp:TextBox>
             </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                  Email
                <span class="style1">*</span>
                <asp:TextBox ID="Address" class="form-control" runat="server" TextMode="MultiLine"
                    Rows="1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddress" runat="server" ControlToValidate="Address"
                    ErrorMessage="* Fill Email" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server" ControlToValidate="Address"
                ErrorMessage="* Enter Valid Email address" 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                ForeColor="Red"></asp:RegularExpressionValidator>
              </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                  Address
                <span class="style1">*</span>
                <asp:TextBox ID="Email" class="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server" ControlToValidate="Email"
                ErrorMessage="* Fill Address" ForeColor="Red"></asp:RequiredFieldValidator>
              
         </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
         
                Phone No
                <asp:TextBox ID="Phone" class="form-control" runat="server"></asp:TextBox>
                    </div>
          </div>
            <div class="clearfix"></div>
         <div class="col-md-4">
              <div class="form-group">
           
                Phone No2
                <asp:TextBox ID="Phone2" class="form-control" runat="server"></asp:TextBox>
            </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                Mobile No
                <span class="style1">*</span>
                <asp:TextBox ID="Mobile" class="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMobile" runat="server" ControlToValidate="Mobile"
                    ErrorMessage="* Fill Mobile" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobile" runat="server" 
                    ErrorMessage="* Enter Valid Mobile Number" ForeColor="Red" ControlToValidate="Mobile" ValidationExpression="^[789]\d{9}$"></asp:RegularExpressionValidator>
             </div>
          </div>
         <div class="col-md-4">
              <div class="form-group">
                FAX No
                <asp:TextBox ID="FAX" class="form-control" runat="server"></asp:TextBox>
              </div>
          </div>
        <div class="clearfix"></div>
         <div class="col-md-4">
              <div class="form-group">
                Website URL
                <asp:TextBox ID="Website" class="form-control" runat="server"></asp:TextBox>
             </div>
          </div>
            <div class="col-md-4">
              <div class="form-group">
                  Latitude<span class="style1">*</span> <asp:TextBox ID="txtlat" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlat"
                    ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                  </div>
                </div>
                        <div class="col-md-4">
              <div class="form-group">
                  Longitude<span class="style1">*</span> <asp:TextBox ID="txtlong" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtlong"
                    ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                  </div>
                </div>
                <div class="col-md-4" style="padding-top:20px">
              <div class="form-group">
                <asp:Button ID="btnsubmit" class="btn  btn-success" runat="server" Text="Submit" OnClick="Submit" />
</div>
             </div>
            </div>
         </div>
</asp:Content>
