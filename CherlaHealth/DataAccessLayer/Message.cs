﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace inventory.DataAccessLayer
{
    class Message
    {

        #region default Constructor
        public Message()
        {    
        }
        #endregion

        public void sendSingleNotification(string deviceId, Notification noti)
        {

            var jsonData = new
            {
                to = deviceId,
                notification = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }
        
        public void sendTopicNotification(string topic, Notification noti)
        {

           var jsonData = new
             {
                 to = "/topics/" + topic,
                 data = new
                 {
                     id = noti.id,
                     title = noti.title,
                     body = noti.body,
                     code = noti.code,
                     image = noti.image

                 }
             };


            /*var jsonData = new
          {
              to = "/topics/" + topic,
              notification = new
              {
                  body = noti.body,
                  title = noti.title

              }
          };*/

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }

        public void sendMultipleNotification(string[] registrationIds, Notification noti)
        {

            var jsonData = new
            {
                registration_ids = registrationIds,
                data = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);
        }
        public void OnLogRequest(Object source, EventArgs e)
        {
            //custom logging logic can go here
        }


        public void PushNotification(string json)
        {
            try
            {

                var applicationID = "AAAA96fbe_o:APA91bGPDEZLT_q7UrNRlUjRIyiNCTsfrKPCNtX-TuIgyjzUEEhXsKz-KchaTKtq6ldDVSnJs7NGxs2C8mybINYtQsQUgakCHqGiFe8X3Q0Wgmy5vrNBj4eH4DF8z0pz4KsrEEEp7JFC";
                
                var senderId = "1063673101306"; 


                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";


                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }

        }
    }

  
    class Notification
    {
        public int id { get; set; }

        public string title { get; set; }

        public string body { get; set; }

        public string image { get; set; }

        public int code { get; set; }
    }
}
