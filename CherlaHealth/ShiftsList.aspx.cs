﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class ShiftsList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindStudy();
            if (!IsPostBack)
            {
                BindStudy();
            }
        }


        private void BindStudy()
        {

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Shifts", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn33 = new SqlConnection(connstrg);
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lbldeleteid = (Label)row.FindControl("ShiftId");
            conn33.Open();
            SqlCommand cmd3 = new SqlCommand("delete FROM Shifts where ShiftId='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'", conn33);
            cmd3.ExecuteNonQuery();
            conn33.Close();
            BindStudy();

        }
        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = GridView1.DataKeys[gRow.RowIndex].Value.ToString();
            txtstor_address.Text = gRow.Cells[1].Text;
            txtcity.Text = gRow.Cells[2].Text;
            txtstate.Text = gRow.Cells[3].Text;
            TextBox2.Text = gRow.Cells[4].Text;
            this.ModalPopupExtender2.Show();
        }
        protected void btnModity_Click(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);
            string DeptId = lblstor_id.Text;
            conn44.Open();
            SqlCommand cmd = new SqlCommand("update Shifts set ShiftName='" + txtstor_address.Text + "', ShiftCode='" + txtcity.Text + "', BeginTime='" + txtstate.Text + "',EndTime='" + TextBox2.Text + "' where ShiftId=" + lblstor_id.Text, conn44);
            cmd.ExecuteNonQuery();
            conn44.Close();
            lblmsg.Text = "Data Updated...";
            BindStudy();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString);

            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from  Shifts where ShiftName like '%'+@date1+'%'  or ShiftCode like '%'+@date1+'%' ", conn44);
            cmd.Parameters.Add("@date1", SqlDbType.VarChar).Value = TextBox1.Text.Trim();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}