﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HikeLetter.aspx.cs" Inherits="CherlaHealth.HikeLetter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Hike Letter</title>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }
        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }

            #Button1 {
                display: none;
            }

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint {
                display: none;
            }

            a {
                display: none;
            }
        }
        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:Label ID="Email" runat="server" Text="" Visible="false"></asp:Label>
            <section class="wrapper">
                <div class="row mt " id="content">
                    <div class="col-lg-offset-2 col-md-8 panel">
                        <div class="row text-center" style="padding-top: 25px">
                            <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                            <asp:Button runat="server" Text="Send Mail" type="button" ID="Button2" OnClick="btn_hikemail" class="btn btn-danger"></asp:Button>
                            <%-- <a href="Letters.aspx" id="hikemail" type="button"  onclick="btn_hikemail"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Send Mail</strong></a>--%>
                            <a href="HikeLetternew.aspx" id="btnPrint" type="button" onclick="goBack()"><strong style="background: #5cb85c; padding: 9px 8px; border-radius: 4px; color: #fff;">Go Back</strong></a>
                            <br />
                            <br />
                            <br />
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p style="font-size: 15px"><strong>Ref: CC to Managing Director, Executive Director, Accounts Manager</strong></p>
                                <h4 class=" text-left">Private & Confidential &nbsp</h4>
                            </div>
                            <div class="col-md-12">
                                <p style="font-size: 15px"><strong>Name:<asp:Label ID="Name1" runat="server" Text="Label"></asp:Label></strong></p>
                                <p style="font-size: 15px"><strong>Employee ID:<asp:Label ID="EmpId" runat="server" Text="Label"></asp:Label></strong></p>

                                <p style="font-size: 15px"><strong>Designation:<asp:Label ID="Designation" runat="server" Text="Label"></asp:Label></strong></p>

                                <p style="font-size: 15px">Sub: <strong>Salary Revision .</strong></p>
                                <p style="font-size: 15px">
                                    We are pleased to inform you that based on your performance and contribution to the company, our Management has revised your compensation to Rs.
                                    <asp:Label ID="Salry" runat="server" Text="Label"></asp:Label>/- (<asp:Label ID="AmountWords" runat="server" Text="Label"></asp:Label>) Per Annum with effect from
                                    <asp:Label ID="Date" runat="server" Text="Label"></asp:Label>.
                                    <br />
                                                        We appreciate your initiative and expect you to take many more such responsibilities in future assignments to ensure company’s growth.<br />
                                    Your salary details are strictly private and confidential. The details in this letter must not be disclosed or discussed to others. Please acknowledge your acceptance of the revised terms by signing a duplicate copy of this letter.

             
                                </p>
                                <p style="font-size: 15px"><strong>Regards</strong></p>

                                <p style="font-size: 15px"><strong>With Best Wishes</strong></p>

                                <p style="font-size: 15px"><strong>HR Department</strong></p>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
</body>
</html>