﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CherlaHealth
{
    public partial class TravelAllowance : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindEmployee();
            }
        }

        protected void BindEmployee()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select EmployeeId,EmployeName from Employees where status='1'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropemp.DataSource = dr;
            dropemp.DataTextField = "EmployeName";
            dropemp.DataValueField = "EmployeeId";
            dropemp.DataBind();
            dropemp.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }

        protected void Condition_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select Sum(convert(float,distance)) as distance from distance where employee_id='" + dropemp.SelectedValue + "' and Month(date)=Month('" + txtmonth.Text + "') and Year(date)=Year('" + txtmonth.Text + "')", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                txtkms.Text = dr["distance"].ToString();
                
            }

            conn.Close();
            if(txtkms.Text=="")
            {
                txtkms.Text = "0";
            }
            else
            {           
                txtkms.Text = (Convert.ToDouble(txtkms.Text) / 1000).ToString();
                txtkms.Text = Math.Round(Convert.ToDouble(txtkms.Text), 2).ToString();
            }
            if (dropcondition.SelectedValue=="2")
            {
                div1.Visible = false;
                txtamt.ReadOnly = false;
                txtamt.Text = "0";
            }
           else
            {
                div1.Visible = true;
                txtamt.ReadOnly = true;
                if (dropcondition.SelectedValue == "0")
                {
                    double value = double.Parse(txtkms.Text) * 5;
                    txtamt.Text = value.ToString();
                }
                else if (dropcondition.SelectedValue == "1")
                {
                    double value = double.Parse(txtkms.Text) * 2;
                    txtamt.Text = value.ToString();
                }
            }

            
        }

        protected void Distance_Change(object sender,EventArgs e)
        {
            if(txtkms.Text!="")
            {
                if(dropcondition.SelectedValue=="0")
                {
                    double value = double.Parse(txtkms.Text) * 5;
                    txtamt.Text = value.ToString();
                }
                else if (dropcondition.SelectedValue == "1")
                {
                    double value= double.Parse(txtkms.Text) * 2;
                    txtamt.Text = value.ToString();
                }
            }
        }

        protected void Submit_Click(object sender,EventArgs e)
        {
            if(txtcharges.Text=="")
            {
                txtcharges.Text = "0";
            }
            if(txtamt.Text=="")
            { txtamt.Text = "0"; }
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd1 = new SqlCommand("Select * from travel_allowance where EmployeeId='" + dropemp.SelectedValue + "' and Month='" + txtmonth.Text + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert('Duplicate Entry')</script>");
            }
            else
            {
                SqlCommand cmd = new SqlCommand("Insert into travel_allowance(Date,EmployeeId,EmployeeName,Month,Condition,Kms,Amount,ExtraCharges) values ('" + System.DateTime.Now.ToString("yyyy-MM-dd") + "','" + dropemp.SelectedValue + "','" + dropemp.SelectedItem.Text + "','" + txtmonth.Text + "','" + dropcondition.SelectedItem.Text + "','" + txtkms.Text + "','" + txtamt.Text + "','" + txtcharges.Text + "')", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                Response.Write("<script>alert('Added Successfully')</script>");
                txtmonth.Text = null;
                dropemp.SelectedValue = null;
                dropcondition.SelectedValue = null;
                txtkms.Text = "0";
                txtamt.Text = null;txtcharges.Text = "0";
            }
        }
    }
}