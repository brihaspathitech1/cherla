﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="CherlaHealth.Messages" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px">Notifications</h2>
            <hr />
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <asp:GridView ID="GridView1" CssClass="table table-responsive" GridLines="None" AutoGenerateColumns="false" runat="server" DataKeyNames="LeaveId" EmptyDataText="Now Data Found">
                    <Columns>
                        <asp:TemplateField HeaderText="S.No" HeaderStyle-BackColor="#3399ff" HeaderStyle-ForeColor="White" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Message" HeaderStyle-BackColor="#3399ff" HeaderStyle-ForeColor="White">
                           <ItemTemplate>
                              
                               Mr/Ms.<asp:Label ID="Label3" runat="server" Text='<%# Eval("EmployeName") %>'></asp:Label> is on the leave,So you have to work in this following duration <br /><br /> <asp:Label ID="Label1" runat="server" Text='<%# Eval("FromDate") %>' ForeColor="#ff0000" Font-Bold="true"></asp:Label> to <asp:Label ID="Label2" runat="server" Text='<%# Eval("ToDate") %>' ForeColor="#ff0000" Font-Bold="true"></asp:Label> with in the time span of <asp:Label runat="server" Text='<%# Eval("BeginTime") %>' ForeColor="#ff0000" Font-Bold="true"></asp:Label> to <asp:Label runat="server" Text='<%# Eval("EndTime") %>' ForeColor="#ff0000" Font-Bold="true"></asp:Label>
                           </ItemTemplate>
                       </asp:TemplateField>
                        
                    </Columns>
                </asp:GridView>
            </div>

            <asp:Label ID="lblname" Visible="false" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
</asp:Content>
