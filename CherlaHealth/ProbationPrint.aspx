﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProbationPrint.aspx.cs" Inherits="CherlaHealth.ProbationPrint" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Probation Confirmation Letter</title>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @page {
            size: auto;
            margin: 0mm !important; /* this affects the margin in the printer settings */
        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
                margin-top: -80px !important;
            }

            .dcprint p {
                font-size: 11px !important;
            }
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }




        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }

            #Button1 {
                display: none;
            }

            #Button2 {
                display: none;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }

            input#btnPrint,#btnpdf {
                display: none;
            }
            input#btnemail {
                 display: none;
            }

            a {
                display: none;
            }
        }




        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .pad {
            padding: 5px;
        }

        .marno {
            margin: 0px !important;
        }

            .marno p {
                margin-bottom: 0px !important;
            }

        .marno1 {
            margin-bottom: -7px !important;
        }

        .date .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px !important;
        }
    </style>
   <style>
    .page21 {
            width:100%;background:#d3d3d357;text-align:justify;padding:100px 50px 0px 70px;margin-top:25px
        }
        tr, td {
            /*border:1px solid black;
            border-collapse:collapse;
            padding:20px;*/
      
       }
        .tab-text { margin-left:40px;font-weight:bold;
        }
        ol {
            margin-left:50px;
        }
   </style> 
</head>
<body>
    <div class="page21">
    <form id="form1" runat="server">
          <div style="text-align:center;padding-top:25px ;">
             <input type="button" id="Button1" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />
                <a href="Probation.aspx" id="btnPrint" type="button"  onclick="goBack()"  ><strong style="background: #5cb85c;padding: 9px 8px;border-radius: 4px;color: #fff;">Go Back</strong></a>

            <asp:Button ID="btnpdf" CssClass="btn btn-success" runat="server" Text="PDF" OnClick="pdf_Click" />
              <asp:Button ID="btnemail" CssClass="btn btn-success" runat="server" Text="Gmail" OnClick="btnemail_Click" Visible="false" />
               <br /><br />
        </div>
    <div>
         <asp:Panel ID="Panel1" runat="server">
        <table width="100%">
                <tr>
                    <td>
                       <%-- <img id="img1" alt="/" src="sign board.jpg" width="1250px" height="120px" />--%>
                     <%--    <img id="img1" runat="server" src="~/sign board.png" width="1250px" height="120px" />--%>
                        <%--<asp:Image ID="Image1" ImageUrl="~/sign board.jpg" Width="1250px" Height="120px" runat="server" />
                    --%></td>
                </tr>
            </table>
             <br /><br /><p><asp:Label ID="lblemail" runat="server" Text="" Visible="false"></asp:Label></p>

        <p>Dear <asp:Label ID="salutation" runat="server"> </asp:Label>&nbsp;&nbsp;
        <asp:Label ID="lblname" runat="server" Text=""></asp:Label> </p>
             
        <br />
        <br />
        <p><strong><u>SUB: COMPLETION OF PROBATIONARY PERIOD</u></strong></p>
        <br />
        <br />
        <p>As you aware, your employment was subject to a probationary period of  <asp:Label ID="lblmonth" runat="server" Text=""></asp:Label> months expiring on <asp:Label ID="lblexpire" runat="server" Text=""></asp:Label></p>
        <br />
        <br />
              
        <p>During this period your performance has been assessed against the Company`s standards of conduct, attendance and job performance and we are pleased to confirm that you have satisfactorily completed your probationary period.</p>
        <br />
        <br />
        <p>This letter is confirmation of your appointment to the position of  <asp:Label ID="lbldesig" runat="server" Text=""></asp:Label>.</p>
        <br />
        <br />
        <p>Warm Regards</p>
        <br />
        <p>C.E.O/Director.</p>
             </asp:Panel>
    </div>
    </form>
        </div>
</body>
</html>
