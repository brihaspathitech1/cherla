﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Probation.aspx.cs" Inherits="CherlaHealth.Probation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title" style="font-size: 24px;color: green; font-weight: bold;">Probation Letter</h2>
            <hr />
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                 <div class="form-group">
                     <asp:UpdatePanel ID="UpdatePanel8" runat="server"><ContentTemplate>
                  Salutation<span style="color:red">*</span> <asp:DropDownList ID="salut"  CssClass="form-control" runat="server">
                      <asp:ListItem>Mr.</asp:ListItem>
                      <asp:ListItem>Mrs.</asp:ListItem>
                      <asp:ListItem>Ms.</asp:ListItem>      </asp:DropDownList>
                       </ContentTemplate></asp:UpdatePanel>
                         </div>
             </div>
            <asp:UpdatePanel ID="Update" runat="server"><ContentTemplate>
              <div class="col-md-3">
                  <div class="form-group">
                      Select Employee <span style="color:red">*</span> <asp:DropDownList ID="dropemp" CssClass="form-control" runat="server" OnSelectedIndexChanged="Employee_Change" AutoPostBack="true"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropemp" ForeColor="Red" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>

                  </div>
              </div>
                </ContentTemplate></asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
            <div class="col-md-3">
                  <div class="form-group">
                      Designation <asp:TextBox ID="txtdesig" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                      </div>
                </div>
                </ContentTemplate></asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
             <div class="col-md-3">
                  <div class="form-group">
                      Join Date <asp:TextBox ID="txtjoin" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                      </div>
                 </div>
                </ContentTemplate></asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
            <div class="col-md-3">
                  <div class="form-group">
                      Probation Period <span style="color:red">*</span>  <asp:TextBox ID="txtprob" CssClass="form-control" OnTextChanged="Period_Change" MaxLength="1" runat="server" AutoPostBack="true"></asp:TextBox>
                      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtprob" FilterType="Numbers" />
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtprob" ForeColor="Red"></asp:RequiredFieldValidator>

                      </div>
                </div>
                </ContentTemplate></asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
             <div class="col-md-3">
                  <div class="form-group">
                      Expire Date <asp:TextBox ID="txtexpire" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                      </div>
                 </div>
                </ContentTemplate></asp:UpdatePanel>
             <asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
             <div class="col-md-3">
                  <div class="form-group">
                     Email <asp:TextBox ID="TextBox1" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                      </div>
                 </div>
                </ContentTemplate></asp:UpdatePanel>

             <div class="col-md-3" style="padding-top:18px">
                  <div class="form-group">
                      <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                      </div>
                 </div>
            </div>
        </div>
</asp:Content>
