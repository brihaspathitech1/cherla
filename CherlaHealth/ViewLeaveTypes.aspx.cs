﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CherlaHealth
{
    public partial class ViewLeaveTypes : System.Web.UI.Page
    {
        String strConnString = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select * from LeaveTypes order by LeaveTypeId desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvLeaves.DataSource = ds;
                gvLeaves.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvLeaves.DataSource = ds;
                gvLeaves.DataBind();
                int columncount = gvLeaves.Rows[0].Cells.Count;
                gvLeaves.Rows[0].Cells.Clear();
                gvLeaves.Rows[0].Cells.Add(new TableCell());
                gvLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                gvLeaves.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void delete_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("delete from LeaveTypes where LeaveTypeId='"+id+"'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
        }

        protected void Type_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("Select * from LeaveTypes where typeid='"+droptype.SelectedValue+"' order by LeaveTypeId desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvLeaves.DataSource = ds;
                gvLeaves.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvLeaves.DataSource = ds;
                gvLeaves.DataBind();
                int columncount = gvLeaves.Rows[0].Cells.Count;
                gvLeaves.Rows[0].Cells.Clear();
                gvLeaves.Rows[0].Cells.Add(new TableCell());
                gvLeaves.Rows[0].Cells[0].ColumnSpan = columncount;
                gvLeaves.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Leave_Click(object sender,EventArgs e)
        {
            Response.Redirect("AddLeaveTypes.aspx");
        }
    }
}