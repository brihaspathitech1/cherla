﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using inventory.DataAccessLayer;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace CherlaHealth
{
    public partial class OfferLetterPrint : System.Web.UI.Page
    {
        String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        private readonly int health;
        private readonly int bucket;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                salutation.Text = Session["salute"].ToString();
                Label2.Text= Session["salute"].ToString();
                lblpdate.Text = System.DateTime.Now.ToString("yyyy-MM-dd ");
              //lblpdate.Text= Convert.ToDatetime(System.DateTime.Now.ToShortDateString("yyyy-MM-ddd"));
                Bindvalues();
            }
        }

        protected void Bindvalues()
        {
            SqlConnection conn = new SqlConnection(connstrg);
            SqlCommand cmd = new SqlCommand("SElect *,convert(varchar,joining_date,103) as dates from offerletter where id='" + Session["id"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblname.Text = dr["name"].ToString();
                lblname2.Text = dr["name"].ToString();

                lbldate.Text = dr["dates"].ToString();
                lblperiod.Text = dr["probation"].ToString();
                lblsal.Text = dr["salary"].ToString();
                lblrupees.Text = dr["words"].ToString();
                lbldesig.Text = dr["designation"].ToString();
                Label1.Text = dr["address"].ToString();
                lblnote.Text = dr["note"].ToString();
                lbltime.Text = dr["time"].ToString();
                string id = dr["candid"].ToString();
                DataSet ds = DataQueries.SelectCommon("SElect Permanent_Address,Email from Candidates where id='" + id + "'");
                lbladdress.Text = ds.Tables[0].Rows[0]["Permanent_Address"].ToString();
                lblemail.Text = ds.Tables[0].Rows[0]["Email"].ToString();

            }
            conn.Close();
        }

        protected void pdf_Click(object sender, EventArgs e)
        {
          //  Image1.Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Offer Letter.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            string imagePath = Server.MapPath("sign board.jpg") + "";
            //for (int i = 0; i < 1; i++)
            //{
            //    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

            //    image.Alignment = Element.ALIGN_TOP;

            //    image.SetAbsolutePosition(40, 700);

            //    image.ScaleToFit(500f, 400f);
            //    image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

            //    pdfDoc.Add(image);
                htmlparser.Parse(sr);
            //}
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnemail_Click(object sender, EventArgs e)
        {
            Smail();
        }


        protected void Smail()
        {

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();
                    {
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");

                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Date: " + System.DateTime.Now.ToString("yyyy-MM-dd") + "</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>To, </p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + lblname.Text + "</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" + lbladdress.Text + "</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'> Sub: OFFER LETTER </ p >");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>" );
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Dear Mr./Ms." + lblname.Text + ", </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");

                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>We are pleased to offer you the position of " + lbldesig.Text + " at Cherla Health Pvt. Ltd subject to satisfactory fulfillment of our company’s pre-employment criteria. Your appointment shall be based at hr@cherlahealth.com.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>The position we are offering you entails a monthly Gross salary of Rs." + lblsal.Text + "/- with an annual cost to company of Rupees " + lblrupees.Text + " /-. You will be on probation for a period of " + lblperiod.Text + " months. </p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'> Note:" + lblnote.Text + "</p>");
                        sb.Append("<p><br></p>");

                        
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'> We would like you to start working at Cherla Health from " + lbldate.Text + " "+lbltime.Text+". The role, in which you join us, is a very important one which entails dealing with important and sensitive information, records and such other matters of the company. You will, therefore, be required to sign a “Code of Conduct and Secrecy Agreement” of our company. </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Please find enclosed a list of documents to be provided on the day of joining. </p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Please reply to this letter immediately, to indicate your acceptance of this offer and date of your joining.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>We are confident you will be able to make a significant contribution to the success of Cherla Health Pvt Ltd and look forward to working with you.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Sincerely,</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>P Purushothama Sarma</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Manager HR</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Cherla Health (P) Ltd</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Documents to be submitted at the time of accepting this offer</p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>• Letter of acceptance</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        
                        sb.Append("<p><br></p>");
                        sb.Append("<p style='padding-left:50px !important;padding-right:50px !important'>Documents to be submitted on the day of joining.</p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<p><br></p>");
                        sb.Append("<table border='1'> ");

                        sb.Append("<tr>");

                        sb.Append("<td>Identity & Address  proof: (Xerox) <br> 1. Passport size photos ( 4 nos) <br>2. PAN Card <br>3. Driver’s license <br> 4. Aadhar Card</td>");

                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td>Educational Qualification Certificates <br>1. 10th class <br>2. +2 <br>3. Degree certificate</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td>Previous employment related <br>1. Salary certificate of previous company <br>2. Resignation copy <br>3. Relieving letter <br>4. Experience certificate <br>5. Last two (02) month’s salary slip from current employer</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");


                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            string path = Server.MapPath("Files");
                            string filename = path + "/" + "Interview Call Letter" + ".Pdf";

                            PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();
                           
                            string imagePath = Server.MapPath("sign.jpeg") + "";
                            for (int i = 0; i < 1; i++)
                            {

                                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                                image.Alignment = Element.HEADER;

                                image.SetAbsolutePosition(30, 650);

                                image.ScaleToFit(550f, 550f);
                                image.Alignment = iTextSharp.text.Image.HEADER | iTextSharp.text.Image.HEADER;


                                pdfDoc.Add(image);
                                htmlparser.Parse(sr);

                                PdfContentByte content = writer.DirectContent;
                                Rectangle rectangle = new Rectangle(pdfDoc.PageSize);
                                rectangle.Left += pdfDoc.LeftMargin;
                                rectangle.Right -= pdfDoc.RightMargin;
                                rectangle.Top -= pdfDoc.TopMargin;
                                rectangle.Bottom += pdfDoc.BottomMargin;
                                content.SetColorStroke(BaseColor.BLACK);
                                //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                                content.Stroke();
                            }
                            pdfDoc.Close();




                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();

                            MailMessage mm = new MailMessage();
                            mm.From = new MailAddress("hr.cherlahealth@gmail.com", "Offer Letter");
                            mm.To.Add(lblemail.Text);
                            //.CC.Add("md@brihaspathi.com");
                            //mm.CC.Add("hr@brihaspathi.com");
                            // mm.CC.Add("hra@brihaspathi.com");

                            mm.IsBodyHtml = true;
                            mm.Subject = "Offer Letter";
                            mm.Body = "Kindly find the below attached Offer Letter <br /><br />Regards<br/><br/>HR<br/><br /><b>Thank you.</b>";
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Offer Letter.pdf"));
                            mm.Priority = MailPriority.High;
                            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                            //smtp.UseDefaultCredentials = true;
                            smtp.Credentials = new System.Net.NetworkCredential("hr.cherlahealth@gmail.com", "HR@cherla");
                            smtp.EnableSsl = true;
                            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.Send(mm);
                        }
                    }
                }
            }
        }
    }
}



       
    
