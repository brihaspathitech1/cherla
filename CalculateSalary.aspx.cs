﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Globalization;

namespace CherlaHealth
{
    public partial class CalculateSalary : System.Web.UI.Page
    {
        string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindStudy4();
            }
            if (!IsPostBack)
            {
                BindGrid111();
            }
        }

        protected void BindGrid111()
        {
            SqlConnection con = new SqlConnection(connstrg);

            SqlCommand cmd = new SqlCommand("SELECT TOP 1 Payslip FROM  Payslip  ORDER BY  Id desc", con);

            cmd.CommandType = CommandType.Text;

            cmd.Connection = con;

            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {

                Label1.Text = dr["Payslip"].ToString();
                Double a = Convert.ToDouble(Label1.Text);
                Label2.Text = (a + 1).ToString();

            }

            dr.Close();

            con.Close();
        }
       
    //without pf
    private void BindStudy4()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        //sqlCmd.CommandText = "SELECT Id,Name FROM EmployeeDetails where PF1='NPF' and Empstatus='1' ";
                        sqlCmd.CommandText = "Select EmployeeId,EmployeName  from Employees where Status='1' and PfStatus='NPF'";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        dropEmployee.DataSource = dt;
                        dropEmployee.DataValueField = "EmployeeId";
                        dropEmployee.DataTextField = "EmployeName";
                        dropEmployee.DataBind();
                        sqlConn.Close();
                        dropEmployee.Items.Insert(0, new ListItem("", "0"));

                    }
                }
            }
            catch { }
        }
        //with pf
        private void BindStudy5()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        //sqlCmd.CommandText = "SELECT Id,Name FROM EmployeeDetails where PF1='PF' and Empstatus='1'";
                        sqlCmd.CommandText = "Select EmployeeId,EmployeName  from Employees where Status='1' and PfStatus='PF'";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        dropEmployee.DataSource = dt;
                        dropEmployee.DataValueField = "EmployeeId";
                        dropEmployee.DataTextField = "EmployeName";
                        dropEmployee.DataBind();
                        sqlConn.Close();
                        dropEmployee.Items.Insert(0, new ListItem("", "0"));

                    }
                }
            }
            catch { }
        }

        private void travelling()
        {
            double zz = 0;
            string xx = string.Empty;
            string yy = string.Empty;
            string connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con1 = new SqlConnection(connstrg1);
            con1.Open();
            //  string str2 = "select * from [travel_allowance] where [EmployeeId] ='" + dropEmployee.SelectedValue + "' and [Month]='" + txtmonth.Text + "'";
            string str2 = "with cte as (select * from [travel_allowance] where[EmployeeId] = '" + dropEmployee.SelectedValue + "' ) select SUM(CAST(cte.Kms AS BIGINT)) as Kms,sum(CAST(cte.Amount AS BIGINT)) as Amount,sum(CAST(cte.ExtraCharges AS BIGINT)) as ExtraCharges from cte group by cte.Amount,cte.Kms,cte.ExtraCharges";

            //string str2 = "select * from Mobile inner join Mobilelimits on Mobile.Name=Mobilelimits.EmployeeName where Mobile.EmpNo ='" + dropEmployee.SelectedValue + "' and Mobile.Monthly='" + txtmonth.Text + "'";
            SqlCommand com123 = new SqlCommand(str2, con1);
            SqlDataReader reader1 = com123.ExecuteReader();
            if (reader1.Read())
            {
                //Mobilebill.Text = reader1["Total"].ToString();
                xx = reader1["Amount"].ToString();
                yy = reader1["ExtraCharges"].ToString();
              //  txtOD.Text = reader1["otheramt"].ToString();
                zz = Convert.ToDouble(xx.ToString()) + Convert.ToDouble(yy.ToString());
            }

            if (zz > 0)
            {
                txttravelling.Text = zz.ToString();
            }


            reader1.Close();
            con1.Close();
            //

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            double zz = 0;
            string xx = string.Empty;
            string yy = string.Empty;
            string connstrg1 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con1 = new SqlConnection(connstrg1);
            con1.Open();
            //string str1 = "select * from Mobile where EmpNo ='" + DropDownList1.SelectedValue + "' and Monthly='"+ TextBox523.Text+ "'";

            //string str2 = "select * from Mobile inner join Mobilelimits on Mobile.Name=Mobilelimits.EmployeeName where Mobile.EmpNo ='" + dropEmployee.SelectedValue + "' and Mobile.Monthly='" + txtmonth.Text + "'";
            //SqlCommand com123 = new SqlCommand(str2, con1);
            //SqlDataReader reader1 = com123.ExecuteReader();
            //if (reader1.Read())
            //{
            //    //Mobilebill.Text = reader1["Total"].ToString();
            //    xx = reader1["Total"].ToString();
            //    yy = reader1["Mlimit"].ToString();
            //    txtOD.Text = reader1["otheramt"].ToString();
            //    zz = Convert.ToDouble(xx.ToString()) - Convert.ToDouble(yy.ToString());
            //}

            //if (zz > 0)
            //{
            //    txtmobile.Text = zz.ToString();
            //}


           // reader1.Close();
            con1.Close();
            //


            //calucalting traveeling using function
            travelling();

            string connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection con = new SqlConnection(connstrg);
            con.Open();
            string str = "select * from dbo.Employees inner join dbo.Tax on dbo.Employees.TaxId=dbo.Tax.Id inner join Department on Department.DeptId=Employees.DeptId where dbo.Employees.EmployeeId='" + dropEmployee.SelectedValue + "'";
            SqlCommand com = new SqlCommand(str, con);
            SqlDataReader reader = com.ExecuteReader();
            if (reader.Read())
            {
                Label13.Text = reader["EmployeName"].ToString();
                Label4.Text = reader["EmployeeID1"].ToString();
                Label14.Text = reader["DOB"].ToString();
                Label15.Text = reader["DOJ"].ToString();
                Label16.Text = reader["Department"].ToString();
                Label17.Text = reader["Desigantion"].ToString();
                ViewState["desc"] = reader["Desigantion1"].ToString();
                Label18.Text = reader["Bankname"].ToString();
                TextBox28.Text = reader["Bankaccountno"].ToString();
                TextBox29.Text = reader["Pfno"].ToString();
                TextBox30.Text = reader["PanNo"].ToString();
                TextBox31.Text = reader["Salary"].ToString();
                TextBox1.Text = reader["PFStatus"].ToString();

                String Name1 = reader["Name1"].ToString();
                TextBox2.Text = reader["HRA"].ToString();
                TextBox3.Text = reader["EA"].ToString();
                TextBox522.Text = reader["DA"].ToString();
                TextBox4.Text = reader["CA"].ToString();
                TextBox10.Text = reader["IT"].ToString();
                TextBox11.Text = reader["LIC"].ToString();
                TextBox7.Text = reader["PF"].ToString();
                TextBox8.Text = reader["ESI"].ToString();
                TextBox12.Text = reader["MA"].ToString();
                TextBox39.Text = reader["casualleaves"].ToString();

               
            }
            reader.Close();
            con.Close();



            String connstrg2 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn88 = new SqlConnection(connstrg2);
            SqlCommand com88 = new SqlCommand();
            com88 = new SqlCommand("Proc_payrol", conn88);
            com88.CommandTimeout = 0;
            com88.CommandType = CommandType.StoredProcedure;
            com88.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "A";
            com88.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropEmployee.SelectedValue;
            com88.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da = new SqlDataAdapter(com88);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable dt = ds.Tables[0];
            conn88.Close();
            if (dt.Rows.Count > 0)
            {
                txtabsent.Text = dt.Rows[0]["Cnt"].ToString();
            }
            else
            {
                txtabsent.Text = "0";
            }


            String connstrg21 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg21);
            SqlCommand com1 = new SqlCommand();
            com1 = new SqlCommand("Proc_payrol", conn1);
            com1.CommandTimeout = 0;
            com1.CommandType = CommandType.StoredProcedure;
            com1.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "H";
            com1.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropEmployee.SelectedValue;
            com1.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da1 = new SqlDataAdapter(com1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1);
            DataTable dt1 = ds1.Tables[0];
            conn1.Close();
            if (dt1.Rows.Count > 0)
            {
                txtholiday.Text = dt1.Rows[0]["Cnt"].ToString();

            }


            else
            {
                txtholiday.Text = "0";
            }



            String connstrg211 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn11 = new SqlConnection(connstrg211);
            SqlCommand com11 = new SqlCommand();
            com11 = new SqlCommand("Proc_payrol", conn11);
            com11.CommandTimeout = 0;
            com11.CommandType = CommandType.StoredProcedure;
            com11.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "P";
            com11.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropEmployee.SelectedValue;
            com11.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da11 = new SqlDataAdapter(com11);
            DataSet ds11 = new DataSet();
            da11.Fill(ds11);
            DataTable dt11 = ds11.Tables[0];
            conn11.Close();
            if (dt11.Rows.Count > 0)
            {
                txtpresent.Text = dt11.Rows[0]["Cnt"].ToString();

            }
            else
            {
                txtpresent.Text = "0";
            }
            String connstrg2111 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn111 = new SqlConnection(connstrg2111);
            SqlCommand com111 = new SqlCommand();
            com111 = new SqlCommand("Proc_payrol", conn111);
            com111.CommandTimeout = 0;
            com111.CommandType = CommandType.StoredProcedure;
            com111.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "P(NOP)";
            com111.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropEmployee.SelectedValue;
            com111.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da111 = new SqlDataAdapter(com111);
            DataSet ds111 = new DataSet();
            da111.Fill(ds111);
            DataTable dt111 = ds111.Tables[0];
            conn111.Close();
            if (dt111.Rows.Count > 0)
            {

                txtnop.Text = dt111.Rows[0]["Cnt"].ToString();
            }
            else
            {
                txtnop.Text = "0";
            }


            String connstrg21111 = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            SqlConnection conn1111 = new SqlConnection(connstrg21111);
            SqlCommand com1111 = new SqlCommand();
            com1111 = new SqlCommand("Proc_payrol", conn111);
            com1111.CommandTimeout = 0;
            com1111.CommandType = CommandType.StoredProcedure;
            com1111.Parameters.Add("@Basic", SqlDbType.VarChar).Value = "WO";
            com1111.Parameters.Add("@EmpName", SqlDbType.VarChar).Value = dropEmployee.SelectedValue;
            com1111.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = txtmonth.Text;
            SqlDataAdapter da1111 = new SqlDataAdapter(com1111);
            DataSet ds1111 = new DataSet();
            da1111.Fill(ds1111);
            DataTable dt1111 = ds1111.Tables[0];
            conn1111.Close();
            if (dt1111.Rows.Count > 0)
            {

                txtoff.Text = dt1111.Rows[0]["Cnt"].ToString();

            }
            else
            {
                txtoff.Text = "0";
            }
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {

            string strDate = txtmonth.Text;
            string[] arrDate = strDate.Split('-');
            string year = arrDate[0].ToString();

            string month = arrDate[1].ToString();
            string day = arrDate[2].ToString();
            int month1 = Convert.ToInt32(month);
            int Year1 = Convert.ToInt32(year);

            string Year11 = Year1.ToString();
            TextBox34.Text = Year11;

            String month11 = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month1);
            TextBox35.Text = month11;
            int days = DateTime.DaysInMonth(Year1, month1);
            //string days1 = days.ToString();
            //TextBox9.Text = days1;

            //TextBox10.Text = (Convert.ToDouble(txtincome.Text) / 100).ToString();

            decimal b = 1;
            //binding actual salry

            decimal jh = Convert.ToDecimal(TextBox31.Text);

            //dividing with months
            decimal jh1 = jh / 12;

            decimal hr = Convert.ToDecimal(TextBox2.Text);
            decimal EA = Convert.ToDecimal(TextBox3.Text);
            decimal CA = Convert.ToDecimal(TextBox4.Text);
            decimal DA = Convert.ToDecimal(TextBox5.Text);
            decimal bbb = days;
            decimal present = Convert.ToDecimal(txtpresent.Text);
            decimal PresenNOP = Convert.ToDecimal(txtnop.Text);
            //decimal Casual = Convert.ToDecimal(TextBox39.Text);


            decimal h = Convert.ToDecimal(txtholiday.Text);

            decimal w = Convert.ToDecimal(txtoff.Text);

            decimal abscent = Convert.ToDecimal(txtabsent.Text);
        
            //actual days
            decimal actualdays = bbb ;
            string WeekDays = actualdays.ToString();
            decimal presentdays = bbb - w - h-abscent;
            string presentdaysempl = presentdays.ToString();
            TextBox33.Text = presentdaysempl;


           
            decimal singleday = jh1 / actualdays;



            //workingdays
            decimal Totalpresent = present + PresenNOP+h+w;
            decimal Totalpresentdecimal = Totalpresent;
            string Totalpresentstring = Totalpresentdecimal.ToString();
            TextBox36.Text = Totalpresentstring;


            decimal singlesalary = singleday * Totalpresentdecimal;

            //decimal basicfromsingle = singlesalary / 2;
            decimal basicfromsingle = singlesalary * Convert.ToDecimal(0.35);
            decimal finalbasic = basicfromsingle;

            decimal finalbasicroundoff = Math.Round(finalbasic);
            string finalbasicroundoffstring = finalbasicroundoff.ToString();
            lblbasic.Text = finalbasicroundoffstring;

            Decimal casuual =Convert.ToDecimal(casual.Text);

            //net salary
             decimal netsalary = (actualdays - Totalpresentdecimal) * singleday;
          //  decimal netsalary = (casuual) * singleday;
            decimal netsalaryRounOff = Math.Round(netsalary);

            string netsalaryString = netsalaryRounOff.ToString();
            lblpay.Text = netsalaryString;
            
            decimal a = b + (hr / 100) * b + (EA / 100) * b + (CA / 100) * b + (DA / 100) * b;
            decimal bb = jh1 / 2;
            decimal bb1 = Math.Round(bb);
            string aa = bb1.ToString();
            //basic salary
            // TextBox6.Text = aa;


            //actual salary

            decimal value = Math.Round(jh1);
            lblactual.Text = Convert.ToString(value);

            decimal q = bbb ;
            string WDays = q.ToString();
           
           
            
            TextBox33.Text = presentdaysempl;

            decimal TP = present + PresenNOP+h+w;
            decimal c = TP;
            string cut1111 = c.ToString();
            TextBox36.Text = cut1111;
            //decimal hr1 = hr * (finalbasic / 100) * 2;
            decimal hr1 =  (finalbasic / 2) ;
            decimal hr2 = Math.Round(hr1);
            string sss = hr2.ToString();
            Label1.Text = sss;

            //Gross salary

            decimal gross = singleday * c;
            int n = Convert.ToInt32(gross);

            lblgross.Text = n.ToString();



            //EDUCATION ALLOWANCE

            decimal EA1 = EA * (finalbasic / 100) * 2;
            decimal EA2 = Math.Round(EA1);
            string ssss = EA2.ToString();
            Label12.Text = ssss;


            //Calculating CONVEYANCE ALLOWANCE

            //decimal CA1 = CA * (finalbasic / 100) * 2;


            decimal CA1 = gross - (finalbasic + hr1);

            decimal CA2 = Math.Round(CA1);

            if (CA2>=288)
            {

                lblca.Text = "800";

            }
            else
            {

                lblca.Text = "0";
            }

            //calculating children education
            //decimal children = gross - (finalbasic + hr1 + Convert.ToDecimal(lblca.Text));
            //decimal childround = Math.Round(children);
            //if (childround >= 200)
            //{

            //    CE.Text = "200";
            //    //lblprofession.Text = "200";

            //}
            //else
            //{

            //    //lblprofession.Text = "200";
            //    CE.Text = "0";
            //}

            //if (jh1<15000)
            //{
            //    lblprofession.Text = "0";

            //}
            //else if(jh1>15000 && jh1<20000)
            //{
            //    lblprofession.Text = "150";
            //}
            //else
            //{
            //    lblprofession.Text = "200";
            //}


            lblprofession.Text = proftax.Text;
            //Calculating special allowance
            //decimal special = gross - (finalbasic + hr1 + Convert.ToDecimal(lblca.Text) + Convert.ToDecimal(CE.Text));
            decimal special = gross - (finalbasic + hr1 + Convert.ToDecimal(lblca.Text)) ;
            decimal specialround = Math.Round(special);

            string specialstring = Convert.ToString(specialround);

            specialalowance.Text = specialstring;
            
            //da
            //DA ALLOWANCE

            decimal DA1 = DA * (finalbasic / 100) * 2;
            decimal DA2 = Math.Round(DA1);
            string ssssss = DA2.ToString();
            Label10.Text = ssssss;


            //decimal total = finalbasic + hr2 + EA2 + DA2+ Convert.ToDecimal(lblca.Text) + Convert.ToDecimal(CE.Text)+ specialround+Convert.ToDecimal(txtmobile.Text);
            decimal total = finalbasic + hr2 + EA2 + DA2 + Convert.ToDecimal(lblca.Text)  + specialround + Convert.ToDecimal(txtmobile.Text);

            string totalstring = total.ToString();

           


            //find here
            //decimal z = total / q;
            //decimal cut = q - c;
            //decimal cut1 = cut * z;

            //decimal PF33 = Math.Round(cut1);
            //string cut11 = PF33.ToString();
            // lblpay.Text = cut11;


           


            //int anInteger;
            ////anInteger = Convert.ToInt32(lblgross.Text);
            //anInteger = int.Parse(lblgross.Text);
            //int gros = Int32.Parse(lblgross.Text);

            //PF
            decimal PF = Convert.ToDecimal(TextBox7.Text);
            decimal PF1 = PF * (finalbasicroundoff / 100);
            decimal PF2 = Math.Round(PF1);
            string sssssss = PF2.ToString();

            //pf for company
            decimal PFc = Convert.ToDecimal(TextBox7.Text);
            decimal PFc1 = PFc * (finalbasicroundoff / 100);
            decimal PFc2 = Math.Round(PFc1);
            string pfcompny = PFc2.ToString();
            //Calaulating pf only for pf candidates
            if (RadioPf.SelectedItem.Text == "PF")
            {

                Label5.Text = sssssss;
                lblpf.Text = Label5.Text;

                //pf for company

                PFcompany.Text = pfcompny;

            }
            else
            {
                Label5.Text = "0";
                lblpf.Text = Label5.Text;
                PFcompany.Text = "0";
            }

            //Calculating admin from basic
            decimal basic = Convert.ToDecimal(ADMIN.Text);
            decimal basic1 = basic * (finalbasicroundoff / 100);
            decimal basic2 = Math.Round(basic1);
            string basiccomapany = basic2.ToString();
            Label6.Text = basiccomapany;
            
            // int ESI2 = 0;//salary less than 21001
            if (jh1 < 21001)
            {
                //decimal ESI = Convert.ToDecimal(TextBox8.Text);
                //decimal ESI1 = ((x * 90) / 100) * (ESI / 100);
                //decimal ESI2 = Math.Round(ESI1);
                //string esi3 = ESI2.ToString();
                //lblesi.Text = esi3;





                decimal IT = Convert.ToDecimal(TextBox10.Text);
                decimal IT1 = IT * (gross / 100);

                decimal IT2 = Math.Round(IT1);
                string IT11 = IT2.ToString();
                Label7.Text = IT11;

                decimal LIC = Convert.ToDecimal(TextBox11.Text);
                decimal LIC1 = LIC * (gross / 100);
                decimal LIC2 = Math.Round(LIC1);
                string LIC11 = LIC2.ToString();
                Label8.Text = LIC11;
                decimal MA2, MA1;
                string MA11;




                if (ViewState["desc"].ToString() == "Pharmacy" || ViewState["desc"].ToString() == "Lab" || ViewState["desc"].ToString()=="Nurse")
                {
                    decimal MA = Convert.ToDecimal(5);
                    MA1 = MA * (jh1 / 100);
                    MA2 = Math.Round(MA1);
                    MA11 = MA2.ToString();

                }
                else
                {
                    decimal MA = Convert.ToDecimal(TextBox12.Text);
                    MA1 = MA * (gross / 100);
                    MA2 = Math.Round(MA1);
                    MA11 = MA2.ToString();

                }
                Label9.Text = MA11;


                decimal Mobile = Convert.ToDecimal(txtmobile.Text);
                lblmobile.Text = Mobile.ToString();
                decimal xx = Convert.ToDecimal(txtOD.Text);
                decimal xxx = Convert.ToDecimal(txtadvance.Text);
                lbladvance.Text = Convert.ToDecimal(xxx).ToString();
                lblother.Text = Convert.ToDecimal(xx).ToString();

                decimal tax = Convert.ToDecimal(txtincome.Text);
                decimal pftax = Convert.ToDecimal(proftax.Text); 

                decimal travelling = Convert.ToDecimal(txttravelling.Text);


                //calculating withpf
                if (RadioPf.SelectedItem.Text == "PF")
                {
                    //net deduction
                    //decimal deducation = PF2 + xxx + xx + IT2 + LIC2 + MA2 + Mobile+ travelling;
                    decimal deducation = PF2 + xxx + xx + IT2 + LIC2 + MA2 +tax+pftax;
                    
                    decimal PF333 = Math.Round(deducation);
                    string f = PF333.ToString();
                    lbldeduction.Text = f;

                    //net salary
                    //decimal deducation1 = PF2 + xxx + xx + IT2 + LIC2 + MA2 + Mobile+ travelling;
                    decimal deducation1 = PF2 + xxx + xx + IT2 + LIC2 + MA2 + tax + pftax;
                    decimal j = total+travelling - deducation1;
                    decimal PF3333 = Math.Round(j);
                    string zxc = PF3333.ToString();
                    lblNet.Text = zxc;

                    //net salary in words

                    Int64 dfdf = Convert.ToInt64(j);
                    Int64 NumVal = Convert.ToInt64(dfdf);
                    lblWords.Text = Rupees(NumVal);

                    //calculating esi from gross salary for company
                    decimal ESIcpny = Convert.ToDecimal(4.75);
                    decimal ESIcpny123 = Convert.ToDecimal(0.9);
                    decimal ESIcpny1 = ESIcpny * (gross / 100) * ESIcpny123;

                    decimal ESIcpny2 = Math.Round(ESIcpny1);
                    string ESIcomapany = ESIcpny2.ToString();
                    Label11.Text = ESIcomapany;
                }
                //calculating withoutpf
                else
                {
                    //decimal deducation = xxx + xx + IT2 + LIC2 + MA2 + Mobile+ travelling;
                    decimal deducation = xxx + xx + IT2 + LIC2 + MA2 + tax + pftax;
                    decimal PF333 = Math.Round(deducation);
                    string f = PF333.ToString();
                    lbldeduction.Text = f;

                    //net salary
                    //decimal deducation1 =  xxx + xx + IT2 + LIC2 + MA2 + Mobile+ travelling;
                    decimal deducation1 = xxx + xx + IT2 + LIC2 + MA2 + tax + pftax;
                    decimal j = total+travelling - deducation1;
                    decimal PF3333 = Math.Round(j);
                    string zxc = PF3333.ToString();
                    lblNet.Text = zxc;

                    Int64 dfdf = Convert.ToInt64(j);
                    Int64 NumVal = Convert.ToInt64(dfdf);
                    lblWords.Text = Rupees(NumVal);

                    //calculating esi from gross salary for company
                    decimal ESIcpny = Convert.ToDecimal(4.75);
                    decimal ESIcpny123 = Convert.ToDecimal(0.9);
                    decimal ESIcpny1 = ESIcpny * (gross / 100) * ESIcpny123;

                    decimal ESIcpny2 = Math.Round(ESIcpny1);
                    string ESIcomapany = ESIcpny2.ToString();
                    Label11.Text = ESIcomapany;
                }

            }
            //salary above 21001
            else
            {

                decimal ESI2 = 0;
                string esi3 = ESI2.ToString();
                lblesi.Text = esi3;





                decimal IT = Convert.ToDecimal(TextBox10.Text);
                decimal IT1 = IT * (gross / 100);

                decimal IT2 = Math.Round(IT1);
                string IT11 = IT2.ToString();
                Label7.Text = IT11;

                decimal LIC = Convert.ToDecimal(TextBox11.Text);
                decimal LIC1 = LIC * (gross / 100);
                decimal LIC2 = Math.Round(LIC1);
                string LIC11 = LIC2.ToString();
                Label8.Text = LIC11;


                //decimal MA = Convert.ToDecimal(TextBox12.Text);
                //decimal MA1 = MA * (gross / 100);
                //decimal MA2 = Math.Round(MA1);
                //string MA11 = MA2.ToString();
                //Label9.Text = MA11;

                decimal MA2, MA1;
                string MA11;




                //if (ViewState["desc"].ToString() == "7" || ViewState["desc"].ToString() == "10")
                if (ViewState["desc"].ToString() == "Pharmacy" || ViewState["desc"].ToString() == "Lab" || ViewState["desc"].ToString() == "Nurse")
                {
                    decimal MA = Convert.ToDecimal(5);
                    MA1 = MA * (jh1 / 100);
                    MA2 = Math.Round(MA1);
                    MA11 = MA2.ToString();

                }
                else
                {
                    decimal MA = Convert.ToDecimal(TextBox12.Text);
                    MA1 = MA * (gross / 100);
                    MA2 = Math.Round(MA1);
                    MA11 = MA2.ToString();

                }
                Label9.Text = MA11;

                decimal Mobile = Convert.ToDecimal(txtmobile.Text);
                decimal xx = Convert.ToDecimal(txtOD.Text);
                decimal xxx = Convert.ToDecimal(txtadvance.Text);
                decimal tax = Convert.ToDecimal(txtincome.Text);
                decimal pftax = Convert.ToDecimal(proftax.Text);
                lblit.Text = txtincome.Text;
                lblmobile.Text = txtmobile.Text;
                decimal travell = Convert.ToDecimal(txttravelling.Text);
                //calculating withpf
                if (RadioPf.SelectedItem.Text == "PF")
                {

                    decimal deducation = ESI2 + PF2 + xxx + xx + IT2 + LIC2 + MA2+tax+ pftax;
                    decimal PF333 = Math.Round(deducation);
                    string f = PF333.ToString();
                    lbldeduction.Text = f;

                    decimal deducation1 = ESI2 + PF2 + xxx + xx + IT2 + LIC2 + MA2 + tax+ pftax;


                    decimal j = total+ travell - deducation1;
                    decimal PF3333 = Math.Round(j);
                    string zxc = PF3333.ToString();
                    lblNet.Text = zxc;

                    Int64 dfdf = Convert.ToInt64(j);
                    Int64 NumVal = Convert.ToInt64(dfdf);
                    lblWords.Text = Rupees(NumVal);
                }
                //calculating withoutpf
                else
                {
                    //decimal deducation = ESI2 + xxx + xx + IT2 + LIC2 + MA2 + singleday + tax+ pftax;
                    decimal deducation = ESI2 + xxx + xx + IT2 + LIC2 + MA2  + tax + pftax;

                    decimal PF333 = Math.Round(deducation);
                    string f = PF333.ToString();
                    lbldeduction.Text = f;

                    decimal j = total+ travell - PF333;
                    decimal PF3333 = Math.Round(j);
                    string zxc = PF3333.ToString();
                    lblNet.Text = zxc;

                    Int64 dfdf = Convert.ToInt64(j);
                    Int64 NumVal = Convert.ToInt64(dfdf);
                    lblWords.Text = Rupees(NumVal);
                }

            }
        }
        
        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lack";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }
            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                result = result + ' ' + RupeesToWords(res) + " Hundred";
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = result + ' ' + " Rupees only";
            return result;
        }
        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }
            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            String connstrg = ConfigurationManager.ConnectionStrings["Cherla"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connstrg))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Payslip WHERE name = @Name and date=@Name1 ", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@Name", this.Label4.Text.Trim());
                        cmd.Parameters.AddWithValue("@Name1", this.txtmonth.Text.Trim());

                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.lblStatus.Text = "Already Payslip Generated";
                        }
                        else
                        {
                            SqlConnection conn = new SqlConnection(connstrg);

                            //String insert = "insert into Payslip(Payslip,Name,EmpNo,DOB,DOJ,Departrment,Designation,Bank,BankAc,PFNo,PanNo,Basic,HRA,EA,DA,CA,IT,LIC,PF,ESI,MA,Total,Deducation,TTotal,Absent,Holiday,Present,NOP,WO,date,Amountwords,Workingdays,Year,Month,paybledays,Deducation1,ASa,Ded,paydayd,MobileBill,GrossSalary,CPF,CESI,Admin,PFStatus,travell)values('" + Label2.Text + "','" + Label13.Text + "','" + Label4.Text + "','" + Label14.Text + "','" + Label15.Text + "','" + Label16.Text + "','" + Label17.Text + "','" + Label18.Text + "','" + TextBox28.Text + "','" + TextBox29.Text + "','" + TextBox30.Text + "','" + lblbasic.Text + "','" + Label1.Text + "','" + Label12.Text + "','" + Label4.Text + "','" + Label3.Text + "','" + Label7.Text + "','" + Label8.Text + "','" + Label5.Text + "','" + lblesi.Text + "','" + Label9.Text + "','" + lblactual.Text + "','" + lbldeduction.Text + "','" + lblNet.Text + "','" + txtabsent.Text + "','" + txtholiday.Text + "','" + txtpresent.Text + "','" + txtnop.Text + "','" + txtoff.Text + "','" + txtmonth.Text + "','" + lblWords.Text + "','" + TextBox33.Text + "','" + TextBox34.Text + "','" + TextBox35.Text + "','" + TextBox36.Text + "','" + lblpay.Text + "','" + txtadvance.Text + "','" + txtOD.Text + "','" + lbldeduction.Text + "','" + txtmobile.Text + "','" + lblgross.Text + "','" + PFcompany.Text + "','" + Label6.Text + "','" + Label11.Text + "','"+ TextBox1.Text+"','"+txttravelling.Text+"')";
                            String insert = "insert into Payslip(Payslip,Name,EmpNo,DOB,DOJ,Departrment,Designation,Bank,BankAc,PFNo,PanNo,Basic,HRA,EA,DA,CA,IT,LIC,PF,ESI,MA,Total,Deducation,TTotal,Absent,Holiday,Present,NOP,WO,date,Amountwords,Workingdays,Year,Month,paybledays,Deducation1,ASa,Ded,paydayd,MobileBill,GrossSalary,CPF,CESI,Admin,PFStatus,travell,special,proftax)values('" + Label2.Text + "','" + Label13.Text + "','" + Label4.Text + "','" + Label14.Text + "','" + Label15.Text + "','" + Label16.Text + "','" + Label17.Text + "','" + Label18.Text + "','" + TextBox28.Text + "','" + TextBox29.Text + "','" + TextBox30.Text + "','" + lblbasic.Text + "','" + Label1.Text + "','" + Label12.Text + "','" + TextBox5.Text + "','" + lblca.Text + "','" + lblit.Text + "','" + Label8.Text + "','" + Label5.Text + "','" + lblesi.Text + "','" + Label9.Text + "','" + lblactual.Text + "','" + lbldeduction.Text + "','" + lblNet.Text + "','" + txtabsent.Text + "','" + txtholiday.Text + "','" + txtpresent.Text + "','" + txtnop.Text + "','" + txtoff.Text + "','" + txtmonth.Text + "','" + lblWords.Text + "','" + TextBox33.Text + "','" + TextBox34.Text + "','" + TextBox35.Text + "','" + TextBox36.Text + "','" + lblpay.Text + "','" + txtadvance.Text + "','" + txtOD.Text + "','" + lbldeduction.Text + "','" + txtmobile.Text + "','" + lblgross.Text + "','" + PFcompany.Text + "','" + Label6.Text + "','" + Label11.Text + "','" + TextBox1.Text + "','" + txttravelling.Text + "','"+specialalowance.Text+"','"+ proftax.Text+"')";
                            
                            SqlCommand comm = new SqlCommand(insert, conn);   
                            conn.Open();
                            comm.ExecuteNonQuery();
                            conn.Close();


                            Session["payslipgenerate"] = Label2.Text;
                          Response.Redirect("EmpPayslip.aspx");

                        }

                    }
                }
            }
        }


        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (RadioPf.SelectedValue == "1")
            {
                BindStudy4();
            }
            else if (RadioPf.SelectedValue == "2")
            {
                BindStudy5();
            }
        }

    }
}